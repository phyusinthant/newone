<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role = [
            [
             'id' => 1,
             'name' => 'admin',
             'display_name' => 'Admin',
             'description' => 'Admin Panel Role'
            ],
            [
             'id' => 2,
             'name' => 'customer',
             'display_name' => 'Customer',
             'description' => 'Customer Role'
            ]
        ];

        foreach ($role as $key => $value) {
            Role::create($value);
        }
    }
}
