<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->delete();

        $users = array(
            ['id' => 1, 'name' => 'admin', 'email' => 'admin@admin.com', 'password' => Hash::make('pwd')],
            ['id' => 2, 'name' => 'staff', 'email' => 'staff@staff.com', 'password' => Hash::make('pwd')],
        );

        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            User::create($user);
        }

        //Assign user role
        
        $user = User::find(1);
        $user->attachRole(1);

        $user = User::find(2);
        $user->attachRole(2);
        

    }
}
