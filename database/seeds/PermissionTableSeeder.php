<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
        	[
                'id' => 1,
        		'name' => 'role-list',
        		'display_name' => 'Display Role Listing',
        		'description' => 'See only Listing Of Role'
        	],
        	[
                'id' => 2,
        		'name' => 'role-create',
        		'display_name' => 'Create Role',
        		'description' => 'Create New Role'
        	],
        	[
                'id' => 3,
        		'name' => 'role-edit',
        		'display_name' => 'Edit Role',
        		'description' => 'Edit Role'
        	],
        	[
                'id' => 4,
        		'name' => 'role-delete',
        		'display_name' => 'Delete Role',
        		'description' => 'Delete Role'
        	],
            [
                'id' => 5,
                'name' => 'role-assign-permission',
                'display_name' => 'Assign Permission to Role',
                'description' => 'Assign Permission to Role'
            ],
            [
                'id' => 6,
                'name' => 'permission-list',
                'display_name' => 'Display Permission Listing',
                'description' => 'See only Listing Of Permission'
            ],
            [
                'id' => 7,
                'name' => 'permission-create',
                'display_name' => 'Create Permission',
                'description' => 'Create New Permission'
            ],
            [
                'id' => 8,
                'name' => 'permission-edit',
                'display_name' => 'Edit Permission',
                'description' => 'Edit Permission'
            ],
            [
                'id' => 9,
                'name' => 'permission-delete',
                'display_name' => 'Delete Permission',
                'description' => 'Delete Permission'
            ],
            [
                'id' => 10,
                'name' => 'user-list',
                'display_name' => 'Display User Listing',
                'description' => 'See only Listing Of User'
            ],
            [
                'id' => 11,
                'name' => 'user-create',
                'display_name' => 'Create User',
                'description' => 'Create New User'
            ],
            [
                'id' => 12,
                'name' => 'user-edit',
                'display_name' => 'Edit User',
                'description' => 'Edit User'
            ],
            [
                'id' => 13,
                'name' => 'user-delete',
                'display_name' => 'Delete User',
                'description' => 'Delete User'
            ],
            [
                'id' => 15,
                'name' => 'user-show',
                'display_name' => 'Show User',
                'description' => 'Show User'
            ]
        ];

        foreach ($permission as $key => $value) {
        	Permission::create($value);
        }

        $permission_data = Permission::get();
        //Assign Admin Role Permission
        $role = Role::find(1);
        
        foreach ($permission_data as $key => $value) {
            $role->attachPermission($value->id);
        }

       


    }
}
