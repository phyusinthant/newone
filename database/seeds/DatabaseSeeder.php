<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         
        Eloquent::unguard();
        
        //role
        $this->call(RoleTableSeeder::class);
        
        //permission and assign role permission
        $this->call(PermissionTableSeeder::class);

        //permission and assign role permission
        $this->call(RevisionPermissionTableSeeder::class);
        
        //user and assing user role
        $this->call(UserTableSeeder::class);

        //UI Permission seeder
        $this->call(UiPermissionTableSeeder::class);

        $this->call(CustomerPermissionTableSeeder::class);
        $this->call(CustomerPermissionRoleTableSeeder::class);
        $this->call(CustomerUiPermissionTableSeeder::class);
        
        $this->command->info('all tables has been seeded!');

    }
}
