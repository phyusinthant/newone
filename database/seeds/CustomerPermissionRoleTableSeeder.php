<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

class CustomerPermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::find(1);
        
        $role->attachPermission(121);
        $role->attachPermission(122);
        $role->attachPermission(123);
        $role->attachPermission(124);
        $role->attachPermission(125);

        $role3 = Role::find(2);
        
        $role3->attachPermission(12);
        $role3->attachPermission(15);
        $role3->attachPermission(123);
        $role3->attachPermission(124);
    }
}
