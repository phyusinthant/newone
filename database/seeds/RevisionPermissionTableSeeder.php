<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;
use App\Models\UiPermission;

class RevisionPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
        	
	        'id' => 14,
			'name' => 'revision-list',
			'display_name' => 'Display Revision Listing',
			'description' => 'See only Listing Of Revision'
        	
        ];

        Permission::create($permission);

        //Assign Admin Role Permission
        $role = Role::find(1);
        
        $role->attachPermission(14);

        $ui_permission = [
        	
            'permission_id' => 14,
    		'grouping' => 'Revision',
    		'display_name' => 'Display Revision Listing',
    		'description' => 'See only Listing Of Revision',
            'dependent_permission' => ''
        	
        ];

        UiPermission::create($ui_permission);

    }
}
