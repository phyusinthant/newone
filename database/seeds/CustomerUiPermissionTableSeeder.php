<?php

use Illuminate\Database\Seeder;
use App\Models\UiPermission;

class CustomerUiPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            
            [
                'permission_id' => 121,
                'grouping' => 'Customer',
                'display_name' => 'Display Customer Listing',
                'description' => 'See only Listing Of Customer',
                'dependent_permission' => ''  
            ],
            [
                'permission_id' => 122,
                'grouping' => 'Customer',
                'display_name' => 'Create Customer',
                'description' => 'Create New Customer',
                'dependent_permission' => '121'
            ],
            [
                'permission_id' => 123,
                'grouping' => 'Customer',
                'display_name' => 'Edit Customer',
                'description' => 'Edit Customer',
                'dependent_permission' => '121'
            ],
            [
                'permission_id' => 124,
                'grouping' => 'Customer',
                'display_name' => 'Show Customer',
                'description' => 'Show Customer',
                'dependent_permission' => '121'
            ],
            [
                'permission_id' => 125,
                'grouping' => 'Customer',
                'display_name' => 'Delete Customer',
                'description' => 'Delete Customer',
                'dependent_permission' => '121'
            ]

        ];

        foreach ($permission as $key => $value) {
            UiPermission::create($value);
        }
    }
}
