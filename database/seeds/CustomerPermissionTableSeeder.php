<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

class CustomerPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            
            [
                'id' => 121,
                'name' => 'customers-list',
                'display_name' => 'Display Customer Listing',
                'description' => 'See only Listing Of Customer'
            ],
            [
                'id' => 122,
                'name' => 'customers-create',
                'display_name' => 'Create Customer',
                'description' => 'Create New Customer'
            ],
            [
                'id' => 123,
                'name' => 'customers-edit',
                'display_name' => 'Edit Customer',
                'description' => 'Edit Customer'
            ],
            [
                'id' => 124,
                'name' => 'customers-show',
                'display_name' => 'Show Customer',
                'description' => 'Show Customer'
            ],
            [
                'id' => 125,
                'name' => 'customers-delete',
                'display_name' => 'Delete Customer',
                'description' => 'Delete Customer'
            ]
        ];

        foreach ($permission as $key => $value) {
            Permission::create($value);
        }
    }
}
