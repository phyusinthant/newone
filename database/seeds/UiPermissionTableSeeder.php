<?php

use Illuminate\Database\Seeder;
use App\Models\UiPermission;

class UiPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
        	[
                'permission_id' => 1,
        		'grouping' => 'Role',
        		'display_name' => 'Display Role Listing',
        		'description' => 'See only Listing Of Role',
                'dependent_permission' => ''
        	],
        	[
                'permission_id' => 2,
        		'grouping' => 'Role',
        		'display_name' => 'Create Role',
        		'description' => 'Create New Role',
                'dependent_permission' => '1'
        	],
        	[
                'permission_id' => 3,
        		'grouping' => 'Role',
        		'display_name' => 'Edit Role',
        		'description' => 'Edit Role',
                'dependent_permission' => '1'
        	],
        	[
                'permission_id' => 4,
        		'grouping' => 'Role',
        		'display_name' => 'Delete Role',
        		'description' => 'Delete Role',
                'dependent_permission' => '1'
        	],
            [
                'permission_id' => 5,
                'grouping' => 'Role',
                'display_name' => 'Assign Permission to Role',
                'description' => 'Assign Permission to Role',
                'dependent_permission' => '1'
            ],
            [
                'permission_id' => 6,
                'grouping' => 'Permission',
                'display_name' => 'Display Permission Listing',
                'description' => 'See only Listing Of Permission',
                'dependent_permission' => ''
            ],
            [
                'permission_id' => 7,
                'grouping' => 'Permission',
                'display_name' => 'Create Permission',
                'description' => 'Create New Permission',
                'dependent_permission' => '6'
            ],
            [
                'permission_id' => 8,
                'grouping' => 'Permission',
                'display_name' => 'Edit Permission',
                'description' => 'Edit Permission',
                'dependent_permission' => '6'
            ],
            [
                'permission_id' => 9,
                'grouping' => 'Permission',
                'display_name' => 'Delete Permission',
                'description' => 'Delete Permission',
                'dependent_permission' => '6'
            ],
            [
                'permission_id' => 10,
                'grouping' => 'User',
                'display_name' => 'Display User Listing',
                'description' => 'See only Listing Of User',
                'dependent_permission' => ''
            ],
            [
                'permission_id' => 11,
                'grouping' => 'User',
                'display_name' => 'Create User',
                'description' => 'Create New User',
                'dependent_permission' => '10'
            ],
            [
                'permission_id' => 12,
                'grouping' => 'User',
                'display_name' => 'Edit User',
                'description' => 'Edit User',
                'dependent_permission' => '10'
            ],
            [
                'permission_id' => 13,
                'grouping' => 'User',
                'display_name' => 'Delete User',
                'description' => 'Delete User',
                'dependent_permission' => '10'
            ],
            [
                'permission_id' => 15,
                'grouping' => 'User',
                'display_name' => 'Show User',
                'description' => 'Show User',
                'dependent_permission' => '10'
            ]
        ];

        foreach ($permission as $key => $value) {
        	UiPermission::create($value);
        }
    }
}
