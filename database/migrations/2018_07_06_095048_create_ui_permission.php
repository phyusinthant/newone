<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUiPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create table for storing ui permissions
        Schema::create('ui_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('permission_id')->unsigned()->unique();
            $table->string('grouping')->nullable();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->string('dependent_permission')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            // $table->foreign('permission_id')->references('id')->on('permissions')
            //     ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ui_permissions');
    }
}
