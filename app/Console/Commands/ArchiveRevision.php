<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \Venturecraft\Revisionable\Revision;

class ArchiveRevision extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'archive:revision {type=sql : sql or csv}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Archive Revision Data to sql or csv file and delete archived data from Revision Table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $nlog = app("Nlog");
        $nlog->init(__METHOD__);

        $path = storage_path().'/archive/';
        $file = 'revisions_'.date('Y-m-d-His');

        // csv or sql
        $type = strtolower($this->argument('type'));

        //To store revision ids to delete after archive
        $ids = array(); 

        //Get revision date on last 3 months to archive 
        $revision_date = date("Y-m-1", strtotime("-3 months"));

        // For Testing
        $revision_date = date("Y-m-d", strtotime("-1 day"));
        // $nlog->log("revision date: ".$revision_date);
        
        if($type == "sql"){

            $host = env('DB_HOST');
            $username = env('DB_USERNAME');
            $password = env('DB_PASSWORD');
            $database = env('DB_DATABASE');
            $table = 'revisions';

            $file .= '.sql';

            //Get ID to archive data before revision date
            $ids = Revision::where("created_at","<", $revision_date)->pluck('id')->toArray();

            // If there is data before revision date, archive to sql file
            if(isset($ids) && count($ids) > 0){

                $this->info('Start archiving data ('.count($ids).' records) which is before '.$revision_date." to sql file ".$file);

                $nlog->log('Start archiving data ('.count($ids).' records) which is before '.$revision_date." to sql file ".$file, [], 'start');

                $command = sprintf('mysqldump -h %s -u %s -p\'%s\' --compact --no_create_info %s %s --where="created_at<\'%s\'" > %s', $host, $username, $password, $database, $table, $revision_date, $path . $file);

                if (!is_dir($path)) {
                    mkdir($path, 0755, true);
                }
                exec($command);

                $nlog->log('End archiving data which is before '.$revision_date." to sql file ".$file, [], 'end');

                $this->info('End archiving data which is before '.$revision_date." to sql file ".$file);


            }else{
                $this->info('There is no data which is before '.$revision_date.' to archive.');
            }

        }
        else if($type == "csv"){

            $file .= '.csv';

            //Get data to archive which is before revision date
            $revisions = Revision::where("created_at","<", $revision_date)->get();

            // If there is data which is before revision date, archive to csv file
            if(isset($revisions) && count($revisions) > 0){

                $this->info('Start archiving data ('.count($revisions).' records) which is before '.$revision_date." to csv file ".$file);

                $nlog->log('Start archiving data ('.count($revisions).' records) which is before '.$revision_date." to csv file ".$file, [], 'start');

                if (!is_dir($path)) {
                    mkdir($path, 0755, true);
                }
                
                $csvFile = fopen($path.$file, 'w');
                foreach ($revisions as $row) {
                    fputcsv($csvFile, $row->toArray());
                    $ids[] = $row->id;
                }
                fclose($csvFile);

                $nlog->log('End archiving data which is before '.$revision_date." to csv file ".$file, [], 'end');

                $this->info('End archiving data which is before '.$revision_date." to csv file ".$file);

            }else{
                $this->info('There is no data which is before '.$revision_date);
            }
        }
        else{

            $nlog->log("Invalid file type to archive. Only allow (csv or sql)");

            $this->info('Invalid file type to archive. Only allow (csv or sql)');

        }

        //If archived file is existed, delete Revision records 
        if(file_exists($path . $file)){

            $this->info('Start deleting data ('.count($ids).' records) which is before '.$revision_date);

            $nlog->log('Start deleting data which is before '.$revision_date, [], 'start');
            
            $result = Revision::destroy($ids);

            $nlog->log('End deleting data which is before '.$revision_date, [], 'end');
            
            $this->info('End deleting data which is before '.$revision_date);

        }

    }

}
