<?php

namespace App\Services;

use Common\Generate as Generate;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Carbon\Carbon; 

class Log
{
    const _PRODUCTION_="production";
    const _LOCAL_="local";

    const _INFO_="info";
    const _WARNING_="warning";
    const _ERROR_="error";
    const _START_="start";
    const _END_="end";

    public $start_time = 0;

    public $data =[
        'uid'=>'',
        'env'=>'',
        'fn_name'=>'Log',
        'logpath'=>'log.log',
        'view_log'=>null,
    ];

    public static function greeting(){        
        return "What up dawg";
    }

    public function init($fnname = null, $context = null){
        try {

            $data = $this->getData();
            $data['uid'] = uniqid();
            // dd(app()->environment());
            $data['env'] = app()->environment();

            if(isset($fnname)){ $data['fn_name'] = $fnname; }
            $data['logpath'] = 'log_'.date('Ymd').'.log';
            $data['view_log'] = new Logger($data['uid']);

            $data['view_log']->pushHandler(new StreamHandler(storage_path().'/logs/'.$data['logpath'], Logger::INFO));
            if(isset($context) && $data['env']!=Log::_PRODUCTION_){
                $data['view_log']->addInfo($data['fn_name'],$context);
            }
            
        } catch (Exception $e) {
            Log::info('Log_construct',$e->getMessage());   
        }

        $this->setData($data);
        return $this->getData();
    }

    public function log($message, $context = [], $type ='info', $err = null){
        try {

            // $current_timestamp = Carbon::now()->timestamp; // Unix timestamp 1495062127
            $current_timestamp = round(microtime(true) * 1000);
        
            if(!isset($context)){
                $context = [];
            }

            if(!is_array($context)){
                $context = (array) $context;
            }

            $data = $this->getData();
            // dd($data);
            if($data['env']!=Log::_PRODUCTION_){
                if($type==Log::_INFO_){
                    $data['view_log']->addInfo("[$current_timestamp]".$data['fn_name']." - ".$message,$context);
                    debug("[$current_timestamp]".$data['fn_name']." - ".$message, $context);    
                }elseif($type==Log::_WARNING_){
                    $data['view_log']->addWarning("[$current_timestamp]".$data['fn_name']." - ".$message,$context);
                    debug("[$current_timestamp]".$data['fn_name']." - ".$message, $context);    
                }    
            }
            
            if($type==Log::_ERROR_){
                if(isset($err)){
                    $data['view_log']->addError("[$current_timestamp]".$data['fn_name']." - ".$message,array('data'=>$context,'error'=>$err));
                    debug("[$current_timestamp]".$data['fn_name']." - ".$message, $context, $err);   
                }else{
                    $data['view_log']->addError("[$current_timestamp]".$data['fn_name']." - ".$message,array('data'=>$context));    
                    debug("[$current_timestamp]".$data['fn_name']." - ".$message, $context); 
                }
            }

            if($type==Log::_START_){
                $this->start_time = $current_timestamp;

                $data['view_log']->addInfo("Start[$current_timestamp]".$data['fn_name']." - ".$message,$context);
                debug("Start[$current_timestamp]".$data['fn_name']." - ".$message, $context);  
            }
            else if($type==Log::_END_){
                $diff = 0;
                if($this->start_time != 0){
                    $diff = $current_timestamp - $this->start_time;
                }

                $this->start_time = 0;

                $data['view_log']->addInfo("End[$current_timestamp] Diff[$diff Milliseconds] ".$data['fn_name']." - ".$message,$context);
                debug("End[$current_timestamp] Diff[$diff Milliseconds] ".$data['fn_name']." - ".$message, $context);  
            }


        } catch (Exception $e) {
            Log::info('Log_log',array('error'=>$e->getMessage()));
        }

        if($type=='error'){
            return \View::make('errors.errorwithbar')
                         ->with('error_id', $data["uid"]);
        }
        else{
            return $data['uid'];
        }

    }
	
    public function getData()
    {
        return $this->data;
    }
 
    public function setData($data)
    {
        $this->data = $data;
    }
}