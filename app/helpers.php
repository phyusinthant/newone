<?php

function flash($title = null, $message = null, $type = 'info')
{
	$flash = app('App\Services\Flash');

	if(func_num_args() == 0){
		return $flash;
	}

	if($type == "success"){
		return $flash->success($title, $message); 	
	}
	else if($type == "error"){
		return $flash->error($title, $message); 	
	}
	else if($type == "overlay"){
		return $flash->overlay($title, $message); 	
	}
	else{
		return $flash->info($title, $message); 	
	}
	
}
