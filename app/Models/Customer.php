<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Customer extends Model
{
    use SoftDeletes;


    public static function boot()
    {
        parent::boot();
    }    


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['name','email','password','status','user_id','phone','address'];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
