<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class RoleUser extends Model
{
	use SoftDeletes;


    // Begin Revision 
    use \Venturecraft\Revisionable\RevisionableTrait; // Need to use for either audit trail or activity log

    public static function boot()
    {
        parent::boot();
    }
    
    //Audit Trail Configuration
    protected $revisionEnabled = true; //Set true to enable audit trail
    protected $revisionCleanup = true; //Remove old revisions (works only when used with $historyLimit)
    protected $historyLimit = 500; //Maintain a maximum of 500 changes at any point of time, while cleaning up old revisions.
    protected $revisionCreationsEnabled = true;
    protected $revisionNullString = 'nothing';
    protected $revisionUnknownString = 'unknown';

    //Activity Configuration
    protected $activityEnabled = true; //Set false not to log any activity
    protected $activityCreationsEnabled = true; //Set true to log create activity
    protected $activityUpdatingEnabled = true; //Set true to log update activity
    protected $activityDeletingEnabled = true; //Set true to log delete activity
    

    // --- Other options of Audit Trail ---
    // $object->disableRevisionField('title'); // Disables title
    // $object->disableRevisionField(array('title', 'content')); // Disables title and content

    // protected $keepRevisionOf = array(
    //     'title'
    // );

    // protected $dontKeepRevisionOf = array(
    //   'category_id'
    // );

    // protected static $recordEvents = ['created'];
    
    //End Revision
    


	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['role_id','user_id'];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $table='role_user';
    protected $dates = ['deleted_at'];

    
}
