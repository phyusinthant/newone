<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Authenticatable
{
    
    use Notifiable;
    use EntrustUserTrait; 

    // Begin Revision 
    use \Venturecraft\Revisionable\RevisionableTrait; // Need to use for either audit trail or activity log

    public static function boot()
    {
        parent::boot();
    }
    
    //Audit Trail Configuration
    protected $revisionEnabled = true; //Set true to enable audit trail
    protected $revisionCleanup = true; //Remove old revisions (works only when used with $historyLimit)
    protected $historyLimit = 500; //Maintain a maximum of 500 changes at any point of time, while cleaning up old revisions.
    protected $revisionCreationsEnabled = true;
    protected $revisionNullString = 'nothing';
    protected $revisionUnknownString = 'unknown';

    //Activity Configuration
    protected $activityEnabled = true; //Set false not to log any activity
    protected $activityCreationsEnabled = true; //Set true to log create activity
    protected $activityUpdatingEnabled = true; //Set true to log update activity
    protected $activityDeletingEnabled = true; //Set true to log delete activity
    

    //End Revision
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','city_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
