<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends EntrustRole
{

	use SoftDeletes;
	
    // Begin Revision 
    use \Venturecraft\Revisionable\RevisionableTrait; // Need to use for either audit trail or activity log

    public static function boot()
    {
        parent::boot();
    }
    
    //Audit Trail Configuration
    protected $revisionEnabled = true; //Set true to enable audit trail
    protected $revisionCleanup = true; //Remove old revisions (works only when used with $historyLimit)
    protected $historyLimit = 500; //Maintain a maximum of 500 changes at any point of time, while cleaning up old revisions.
    protected $revisionCreationsEnabled = true;
    protected $revisionNullString = 'nothing';
    protected $revisionUnknownString = 'unknown';

    //Activity Configuration
    protected $activityEnabled = true; //Set false not to log any activity
    protected $activityCreationsEnabled = true; //Set true to log create activity
    protected $activityUpdatingEnabled = true; //Set true to log update activity
    protected $activityDeletingEnabled = true; //Set true to log delete activity
    

    //End Revision
    

    

	 /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


	/**
	 * BelongsToMany relations with the user model.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function users()
	{
	   return $this->belongsToMany(Config::get('auth.providers.users.model'),Config::get('entrust.role_user_table'),Config::get('entrust.role_foreign_key'),Config::get('entrust.user_foreign_key'));
	    
	}
    
}
