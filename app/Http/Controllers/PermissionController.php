<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use DB;

class PermissionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $permissions = Permission::orderBy('id','DESC')->paginate(5);

            return view('backend.permissions.index',compact('permissions'))
                ->with('i', ($request->input('page', 1) - 1) * 5);

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            return view('backend.permissions.create');

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        $this->validate($request, [
            'name' => 'required|unique:permissions,name',
            'display_name' => 'required',
            'description' => 'required',
        ]);

        try{

            $permission = new Permission();
            $permission->name = $request->input('name');
            $permission->display_name = $request->input('display_name');
            $permission->description = $request->input('description');
            $permission->save();

            if(isset($permission->id) && $permission->id != ""){
                flash('Success!', 'Permission created successfully', "success");    
            }else{
                flash('Error!', 'Permission created failed', "error");    
            }

            return redirect()->route('permissions.index');

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $permission = Permission::find($id);

            $rolePermissions = Role::join("permission_role","permission_role.role_id","=","roles.id")
                ->where("permission_role.permission_id",$id)
                ->get();

            return view('backend.permissions.show',compact('permission', 'rolePermissions'));

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $permission = Permission::find($id);
            $role = Role::get();
            
            $rolePermissions = $permission->roles()->pluck('role_id','role_id')->toArray();

            return view('backend.permissions.edit',compact('role','permission','rolePermissions'));

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        
        $this->validate($request, [
            'display_name' => 'required',
            'description' => 'required',
        ]);

        try{

            $permission = Permission::find($id);
            $permission->display_name = $request->input('display_name');
            $permission->description = $request->input('description');
            $result = $permission->save();

            if($result){
                flash('Success!', 'Permission updated successfully', "success");    
            }else{
                flash('Error!', 'Permission updated failed', "error");    
            }

            return redirect()->route('permissions.index');

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $permission = Permission::find($id);

            $result = false;
            if(isset($permission)){
                $result = $permission->delete();    
            }

            if($result){
                flash('Success!', 'Permission deleted successfully', "success");    
            }else{
                flash('Error!', 'Permission deleted failed', "error");    
            }

            return redirect()->route('permissions.index');

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }
}
