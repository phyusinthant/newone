<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use DB;
use Hash;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    const _ACTIVE_='A';
    const _INACTIVE_='I';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $roles = Role::pluck('display_name','id');

            return view('backend.users.index',compact('roles'));
            
        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
        
    }

    public function getData(Request $request){

        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            // return Datatables::of(User::select(array('users.id','users.name','users.email','users.id'))->with('roles'))->make(true);

            $users = User::select(array('users.id','users.name',
                    'users.email'
                ,'users.id'))
                    ->with('roles');
                   /* ->join('role_user as ru','ru.user_id','users.id')
                    ->join('roles as r','r.id','ru.role_id')
                    ->wherenull('r.deleted_at')
                    ->wherenull('ru.deleted_at')
                    ->groupby('users.id');*/

            return Datatables::of($users)
                ->filter(function ($query) use ($request) {
                    if ($request->has('name')) {
                        $query->where('users.name', 'like', "%{$request->get('name')}%");
                    }

                    if ($request->has('email')) {
                        $query->where('users.email', 'like', "%{$request->get('email')}%");
                    }

                    if ($request->has('roles')) {
                        $query->whereHas('roles', function ($q) use ($request) {
                                    $q->where('roles.id', '=', "{$request->get('roles')}");
                        });
                    }
            })
            ->make(true);


        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $roles = Role::pluck('display_name','id');
            $cities=City::pluck('name','id');

            return view('backend.users.create',compact('roles','cities'));
        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email|max:255',
            'password' => 'required|same:confirm-password|max:255',
            'roles' => 'required'
        ]);

        //To ignore deleted_at
        //'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL|max:255',

        try{

            $input = $request->all();
            $input['password'] = Hash::make($input['password']);

            $user = "";


            DB::beginTransaction();

                $user = User::create($input);
                $user_id=isset($user->id)? $user->id:0;

                $user->detachRoles();

                if($request->input('roles') != ""
                 && is_array($request->input('roles'))
                 && count($request->input('roles')) > 0
                )
                {
                    $user->attachRoles($request->input('roles')); 
                    /* foreach($request->input('roles') as $k=>$val){

                        $user_outlet=['user_id'=>$user_id,
                                  'role_id'=>$val  ]; 
                        RoleUser::create($user_outlet);

                     } */                        
                   
                }

             DB::commit();


            if(isset($user->id) && $user->id != ""){
                flash('Success!', 'User created successfully', "success");    
            }else{
                flash('Error!', 'User created failed', "error");    
            }

            return redirect()->route('users.index');
            
        }
        catch(\Exception $ex) {
            
            DB::rollback();
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            if((Auth::user()->roles()->first()->name == 'admin') || (Auth::user()->roles()->first()->name == 'staff')){
                $user = User::find($id);

                return view('backend.users.show',compact('user'));
            }else{
                $member = User::select(array("users.name","users.email","members.api_key","members.phone","members.address","members.status","roles.display_name"))
                    ->join("members","members.user_id","users.id")
                    ->join("role_user","role_user.user_id","users.id")
                    ->join("roles","roles.id","role_user.role_id")
                    ->wherenull("users.deleted_at")
                    ->wherenull("members.deleted_at")
                    ->wherenull("roles.deleted_at")
                    ->where("users.id",$id)
                    ->first();

                $status=[self::_ACTIVE_=>'Active',self::_INACTIVE_=>'Inactive'];

                return view('backend.members.show',['status'=>$status,'member' =>$member ]);
            }

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $user = User::find($id);
            $roles = Role::pluck('display_name','id');
            $userRole = $user->roles->pluck('id')->toArray();
            $cities=City::pluck('name','id');

            return view('backend.users.edit',compact('user','roles','userRole','cities'));

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$id."|max:255",
            'password' => 'same:confirm-password|max:255',
            'roles' => 'required'
        ]);

        try{

            $input = $request->all();
            if(!empty($input['password'])){ 
                $input['password'] = Hash::make($input['password']);
            }else{
                $input = array_except($input,array('password'));    
            }

            $result = false;

            DB::beginTransaction();

                $user = User::find($id);
                $result = $user->update($input);

                $user->detachRoles();

                if($request->input('roles')!=""
                 && is_array($request->input('roles'))
                 && count($request->input('roles')) > 0
                )
                {
                    $user->attachRoles($request->input('roles'));    
                    /*$arrexisting_ids=RoleUser::where('user_id',$id)
                                    ->delete();
                    $ids=[];
                  
                    foreach($request->input('roles') as $k=>$val){
                    
                    $user_outlet=['user_id'=>$id,
                                  'role_id'=>$val  ]; 
                    RoleUser::create($user_outlet);              

                    }*/
                } 

            DB::commit();

            if($result){
                flash('Success!', 'User updated successfully', "success");    
            }else{
                flash('Error!', 'User updated failed', "error");    
            }

            return redirect()->route('users.index');
        
        }
        catch(\Exception $ex) {

            DB::rollback();
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $user = User::find($id);

            $result = false;
            if(isset($user)){
                $result = $user->delete();    
            }

            if($result){
                flash('Success!', 'User deleted successfully', "success");    
            }else{
                flash('Error!', 'User deleted failed', "error");    
            }

            return redirect()->route('users.index');

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  string $action
     * @return \Illuminate\Http\Response
     */
    public function multiDestroy(Request $request)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try {
            $ids = $request->input("ids");
            $action = $request->input("action");

            $result = false;

            DB::beginTransaction();

                if($action == 'delete_selected' && $ids != ""){
                    $arrIds = explode(",", $ids);
                    $result = User::destroy($arrIds);
                }
                else if($action == 'delete_all'){
                    $ids = User::pluck('id')->toArray();
                    $result = User::destroy($ids);
                }

            DB::commit();

            if($result){
                flash('Success!', 'Users deleted successfully', "success");        
            }
            else{
                flash('Error!', 'Users deleted failed', "error");    
            }
            
            return redirect()->route('users.index');

        } catch (Exception $e) {

            DB::rollback();
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }
}
