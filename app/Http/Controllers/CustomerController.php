<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use DB;
use Hash;
use Yajra\Datatables\Datatables;

class CustomerController extends Controller
{

    const _ACTIVE_='A';
    const _INACTIVE_='I';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{
            $roles = Role::pluck('display_name','id');

            return view('backend.customers.index',compact('roles'));

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    public function getData(Request $request){

        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{
            $customers = User::select(array('customers.id','users.name',
                        'users.email','customers.phone','customers.address','customers.id',"roles.display_name"))
                    ->join("role_user","role_user.user_id","users.id")
                    ->join("roles","roles.id","role_user.role_id")
                    ->wherenull("users.deleted_at")
                    ->wherenull("roles.deleted_at")
                    ->join("customers",'customers.user_id','users.id')
                    ->wherenull("customers.deleted_at");
            
            return Datatables::of($customers)
                ->filter(function ($query) use ($request) {
                    if ($request->has('name')) {
                        $query->where('users.name', 'like', "%{$request->get('name')}%");
                    }

                    if ($request->has('email')) {
                        $query->where('users.email', 'like', "%{$request->get('email')}%");
                    }

                    if ($request->has('roles')) {
                        $query->whereHas('roles', function ($q) use ($request) {
                                    $q->where('roles.id', '=', "{$request->get('roles')}");
                        });
                    }
            })
            ->make(true);

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{
            $roles = Role::pluck('display_name','id');
            $status=[self::_ACTIVE_=>'Active',self::_INACTIVE_=>'Inactive'];
            $names = Customer::join("users","users.id","customers.user_id")->pluck('users.name')->all();
            
            return view('backend.customers.create',['status'=>$status,'roles'=>$roles,
                'names' =>(count($names)>0)?json_encode($names):json_encode([])   
                ]);

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email|max:255',
            'password' => 'required|same:confirm-password|max:255',
            'roles' => 'required'
        ]);

        try{

            $input = $request->all();
            $input['password'] = Hash::make($input['password']);
            
            $user = "";
            $customer = "";


            DB::beginTransaction();
                $user_data = [
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'password' => $input['password']
                ];

                $user = User::create($user_data);
                $user_id=isset($user->id)? $user->id:0;

                $user->detachRoles();

                if($request->input('roles') != ""
                 && is_array($request->input('roles'))
                 && count($request->input('roles')) > 0
                )
                {
                    $user->attachRoles($request->input('roles'));
                }

                $customer_data = [
                    'user_id'   => $user_id,
                    'phone'     => $input['phone'],
                    'address'   => $input['address'],
                    'status'    => $input['status']
                ];
                $customer = Customer::create($customer_data);

            DB::commit();

            if(isset($customer->id) && ($customer->id != "")){
                flash('Success!', 'Customer created successfully', "success");
            }else{
                flash('Error!', 'Customer created failed', "error");    
            }

            return redirect()->route('customers.index');

        }
        catch(\Exception $ex) {
            DB::rollback();
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $customer = User::select(array("users.name","users.email","customers.id as customer_id","customers.phone","customers.address","customers.status","roles.display_name"))
                    ->join("customers","customers.user_id","users.id")
                    ->join("role_user","role_user.user_id","users.id")
                    ->join("roles","roles.id","role_user.role_id")
                    ->wherenull("users.deleted_at")
                    ->wherenull("customers.deleted_at")
                    ->wherenull("roles.deleted_at")
                    ->where("customers.id",$id)
                    ->first();

            $status=[self::_ACTIVE_=>'Active',self::_INACTIVE_=>'Inactive'];

            return view('backend.customers.show',['status'=>$status,'customer' =>$customer ]);

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $customer = Customer::find($id);
            $user = User::find($customer->user_id);
            $roles = Role::pluck('display_name','id');
            $userRole = $user->roles->pluck('id')->toArray();
            $status=[self::_ACTIVE_=>'Active',self::_INACTIVE_=>'Inactive'];
            $names = Customer::join("users","users.id","customers.user_id")->pluck('users.name')->all();

            return view('backend.customers.edit',['customer'=>$customer,'user'=>$user,'status'=>$status,'roles'=>$roles,'userRole'=>$userRole,
                'names' =>(count($names)>0)?json_encode($names):json_encode([])   
                ]);

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
       
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        $customer = Customer::find($id);

        if(isset($customer)){
            $this->validate($request, [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users,email,'.$customer->user_id."|max:255",
                'password' => 'required|same:confirm-password|max:255',
                'roles' => 'required'
            ]);
        }

        try{
            $input = $request->all();
            if(!empty($input['password'])){ 
                $input['password'] = Hash::make($input['password']);
            }else{
                $input = array_except($input,array('password'));    
            }

            $user_result = false;
            $result = false;
            DB::beginTransaction();
                $customer = Customer::find($id);

                $user = User::find($customer->user_id);
                $user_data = [
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'password' => $input['password']
                ];
                $user_result = $user->update($user_data);

                if($user_result){
                    $user->detachRoles();

                    if($request->input('roles')!=""
                     && is_array($request->input('roles'))
                     && count($request->input('roles')) > 0
                    )
                    {
                        $user->attachRoles($request->input('roles'));
                    } 

                    $customer_data = [
                        'user_id' => $user->id,
                        'phone'   => $input['phone'],
                        'address'   => $input['address'],
                        'status'   => $input['status']
                    ];
                    $result = $customer->update($customer_data);
                }
                

            DB::commit();

            if($result){
                flash('Success!', 'Customer updated successfully', "success");    
            }else{
                flash('Error!', 'Customer updated failed', "error");    
            }

            return redirect()->route('customers.index');

        }
        catch(\Exception $ex) {
            DB::rollback();
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $customer = Customer::find($id);
            $user = User::find($customer->user_id);
            
            $result = false;
            if(isset($customer)){
                $result = $customer->delete();    
                $result = $user->delete();    
            }
            
            if($result){
                flash('Success!', 'Customer deleted successfully', "success");    
            }else{
                flash('Error!', 'Customer deleted failed', "error");    
            }

            return redirect()->route('customers.index');
        }    
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  string $action
     * @return \Illuminate\Http\Response
     */
    


}
