<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Permission;
use App\Models\UiPermission;
use DB;
use Yajra\Datatables\Datatables;

class RoleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            return view('backend.roles.index');

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    public function getData(Request $request){

        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $roles = Role::select(array('id','display_name','description'));

            return Datatables::of($roles)->make(true);

        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            return view('backend.roles.create');
        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        $this->validate($request, [
                'name' => 'required|unique:roles,name|max:255',
                'display_name' => 'required|max:255',
                'description' => 'required|max:255',
        ]);

        try{

            $role = new Role();
            $role->name = $request->input('name');
            $role->display_name = $request->input('display_name');
            $role->description = $request->input('description');
            $role->save();

            if(isset($role->id) && $role->id != ""){
                flash('Success!', 'Role created successfully', "success");    
            }else{
                flash('Error!', 'Role created failed', "error");    
            }

            return redirect()->route('roles.index');
        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $role = Role::find($id);
            $rolePermissions = Permission::join("permission_role","permission_role.permission_id","=","permissions.id")
                ->where("permission_role.role_id",$id)
                ->get();
            
            return view('backend.roles.show',compact('role','rolePermissions'));
        }
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $role = Role::find($id);

            return view('backend.roles.edit',compact('role'));
        }    
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        $this->validate($request, [
                'display_name' => 'required|max:255',
                'description' => 'required|max:255',
        ]);

        try{

            $role = Role::find($id);
            $role->display_name = $request->input('display_name');
            $role->description = $request->input('description');
            $result = $role->save();

            if($result){
                flash('Success!', 'Role updated successfully', "success");    
            }else{
                flash('Error!', 'Role updated failed', "error");    
            }

            return redirect()->route('roles.index');

        }    
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $role = Role::find($id);

            $result = false;
            if(isset($role)){
                $result = $role->delete();    
            }

            if($result){
                flash('Success!', 'Role deleted successfully', "success");    
            }else{
                flash('Error!', 'Role deleted failed', "error");    
            }
            
            return redirect()->route('roles.index');

        }    
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }


    /**
     * Display view to assign permission to role
     *
     * @param  int  $id
     */
    public function assignPermission($id){

        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try{

            $role = Role::find($id);
            $uiPermissions = UiPermission::orderBy('grouping','ASC')->get();

            $uiPermissionGroups = array();
            foreach ($uiPermissions as $key => $value) {
                $uiPermissionGroups[$value->grouping][] = $value;
            }

            ////Testing Data
            // foreach ($uiPermissions as $key => $value) {
            //     $uiPermissionGroups["Test"][] = $value;
            // }

            // foreach ($uiPermissions as $key => $value) {
            //     $uiPermissionGroups[$value->grouping."2"][] = $value;
            // }

            // foreach ($uiPermissions as $key => $value) {
            //     $uiPermissionGroups[$value->grouping."3"][] = $value;
            // }

            // foreach ($uiPermissions as $key => $value) {
            //     $uiPermissionGroups[$value->grouping."4"][] = $value;
            // }
            

            //Get Permission Ids array associated with selected role
            $rolePermissions = $role->perms()->pluck('permission_id','permission_id')->toArray();
            

            return view('backend.roles.assign_permission',compact('role','uiPermissionGroups','rolePermissions'));

        }    
        catch(\Exception $ex) {
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }

    }


    /**
     * Store Role Permission
     *      
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeRolePermission(Request $request, $id){

        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        $this->validate($request, [
            'uiPermissions' => 'required'
        ]);

        try{

            $result = false;

            DB::beginTransaction();

                $role = Role::find($id);

                //Delete all existing permissions assigned to Role
                $role->perms()->detach();
                
                //To store role permission ids
                $permission_ids = array();    

                //Consolidate Assign UI Permissions
                foreach ($request->input('uiPermissions') as $key => $value) {
                    
                    $ui_permission = UiPermission::find($value);

                    //Get Main Permission
                    $permission_ids[] = $ui_permission->permission_id;

                    //Get Dependent Permission
                    if($ui_permission->dependent_permission != "")
                    {
                        $dependent_permissions = explode(",", $ui_permission->dependent_permission);


                        $permission_ids = array_merge($permission_ids, $dependent_permissions);
                    }
                }

                $permission_ids = array_unique($permission_ids);
                $permission_ids = array_values($permission_ids);

                //Assign Permissions to Role
                $result = $role->attachPermissions($permission_ids);

            DB::commit();

            $new_role_permission_ids = $role->perms()->pluck("id")->toArray(); 

            $diff_permissions = array_diff($permission_ids, $new_role_permission_ids);
            
            // $log->log("new role perms ", $new_role_permission_ids);
            // $log->log("assign permission arr ", $permission_ids);
            // $log->log("diff arr ", $diff_permissions);

            if(count($diff_permissions) < 1){
                flash('Success!', 'Assigned Permission to Role successfully', "success");    
            }else{
                flash('Error!', 'Assigned Permission to Role failed', "error");    
            }

            return redirect()->route('roles.index');

        }    
        catch(\Exception $ex) {

            DB::rollback();
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }


     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  string $action
     * @return \Illuminate\Http\Response
     */
    public function multiDestroy(Request $request)
    {
        $log = app('Log');
        $log->init(__METHOD__);
        $log->log('Begin');

        try {
            $ids = $request->input("ids");
            $action = $request->input("action");

            $result = false;

            DB::beginTransaction();

                if($action == 'delete_selected' && $ids != ""){
                    $arrIds = explode(",", $ids);
                    $result = Role::destroy($arrIds);
                }
                else if($action == 'delete_all'){
                    $ids = Role::pluck('id')->toArray();
                    $result = Role::destroy($ids);
                }

            DB::commit();

            if($result){
                flash('Success!', 'Roles deleted successfully', "success");        
            }
            else{
                flash('Error!', 'Roles deleted failed', "error");    
            }
            
            return redirect()->route('roles.index');

        } catch (Exception $e) {

            DB::rollback();
            return $log->log("Error", $ex->getMessage(), "error", $ex);
        }
    }

}
