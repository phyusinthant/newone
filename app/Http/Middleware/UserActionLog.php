<?php

namespace App\Http\Middleware;

use Closure;

class UserActionLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $userAction = null)
    {

        $action = $request->route()->getAction();

        $log = app('Log');
        $log->init($action["uses"]);
        $log->log("Action Begin");
        
        // $log->log("Action", $action);
        // $log->log("Action Begin", ['request' => $request->all()]);

        if(isset($userAction)){
            $log->log($userAction);
        }

        return $next($request);
    }

    public function terminate($request, $response)
    {
        $action = $request->route()->getAction();
        $log = app('Log');
        $log->init($action["uses"]);

        $log->log("Action End");
        // $log->log('Action End', ['response' => $response]);
    }
}
