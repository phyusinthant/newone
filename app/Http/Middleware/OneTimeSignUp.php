<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class OneTimeSignUp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(User::count() > 0)
        {
            return redirect('/login');
        }

        return $next($request);
        
    }
}
