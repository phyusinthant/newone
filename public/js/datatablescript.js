
function confirmDelete(action, actionlink, name ){

	if(name == undefined)
		name = "record";
	var msg = "";
	if(action == "delete_single")
		msg = " to delete single "+name+"?";
	else if(action == "delete_all")
		msg = " to delete all "+name+"?";
	else if(action == "delete_selected")
		msg = " to delete selected "+name+"?";

	swal({   
		title: "Are you sure "+msg,   
		text: "You will not be able to recover this item!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		cancelButtonText: "No, cancel plx!",   
		closeOnConfirm: false,   
		closeOnCancel: false 
	}, 
	function(isConfirm){   
		if (isConfirm) {     
			
			var selectedIds = "";
			var deleteActionTemplate = $("#deleteActionTemplate").html();

			if(actionlink != "" && (action == "delete_single" || action == "delete_all" || action == "delete_selected")){

				console.log("action");
				console.log(action);

				if(action == "delete_selected"){
					console.log("delete_selected");
					selectedIds = rows_selected.join();
					console.log(selectedIds);
				}

				$("#deleteAction").html(deleteActionTemplate.formatUnicorn({"ids":selectedIds,"action":action,"actionlink":actionlink}));

				document.forms['deleteForm'].submit();
			}
			else{
				swal({
					title: "Failed",
					text: "Invalid Parameters",
					timer: 1000,
					showConfirmButton: true,
					type: "error"
				});
			}
		} 
		else{

			swal({
			  title: "Cancelled",
			  text: "It is safe :)",
			  timer: 1000,
			  showConfirmButton: true,
			  type: "error"
			});

			// swal("Cancelled", "It is safe :)", "error");   
		}
	});
}


//
// Updates "Select all" control in a data table
//
function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input.idClass[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input.idClass[type="checkbox"]:checked', $table);
   // var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);
   var chkbox_select_all  = $('thead input[name="select_all"]');
   

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      // chkbox_select_all.checked = false;
      // if('indeterminate' in chkbox_select_all){
      //    chkbox_select_all.indeterminate = false;
      // }
      $('thead input[name="select_all"]').each( function (){
        $(this).prop('checked',false);
        $(this).prop('indeterminate',false);
      });

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      // chkbox_select_all.checked = true;
      // if('indeterminate' in $(this)){
      //    chkbox_select_all.indeterminate = false;
      // }
     $('thead input[name="select_all"]').each( function (){
        $(this).prop('checked',true);
        $(this).prop('indeterminate',false);
      });
   // If some of the checkboxes are checked
   } else {
      // chkbox_select_all.checked = true;
      // if('indeterminate' in chkbox_select_all){
      //    chkbox_select_all.indeterminate = true;
      // }
     $('thead input[name="select_all"]').each( function (){
        $(this).prop('checked',true);
        $(this).prop('indeterminate',true);
     });
   }
}


function dataTableCheckBoxInit(table){

	 // Handle click on checkbox
   $('#data-table tbody').on('click', 'input.idClass[type="checkbox"]', function(e){
      
      var $row = $(this).closest('tr');

      var ckValue = $(this).val();

      // Get row data
      // var data = table.row($row).data();

      // Get row ID
      // var rowId = data.id;
      var rowId = ckValue;

      // Determine whether row ID is in the list of selected row IDs
      var index = $.inArray(rowId, rows_selected);

      // If checkbox is checked and row ID is not in list of selected row IDs
      if(this.checked && index === -1){
         rows_selected.push(rowId);

      // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
      } else if (!this.checked && index !== -1){
         rows_selected.splice(index, 1);
      }

      if(this.checked){
        // $row.addClass('selected');
        $("input.idClass[type=checkbox][value="+ckValue+"]").each( function (){
            $(this).prop('checked',true);
            var $row = $(this).closest('tr');
            $row.addClass('selected');
        });
      } else {
        // $row.removeClass('selected');
        $("input.idClass[type=checkbox][value="+ckValue+"]").each( function (){
            $(this).prop('checked',false);
            var $row = $(this).closest('tr');
            $row.removeClass('selected');
        });
      }

      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });

   // Handle click on table cells with checkboxes
   $('#data-table').on('click', 'tbody td, thead th:first-child', function(e){
      $(this).parent().find('input.idClass[type="checkbox"]').trigger('click');
   });


   // Handle table draw event
   table.on('draw', function(){
      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);
   });
}

function selectAllFun(el){
    if(el.checked){
       $('#data-table tbody input.idClass[type="checkbox"]:not(:checked)').trigger('click');
    } else {
       $('#data-table tbody input.idClass[type="checkbox"]:checked').trigger('click');
    }
}

function datatablescriptjslog(){
  console.log("datatablescriptjslog");
}


//No use
function checkallInit(){
	
    $('#multidel_checkall').on('change', function(event){
    	console.log("check record");
        if(this.checked)
        {
            $('.multidel').each(function(){
                this.checked = true;
            })                
        }
        else 
        {
            $('.multidel').each(function(){
                this.checked = false;
            })  
        }
    });
}