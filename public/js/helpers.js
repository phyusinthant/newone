function helpersjslog(){
	console.log("helpersjslog");
}

String.prototype.formatUnicorn = String.prototype.formatUnicorn ||
	function () {
	    "use strict";
	    var str = this.toString();
	    if (arguments.length) {
	        var t = typeof arguments[0];
	        var key;
	        var args = ("string" === t || "number" === t) ?
	            Array.prototype.slice.call(arguments)
	            : arguments[0];

	        for (key in args) {
	            str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
	        }
	    }

	    return str;
	};




function switchLanguage(locale){
	console.log('switch language');

	var _token = $("input[name=_token]").val();

	$.ajax({
		url: "/language",
		type: "POST",
		data: {locale: locale, _token: _token},
		datatype: "json",
		success: function(data){

			$('div[id^="flag"]').filter(
		    function(){
		        return this.hide();
		    });

			$('span[id^="flag'+locale+'"]').show();
			$("#"+locale).addClass("active");

		},
		error: function(data){

		},
		beforeSend: function(){

		},
		complete: function(data){
			window.location.reload(true);
		}
	});
}

// jQuery plugin to prevent double submission of forms
jQuery.fn.preventDoubleSubmission = function() {

  $(this).on('submit',function(e){
  		// alert("preventDoubleSubmission");
  		// console.log("preventDoubleSubmission");
  		
	    var $form = $(this);

	    // var data = $(this).serialize(); //all input variables  
	    // console.log(data); //print data in console

	    //Disable submit button
	    var submitButton = $(this).closest('form').find(':submit');
	    submitButton.prop('disabled', true);
	    // alert(submitButton.attr("name")); // name of submit button

	    if ($form.data('submitted') === true) {
	    	// alert("not submitted");
	      	// Previously submitted - don't submit again
	      	e.preventDefault();
	    } else {
	    	// alert("submitted");
	      	// Mark it so that the next submit can be ignored
	      	$form.data('submitted', true);
	    }
  });

  // Keep chainability
  return this;
};


// Use it like this:
// $('form').preventDoubleSubmission();

// If there are AJAX forms that should be allowed to submit multiple times per page load, you can give them a class indicating that, then exclude them from your selector like this:
// $('form:not(.js-allow-double-submission)').preventDoubleSubmission();


function disableAjaxButton(btn){
  // console.log("disableAjaxButton");
  $(document).ajaxStart(function () {
      // console.log("ajaxStart");
      btn.attr("disabled", true);
  });
  $(document).ajaxComplete(function () {
      // console.log("ajaxComplete");
      btn.attr("disabled", false);
  });
}





