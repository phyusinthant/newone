var map, markers = {}, tile_layer, layers = {}; // global variables
var marker, unitID, unitEventID, polyline, mapWaypts = []; // global 
var report_result={};
var trip_table=[];
var engine_table=[];
var trip_total=[];
var engine_total=[];
var theft_table=[];
var theft_total=[];
var filling_table=[];
var filling_total=[];
var resource_id=0;
var all_zones=[];
var trip=[];
var arr_markers=[];
var timeline='';
var flag=false;
var ride_begin='';
var ride_end='';


function calculateTime(){
    var start=$('#start_date').val();
    var no_of_hours=$('#no_of_hours').val();

    if(start ==''){
        msg('Select From Date.');
       
    }

    if(no_of_hours =='' || isNaN(no_of_hours) == true || no_of_hours <= 0){
        msg('Invalid No. of hours.');
       
    }

    var start_date=new Date(start);
   
    var end=start_date.getTime() + (no_of_hours*60*60*1000);

    //var end_date =new Date(end);
    var utc = start_date.toUTCString();

    //alert(utc);

    var utc_date=new Date(utc);
   
    var from =Math.round(utc_date.getTime()/1000);

    var to =  from + 3600 * no_of_hours;
   
   
    var from_to =[from,to];
    return from_to; 
}

function calculateDate(){
    var start=$('#start_date').val();
    var no_of_hours=$('#no_of_hours').val();

    var start_date=new Date(start);
   
    var end=start_date.getTime() + (no_of_hours*60*60*1000);

    var end_date =new Date(end);
    //var end_date =new Date(end);
    var utc = start_date.toUTCString();
    var end_utc = end_date.toUTCString();

    //alert(utc);

    var utc_date=new Date(utc);
    var utc_end_date=new Date(end_utc);
   
   
   
    var from_to =[utc_date,utc_end_date];
    return from_to; 
}

// Print message to log
function msg(text) { 
    //alert(text);
    swal({   
        title: text,   
        //text:  msg5,   
        type: "warning",   
        showCancelButton: false,   
        confirmButtonColor: "#DD6B55",   
        closeOnConfirm: false,   
        closeOnCancel: false 
    }); 
}



function init() { // Execute after login succeed
    var sess = wialon.core.Session.getInstance(), // get instance of current Session
        flags = wialon.item.Item.dataFlag.base | wialon.item.Unit.dataFlag.lastMessage ,// specify what kind of data should be returned        
        renderer = wialon.core.Session.getInstance().getRenderer();

    var res_flags = wialon.item.Item.dataFlag.base | wialon.item.Resource.dataFlag.reports|wialon.item.Resource.dataFlag.base | wialon.item.Item.dataFlag.messages | wialon.item.Resource.dataFlag.notifications | wialon.item.Resource.dataFlag.zones;

    sess.loadLibrary("resourceReports");
    sess.loadLibrary('resourceZones'); 
    sess.loadLibrary("resourceNotifications"); 
    sess.loadLibrary("itemIcon");
    
    renderer.addListener("changeVersion", update_renderer);

    sess.updateDataFlags( // load items to current session
        [
        {type: "type", data: "avl_resource", flags:res_flags , mode:0},
        {type: "type", data: "avl_unit", flags: flags, mode: 0}], // Items specification
        function (code) { // updateDataFlags callback
            if (code) { console.log(wialon.core.Errors.getErrorText(code)); return; } // exit if error code
            
            var res = sess.getItems("avl_resource"); // get loaded 'avl_resource's items
            //console.log(res);
            if (!res || !res.length){ msg("Resources not found"); return; } // check 

            res.forEach(function(resource) {
                
                resource_id = resource.getId();
              });
            //console.log(resource_id) ;
            var units = sess.getItems("avl_unit"); // get loaded 'avl_resource's items
            if (!units || !units.length){ msg("No units found"); return; } // check if units found
            
            //filter units from member
            var str_devices=$('#devices').val();
            var devices = str_devices.split(',');

            for (var i = 0; i< units.length; i++) // construct Select list using found resources
                
                if(devices.indexOf(units[i].getId().toString()) !== -1){
                  $("#units").append("<option value='"+ units[i].getId() +"'>"+ units[i].getName()+ "</option>");
                }
                
            
            $("#build").click( show_track );  // bind action to select change event
            $('#div-info').show();
            $("#tracks").on("click", ".close_btn", delete_track); //click, when need delete current track
            //$("#tracks").on("click", ".unit", focus_track); //click, when need to see any track
            getZones(resource_id);
    });
 
}

function getZones( res_id ){ // get geofences by resource id
  
  if(res_id){ // check if resource id exists
    var res = wialon.core.Session.getInstance().getItem(res_id); // get resource by id
    if(!res){ console.log("Unknown resource id: "+res_id); return; } // exit if resource not found
    var zones = res.getZones(); // get resource's zones

    var index =0;
    for (var i in zones) { // construct Select list using found zones
      all_zones[index] ={"name":zones[i].n,"d":zones[i].d,"min_x":zones[i].b.min_x,"min_y":zones[i].b.min_y,"max_x":zones[i].b.max_x,"max_y":zones[i].b.max_y};
      index++;

     // $("#ride_begin").append("<option value='" + zones[i].id + "'>" + zones[i].n + "</option>");
      //$("#ride_end").append("<option value='" + zones[i].id + "'>" + zones[i].n + "</option>");

      ride_begin +="gz"+resource_id+"_"+zones[i].id+",";
      ride_end +="gz"+resource_id+"_"+zones[i].id+",";

    }
  }
}


function show_track () {
    $("#loader").show();
     trip=[];
     arr_markers=[];
     var from_to =calculateTime();
     var from = from_to[0];
     var to = from_to[1];
    
    var unit_id =  $("#units").val(),
        sess = wialon.core.Session.getInstance(),    
        renderer = sess.getRenderer(),       
        
        unit = sess.getItem(unit_id),
        color = $("#color").val() || "ffffff"; // track color
        color =color.replace('#','');

        if (!unit) { $("#loader").hide(); return;} // exit if no unit

        // check the existence info in table of such track 
        if (document.getElementById(unit_id) || flag == true)
        {
            $("#loader").hide();
            msg("You already have this track.");
            return;
        }
      
        var pos = unit.getPosition(); // get unit position
       
        if(!pos) return; // exit if no position

        
        // callback is performed, when messages are ready and layer is formed
        callback =  qx.lang.Function.bind(function(code, layer) 
        {
            if (code) { 
                $("#loader").hide();
                showTimeLine();
                if(code == 6)code =1001;
                msg(wialon.core.Errors.getErrorText(code)); return; 
            } // exit if error code
           
            if (layer) { 
                var layer_bounds = layer.getBounds(); // fetch layer bounds

                if (!layer_bounds || layer_bounds.length != 4 || (!layer_bounds[0] && !layer_bounds[1] && !layer_bounds[2] && !layer_bounds[3])) // check all bounds terms
                    return;
                
               flag =true;
                // if map existence, then add tile-layer and marker on it
                if (map) {
                   //prepare bounds object for map
                    var bounds = new L.LatLngBounds(
                    L.latLng(layer_bounds[0],layer_bounds[1]),
                    L.latLng(layer_bounds[2],layer_bounds[3])
                    );
                    map.fitBounds(bounds); // get center and zoom
                    // create tile-layer and specify the tile template
                    if (!tile_layer)
                        tile_layer = L.tileLayer(sess.getBaseUrl() + "/adfurl" + renderer.getVersion() + "/avl_render/{x}_{y}_{z}/"+ sess.getId() +".png", {zoomReverse: true, zoomOffset: -1}).addTo(map);
                    else 
                        tile_layer.setUrl(sess.getBaseUrl() + "/adfurl" + renderer.getVersion() + "/avl_render/{x}_{y}_{z}/"+ sess.getId() +".png");
                    // push this layer in global container
                    layers[unit_id] = layer;
                    // get icon
                    var icon = L.icon({ iconUrl: unit.getIconUrl(24) });
                   
                    //create or get marker object and add icon in it
                    var marker = L.marker({lat: pos.y, lng: pos.x}).addTo(map);
                    
                    marker.setLatLng({lat: pos.y, lng: pos.x}); // icon position on map
                   // marker.setIcon(icon); // set icon object in marker
                    markers[unit_id] = marker;

                }
                // create row-string with data
                var row = "<tr id='" + unit_id + "'>";  
                // print message with information about selected unit and its position
                row += "<td class='unit'>Position " + pos.x + ", " + pos.y + "<br> Mileage " + layer.getMileage() + "</td>";
                row += "<td class='close_btn'>x</td></tr>";
                //add info in table
                $("#tracks").append(row);

                showUnit();
                
                //collect data for show timeline
                //collectData();               
                //collectNoti(from,to); 
                collectTrip(from,to);
                            
                
            }
               

    });
    // query params
    


    console.log(new Date(from*1000));
    console.log(new Date(to*1000));
    params = {
        "layerName": "route_unit_" + unit_id, // layer name
        "itemId": unit_id, // ID of unit which messages will be requested
        "timeFrom": from, //interval beginning
        "timeTo": to, // interval end
        "tripDetector": 1, //use trip detector: 0 - no, 1 - yes
        "trackColor": color, //track color in ARGB format (A - alpha channel or transparency level)
        "trackWidth": 5, // track line width in pixels
        "arrows": 0, //show course of movement arrows: 0 - no, 1 - yes
        "points": 1, // show points at places where messages were received: 0 - no, 1 - yes
        "pointColor": color, // points color
        "annotations": 1, //show annotations for points: 0 - no, 1 - yes
        "flags":508  
       
    };

    renderer.setLocale(wialon.util.DateTime.getTimezoneOffset(),'en',function(){
        
        renderer.createMessagesLayer(params, callback);
    });
     
}

function getTrips(unit_id,g, i, h, f)
{

        return wialon.core.Remote.getInstance().remoteCall("unit/get_trips", {
          itemId : unit_id,
          timeFrom : g,
          timeTo : i,
          msgsSource : h
        }, wialon.util.Helper.wrapCallback(f));
}


function collectData(){

    trip=[];
    var unit_id = $('#units').val();
    var from_to =calculateTime();
    var from = from_to[0];
    var to = from_to[1];

    var sess = wialon.core.Session.getInstance();
    var ml = sess.getMessagesLoader();
    ml.loadInterval(unit_id, from, to, 0,0, 100, // load messages for given time interval
        function(code, data){ // loadInterval callback
            if(code){ console.log(wialon.core.Errors.getErrorText(code)); return; } // exit if error code
            else {                         
                //alert(data.count);
                //console.log(data.messages);

              getTrips(unit_id,from,to,1,
                function(code, data){ // getMessages callback

            if(code){ console.log(wialon.core.Errors.getErrorText(code)); return; } // exit if error code

            console.log(data);

            if(data.length > 0){

                for(var i=0;i<data.length;i++){

                        //console.log(data[i]['from'].p.y);
                        var first_time = data[i]['from'].t;
                        var to = data[i]['to'].t;
                       
                        var from_y = data[i]['from'].p.y;
                        var from_x = data[i]['from'].p.x;
                        var to_x =data[i]['to'].p.x;
                        var to_y =data[i]['to'].p.y;


                        var myArray = {'from': first_time,
                                       'to': to, 
                                       'from_lat':from_y,
                                       'from_lon':from_x,
                                       'to_lat':to_y,
                                       'to_lon':to_x,
                                       'address':'Trip','geo':''};

                      
                        trip[i] = myArray;

                        //get address
                        /*var user = wialon.core.Session.getInstance().getCurrUser();
                        var add_params="[{\"lon\":"+x+",\"lat\":"+y+"}]";
                        var url = "https://geocode-maps.wialon.com/hst-api.wialon.com/gis_geocode?coords="+add_params+"&flags=1255211008&uid="+ user.getId();

                        
                        (function(i)
                        {
                            $.ajax({
                                    timeout: 5000,
                                    url: url,
                                    type: 'post',
                                    dataType: 'jsonp',
                                    jsonp: 'callback',
                                    async :false,
                                    success: function(d) {
                                       
                                        trip[i].address = d[0];
                                    },
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                        console.log(XMLHttpRequest);
                                        console.log(errorThrown);
                                        console.log(textStatus);
                                    }
                                }); 
                        })(i);*/                              

                        //get geofence
                       /* (function(i){
                         _getGeofencesInPoint(y, x,i);
                        })(i);*/

                    }//end of loop
                    console.log(trip);
                    
                }
                //set address in trip array
                

            });//end of get trips

            } // print success message 
        }
    );//end of load messages 
}

function collectTrip(from,to){

    var sess =wialon.core.Session.getInstance();
    var res = sess.getItem(resource_id);
    var renderer =sess.getRenderer();
    var id_unit =$('#units').val();
    var interval = {
    "from": from,
    "to": to,
    "flags": wialon.item.MReport.intervalFlag.absolute
  };
  var template = {// fill template object
    "id": 0,
    "n": "Trip",
    "ct": "avl_unit",
    "p":"",
    "tbl": [

      {"n":"unit_trips",
      "l":"Trips",
      "c":"time_begin,location_begin,time_end,location_end,coord_begin,coord_end",
      "cl":"time_begin,location_begin,time_end,location_end,coord_begin,coord_end",
      "s":"",
      "sl":"",
      "p":"",
      "sch":{"f1":0,"f2":0,"t1":0,"t2":0,"m":0,"y":0,"w":0},
      "f":0
      }  
      
      ]
  };

  renderer.setLocale(wialon.util.DateTime.getTimezoneOffset(),'en',function(){

    res.execReport(template, id_unit, 0, interval, 

      function(code, data) { 

        console.log(data);
        if (code) {
          console.log(wialon.core.Errors.getErrorText(code));
          return;
        } // exit if error code
        if (!data.getTables().length) { 
          console.log("<b>There is no data generated</b>");
          return;
        } else{

            var tables = data.getTables();
            trip=[];
            console.log(tables);
            var row_counts=0;

            
            if (!tables) return; // exit if no tables

              for (var n = 0; n < tables.length; n++)
              {
                row_counts = row_counts + parseInt(tables[n].rows);
              }

              for (var i = 0; i < tables.length; i++) { // cycle on tables
      
                var table_name = tables[i].label;                
                var index =0;

                (function(i){
                     data.getTableRows(i, 0, tables[i].rows, // get Table rows
                  function(code, rows) { // getTableRows callback
                    if (code) {
                      console.log(wialon.core.Errors.getErrorText(code));
                      return;
                    } // exit if error code                
                   
                        for (var j in rows) { 

                          if (typeof rows[j].c == "undefined") continue; // skip empty rows
                          
                         
                          var tmp_data='';
                          for (var k = 0; k < rows[j].c.length; k++) // add ceils to table
                          {
                            
                            tmp_data += getTableValue(rows[j].c[k]) +'&&';
                            
                          }                          

                           var arr_tmp=tmp_data.split('&&');
                           var time_begin =arr_tmp[0];
                           var location_begin =arr_tmp[1];                   
                           var time_end =arr_tmp[2];                   
                           var location_end =arr_tmp[3];                   
                           var coord_begin =arr_tmp[4];                   
                           var coord_end =arr_tmp[5];                   
                         
                          trip[index] = {"time_begin":time_begin,
                                                "location_begin":location_begin,
                                                "time_end":time_end,
                                                "location_end":location_end,
                                                "coord_begin":coord_begin,
                                                "coord_end":coord_end};
                          index++;

                          if(index == row_counts){
                            collectNoti(from,to);
                          }
                         
                        }
                    
                  },
                this);//end of rows

              })(i);

              }//end of result tables
            
        } 
      });//end of execute report

  });//end of setlocate
}


function collectNoti(from,to){
    var sess =wialon.core.Session.getInstance();
    var res = sess.getItem(resource_id);
    var renderer =sess.getRenderer();
    var id_unit =$('#units').val();
    var interval = {
    "from": from,
    "to": to,
    "flags": wialon.item.MReport.intervalFlag.absolute
  };

  var template = {// fill template object
    "id": 0,
    "n": "Markers",
    "ct": "avl_unit",
    "p":"",
    "tbl": [

      {"n":"unit_fillings",
      "l":"Filling",
      "c":"time_end,location_end",
      "cl":"Time,Location",
      "s":"",
      "sl":"",
      "p":"",
      "sch":{"f1":0,"f2":0,"t1":0,"t2":0,"m":0,"y":0,"w":0},
      "f":0
      },
      {"n":"unit_thefts",
      "l":"Theft",
      "c":"time_begin,location_begin",
      "cl":"Begin,Location",
      "s":"",
      "sl":"",
      "p":"",
      "sch":{"f1":0,"f2":0,"t1":0,"t2":0,"m":0,"y":0,"w":0},
      "f":0
      },
      {"n":"unit_stops",
      "l":"Stops",
      "c":"time_begin,location",
      "cl":"Begin,Location",
      "s":"",
      "sl":"",
      "p":"",
      "sch":{"f1":0,"f2":0,"t1":0,"t2":0,"m":0,"y":0,"w":0},
      "f":0
      },
      {"n":"unit_stays",
      "l":"Parking",
      "c":"time_begin,location",
      "cl":"Begin,Location",
      "s":"",
      "sl":"",
      "p":"",
      "sch":{"f1":0,"f2":0,"t1":0,"t2":0,"m":0,"y":0,"w":0},
      "f":0
      }    
      
      ]
  };

  renderer.setLocale(wialon.util.DateTime.getTimezoneOffset(),'en',function(){

    res.execReport(template, id_unit, 0, interval, 

      function(code, data) { 

        console.log(data);
        if (code) {
            $("#loader").hide();
          console.log(wialon.core.Errors.getErrorText(code));
          return;
        } // exit if error code
        if (!data.getTables().length) { 
          console.log("<b>There is no data generated</b>");
          $("#loader").hide();
          showTimeLine();
          return;
        } else{

            var tables = data.getTables();
            arr_markers=[];
            console.log(tables);
            var row_counts=0;

            
            if (!tables) return; // exit if no tables

              for (var n = 0; n < tables.length; n++)
              {
                row_counts = row_counts + parseInt(tables[n].rows);
              }

              for (var i = 0; i < tables.length; i++) { // cycle on tables
      
                var table_name = tables[i].label;                
                var index =0;

                (function(i,table_name){
                     data.getTableRows(i, 0, tables[i].rows, // get Table rows
                  function(code, rows) { // getTableRows callback
                    if (code) {
                        $("#loader").hide();
                      console.log(wialon.core.Errors.getErrorText(code));
                      return;
                    } // exit if error code                
                   
                        for (var j in rows) { 

                          if (typeof rows[j].c == "undefined") continue; // skip empty rows
                          
                         
                          var tmp_data='';

                          for (var k = 0; k < rows[j].c.length; k++) // add ceils to table
                          {
                            
                            tmp_data += getTableValue(rows[j].c[k]) +'&&';
                            
                          }                          

                           var arr_tmp=tmp_data.split('&&');
                           var time =arr_tmp[0];
                           var location =arr_tmp[1];                   
                         
                          arr_markers[index] = {"table":table_name,"time":time,"location":location};
                          index++;

                          if(index == row_counts){
                            showTimeLine();
                          }
                         
                        }
                    
                  },
                this);//end of rows

              })(i,table_name);

              }//end of result tables
            
        } 
      });//end of execute report

  });//end of setlocate

   
}

function arrangeTimelineArray(){

  console.log(arr_markers);

    //create items array for set dataset
    var locations = trip;
    var arrevents = arr_markers;
  
    var arr_items=[];
    var index=0;
    for(var i=0;i< locations.length;i++){

        arr_items[index]={id: index, 
        content:'Trip',
        start: new Date(locations[i].time_begin),
        end:new Date(locations[i].time_end),
        title:"<p style='font-family:Myanmar3'>"+locations[i].location_begin
            +"("+locations[i].time_begin+") - "+locations[i].location_end+"("+locations[i].time_end+")</p>",
        data:locations[i]

      };
      index++;
    
    }

    console.log(arrevents);
    for(var e=0;e< arrevents.length;e++){

        console.log(arrevents[e].table);

        (function(e){
          if(arrevents[e].table == 'Stops'){
            arr_items[index]={id: index, content:'',
                            start: new Date(arrevents[e].time),
                            style:'background: url(/img/stop_30.jpg) center center;width: 30px; height: 30px;',
                            data:arrevents[i],
                            title:"<p style='font-family:Myanmar3'>"+arrevents[e].location+" ("+arrevents[e].time+") <p>"
                          };
        }
        else if(arrevents[e].table == 'Parking'){
            arr_items[index]={id: index, content:'',
            start: new Date(arrevents[e].time),
            style:'background: url(/img/parking_30.png) center center;width: 30px; height: 30px;',
            title:"<p style='font-family:Myanmar3'>"+arrevents[e].location+" ("+arrevents[e].time+")</p>",
            data:arrevents[i]};
        }
        else if(arrevents[e].table == 'Filling'){
            arr_items[index]={id: index, content:'',
            start: new Date(arrevents[e].time),
            style:'background: url(/img/filling_30.png) center center;width: 30px; height: 30px;',
            title:"<p style='font-family:Myanmar3'>"+arrevents[e].location+" ("+arrevents[e].time+")</p>",
            data:arrevents[i]};
        }
        else if(arrevents[e].table == 'Theft'){
            arr_items[index]={id: index, content:'',
            start: new Date(arrevents[e].time),
            style:'background: url(/img/theft_30.png) center center;width: 30px; height: 30px;',
            title:"<p style='font-family:Myanmar3'>"+arrevents[e].location+" ("+arrevents[e].time+")</p>",
            data:arrevents[i]};
        }
        else{
           arr_items[index]={id: index, content:arrevents[e].table,
            start: new Date(arrevents[e].time),
            title:arrevents[e].location,
            data:arrevents[i]}; 
        }
    
        index++;
        })(e);
    
    }

   
    return arr_items;
}



function showTimeLine(){
 
    

     var from_to =calculateTime();
     var from = from_to[0];
     var to = from_to[1];

     var arr_items =arrangeTimelineArray();
     console.log(arr_items);

    // Create a DataSet (allows two way data-binding)
    $('#range-selector').html('');
    var container = document.getElementById('range-selector');
    var data = {
        nodes: arr_items
    };
    var options = {
        height : '400px',
        min : new Date(from * 1000),
        max : new Date(to * 1000),
        showCurrentTime: false
    };

    $("#loader").hide();
    var items = new vis.DataSet(arr_items);

    timeline = new vis.Timeline(container);
    timeline.setOptions(options);
    timeline.setItems(items);
    timeline.addCustomTime(new Date());


}


function _getGeofencesInPoint(lat, lon,i) {

  // get selected resource
  var resourceId = resource_id;
  
  // object with requested resources to find zones in
  var requestZoneId = {};
  
  var session = wialon.core.Session.getInstance();
  
  if (resourceId === 0) {    
    // add all resources to the search
    session.getItems('avl_resource').forEach(function(resource) {
      requestZoneId[resource.getId()] = [];
    });
  } else {
    // empty array means "search for all geofences in this resource"
     
    requestZoneId[resourceId] = [];
  }
  
  
  console.log(requestZoneId);
  // do a request
  wialon.util.Helper.getZonesInPoint({
    lat: lat, lon: lon,
    zoneId: requestZoneId
  }, function(error, data) {
    if (error) {
      console.log(wialon.core.Errors.getErrorText(error));
      return;
    }
      
    getzonedetails(data,i);
    
  });
}

function getzonedetails(data,i){
    var session = wialon.core.Session.getInstance();
  for (var resourceId in data) if (data.hasOwnProperty(resourceId)) {
    var foundZonesIds = data[resourceId];
    
    resourceId = parseInt(resourceId);
    var resource = session.getItem(resourceId);
     console.log(resource);
    
    console.log(foundZonesIds);
    
    foundZonesIds.forEach(function(zoneId) {

      console.log(zoneId);
      var zone = resource.getZone(zoneId);
      trip[i].geo = zone.n;
     
    });
  }
}


function update_renderer () {
    var sess = wialon.core.Session.getInstance(),
        renderer = sess.getRenderer();
    if (tile_layer && tile_layer.setUrl)
        tile_layer.setUrl(sess.getBaseUrl() + "/adfurl" + renderer.getVersion() + "/avl_render/{x}_{y}_{z}/" + sess.getId() + ".png"); // update url-mask in tile-layer
}

function focus_track (evt) {
    var row = evt.target.parentNode, // get row with data by target parentNode
        unit_id = row.id; // get unit id from current row
    // get bounds for map
    if (layers && layers[unit_id])
        var bounds =  layers[unit_id].getBounds();
    if (bounds && map)
    {
        // create object with need params
        var map_bounds = new L.LatLngBounds(
            L.latLng(bounds[0],bounds[1]),
            L.latLng(bounds[2],bounds[3])
        );
        // set view in geting bounds
        map.fitBounds(map_bounds); // get center and zoom
    }
}

function delete_track (evt) {
   
    var row = evt.target.parentNode, // get row with data by target parentNode
    
    unit_id = row.id, // get unit id from current row
     //unit_id = $('#units').val(), // get unit id from current row
        sess = wialon.core.Session.getInstance(),
        renderer = sess.getRenderer();
    if (layers && layers[unit_id])
    {
        // delete layer from renderer
        renderer.removeLayer(layers[unit_id], function(code) { 
            if (code) 
                console.log(wialon.core.Errors.getErrorText(code)); // exit if error code
            
        });
        delete layers[unit_id]; // delete layer from container
    }
    // move marker behind bounds
    console.log(markers[unit_id]);
    if (map)
        map.removeLayer(markers[unit_id]);
    delete markers[unit_id];
    // remove row from info table
    $(row).remove();
    flag =false;

    //clean data
    trip=[];
    arr_markers=[];
   showTimeLine();
}

function init_map() {
    // create a map in the "map" div, set the view to a given place and zoom
    map = L.map('map').setView([53.9, 27.55], 10);
    var sess = wialon.core.Session.getInstance(); // get instance of current Session    
    // add WebGIS tile layer
    L.tileLayer(sess.getBaseGisUrl("render") + "/gis_render/{x}_{y}_{z}/" + sess.getCurrUser().getId() + "/tile.png", {
        zoomReverse: true, 
        zoomOffset: -1
    }).addTo(map);
}

// execute when DOM ready
$(document).ready(function () {

    wialon.core.Session.getInstance().initSession("https://hst-api.wialon.com"); // init session
    // For more info about how to generate token check
    // http://sdk.wialon.com/playground/demo/app_auth_token
    var api_key =$('#api_key').val();
    wialon.core.Session.getInstance().loginToken(api_key, "", // try to login
        function (code) { // login callback
            // if error code - print error message
            if (code){ msg(wialon.core.Errors.getErrorText(code)); return; }
           
            init_map();
            init(); // when login suceed then run init() function
    });
});



function getTableValue(data) { // calculate ceil value
  if (typeof data == "object")
    if (typeof data.t == "string") return data.t; else return "";
  else return data;
}

//show current position
function showUnit(){ // show selected unit on map
    var val = $("#units").val(); // get selected unit id
    if (!val) return; // exit if no unit selected
    var sess = wialon.core.Session.getInstance(); // get instance of current Session
    renderer =sess.getRenderer();
    // specify what kind of data should be returned
    var flags = wialon.item.Unit.dataFlag.lastMessage;

    var unit = null;
    if (unitID) { // check if we already have previous unit
        unit = sess.getItem(unitID);
        sess.updateDataFlags( // remove previous item from current session
            [{type: "id", data: unitID, flags: flags, mode: 2}], // item specification
            function(code) {
                if (code) { console.log(wialon.core.Errors.getErrorText(code)); return; }
            
                if (unitEventID) unit.removeListenerById(unitEventID); // unbinding event from this unit
        });
    }

    unitID = val;
    unitEventID = null; // empty event ID
    mapWaypts = []; // remove all old checkpoints if they were here

    sess.updateDataFlags( // load item with necessary flags to current session
        [{type: "id", data: unitID, flags: flags, mode: 1}], // item specification
        function(code) {
            if (code) { console.log(wialon.core.Errors.getErrorText(code)); return; }

            unit = wialon.core.Session.getInstance().getItem(val); // get unit by id
            if(!unit) return; // exit if no unit
            var position = unit.getPosition(); // get unit position
            if (!position) return; // exit if no position 
            if (map) { // check if map created and we can detect position of unit

                var icon = L.icon({
                    iconUrl: unit.getIconUrl(32),
                    iconAnchor: [16, 16]
                });


                if (!marker) {
                    marker = L.marker({lat: position.y, lng: position.x}, {icon: icon}).addTo(map);
                } else {
                    marker.setLatLng({lat: position.y, lng: position.x});
                    marker.setIcon(icon);
                }
                map.setView({lat: position.y, lng: position.x});

               if (!polyline) {
                    polyline = L.polyline([{lat: position.y, lng: position.x}], {color: 'blue'}).addTo(map);
                }

            }

            unitEventID = unit.addListener("messageRegistered", showData); // register event when we will receive message
    });
}

function showData(event) {
    var data = event.getData(); // get data from event
    console.log(data);
    if (!data.pos) return; // exit if no position 

    var msg_time =data.t; // here we will put all required information
    var position = { // get unit position 
        x: data.pos.x,
        y: data.pos.y
    };


    if (map) { // check if map created

        marker.setLatLng({lat: position.y, lng: position.x});

        if (polyline) {
            polyline.addLatLng({lat: position.y, lng: position.x});
        }
        map.setView({lat: position.y, lng: position.x});

    }

    updateTimeLine(msg_time);
     
}

function updateTimeLine(msg_time){

   var from_to =calculateTime();
   var from = from_to[0];
   var to = from_to[1];

   if(msg_time > to){   
    return;
   } 


   timeline.removeCustomTime(undefined);
   timeline.addCustomTime(new Date(msg_time * 1000));

   
   
   //get data
    var sess =wialon.core.Session.getInstance();
    var res = sess.getItem(resource_id);
    var renderer =sess.getRenderer();
    var id_unit =$('#units').val();
    var interval = {
    "from": from,
    "to": to,
    "flags": wialon.item.MReport.intervalFlag.absolute
  };
  var template = {// fill template object
    "id": 0,
    "n": "Markers",
    "ct": "avl_unit",
    "p":"",
    "tbl": [

      {"n":"unit_trips",
      "l":"Trips",
      "c":"time_begin,location_begin,time_end,location_end,coord_begin,coord_end",
      "cl":"time_begin,location_begin,time_end,location_end,coord_begin,coord_end",
      "s":"",
      "sl":"",
      "p":"",
      "sch":{"f1":0,"f2":0,"t1":0,"t2":0,"m":0,"y":0,"w":0},
      "f":0
      },

      {"n":"unit_fillings",
      "l":"Filling",
      "c":"time_end,location_end",
      "cl":"Time,Location",
      "s":"",
      "sl":"",
      "p":"",
      "sch":{"f1":0,"f2":0,"t1":0,"t2":0,"m":0,"y":0,"w":0},
      "f":0
      },
      {"n":"unit_thefts",
      "l":"Theft",
      "c":"time_begin,location_begin",
      "cl":"Begin,Location",
      "s":"",
      "sl":"",
      "p":"",
      "sch":{"f1":0,"f2":0,"t1":0,"t2":0,"m":0,"y":0,"w":0},
      "f":0
      },
      {"n":"unit_stops",
      "l":"Stops",
      "c":"time_begin,location",
      "cl":"Begin,Location",
      "s":"",
      "sl":"",
      "p":"",
      "sch":{"f1":0,"f2":0,"t1":0,"t2":0,"m":0,"y":0,"w":0},
      "f":0
      },
      {"n":"unit_stays",
      "l":"Parking",
      "c":"time_begin,location",
      "cl":"Begin,Location",
      "s":"",
      "sl":"",
      "p":"",
      "sch":{"f1":0,"f2":0,"t1":0,"t2":0,"m":0,"y":0,"w":0},
      "f":0
      }    
      
      ]
  };

  renderer.setLocale(wialon.util.DateTime.getTimezoneOffset(),'en',function(){

    res.execReport(template, id_unit, 0, interval, 

      function(code, data) { 

        console.log(data);
        if (code) {
            $("#loader").hide();
          console.log(wialon.core.Errors.getErrorText(code));
          return;
        } // exit if error code
        if (!data.getTables().length) { 
          console.log("<b>There is no data generated</b>");
          
          return;
        } else{

            var tables = data.getTables();
            
            console.log(tables);
            var row_counts=0;
            
            if (!tables) return; // exit if no tables

              trip=[];
              arr_markers=[];
              for (var n = 0; n < tables.length; n++)
              {
                row_counts = row_counts + parseInt(tables[n].rows);
              }

              for (var i = 0; i < tables.length; i++) { // cycle on tables
      
                var table_name = tables[i].label;                
                var index =0;
                var trip_index = 0;
                var total_index=0;

                (function(i,table_name){
                     data.getTableRows(i, 0, tables[i].rows, // get Table rows
                  function(code, rows) { // getTableRows callback
                    if (code) {
                        
                      console.log(wialon.core.Errors.getErrorText(code));
                      return;
                    } // exit if error code                
                   
                        for (var j in rows) { 

                          if (typeof rows[j].c == "undefined") continue; // skip empty rows
                          
                         
                          var tmp_data='';

                          for (var k = 0; k < rows[j].c.length; k++) // add ceils to table
                          {
                            
                            tmp_data += getTableValue(rows[j].c[k]) +'&&';
                            
                          }                          

                          if(table_name == 'Trips'){
                             var arr_tmp=tmp_data.split('&&');
                             var time_begin =arr_tmp[0];
                             var location_begin =arr_tmp[1]; 
                             var time_end =arr_tmp[2]; 
                             var location_end =arr_tmp[3]; 
                             var coord_begin =arr_tmp[4]; 
                             var coord_end =arr_tmp[5]; 

                             trip[trip_index] = {"time_begin":time_begin,
                                                "location_begin":location_begin,
                                                "time_end":time_end,
                                                "location_end":location_end,
                                                "coord_begin":coord_begin,
                                                "coord_end":coord_end};
                            trip_index++;                    
                             

                          }
                          else{

                             var arr_tmp=tmp_data.split('&&');
                             var time =arr_tmp[0];
                             var location =arr_tmp[1];                   
                           
                            arr_markers[index] = {"table":table_name,"time":time,"location":location};
                            index++;
                          }
                          total_index++;
                           

                          if(total_index == row_counts){

                            var arr_items =arrangeTimelineArray();
                            var items = new vis.DataSet(arr_items);
                            timeline.setItems(items);
                            
                          }
                         
                        }
                    
                  },
                this);//end of rows

              })(i,table_name);

              }//end of result tables
            
        } 
      });//end of execute report

  });//end of setlocate
}

//Reporting.......
$('#report_trip').on('change',function(){
     if($('#report_trip').is(':checked')){
        $('#div-geo').hide();
    }
    else{
       $('#div-geo').show(); 
    }
});

$('#report_area').on('change',function(){
     if($('#report_area').is(':checked')){
        $('#div-geo').show();
    }
    else{
       $('#div-geo').hide(); 
    }
});

$('#btn_generate').on('click',function(){
        $("#loader").show();
        if($('#report_trip').is(':checked')){
             executeReport();
        }
        if($('#report_area').is(':checked')){
            
            /*if($('#ride_begin').val() == '' && $('#ride_end').val() == ''){
                msg('Select geofences for start and end.');
                $("#loader").hide();
                return;
            }*/

             executeAreaReport();
        }      
       
});
 

function executeReport() { // execute selected report
  
  var id_res = resource_id,   
    id_unit = $("#units").val();
    
  var  start_date = $("#start_date").val();
  if (!id_res) {
    $("#loader").hide();
    msg("Select resource");
    return;
  } // exit if no resource selected
  if (!id_unit) {
    $("#loader").hide();
    msg("Select unit");
    return;
  } // exit if no unit selected
   if (!start_date) {
    $("#loader").hide();
    msg("Select from date");
    return;
  }

   if (!$('#no_of_hours').val()) {
    $("#loader").hide();
    msg("Select No. of hours");
    return;
  }


  var sess = wialon.core.Session.getInstance(); // get instance of current Session
  renderer = sess.getRenderer();
  var res = sess.getItem(id_res); // get resource by id
 
  var from_to =calculateTime();
  var from =from_to[0];
  var to =from_to[1];

  // specify time interval object
  var interval = {
    "from": from,
    "to": to,
    "flags": wialon.item.MReport.intervalFlag.absolute
  };


  $("#btn_generate").prop("disabled", true); // disable button (to prevent multiclick while execute)
  
  //grouping
  var gp="{\"grouping\":\" {\\\"type\\\":\\\"day\\\"}\" }";

  var c="driver,time_begin,location_begin,time_end,location_end,duration,duration_prev,mileage,max_speed,fuel_consumption_fls,fuel_level_end,avg_fuel_consumption_fls,coord_begin,coord_end";

  var template1 = {// fill template object
    "id": 0,
    "n": "Trips",
    "ct": "avl_unit",
    "p":"",
    "tbl": [{
        "n": "unit_trips",
        "l": "unit_trips",
        "c": c,
        "cl":c,
        "s": "",
        "sl": "",
        "p": gp,
        "sch": {
          "f1": 0,
          "f2": 0,
          "t1": 0,
          "t2": 0,
          "m": 0,
          "y": 0,
          "w": 0
        },
        "f":4368  //4496
      },
      {
        "n": "unit_engine_hours",
        "l": "unit_engine_hours",
        "c": "duration_stay,fuel_avg_consumption_fls_idle",
        "cl": "duration_stay,fuel_avg_consumption_fls_idle",
        "s": "",
        "sl": "",
        "p": gp,
        "sch": {
          "f1": 0,
          "f2": 0,
          "t1": 0,
          "t2": 0,
          "m": 0,
          "y": 0,
          "w": 0
        },
        "f": 4496
      },
      {
        "n": "unit_thefts",
        "l": "unit_thefts",
        "c": "thefted",
        "cl": "thefted",
        "s": "",
        "sl": "",
        "p": gp,
        "sch": {
          "f1": 0,
          "f2": 0,
          "t1": 0,
          "t2": 0,
          "m": 0,
          "y": 0,
          "w": 0
        },
        "f": 4496
      },
      {
        "n": "unit_fillings",
        "l": "unit_fillings",
        "c": "filled",
        "cl": "filled",
        "s": "",
        "sl": "",
        "p": gp,
        "sch": {
          "f1": 0,
          "f2": 0,
          "t1": 0,
          "t2": 0,
          "m": 0,
          "y": 0,
          "w": 0
        },
        "f": 4496
      },
      
 ]
  };

  

  renderer.setLocale(wialon.util.DateTime.getTimezoneOffset(),'en',function(){

  //for template
  res.execReport(template1, id_unit, 0, interval, // execute selected report

  function(code, data) { // execReport template
    
    if (code) {
        $("#loader").hide();
      console.log(wialon.core.Errors.getErrorText(code));
      return;
    }
    if (!data.getTables().length) { // exit if no tables obtained
      $("#loader").hide();
      msg("There is no data generated.");
      $('#btn_generate').attr('disabled',false);
      return;
    }  // exit if error code
     else{
      console.log("trips");
      console.log(data);
      showReportResultTrip(data);      

    } //else of trip
  });//end of trip 

  });//end of set timezone
  
}

function showReportResultTrip(result) { // show result after report execute
  trip_table=[];
  engine_table=[];
  theft_table=[];
  filling_table=[];
  trip_total=[];
  engine_total =[];
  theft_total=[];
  filling_total=[]; 

  var tables = result.getTables(); // get report tables
  if (!tables) return false; // exit if no tables
  var count =tables.length;
  var row_counts=0;

  for (var n = 0; n < tables.length; n++)
  {
    row_counts = row_counts + parseInt(tables[n].rows);
  }
 
  for (var i = 0; i < tables.length; i++) { // cycle on tables

    var table_name = tables[i].label;                
    var index =0;
    var trip_index=0;
    var engine_index = 0;
    var theft_index=0;
    var fill_index=0;
    var total_index=0;

    if(table_name == 'unit_trips')
       trip_total = tables[i].total;
    if(table_name == 'unit_engine_hours')
       engine_total = tables[i].total;
    if(table_name == 'unit_thefts')
       theft_total = tables[i].total; 
    if(table_name == 'unit_fillings')
       filling_total = tables[i].total; 

  
   (function(i,table_name){
   result.getTableRows(i, 0, tables[i].rows, // get Table rows
     function( code, rows) { // getTableRows callback
      $("#loader").hide();
        if (code) {console.log(wialon.core.Errors.getErrorText(code)); return;}


         // exit if error code
        for(var l in rows) { // cycle on table rows
          if (typeof rows[l].c == "undefined") continue; // skip empty rows

          if(table_name == 'unit_trips'){

              var tmp_array=[];
              for (var k = 0; k < rows[l].c.length; k++) // add ceils to table
              {
                tmp_array[k] = getTableValue(rows[l].c[k]);
              }
              trip_table[trip_index]= tmp_array;
              trip_index++; 
          }
          else{
            var tmp_data='';
          for (var k = 0; k < rows[l].c.length; k++) // add ceils to table
          {            
            tmp_data += getTableValue(rows[l].c[k]) +'&&';            
          } 

            //add separate table
            if(table_name == 'unit_engine_hours'){
               var arr_tmp=tmp_data.split('&&');
               var date =arr_tmp[1];
               var duration_stay =arr_tmp[2];
               var fuel_avg_consumption_fls_idle =arr_tmp[3]; 
               
               engine_table[engine_index] = {
                                  "date":date,
                                  "duration_stay":duration_stay,
                                  "fuel_avg_consumption_fls_idle":fuel_avg_consumption_fls_idle,
                                 
                                  };
              engine_index++;                    
               

            }
            if(table_name == 'unit_thefts'){

               var arr_tmp=tmp_data.split('&&');
               var date =arr_tmp[1];
               var thefted =arr_tmp[2];
              
               
               theft_table[theft_index] = {"thefted":thefted,
                                  "date":date,
                                 
                                  };
              theft_index++;  

            }
            if(table_name == 'unit_fillings'){

               var arr_tmp=tmp_data.split('&&');
               var filled =arr_tmp[2];
               var date =arr_tmp[1]; 
              
               
               filling_table[fill_index] = {"filled":filled,
                                  "date":date,
                                 
                                  };
              fill_index++; 

            }
          }         

            total_index++;                          

              if(total_index == row_counts){

               showReportData();
                
              }
        }//end of rows loop
        
      }
    );
  })(i,table_name);
    
  }  
  
}



/*function showReportResult(result) { // show result after report execute
  trip_table=[];
  trip_total=[];  
  var tables = result.getTables(); // get report tables
  if (!tables) return; // exit if no tables
  for (var i = 0; i < tables.length; i++) { // cycle on tables
    // html contains information about one table
     trip_total = tables[i].total;

      result.getTableRows(i, 0, tables[i].rows, // get Table rows
      qx.lang.Function.bind( function( code, rows) { // getTableRows callback
        $("#loader").hide();
        if (code) {console.log(wialon.core.Errors.getErrorText(code)); return;} // exit if error code
        for (var j in rows) { // cycle on table rows
           //console.log(rows[j]);
          if (typeof rows[j].c == "undefined") continue; // skip empty rows
         
          var tmp_array=[];
          for (var k = 0; k < rows[j].c.length; k++) // add ceils to table
          {
            tmp_array[k] = getTableValue(rows[j].c[k]);
          }
          trip_table[j]= tmp_array;         
          
        }
      
      }, this)
    );

  }
  console.log(trip_total);
  console.log(trip_table);
  return true;
}
*/
//driver,time_begin,location_begin,time_end,location_end,duration,duration_prev,mileage,max_speed,fuel_consumption_fls,fuel_level_end,avg_fuel_consumption_fls,coord_begin,coord_end

function showReportData(){
 
  if(trip_table.length <= 0){
    msg('There is no data generated.');
    $("#loader").hide();
    $('#btn_generate').attr('disabled',false);
    return;
  }


  var unit_name =$('#units option:selected').text();

  var html = "<div class='wrap'>";
  html +="<table id='tbl-data' summary='Code page support in different versions of MS Windows.' rules='groups' frame='hsides' border='1' class='table table-striped' style='width:100%'>";
  html +="<caption style='font-size:23px;font-family:Myanmar3'>Custom Report For "+unit_name+"</caption>";
  html +="<tr style='background-color:rgb(211,211,211);'><td>No.</td>";
  html += "<td>Grouping</td>";
  html +="<td>Driver</td>";
  html +="<td>Beginning</td>";
  html +="<td>Initial location</td><td>End</td>";
  html +="<td>Final location</td>";
  html +="<td>Duration (Engine On)</td>";
  html +="<td>Off-time</td>";
  html +="<td>Mileage</td>";
  html +="<td>Max speed</td>";
  html +="<td>Consumed by FLS</td>";
  html +="<td>Final fuel level</td>";
  html +="<td>Stolen (Fuel Theft)</td>";
  html +="<td>Avg consumption by FLS</td>";
  html +="<td>Idling</td>";
  html +="<td>Consumed by FLS in idle run</td>";
  html +="<td>Filled</td>";
  html +="</tr>";
  for (var i =0;i< trip_table.length;i++) {

    
    html += "<tr>";
    html +="<td>"+trip_table[i][0]+"</td>";
    html +="<td>"+trip_table[i][1]+"</td>";
    html +="<td>"+trip_table[i][2]+"</td>";
    html +="<td>"+trip_table[i][3]+"</td>";

    //check begin geo zone
    var begin_coo = trip_table[i][14];
    var end_coo = trip_table[i][15];
    var arr_begin=begin_coo.split(',');
    var begin_y = parseFloat(arr_begin[0]);
    var begin_x =parseFloat(arr_begin[1]);
    var arr_end = end_coo.split(',');
    var end_y = parseFloat(arr_end[0]);
    var end_x = parseFloat(arr_end[1]);

    var zone_name='';
    for (var z=0;z< all_zones.length;z++){
  
      if(begin_y >= all_zones[z].min_y && begin_y <= all_zones[z].max_y && begin_x >= all_zones[z].min_x && begin_x <= all_zones[z].max_x){
        zone_name = all_zones[z].name;
        break;
      }
    }

    if(zone_name !=''){
        html +="<td>"+zone_name+":"+trip_table[i][4]+"</td>";
    }
    else{
        html +="<td>"+trip_table[i][4]+"</td>";
    }

    
    
    html +="<td>"+trip_table[i][5]+"</td>";

    //check eng geo zone
    var zone_name='';
    for (var z=0;z< all_zones.length;z++){
  
      if(end_y >= all_zones[z].min_y && end_y <= all_zones[z].max_y && end_x >= all_zones[z].min_x && end_x <= all_zones[z].max_x){
        zone_name = all_zones[z].name;
        break;
      }
    }

    if(zone_name !=''){
        html +="<td>"+zone_name+":"+trip_table[i][6]+"</td>";
    }
    else{
        html +="<td>"+trip_table[i][6]+"</td>";
    }

    html +="<td>"+trip_table[i][7]+"</td>";
    html +="<td>"+trip_table[i][8]+"</td>";
    html +="<td>"+trip_table[i][9]+"</td>";
    html +="<td>"+trip_table[i][10]+"</td>";
    html +="<td>"+trip_table[i][11]+"</td>";
    html +="<td>"+trip_table[i][12]+"</td>";

    //fill fuel theft
    if(theft_table.length>0){

      var theft=0;
      for(var k=0;k<theft_table.length;k++)
      {
          if(theft_table[k].date ==trip_table[i][1]){
             theft = theft_table[k].thefted;
             break;
          }
             
       
      }
      html +="<td>"+theft+"</td>";     
    }
    else{
       html +="<td>-</td>";
    }
    //end fill fuel theft

    html +="<td>"+trip_table[i][13]+"</td>";

    if(engine_table.length>0){

      var idle='';
      var fls='';
      
      for(var k=0;k<engine_table.length;k++)
      {
          
        
          if(engine_table[k].date ==trip_table[i][1]){

             idle = engine_table[k].duration_stay;
             fls =engine_table[k].fuel_avg_consumption_fls_idle;
             break;
          }
             
       
      }
      html +="<td>"+idle+"</td>";     
      html +="<td>"+fls+"</td>";     
      
    }
    else{
       html +="<td>-</td>";
       html +="<td>-</td>";
       
    }

    //fill fuel filling
    if(filling_table.length>0){

      var fill='';
      for(var k=0;k<filling_table.length;k++)
      {
          if(filling_table[k].date ==trip_table[i][1]){
             fill = filling_table[k].filled;
             break;
          }
             
       
      }
      html +="<td>"+fill+"</td>";     
    }
    else{
       html +="<td>-</td>";
    }
    //end fill fuel filling
    
    html +="</tr>";


    

  }//end of loop

   //add total
    var begin_coo = trip_total[14];
    var end_coo = trip_total[15];
    var arr_begin=begin_coo.split(',');
    var begin_y = parseFloat(arr_begin[0]);
    var begin_x =parseFloat(arr_begin[1]);
    var arr_end = end_coo.split(',');
    var end_y = parseFloat(arr_end[0]);
    var end_x = parseFloat(arr_end[1]);

    

  html += "<tr style='background-color:rgb(211,211,211);'><td>"+trip_total[0]+"</td>";
  html +="<td>"+trip_total[1]+"</td>";
  html +="<td>"+trip_total[2]+"</td>";
  html +="<td>"+trip_total[3]+"</td>";

  var zone_name='';
for (var z=0;z< all_zones.length;z++){

  if(begin_y >= all_zones[z].min_y && begin_y <= all_zones[z].max_y && begin_x >= all_zones[z].min_x && begin_x <= all_zones[z].max_x){
    zone_name = all_zones[z].name;
    break;
  }
}

if(zone_name !=''){
    html +="<td>"+zone_name+":"+trip_total[4]+"</td>";
}
else{
    html +="<td>"+trip_total[4]+"</td>";
}

  html +="<td>"+trip_total[5]+"</td>";

    var zone_name='';
for (var z=0;z< all_zones.length;z++){

  if(end_y >= all_zones[z].min_y && end_y <= all_zones[z].max_y && end_x >= all_zones[z].min_x && end_x <= all_zones[z].max_x){
    zone_name = all_zones[z].name;
    break;
  }
}

if(zone_name !=''){
    html +="<td>"+zone_name+":"+trip_total[6]+"</td>";
}
else{
    html +="<td>"+trip_total[6]+"</td>";
}


  html +="<td>"+trip_total[7]+"</td>";
  html +="<td>"+trip_total[8]+"</td>";
  html +="<td>"+trip_total[9]+"</td>";
  html +="<td>"+trip_total[10]+"</td>";
  html +="<td>"+trip_total[11]+"</td>";
  html +="<td>"+trip_total[12]+"</td>";
  if(theft_total[2] != undefined)
    html +="<td>"+theft_total[2]+"</td>";
  else 
    html +="<td>-</td>";  
  html +="<td>"+trip_total[13]+"</td>";
  if(engine_total[2] != undefined)
    html +="<td>"+engine_total[2]+"</td>";
  else 
    html +="<td>-</td>";  
  if(engine_total[3] != undefined)
    html +="<td>"+engine_total[3]+"</td>";
  else 
    html +="<td>-</td>";

  if(filling_total[2] != undefined)
    html +="<td>"+filling_total[2]+"</td>";
  else 
    html +="<td>-</td>";    
 
  html +="</tr>";

  //add Remarks
  html +="<tr><td colspan='17'></td></tr>";
  html +="<tr><td colspan='2'>Remarks:</td><td colspan='16'>"+$('#remarks').val();+"</td></tr>";

   html +="</table></div>";
   $('#div-data').html(html);
  $('#report-modal').modal('show');
  $('#btn_generate').attr('disabled',false);
}


$('#btn_map').click(exportData);
function exportData(){
    $("#loader").show();
      // get data from corresponding fields
  //14938601- salesteam resources id
  //21 -trip report id
  var id_res=resource_id, id_templ="21", id_unit=$("#units").val();
  if(!id_res){ msg("Select resource"); return;} // exit if no resource selected
  if(!id_templ){ msg("Select report template"); return;} // exit if no report template selected
  if(!id_unit){ msg("Select unit"); return;} // exit if no unit selected

  var sess = wialon.core.Session.getInstance(); // get instance of current Session
  
  var res = sess.getItem(id_res); // get resource by id

  var from_to =calculateTime();
  var from =from_to[0];
  var to =from_to[1];

  // specify time interval object
  var interval = { "from": from, "to": to, "flags": wialon.item.MReport.intervalFlag.absolute };
  var template = res.getReport(id_templ); // get report template by id

  $("#btn_map").prop("disabled", true); // disable button (to prevent multiclick while execute)

  var exportparams ={
                "format":2,
                "compress":0,
                 "pageOrientation":'landscape',
                 "attachMap":"1",
                 "extendBounds":"1",
                 "outputFileName":"Online_report"};

  //console.log(template);               
                 
  res.cleanupResult(function(code){
  res.execReport(template, id_unit, 0, interval, // execute selected report
    function(code, data) { // execReport template
      $("#btn_map").prop("disabled", false); // enable button
      if(code){ console.log(wialon.core.Errors.getErrorText(code)); return; } // exit if error code

      // console.log(data) ;
     /*  var url = data.getExportUrl(2,exportparams);
      
        window.location.href = url; // promt to save file    */
        var map_url =data.getMapUrl(500,500);
        window.location.href = map_url;  
        $("#loader").hide();

  });//end of execreport
});//end of clean up
   
}


function exportTableToPDF(){

    var unit_name =$('#units option:selected').text();
    var title ="Custom Report For "+unit_name;
    var doc = new jsPDF('l');
    
    doc.addFileToVFS('mm3.ttf','AAEAAAATAQAABAAwR0RFRgIAA0IAAYMAAAAAKkdQT1MY6Am0AAGDLAAAAopHU1VCp0GS9QABhbgAAF+iT1MvMjSYbGUAAYKoAAAAVmNtYXBAP0MtAAFKsAAAAtJjdnQgB4MEJAABJ7gAAAAyZmVhdIBSCzcAAeVcAAAASGZwZ20Fi4VCAAAG8AAAAMZnYXNw//8AAwAB5aQAAAAIZ2x5ZlxAhZYAAAvcAAEb3GhlYWT3Xx11AAEn7AAAADZoaGVhB/IELQABKCQAAAAkaG10eJeRfyAAAAE8AAAFsmxvY2H2SDx4AAAJAAAAAtxtYXhwAoQCSwABTYQAAAAgbW9yeNN3x1EAAU2kAAA1BG5hbWUMsTCgAAE0iAAAFidwb3N0G711hwABKEgAAAxAcHJlcCGRKqQAAAe4AAABRwFsACEAAAAAAU0AAAD6AAABTQCCAZgATQH0AAUB9AAsA0EAPQMKACoAtAAwAU0AMAFNAB0B9ABFAjQAHgD6ADgBTQAnAPoARgEW//cB9AAYAfQAbwH0AB4B9AArAfQADAH0ACAB9AAiAfQAFAH0ADgB9AAeARYAUQEWAFACNAAcAjQAHgI0ABwBvABEA5kAdALSAA8CmwARApsAHALSABACYwAMAiwADALSACAC0gATAU0AEgGFAAoC0gAiAmMADAN5AAwC0gAMAtIAIgIsABAC0gAiApsAEQIsACoCYwARAtIADgLSABADsAAFAtIACgLSABYCYwAJAU0AWAEW//cBTQAiAdUAGAH0AAABTQATAbwAJQH0AAMBvAAZAfQAGwG8ABkBTQAUAfQAHAH0AAkBFgAQARb/ugH0AAcBFgATAwoAEAH0ABAB9AAdAfQABQH0ABgBTQAFAYUAMwEWAA0B9AAJAfQAEwLSABUB9AARAfQADgG8ABsB4ABkAMgAQwHgAIICHQAoAkQALwIwABMCjwAAAjAA7QIwAIMCMABkAjAAGAOp//cCJf/8AiX/9wOi/+8CJf/3Aib/+AOZ//MCFP/3ArwAAAIl//QDi//0AiX/9wIl//UCCv/0AiX/9AQS//gDrv/3A6T/9wIlAAECJf/3AiX/+AIl//cCJf/3AiX/9wOL//QCJf/3A6v/9wItAAQDpP/4AiX/9wOO//QDqP/3AiX/9wNS//MDqv/3BFj/9wIl//QCJf/0AhP/9QQF//cHnP/0ApQABgQIAAYCCQAGAgwABgIJAAYCFQAGA9r//wIMAAYCDAAGAhUABgL7AAYCJQAGAhAABgK5AAYCxgAAAiIABgIMAAYFDP/0AkAAAAIK//QBy//eAgr/9AIK//cCJf/0AiX/9wIe//cCJf/3AiX/9wDeADQBdwA0AiX/9AK4//QCCv/3A8v/9QIlAAACJQAAAiUAAgIlAAICEwAAAy0AAAOwAAYFVwAGAhUABgIlAAYAAfyoAAH94gAB/fIAAf1hAAH9+gAB/fQAAf1fAAH+CACJ/iQABP6kAAH9iQAB/jMAAf2kAAH+MwAE/boAA/1rAAH9WAAB/VwAAf37AAH95gAA/fsAAf3xAAH97wAB/fEAAf1nAAH98QAB/WYAAf4tAAH81wAB/QoAAf1qAAH9hwDK/zIBo/+BAAH+DQAB/g0AAf6lAAH+XQHb//UAAf4nAAH+agAB/pUBPgAGAAH+QgCO/wkAAf4JAAH+PQF3/zoAov/3AKL/9wCi//cAov/3AKL/9wCi//cAAf2XAI7+MQCa//8AogAAAI791wCO/b4Aov/3AKL/9wAB/ksAAf2GAAH9fQAB/d0ABf2IAAH+NQAB/fgAw//sAV7/7AOL//QCJQAAAiUAAAIl/+8CMv/0AiX/8QIl//cEEv/4BBL/+AIH//0B8gABAAH9+ABk/5gABP8UAAAAAAAAAAAAAAAAAAAAAAIsAAcCJQAGAk3/qgIAAAYB/gAGAkz/nwH+AAYCAAAGAiX/iwIAAAYCJQAGAiUABgIl/5QB+wAGAiUABgIl/8MCDgAGAln/YAIl/3QCL/+IAiUABgIlAAYCAAAGAgMABgIFAAYB/gAGAjb/qgH7AAYCMf+PAiUABgIl/4gCFP+2Ajb/nAJJ/8ICJQAGAiX/nwIl//MCJf+UAiX/uAIlAAYDKAAGAiwABgIl/78Cqv//AiX/rQIl/8kCpgAFAjIABgIoAAYAAP3vAAD98wFA/1cDJ/+QAAH98AAy/eICJf/5AAD+egAA/hYC7QAkAu0AHgLtAA4C7QAoAu0ACALtACgC7QAAAu0ADALtAAMC7QAoAu0AFQLtACgC7QABAu0AFALtADACygAfAvIAIQI6AAADAQBqAwEAHwMBAEgDAQB5AxkAUwMZAFMApv9ZAKH/+AAA/ND87gAAsAAssAATS7AqUFiwSnZZsAAjPxiwBitYPVlLsCpQWH1ZINSwARMuGC2wASwgINovsAcrXFggIEcjRmFqIFggZGI4GyEhWRshWS2wAixLUlhFI1khLbADLGkYILBAUFghsEBZLbAELLAGK1ghIyF6WN0bzVkbS1JYWP0b7VkbIbAFK1iwRnZZWN0bzVlZWRgtsAUsDVxaLbAGLLEiAYhQWLAgiFxcG7AAWS2wByyxJAGIUFiwQIhcXBuwAFktsAgsEhEgOS8tAAC4Af+FAEuwCFBYsQEBjlmxRgYrWCGwEFlLsBRSWCGwgFkdsAYrXFgAsAMgRbADK0SwBSBFsgMyAiuwAytEsAQgRbIF+QIrsAMrRLAGIEWyA08CK7ADK0SwByBFugAGf/8AAiuwAytEsAggRbIHLwIrsAMrRLAJIEWyCCYCK7ADK0SwCiBFsgkiAiuwAytEsAsgRbIKHAIrsAMrRAGwDCBFsAMrRLAPIEWyDE0CK7EDRnYrRLAOIEWyDxICK7EDRnYrRLANIEW6AA4BIwACK7EDRnYrRLAQIEW6AAx//wACK7EDRnYrRLARIEW6ABB//wACK7EDRnYrRLASIEWyEVgCK7EDRnYrRLATIEWyEicCK7EDRnYrRLAUIEW6ABN//wACK7EDRnYrRLAVIEWyFBwCK7EDRnYrRLAWIEWyFRsCK7EDRnYrRFkAAAAAKgAqACoAKgBOAHQApADyAVgBugHQAfYCHAKCApYCuALEAtoC6gMUAzQDYgOiA74D+AQsBEQEkgTIBO4FHgUyBUYFWAWYBe4GIAZuBpwG1gcSB0gHigfKB+wIFghiCJIIzAkACTIJbgmyCfQKNgpeCpYKxgsOC14LmgvEC9wL6gwADBIMIAw0DIgMwAzyDTANXg2QDfoOOA5oDp4O6g8OD2QPoA/GEAYQPBBwELQQ4BEUEUQRjhHWEhQSOBJqEngSrBLOEugS9hMEEywTUhOWE9oUSBSiFN4VahW2FfoWghbkF0YXohgsGKoZIBmMGfIamhsAG14bpBviHFQckhzaHSIdmh3oHlgeph8GHzgfriAYIJghJCHYIpAi+COeJBQktiYmJsgngCguKOopeCoUKuIrdCwYLLQtci36LqgvPi/4MKQxMjHSMhAyWjKEMtozLjOEM+I0MDSINOY0+jUaNco2aDa0N3Y3yjgkOIQ5CjloOeY6qDuSPFQ9Jj2APc4+AD6GPtA/Dj+GP+RANECIQPBBZkHUQjpChEMmQ4xD7kQ0RHJE6EUeRWBFqEYURlpGuEb8R1xHvkggSKJI0EkcSVBJlEmsSdBKNEpSSnpKoEroSx5LQEt0S4xL0EwYTF5MlkzQTQxNRk2OTdROOE6aTt5PNk+2UDBQglEEUZZSBlJcUnRSnFK0UthTWFPEVChUqFTyVYJWElcSWFJY9FnyWkhablqSWpJaklqSWpJatFsuW/5cxF1uXmZfJF/WYMBhmmJkYy5kDGTOZaxmiGdaaG5pPmoMasZrgGxebQxtyG6Gb2hwKHEGcb5yinNudER1NnX8dvR32njWeah6RnsCe7p8cn1Eff5+tH+YgFyBIIFwgcCCDoKEgtKDMIPCg9aEOIRyhMaFIoWChfqGVobIh0aHnofwiGCIzIlaieaKTor8izaLqIv0jFyMkoyyjMqM4IzujSaNjI3uAAIAIQAAASoCmgADAAcALrEBAC88sgcEF+0ysQYF3DyyAwIX7TIAsQMALzyyBQQX7TKyBwYY/DyyAQIX7TIzESERJzMRIyEBCejHxwKa/WYhAlgAAAIAgv/3AO0CpAAMABUAADcjJy4BNTQ2MzIVFAcCMhYUBiMiJjS9DQkDIhwZNQJJLCAfGBYesGwg+yAjKlANGP4zIC4dHS4AAAIATQGvAUsCpAALABcAAAEjJjU0NjMyFhUUDwEjJjU0NjMyFhUUBwErFSAZEhEZGbAVIBkSERkZAa+pIxEYGBAudyipIxEYGBAudwACAAUAAAHwApYAGwAfAAABFSMHIzcjByM3IzUzNyM1MzczBzM3MwczFSMHJyMHMwHXbR86H4YhOiFrdBVudh86H4UeOh5haRQmhRWGAQ832NjY2DeGN8rKyso3hoaGAAADACz/qQHJAtcAKAAuADQAABM0Njc1MxUWFxUjLgEnFR4HFRQGBxUjNS4BJzUzHgEXES4BFzUGFRQWFxE2NTQmNFpYInEwDw1GPwo5EywQHAoJYWAiP04tDxJSR2NPsWcpYXIsAgBATgo/Pw0ob0BBCPYGIAwdEiAdKBdLTxBXVwIVHIJPSQEBGjdXLeYVUiMynP73FmAuOQAAAAAEAD3/8wMEAqQADQAbADUARQAAATIWFRQGBwYjIiY1NDYTMj4BNTQmIyIOARUUFgEyHgEzMjY3MwEjAQYjIicWFRQGIyImNTQ2FyIGFRQWMzI2NTQnLgMCnTE2LigzQjZChwIrTCkpHS5KHyL+6x8mLykvRSgn/n8wAV80OicaB3dWMUWHVjBhIRo9ZgkUGwkIAXM4MzpyKTNHOl6U/qtMaTAhL2htIBwkAngbGx4m/U8Cch4KGRNdp0s0X5cer0kbIo9VEx4FDQgGAAADACr/8wLuAqQALQA4AEIAAAAUBgcWFzY1NCYnNTMVDgEPAQYHFjMyNjcXDgEjIicOASMiJjU0PwEmNTQ2MzIDPgE1NCYjIgYVFAcOARUUFjMyNyYB60ZVPEhbGyXYJiYSGig8RVUbJxgPFlEwUlg4Yz1QW50mI1pAO149OC0jIisdQTVMNTtSSgJgak4odVl5PRcSBBUVBRcfLkVRXBceCzU8WzEqUUd+WhZhMEBa/vwhRColMC4kPp0qSTA6U0FbAAAAAQAwAa8AhQKkAAsAABMjJjU0NjMyFhUUB2UVIBkSERkZAa+pIxEYGBAudwABADD/TwEwAqQAFwAABS4ENTQ+AzcXDgMVFB4CFwEkGjhHNiUlMk0yIQksNjIWFjI2LLEQLVBbgEVIgVVPJhUQJDpdgVldhl88IgAAAQAd/08BHQKkABYAABMeBBUUDgMHJz4DNTQuAScpGjhHNiUlMk0yIQksNjIWLEI8AqQQLVBbgEVIgVVPJhUQIzxdgVh5olYvAAAAAAEARQEJAbACpABLAAATNDY9ASc1DgMjIjU0PgE3Jy4CNTQ2MzIeARc1NCY1NDYyFhUUBh0BPgIzMhYVFA4CBx4DFRQGIyIuAScVFBYVFAYjIibWHAEmMQwUESQlWScLFlIyFA4UFzMsGRQeFRscOCYNEBQXJUUiHkQnGxMQFCUyHhwXEA4VAS4OUhoIEQcYLRcNIxgPFhgHDxIUGA8VGjcYDSJgDA8UFRANVx0aEDglFhESEgYXExMUBRMUERQnNwwHMlgGEBgWAAAAAAEAHgAAAhYB+gALAAATNTMVMxUjFSM1IzX5QtvbQtsBHtzcQtzcQgABADj/cwDDAGYAFAAAFzAHBiMiJjU0NjMyFhUUBgcnNjU0jggJChsgIRoiLj4yCVICAgIbGBkgMyYtVBkTODIOAAAAAAEAJwDCAR0BAQADAAATMxUjJ/b2AQE/AAEARv/1ALUAZAALAAA3MhYVFAYjIiY1NDZ9FyEiFxYgIWQiFxYgIBYXIgAAAf/3//IBHwKkAAMAAAEDIxMBH+RE5QKk/U4CsgAAAAACABj/8gHcAqQAEAAYAAAXIi4DNTQ2NzYzMhYVFAYDIhEQIBE0Jvo0UjAgDDUrN09ifHxlgwEEQg4wSmFYK1ieKjTAmpjAApj+vf7FATydpQAAAAABAG8AAAGKAqQAEQAAEyIHNTcXERQWFxUhNT4BNRE0txI2tAgkO/7sOCcCURYOWwL9qCQWAQ8PAh0vAcUvAAAAAQAeAAAB2wKkAB0AABM+BDMyFhUUDwEzMjY3FwchNTc2NTQmIyIGBx8FCiEpSS5Oa4Co7yIhHA03/nqygk4/ND8eAd0XIkErImZLcYawFysFiQy9ino/TjlJAAABACv/8gGwAqQALAAANzIWMzI2NTQmJyYjNT4DNTQmIyIGByc+ATMyFhUUBgceARUUBiMiJjU0NlEXWyI3TDYvHU0iMTYdPTEvRx8PGllSRVcqNDxEnXc0PRVOOFY/NE8RCw0MFyc4JC88MzcER1tMPSk+IxpRS2p/HxoQEwAAAAIADAAAAdgCpAAKAA0AACUVIxUjNSE1ATMRIxEDAdhmTf7nATosTvDnQKenQAG9/kMBV/6pAAAAAQAg//IBtgKwACgAACU0LgMjIjU0PwEzMjY3FwcGKwEHHgEVFA4DIyImNTQzMhYzMjYBZS9DUz0XDAFt0RATCwkmBBPEKpKOJTlLRSM4QiscWRo7UMI1Ui0dCQgDAu0KEAdZCVUadXE7XDgkDSEdJT5jAAACACL/8gHUAqwAFAAgAAAFIiY1NDc+ATcXDgEHPgEzMhYVFAYDIgYVFBYzMjY1NCYBAmV7pzdpVQJ2nhQvMCFWZnVtRC9MQjU4Rw6hhM53JiMHEBOZcRwRcWBnggGMPDt0hVlSXmcAAAEAFP/4AcEClgAKAAABFQMjEyMiBgcnNwHB1EHG2SkrIBE7ApYQ/XICVB4zCJMAAAMAOP/yAb0CpAAXACQANQAAAR4BFRQGIyImNTQ2Ny4BNTQ2MzIWFRQGBycOARUUFjMyNjU0Jic+BjU0JiMiBhUUFgEiWkFrWlVrMVFPLWxSS2E8XDwsJEY5MT0qQgEZCxcMDwc8My89OgFzQ1s6TVxcSTNFPUVEMURaUD4zSI8sI0YwQVA9MSo/oAERChUTGyESND04KypKAAAAAAIAHv/qAcsCpAAUACMAABcnPgE3BiMiJjU0NjMyFhUUBgcOARMyPgE9ATQjIg4CFRQWOwNynx9KTFFjd1lhfFdOM2RnEzAxhB8sFwpDFhQUmno5cFtlh558ZbE5JSIBJAslGyf+ITY4H1lpAAIAUf/1AMABywALABcAABMyFhUUBiMiJjU0NhMyFhUUBiMiJjU0NogXISIXFiAhFhchIhcWICEByyIXFiAgFhci/pkiFxYgIBYXIgACAFD/cwDbAcsAEwAfAAAXMAcGIyImNDYzMhYVFAYHJzY1NAMyFhUUBiMiJjU0NqYICQocHyEaIi48NAlSLBchIhcWICECAgIbMh8zJi1UGRM4Mg4BzSIXFiAgFhciAAABABz/9gIYAgQABgAABSU1JRUNAQIY/gQB/P5cAaQK5kLmSL+/AAAAAgAeAHgCFgGCAAMABwAAARUhNQUVITUCFv4IAfj+CAGCQkLIQkIAAAEAHP/2AhgCBAAGAAAXNS0BNQUVHAGk/lwB/ApIv79I5kIAAAIARP/4AZ4CpAAeACoAABMyFhUUDgEHBgcjNj8BNjU0JiMiBhUUFhUUBiMiNDYTMhYVFAYjIiY1NDbmTGwiIiUyDxECHBwlPy0lOiYXEjBdTBYgHxgWHh4CpFVGJEsvLUBaTUFAVUcyRiYYCTAREhd8U/2/IRYXHR0WGCAAAAAAAgB0//IDKQKkADIAPgAAASIGFRQWMzI3Fw4BIyImNTQ2MzIWFRQGIyImJwYjIiY1NDYzMhYXNzMHBhUUMzI2NTQmBzQmIyIGFRQWMzI2AeJ+nKSAWWsMSVs1m9TVmIu9bkokMwQ2RyUzcVMaHREKRUECJzFQqRwaEilfIBwwSAKFu4eCqDAeIRjEkJLMqXxhjyghSEEvWqIVHyb+DAYuektwoO8fG29RJy6WAAIADwAAAsICogAaAB0AACUVIzU+ATU0LwEhBwYVFDMVIzU+ATcTMxMeASUzAwLC/ygeEyn++i4IRMYlKS7QFPkaHv4453QTExMBDhMYK2B1FRIpExMCNW0B6/3IOB3sARMAAAADABEAAAJRApYAHQAqADUAACUUDgIjITU+ATURNCYnNSEgFRQOAwceBAUyPgI1NCcmIxUUFgMzMjY1NCYrASIVAlEWM2RF/rI+IiE/ARgBBhgZNA4WFxc9Ih7+xS1COh9UMIMaGl9GTVpWLBa0Hjo5IxMCIDgBvDcfBBOqITMbGAMGBgYcIDu0DB06K1wjFPgYEQFJPztDRh4AAAABABz/8gJ5AqQAHQAAEzQ2MzIWMzI3MxcjJiMiBhUUHgIzMjcXDgEjIiYcwI9AcAkhCRUJFzegbYEtTFEudmkSLo5VlLgBRZzDISHiuqKIV39BHWUSPUK6AAAAAAIAEAAAAq0ClgAWACUAAAEUDgMjITU+ATURNCYnNSEyHgMHNC4DIyIGFREUFjMgAq0UNlSLWP7kOR8dOwEOW49XORVtESxGckkfFRYeAT4BTi1WXEMsEwQgNgG8Nx4FEylBWVY2K09ROiUQF/4EGBEAAAAAAQAMAAACVQKWACkAADcUFjsBMjY3MwchNT4BNRE0Jic1IRcjLgErASIGHQEzMjY3MxUjLgErAckhSBtrXCUcLf3kNyAgNwITAxkNNlWHFA2aQCUJFxcKJj6aUB0OM1GpEwQhNQG8NSEEE49DJg0V3ic46D4iAAAAAQAMAAACIgKWACQAACUjLgErARUUFhcVITU+ATURNCYnNSEXIy4BKwEiBh0BMzI2NzMB3xcJKD2RITr+6DkeIDcCEwMZDTZViBQMkTwpCRfnPSPaNiEDExMEIz4BsTUhBBOPQyYNFd4jPAAAAQAg//ICxQKkAC4AAAEiDgMVFBYzMjY9ATQmJzUzFQ4BHQEOASMiLgM1NDYzMhYzMjY3MxcjLgEBiDlYNiINjXo2Tx85/ywaHp1ALVpiSjHBljpxCRAYBRYIFyBgAnwpQFZPKIuhIxiiOB0EEhIEHSzLGiwSMEl6TpnGIRMO005dAAAAAQATAAACvgKWACsAABMhNTQmJzUhFQ4BFREUFhcVITU+AT0BIRUUFhcVITU+ATURNCYnNSEVDgEV0QEvHjoBFjoeIDj+6joe/tEgOP7qOh4eOgEWOh4BZ8I2HwUTEwUfNv5ENSAFExMEIj/DzjUgBRMTBCI/AbE2HwUTEwUfNgABABIAAAE7ApYAEwAANxE0Jic1IRUOARURFBYXFSE1PgFzIUABKT8jJD7+1z8ibQG8Nx8EExMDIDf+RDchAhMTAiAAAAEACv/yAXIClgAaAAA3Mh4CMzI1ETQmJzUhFQ4BFREUBiMiJjU0NjsVGAgODCYgPQEfPR9YUSs4HWwaIBpCAc83HwQTEwQfN/6OXmcoIBQeAAAAAAEAIgAAAtMClgAzAAABNSEVDgEPARceARcVITU2MzY1NCYvAQcVFBYXFSE1PgE1ETQmJzUhFQ4BHQE3NjU0JiciAZ0BBjQzMb7pRDIn/s8MECpJOWoaIDr+5jsfIDoBHD0fsU4TFw4CgxMTBBkxvPpJIgETEwECFBJgOGkVuzcfBBMTBCM+AbE1IQQTEwQfN82hSB8PDQIAAQAMAAACVgKWACAAACUyPgg1MwchNT4BNRE0Jic1IRUOARURFBYzAWIaLSIgFBULDgQMGTD95jcgIDcBGjsiHzcnBgcQChcKGwgbAa4TBCE1Abw1IQQTEwQhNf4nGg8AAQAMAAADXwKWACQAAAkBIwMRFBYXFSM1PgE1ETQmJzUzGwEzFQ4BFREUFhcVITU+ATUCov8ADvskOus+IyE+xufdxzgfIDf+6DsgAj39wwIm/m1MMQMTEwQvTQGWNx8EE/4HAfkTBSA1/kQ1IQQTEwQjPgABAAz/9QLDApYAHwAAAQ4DFREjAREUFhcVIzU+ATURLgEjNTMBETQmJzUzAsMcGx0LEf5GJDrrPiMeIyCrAYElO+sCgwMHGDMr/fICJv54TDEDExMEL00BuSMUE/4cAVFMLwUTAAAAAAIAIv/yArACpAALACEAAAUiJjU0NjMyFhUUBhM0LgMjIg4DFRQeAjMyPgIBY4e6uI+Qt7lHHCs6Nx0xTi0eCxQsVzw9WS0UDr+al8LBnJe+AVZIcEQsECo/Vk8pM2JiPDxiYgACABAAAAIeApYAHAAnAAABFA4DIyInFRQWFxUhNT4BNRE0Jic1ITIeAiURFjMyNTQmIyIGAh4MIzpkQh4nIzv+6DkbHDgBCC1UUzL+rCIXrmBlFA4B4RowNicaA7Y3IQITEwUhPwGxNh4GExEmSjr+/AOTTUkOAAACACL/TgK9AqQAGQAtAAABFA4FBx4BFxUjBiMiLwEuATU0NiAWBzQmIyIOAxUUHgIzMj4DArAYJTMwNiEPPYBWBgsY82kvZIO4AR64cnNfMk4uHwsoQkUmMU0uHgsBSzdgQTYgGAkDT0cCEgF6NyKgiJjDwpWMpyk/VE0oWIFCHilAVVAAAAAAAgARAAACkwKWACAAKgAAATIeAhUUBwYjFx4BFxUjAwcVFBYXFSE1PgE1ETQmJzUTPgE1NCYjIgYVASUrTlMysAMCzhUlHaHuOCA6/us4HR04u3lxWV4eFQKWDiNKNYUhAf0ZFAITATQCxTYgBBMTBCM+AbE1HwYT/sECPFNGQw8VAAAAAQAq//IB6wKkAC4AACU0LgM1NDYzMhYzMjczFyMuASMiBhUUFhceARUUBiMiJiMiBgcjJzMeATMyNgGGQl5dQmdGKmIKGgYVFhkYYzwuOkRPY1h2VjJnDAsQARYeFyViQzdEhy9JNDdWOUxfIiLVWFQ0KShILDVnPk1pIhMO1FtWPwAAAQARAAACUQKWABcAABMjIgYHIzchFyMuASsBERQWFxUhNT4BNf42VDkSGAYCNAYYETlVNiI+/tw+IAJsLlKqqlMt/gE3IAMTEwQhQAAAAAABAA7/8gLBApYAJQAANxQWMzI2NzY1ETQmJzUzFQ4BFREUDgIjIiY1ETQmJzUhFQ4BFc5LYThaFBckOug+IBU1aU2Aex87ARs8H+lkZyskK10BDksxBBMTCCxM/vs7WE8qfYIBODceBRMTBR82AAAAAQAQ//UCuQKWABsAAAEVDgEHAyMDLgEnNSEVBiMGFRQXGwE2NTQmJzUCuSUfGN4P9h0nJgEKHAEuKZeTEyApApYTAiA7/c8CJUIlAhMTAgMcFFv+rgFvMBkVEwITAAAAAQAF//UDpAKWACwAAAEVDgEHDgEHIwsBIwMuASc1MxUOARUUFxsBJy4BJzUhFQYVFBcbATY1NCYnNQOkKB0OUzo/D6CaD8EZJyf1JhsLkGshGCIrAQtNG4R9EB8mApYTCBgn36bCAaf+WQIZRisEExMCDRESHP6IARpSPB0BExMBIgtG/qsBUy0eFBMEEwAAAQAKAAACwAKWADQAAAEVDgEPARMeARcVITU2MzY1NC8BBwYVFBYXFSM1PgE/AScuASc1IRUHBhUUHwE3NjU0Jic1ArgxNS+SwB4oKf7XGAMyM193NCAs6ScqQJ1tRj0yAS4cMFUqcSgdKQKWEwMhOrb+7isaBRMTAgEcFkyMlEEVEQ8DExMDIE/BoGc0AhMTAQEcIHY7ijITEQ0CEwAAAAEAFgAAAr8ClgAmAAABFQ4BDwEVFBYXFSE1PgE9AScuAic1IRUGIwYVFB8BNzY1NCYnNQK/JzgrlCRD/s5EIYMyNCIaARgLDi4RlI8OHicClhMDLULiwjkfAhMTAyFBrsBJPxQBExMBAxoTGd7iFw4SDgETAAAAAAEACQAAAlUClgAYAAAlMj4FNzMHITUBIyIOAgcjNyEVAQGSHC4fGw0QBAcXGP3MAbXdM0IhDAYaFAIO/lAmCgocDikMF7APAmEULCQhqw/9nwAAAAEAWP9kASsClgAMAAAFFSMRMxUjIgYVERQzASvT01oYFTGDGQMyGRUX/WA0AAAAAAH/9//yAR8CpAADAAADMxMjCUPlRAKk/U4AAAABACL/ZAD1ApYACwAAFzMyNRE0KwE1MxEjIlotMVbT04MsAqA0GfzOAAAAAAEAGAEpAb4ClgAGAAATIxMzEyMDXES1PLVEjwEpAW3+kwEhAAEAAP+DAfT/tQADAAAFITUhAfT+DAH0fTIAAAABABMB+wDyAqYACAAAEyMnJjU0MzIX8iiaHSITFwH7YRMYHxcAAAAAAgAl//YBugHMADAAPQAANzQ+Bzc1NCMiBhUUFhUUBiMiJjU0NjMyFh0BFBYzMjcVDgEjIiYnBiMiJjc1DgEdARQWMzI3PgElCBQSJBgxGjgNTB4qBRsSERphR1I+DREVFxonGR8dBFY8Ljv6WkglGiInEAphEh8bFhcPFQsWBT1THRQIHAQRGRoRL0FPUcMhGRMaHRUiJ0k8SZEhPiwEIC0XCRQAAAAAAgAD//YB1AKrABcAJAAAExE+ATMyFhUUBiMiJjURNCYjIgc1NzY3ExUUFjMyNjU0JiMiBpkPTi5LZYdjPGkTHgsGHUMxBTonPkRFOyc8Aqn+ziUwfVxrkikXAgcfFAEQCBMR/pf8Ex1cU1ttLgAAAAEAGf/2AZwBzAAhAAA3NDYzMhYVFAYjIi8BLgEjIgYVFBYzMjY3Fw4EIyImGYNYP1scFCINBggYGz1LV0QrPiQOCg8nJj4hU2vVb4g9KhEZLhYcFGFNVm0qNAkUGjQgG30AAAACABv/9gHrAqsAHQAqAAAFJzUGIyImNTQ2MzIXNTQmIyIHNTY3FxEUFjMyNxUjMjY9ATQmIyIGFRQWAVgEMFBTZnpWNjMSHA4IWzgFERsFEvAmMz4oOURMCgNAQ3dgapUrnB8UARAYFAL9ySMWARArEeYoPGZVXG8AAAAAAgAZ//YBqAHMABUAGwAAEx4DMzI2NxcOASMiJjU0NjMyFhclMy4BIyJhAh4wMRswRyQQH25HVmV1XExUC/7OzAoqLloBFT9bLRMwOQdRVnlnb4dbXCBBMgAAAAABABQAAAF/AqsAIQAAASIuAiMiHQEzFSMRFBYXFSE1PgE1ESM1Mz4BMzIWFRQGAVUQFw0bEzl7eiA9/vw2HVJSAlBeLTsZAkQXHRdZdCD+xjgfAg8PAx83ATogcHkjGxEYAAAAAwAc/yYB1gHMAC8APQBMAAA3NDY3LgE1NDYzMh8BFjsBFSMWFRQGIyInIicOARUUHwEeARUUBwYjIiY1NDY3LgEXDgEVFBYzMjY1NCYjIhM0LgIjIgYdARQWMzI2SRs+MithRigoFh0aTVMTXT8KHAESFChOgTdCN1R5R2YtNR8WSh8SUEJVaDhEZXkIFCwfIyc5LSMoNhMgOhk+L0RfDwgKJyspRVYDAgYqDxgDBgI6LzgtRDkoHDcnDxYoJSARISg2KxsWARgRMD0pMiwDSFkxAAABAAkAAAHnAqsAKgAAExUUFhcVIzU+ATURNCYjIgc1NzY3FxE+ATMyHQEUFhcVIzU+AT0BNCMiBp0YLNgrFRMgCAQbRC8FI0QsexMp1CsZSx0zAVfxNB4FDw8GHDUB1yATARAIExED/tAtJ5/HNBsIDw8EIDPGah0AAAAAAgAQAAAA/QKrABIAHQAAEyIHNTcXERQWFxUjNT4BPQE0JhMyFhUUBiMiJjQ2PgwemwQZMe00Gw8wFh4eFhUdHgGKBA83A/6dNh0EDw8DHjboIRsBIR4VFh0eKh4AAAAAAv+6/yYAwgKrABgAIwAAEzAHNTY3FxEUBiMiJjU0NjMyFjMyNjURNBMyFhUUBiMiJjQ2TS1QTAVWUyk1GBEVMhMeEiEWHh4WFR0eAYoEEBkdA/43anAfGBAXPD5NAXs8ASEeFRYdHioeAAAAAQAHAAAB+QKrADQAABM1NzY3FxE3NjU0Jic1MxUjIg4BDwEXHgEXFSM1MzI1NC4BLwEVFBYfARUjNT4BNRE0JiMiBx5XJgSJFxUdzAgaJVM+HZkfNSHaExUGDQKMGR4U6jYVEhkGAm8QCBgMAv5cehQOCggBDg8LOTobwiYhAg8PDwQLDwS7uBoYAQEPDwkTJwHiJBkAAAABABMAAAEBAqsAFAAAEzU2NxcRFBYXFSM1PgE1ETQmIyIHE2M8BBsw7C8eEhgJFgJvEBgUAv2rKRkDDw8EHCgB3SMaAgAAAAABABAAAAMHAcwAPwAAEzU2NxcVPgEzMhc2MzIdARQfARUjNT4BPQE0JiMiBxUUFhcVIzU+AT0BNCMiBgcGFREUFhcVIzU+AT0BNCYjIhNIRAc+PCFPG05VdCsa2ywWICtCKx0r4CsZQho6Ew8cKN4qHA8UDwGOERQZAksvHlRUss44AwIPDwUYK9M+MD38LyABDw8DGirZaRcRDwT+5h0VAg8PARwp/SQcAAAAAQAQAAAB5QHMACoAABM1NjcXFT4BMzIWHQEUFhcVIzU+AT0BNCMiBgcRFBYXFSM1PgE9ATQmIyIQSkAHMzsjNz8YJdAmGUkZKiQcJtQmGBAVEwGOERYXAk8wIU9H5SUZBA8PAyIv0WEXIv7nGxYDDw8DHSv4JRsAAgAd//YB1gHMAAoAFgAAEzIWFRQGIiY1NDYHFBYzMjY1NCYjIgb6YHyAvH17IU4/OEBQPzVBAcyAYmiMh2Vng7lrlmBVZ4JWAAAAAAIABf8nAdYBzAAfACwAABM1NjcXFTYzMhYVFAYjIiYnFRQWFxUjNT4BNRE0JiMiFxUUFjMyNjU0JiMiBgk/UQZAUEleeVkgKxogOPIsGhAZEI1FIzZDQzgjQwGJEBMgAk1PeF1tlBIZnS8bARIRBBonAdQjFjz2FixnU1dpLAAAAgAY/ycB6AHNABYAJAAAATczERQWFxUjNT4BPQEGIyImNTQ2MzIHFB4BMzI3Nj0BNCMiBgFoNgsWKew1JERZRlp/Xja9FD4xMiQOXUBKAakg/aogFQkOEQQeKrxKeF9slNwrUEMfCyLOZGcAAQAFAAABTwHMACMAAAEiJiMiBh0BFBYXFSM1PgE9ATQmIyIHNTY3FxU+ATMyFhUUBgEoDywHFDIiM/AwFw8TEBNQRAUlNB8aHRUBaiM6GOErHgIPDwkWJvohGwQQGhwCXDUpHBkVGAABADP/9gFcAcsALgAAATI3MxcjLgEjIgYVFB8BHgEVFAYjIiYjIgcjNTMWFxYzMjY1NC8BLgE1NDYzMhYBHAEPCwQPETIrIisrbC0nUzoZVAoPCA0QEBYgOSYtNDpANU07IToBuAqIQzgmHisZQBs3JTRMEgycRh0pJyEtHiEkQio2RRMAAAAAAQAN//YBFwJDABwAABMVIxEUFjMyNxcGIyI1ESMmNTQ+Azc2NzIdAf9lGBwfHQ0wSFk1BAYRGCoZEAQHAcIg/uIvKyMLTH8BLQMEBQYLEzIjFwUNdAAAAAEACf/2Ad8BwgAjAAAlFQYHJzUHBiMiJj0BNCYnNTMRFBYzMjc2PQE0Jic1MxEUFjMB3047BCsrPDdCGiSSKh0uKhMbL54XIjIOFRgCUysrRzv8IxsCDv66Hy0iDybrJRgCEf6pIxYAAQAT//IB3QHCABwAAAEVDgEHAwYjIicDLgEnNTMVDgEVFBcbATY1NCc1Ad0XFhSAFAgLD3gjHBzEGhQJZmMGLwHCDwIaMv6/Mi8BH04kAQ8PAgwOEBX/AAEDDw8cBA8AAQAV//ICtgHCAC8AAAEuASc1MxUOARUUHwE3NjU0Jic1MxUOAQcDBiIvAQcGIyInAy4BJzUzFQ4BFRQXEwFfGx0hyyMXEVRaERIaew8NDYoUEBBbcBIHCBKHERMRtBoUBmMBNlApBA8PBQ4RDS7g3ioYDQ0FDw8FEh/+pjEr7PAnLAFWJhgBDw8DDhAPD/77AAAAAAEAEQAAAd8BwgAyAAATPgM1NCc1MxUGDwEXHgEzFSM1PgE1NC8BBwYVFDMVIzU+AT8BJy4BKwE1MxUGFRQX+AseDwsonjMeU4AYIxfJGQ8GW08UKJEZGxVyXhcgFgnPKy4BMBEpFRYIEwMPDwIqeMQiGg8PAgkNCQmMex8KEg8PAhIfpZAjGQ8PARUURQAAAAEADv8mAdsBwgApAAAXMjY1NC8CJic1MxUOARUUHwETNjU0IzUzFQ4BBwMOASMiJjU0NjMyFpccPiwPdQwnziAaCnNhBDCHEhUJmihPMiAqGhEWMoZ8HRZPHv4aBg4PAQwODxf9ARQJCBkPDwITGP5obFwhGRIaEgAAAAABABsAAAGiAcIAEwAAJTI+AjcXByE1ASMiBgcjNyEVAQEQJy4aCgcSDv6HAQqKLSAHEgMBW/7zHgoiHiMEhw8BlSE3dg/+awABAGT/SwFeAqgAHwAABS4BPQE0Jic+AT0BND4CNxUOAR0BFAYHHgEdARQWFwFeWE0jMjIjFC43LDQnJjg4Jic0tQI2VLE0LhAPLjSyLDcdCgELDTY4qD40Dw40Pqg4Ng0AAAAAAQBD//IAhQKkAAMAABcRMxFDQg4Csv1OAAAAAAEAgv9LAXwCqAAiAAATMh4DHQEUFhcOAR0BFA4CBzU+AT0BNDY3LgE9ATQmJ4IhKi4bESMyMiMULjcsNCcmODgmJzQCqAUQHTUlsTQuDxAuNLIsNx0KAQsNNjioPjQPDjM/qDg2DQAAAQAoALoB9gFAABMAABMyFjMyNjcXDgEjIiYjIgYHJz4BozJ+IxcfFDYiNiMyfiMXHxQ2IjYBQEQWHSAwJUQWHSAwJQABAC8ANwG0AboACwAAExc3FwcXBycHJzcnWpeLL4yVLZWNL42UAbaPkyuUjS+NlSyVjgAAAQATANcCHAEoAAMAABMhFSETAgn99wEoUQAAAAEAAADXAooBKAADAAARIRUhAor9dgEoUQAAAAABAO0B5QGrAw0AFAAAARYXFgcGJyYnJicmNzYXFSYHBgczAUwxFxcdGzEyFgoBAio0RyMZEgQMAnYEJCQjIgEDJA4UUkBMESwFKBscAAAAAAEAgwHaAUEDAwAUAAATJicmNzYXFhcWFxYHBic1Fjc2NyPjMhcXHR0vMRgKAQEqNEcjGRMDCwJxBCQkIyMDASYNFE5ETBEsBSgZHgACAGQB5QIWAw0AEwAnAAATFhcWBwYnJicmJyY3NhcVJgcGByEWFxYHBicmJyYnJjc2FxUmBwYHwzEXFx0bMTIWCgECKjRHIxkSBAEAMRcXHBwxMhYKAQIqNEchGhMEAnYEJCQjIgEDJA4UUkBMESwFKBscBCQmISIBAyQOFFJATBEsBCcbHAAAAgAYAdYBywL/ABMAJwAAASYnJjc2FxYXFhcWBwYnNRY3NjchJicmNzYXFhcWFxYHBic1Fjc2NwFsMhcXHR0vMhcKAQIqNEchGhME/wAyFxcdHS8yFwoBASo0RyMZEwMCbQQkJCMjAwIkDhRSQEwRLAQnGxwEJCQjIwMCJA4UTkRMESwFKBkeAAH/9//4A0UBxwBPAAAlBiMiJy4BJyY1NDMyFjMyNzY1NCcmIyIHBgcVFAcGIyImNTQ3NjcmNyYnJiMiBwYVFB4CFxYVFAYjIicmJyY1NDc2MzIXNjc2MzIXFhUUAwNIWRMXBAcFGiMHIgk8Mi4uL0BELicGbgkKDxQRRgYBAQUtLz5BLy8MFh8UDxQPCAw2Hx9DQ2B+Qw8RRlxeQ0Q9RQQBAQEIGiUJMi5DRC4vMSZDDpREBBYOEQstZAoMQC0vMjJDFSwqJA0LEQ8WBSY8OzthRkZtFhNFQ0RgYQAC//z/9QG5Ab8AMAA+AAAlFAcGIyInJjU0NzYzMhcWFzY1NCcmIyIPASciNzYVBiMiJjU0NyY1Nz4BNzYzMhcWByYnJiMiBzAXFhcWMzIBuUREXoFGEAVDTyQnTCchLi5DSjIOAQECAwsTDxgFAQECCghGbWJCQqMUPSMUJyUKAwsxSB3bXkREcB8lEAVBDhxYKjRCLS47EAICAwQVFg8ICAECAQINClJCQvBMFwskDgQNOgAB//f/9QHAAb4AKAAAJRQHBiMiJjU0NzY1NCcmIyIHBhUUFxYVFAYjIicmNTQ+Ajc2MzIXFgHAUQsPDhcLPioyQz8sMUAKFw4NDVILEhoPQFxfST/Zf1oLFg4OC0NkSScuKS1IZ0AKDw8VC1mAFi8tKA88QTsAAAAB/+//9QM8AcUAYQAABSInLgEnBiMiJyYnJjc2NzY3Njc0JyYHBhUUIyI1NDc2MzIXFgcGBw4BBw4BBwYWBxYXFjMyNzY3JjcmJyY1NDYzMhcWHQEWFxYzMjc2NTQnJiMmBgciNTQ2MzIXFhUUBwYCWV1GCBAHRH5LQEAPCAQIFg4eOgEWDw8SIyQhIiccET8DATMFGhYFCgQMBAMKLy8yQC0tBQICBEgRFBAGDG4GJzFAPy8vMC88CRoPIi8jWkdCREMLRgoUC28zNEMQHi4NEQoYHg4MBwECCiQkIRcXCiE7NSYDDAgCBwUUFggnISAuLUEMCWMuDBEOFgVElA5CJTEvL0JCMDACCAIlFxNHQmBgREMAAAAAAf/3//UBpwG/ADUAAAEUKwE9ARYVFAYjIicmNycmIyIHBhUUFxYzMjc2NzYzMhUUBwYjIicmNTQ3NjMyFxYfBAGlAwEGFw8VCgICCTBMRC4tLzE/VCUFBQkZJBU6el5EREJCYjQ2LRwLBgEDAVMCAgELCA8WFQECCzkuLUFAMS80BQkaIhAfUkRGXV9CQhoYHw0JAgMAAAAD//j/9QHAAb8ADwAfACsAACUUBwYjIicmNTQ3NjMyFxYHNjU0JyYjIgcWFRQHFjMyJzQnFA8BDgEHBhc2AcBFQ15cQ0NCQWFhQUJ1Li0rQhgWRUcTFkJrRAEEDhICCDJB2V5DQ0NDYmFBQEREyitBQS8uBjtbWz4Gn0cpAgEHGTAYSC8sAAAAA//z//UDNAHBAD0AUABdAAAlFAcGIyInBiMiJy4BJyY3NjMyFxYXNjcuATU0NjcmJyYjIg8BFgYjIiY1NDcmMzU+ATc2NzYzMhc2MzIXFgc0JyYjIgYHFhUUBxYXFjMyNzYFJicmIyIHHgEXFjMyAzREQ197RUZ6a0YECgYWDj1GICJSIScLAQEBAQotLDpMMAwCFgkPGAQCAgEKChouNjV8RER8XkRERy8vQTxVCwMDCi0rOkEvL/4OFDgWEyEfAwcFMEkW3F5EQ2hqUQUPCCMiOgoWWiQzCA4ICA8INiMkOQsJDxYPCQcCAQEMDR8YGmNlRERdPy8vRzcRDg8PNiclLzFaPxIIFQQIBDoAAAAC//f/9wHKAcEAOgBDAAAlFAYjIicmJwYnJicmNTQ3NjMyFyYnJiMiBwYVFBcWMzI3NjMyFRQHBiMiJyY1NDc2MzIXFhcWFRQHFicmIyIHBhYXFgHKFg8PEQgNKUIsIyIiIyk2MgIZMkBCLy4vLkcxHQ0KIjkoKWRCQkNEXlhKDhAUDymEJyAdBgUdFCaDDxUUDhQaAwMhIScoGRo2KBYuLy5BQDIwGAgkIRINQ0NfXkNEPwwlKyIZHzs/LxQPFwECAAAAAwAA/pUCWQHPACUANQA/AAAFFAcGIyInJj0BBiMiJyYnJjc2NzYXFhcWBxEUFjMyNjURNDc2FQc2JyYnIgcWFxYHFjMWNzYnNicGBwYVBhc2AlkpKD4+KyohMmJAPwIBQkJhYEFAAgJKKiAfKSQj1wEuLkMXE0MCAkkSF0AwL8gCRhEQEAE1QNU/LCspKEDqDkNCY19BQQEBRERhZkf+9CAqKiACfiICAibDQC8uAgg7W1dBBwIwLkNIKQ8nJhg6NCsAAAH/9P6VAzABvwBDAAAFFhQVFCMhIiY1ETQ2MzIXFjMyNzY1NCcmIyIPARYGIyImNTQ3JjUzFSI1NzY3NjMyFxYVFAcGIyInFRQWMyEyJyYzMgMvAVf9t09KJxUNBydSQC8wLi9CTDALARYJDxgGAgICAgESRW5hQkNFRVxKOCIwAkkTBQUqH/YGDAZdTU4BIBAXBTYwMEFEKyw5CwkPFg8ICAIBAQEBARhRQkBgXkVFJeoxIiYmAAAB//T+lQMwAcMAZQAABRYUFRQjISImNRE0NjMyFxYzMjc2NyY3JicmIyIPARYGIyImNTQ3JjUzFSI1NzY3NjMyFxYdARYXFjMyNzY1NCcmIyYGByI1NDYzMhcWFRQHBiMiJy4BJwYjIicVFBYzITInJjMyAy8BV/23T0onFQ0HJ1I9Ly8EAQEELi4/TDALARYJDxgGAgICAgESRW5hQkMGJjBAQC8vLy87DBoPIS4mWUdBRERcXEYJDwdFfEo4IjACSRMFBSof9gYMBl1NTgEgEBcFNi0vPgoKPCkpOQsJDxYPCAgCAQEBAQEYUUJAYAtBJDEvL0NCLy8CCAIlFxJFQ15gRERGCBULbiXqMSImJgAAAAAB//f+qQIKAb8AXQAAASI9AQYnJicmPQE0MzIXFhcWPwE1BiMiJyY1NDc2MzIXFh8EFCMWFRQGIyInJjcnJiMiBwYVFBcWMzI3NjsBMh0BFAcOAQcGJyYnFRYXFj8BNjMyHQEzMhUUIwGCIzlIZ1gPHhAPN1k5Og84Sl5EREJCYjQ2LRwLBgEDAwUXDxUKAgIJMExELi0vMT9PLQoRDiQPHyoNLThQNT9JUx0cDw4eQCQk/qkkXRcDATcKFH4oEDsBAi0LHSRERl1fQkIaGB8NCQIDAggIDxYVAQILOS4tQUAxLzYMI3ETChYcBhcCAR4fJAECGRYMKHwlIgAB//X+qQGnAb8AWAAABRQGKwEiJyY1ND4CMzIXFhUUBiMiJyYjIgcGFRQ7ATI2PQEGIyInJjU0NzYzMhceARcUIxYVFCMiJyY3MhcWBxQvAiYjIgcGFRQXFjMyNzYzMh4CFREBpktRtS0bGBUkMx48LwsWDg4LGCErFAketTEjOkldRENBQGNrRwQKBwIHJRINAQECAQEBAQMMMEtDLi0vLkFMKgQSChYTDLxOTSsnNR4xIxMqCxANFwsYIxEaMSQw2ShERGBfQUJRBQ0IAQoMIhIDAQECAQEBBA45LS1BQDEwMgkHCw4I/vUAAAH/9P6VAdoBvwBNAAABFCsBIj0BBgcGLwEmPQE0OwEyFxYzMjc2NTQnJiMiDwEWBwYjIiY1NDciJz8BNjc2NzYzMhcWFRQHBiMiJxUWNzY3PgE3PgEzMh0BNzIB2iRkIyZPYisgFiMNEQstT0EvMC8uQ04uCgEBCxMPGAYBAgMBARAbLjY0YkJDRUZcUTEySCIoEiQSBQgFHT4k/rkkJNgWBgMcFw8RliMMNi8wQUEtLjkLAgEVFg8ICAIDAgMTHxgaQkJfXkVEKkcjAwINBgwHAQEk4wIAAAH/9P/1AcEBvwBGAAAlFAcGIyInJic8AScmNzY3Njc2NzQnJgcGFRQjIjU0PgIzMhcWBwYHDgEHDgEHBhYHFhcWMzI3NjU0JyY1NDYzMhceAwHBRUViSUBBEAEGCQsLDx86AxgLDg8jJBEdJBQoICABAzMHGRMMCwcCAwELLS8xRTAwUhAUDwYMGyseEOJhRkYxMUEDBwUiGB4IEAwZHQ4OBwUFCiQkEh8XDSAfKDYlBAsIBQ4TBAsJKiAgMTFEaTMMEQ4WBREvOD8AAAL/+P/1A6wBvwBlAH0AAAEWFRQHBiMiJyY1NDMyFjMyNzY1NCcmIyIPAQYHFAcGFRQXFh0BFAcGIyInJj0BNDc2NTQuAjUuAScmJyYjIgcGFRQXFjMyNjMyFRQOAiMiJyY1NDc2MzIXHgEXNjMyFzc2MzIBJjc2NzU0JiMiBh0BFhUUBxUUFjMyNjUDhSdiO0QlHhkiCSUKMChDGy9PNSgYCRAFEREFICArLB8fAhIGBwYECQYEHSozUC0cQy4yCSAIIQ4XHg9KQGAnQ3dJPgcKAx8/QB8SPkp3/rUYAwEUFg8PFRQUFQ8PFgFdO0Z9RCYKBxskChwtVzInRB8QCRkGCiAcJSQICB0sIyEhIi0eCQYiJwkUFBAFBQ8KBRQeQiYyUS0iCSQNEQkDK0J5RTtgKQUIAjo6DSz+uys4LiYjDxcXDyQtJzUqFxAaGhAAAAL/9//vA0oBwQA1AEcAACUGLwEiNTQ7AgcVIiY1NDc7ARYXFjc2NTQnJiMiBwYHFgcGIyInJjU0NzYzMhc2MzIXFhUUJSY3JicmIyIHBhUUFxYzMjc2AwVLbhYEBAEBAQ0aIgMDDA1ONDEwL0JDLisHCkREZF1GREFDY35CRn9gQkX+MAICCSwsO0UuLS8xQDwsLDpLCAEBAQEBHA8VBwIBBTQwQUEvLy4rMWFOTURGXmBBQWttQ0VdXUkUFzkmJiwsQ0ExLygoAAAAAAP/9//1A0ABvwAXACoAPwAAJRYHBiMiJwYjIicmNTQ3NjMyFzYzMhcWBzQnJiMiDgIHFgcWFxYzMjc2JSY1PAE3JicmIyIHBhUUFxYzMjc2Az8BQ0NgeUZFfFxEQ0FAYntEQIFfQkNFLy5CHjUoHAQCAgsrLDlBLy/+fwMBCS0sOkItLS4vPzsrLtxfQ0NnaUREX2BBQmZmQ0NdQS0uEyMwHRobOSUmMDEgEA8IDgg2JCQtLUJCLy8lJgAAAQAB//QBkgHBADAAAAEWFRQHFhUUBwYHBicmNTQzMhcWNzY3NicGKwEiNTQ7AzYnJiMiBwYjIiY3NhcWAY8DLCwDGK6TLwMmGAkZZXUMB0ADBCIkJCIFAUMJDnNyDAgaEhYDGK2wAUkODDQjJi8MCnUBA3EHByMYRAIBPioTASQjCjI/Ph0XE3kCAgAD//f/9wHBAb8ADwAfACgAACUUBwYjIicmNTQ3NjMyFxYHNCcmIyIHBhUUFzYzMhc2ByYjIgcVFjMyAcFEQ2BdREJBQmBgQ0RHLy5DQi0tDzRYWjgPPSY+PSEyLTfcXkRDRENeYEFCQ0JePy8uLS1CIx8/QSFaMzIBIwAAAv/4/pUBsgG/AD4ATAAAJRYHBgcRFCMiNREGJyYnJjU0NzYzMhcWFx4BFzYnJicuAScmJyY1NDc2MzIXFhUUIyInLgEjIgYXFhceARcWByYnJicmIyIHBhUUFxYBqgg2GjgjJDU5RRoeFSBCODAYFxYMAkAGCC4SLhVPHAwcIjU5IB8gDwsHGxwXGgcOMBw5F0q8AgQSHiMeFwwIDh2RRi4VCv67JCQBPAEMDyAoLSQjLyESJiIpExI1MhsLCQQTMBMXJhwhHhwTHwcHFxkJFQwGDg4uqgwPNBQXEg4RFBQkAAAAAf/3//cBwwHBACoAACUUBwYjIicmNTQ3Njc2MzIWFRQHBhUUFxYzMjc2NTQnJjU0NjMyFx4DAcNDQmFgQ0MfHzYKCg8UD1UuLkNCLi9XEBQPBwwcLR8Q4WFERUVEYT07PCUHFw8RDDdmQzExMTJCaDUMEQ4WBREvOT8AAAL/9//3AcMBwQAmADAAACUUBwYjIicmNTQ3Njc2MzIWFRQHBhUUFxYzMjcmJyY1NDc2MzIXFgc0JwYVFBcWFzYBw0NCYWBDQx4fNgoKDxQPVC4uQ0wwVhoJPwoPDwtxRz8VGRQjBOJgRkVFRmA7PDwlBxcPEQw2ZkQxMUAfVh4dTT4KC02HUzggJCwdGAsRAAL/9//3AcMBzQAdAC4AACUUBwYjIicmNTQ3Njc2FxYXFjcyNjU0JjU0MzIXFgc0JwYHBiYnBhUUFxYzMjc2AcNDQmFgQ0MaHTcfDAgECzMZIwIcDgt6RygkUydGECIuLkNCLi/iYEZFRUZgPTk/IxMOCCNRAikZBBMFIAdHk0MxVQIBLyYxQEQxMTEyAAAAAAH/9P/zAyYBwQBWAAAlBiMiJjU0MzIWMzI3NjU0JyYjIgcGBxUUBwYjIicuAScuASc1JjU0NjMyFxQjJzU3HwEWMzI3NjcmNyYnJiMiBwYHBiMiNTQ3NjMyFz4BNzYzMhcWFRQC5UdZJi4hDBoPPDAuMC9AQS4nBUJCYzY0FyQOBAoHBBgPFAoDAgEECzJKPy4tBgICBi8uPVYkBAUFHSUUPHp8RQgQCEZbXEVDN0QSFiYJMS1CRC8wMSY8EF9CQhsLHBAFDQoBCAgPFxYBAQIBBAw7KSo8DQ47LCszAwgdIw4gUWsLEwpFRUNhXgAAAAL/9//3AcMBwQAsADQAACUUBwYjIicmJyY1NDc+ATc2MzIWFRQHDgEVFBc2MzIXNjU0JyY1NDYzMhceAQcmIyIHFjMyAcNDQmFsRgoDJx8QKxoKCg8UDygtCDtcWzsJVxAUDwcMOT93LENFLC5DP+JgRkVVBw02TDw7HjATBxcPEQwaVysYG0RFGRtnNQwRDhYFIXW3Pj4xAAAAAAH/9//1A0UBxQBPAAAFIicmJwYjIicmNTQ3PgE3NjMyFhUUBw4BFRQXFjMyNzY3JjcmJyY1NDYzMhcWHQEWFxYzMjc2NTQnJiMiBiMiNTQ3PgE3PgEzMhcWFRQHBgJgXEYRD0R9YENDHxArGgoKDxQPKC0vLkI/Li0FAgIGRhEUDwcMbgYnLkRBLi4wLz0XEgkjGgQQDgUMB1hJQkRDC0UTFmxFRmA8Ox4wEwcXDxEMGlcrQzIxLi1BDAlkLQwRDhYFRJQORCUwLi5FQjAwCCUZCQEDAgEBR0JhX0RDAAAAAgAE/poCKAHBACoAMgAAASI1ETQnJiMiBwYHNhcWFRQOAiMiJyYnJicmNzY3NjMyFxYVETMyFRQjATYnJgcWFxYBqyMuLkA2KywNXzA2BAsRDDsdHSw9BAUEBD5GW1xERDQlJf7MBCgcPQo2Gv6aJAIKWC0uIiM1AjY8Sw8dFw4NDSw+VA0LV0FERERx/hkkIwGjMy4gBDQtFwAAAf/4//UDPwHDAEQAAAEWFRQHBiMiJicmJyY1NicmIyIHBhUUFxYzMjYzMhUUBiMiJyY1NDc2MzIXFhcWBwYXFjMyNzY1NCcmIyIGIyI1NDYzMgL+QUNDXTlmHxkJBAIwMEA/Ly4tMDoZEwkiMyJXR0FDRFtbRy4QBgEFMzE+QS4vLy8+FxEJIjEgWwF9Q15hQ0M5MSc2BwpQMDAvLkRCLTEKJRcTREFgYENERS9PCwpNMy8uL0NCLy8KJhYUAAAAAv/3//cBwQG/AA8AHwAAJRYHBiMiJyY1NDc2MzIXFgc0JyYjIgcGFRQXFjMyNzYBwAFEQ2BdQ0NBQmBfQ0JELy5DQi0tLi8/Qi8v3F5EQ0NDX2BBQkNBXz8vLi0tQkIvLy8xAAH/9P/1AycBwwBVAAABFhUUBwYjIicuAScGBwYjIicmNTQzMhcWFxYzMjc2NTQnJiMiBwYHBiMiJjU0NzM2FScXByM2PwE2MzIXFgcUFxYzMjc2NTQnJiMiBwYjIjU0NzYzMgLmQURDXVxGCA8HJDE4NXg9FiUeBAMHKFJALzAuL0IjLCwNCxIPGQcBAQEBAgMBBBFFbmNAQQEvLkJBLi8vLzwXDQgJIhoVJloBfkNfYERDRQgVCzAdIFUfDSIcAgc3MDBBQS0tFhUcFRYPCgoBAgEBAwEGFlE9PlpOMTAuL0RDLy8FAyUZCQcAAf/3//UDQwHGAEwAACUGIyImJyY1NDMyFhcWMzI3NjU0JyYjIgcGBxUUBgcGIyInJjU0NzYzMhYVFAcGFRQXFjMyNzY3JjcmJyY1NDYzMhceARc2MzIXFhUUAwFGWwoUCikjCA0MBQs7Mi4wL0BJLyAEJh5HYFxDQlELDw4XCz4tLj9CNSYHAgIIOwkWDg0NEBwLRXVcREQ7RgICCB4jAwMBMi1CRC8vOCQ2EC1bHUA9PWuBWQsVDw4LQWdLKyovI0IMD1lACg8NFwsTKhdkRURhXwAB//f+lQHBAb8AXwAABRQPAQYnJicVFBY7ATInJg8BBiMiNTQ2MzIXHgEVFAYrASImPQE0MzIXFhcWPwE1BiMiJyY1NDc2MzIXFh8EFCMWFRQGIyInJjcnJiMiBwYVFBcWMzI3NjczMh0BAcAPHFNmUDUiMcAXCAEHAhMIICcWQwsBATEmwFFJHg4QPlNTOg8+Xl5EREJCYjQ2LRwLBgEDAwUXDxUKAgIJMExELi0vMT9UJQoVJyQWEwoVOwMBIHExIiAEBAEKIxYXOAUMBSg3TU66KA48AgItCzE4REZdX0JCGhgfDQkCAwIICA8WFQECCzkuLUFAMS80DAIjcQAAAv/z//UC7wHDAFYAYQAAARYVFAcGIyInLgEnJjc2NzYzMhc+ATU0JyYjIgcGBwYHFhUUBwYHBicmNTQzMhcWNzY3NicmJwYrASI1NDsBNjc2NyY3JiMiBwYjIiY3NjMyFzY3NjMyEyYjIgcGBxYzMjYCyCdiQEkjKwMGBBsPDGQcIisvAQEbLVBTLgUGAiosAxiwki4CJRcJG2J3DAQWEhQDBSIkJCYWCxgEAwIScW8MCBoSFgQXqn0zCxQ+SXYIJBoSEjMREQ4lTQFjO0Z8RCsPAQICEB9mLg4VBwwHMidCOgUNOCEnLwoKdAICcAoEIxhEAgI9GBIPBAEkIwQIDhQHCD09HRcTdz4LDin+wREIFjYFKQAAAAL/9/6VA1YBxgBKAIQAACUUBwYjIiY1NDc2NTQnJiMiBwYHFhQVFAcGIyImNzY1PAEnJjU0NjcmJyYjIgcGFRQeAhcWFRQGIyInJicmNTQ3NjMWFzYzMhcWExQrASInNTQnJicmBxUUBwYjIiY3Nj0BJjU0NyYnIgYVFBcWFRQjIicmNTQ3NjMyFzYzFhceAQc3NgNGbgwGDxQQTC4tRDwuLQYBBAUdEBgEAwECAQEGLS0/QS8vDBYfFA8UDwgMNh8fQ0Ngg0FGeWJCQhAkdBgCBBxVYgoDAx8QGAQCAgILZDA+FwglHxQOMzROYjIzW4sqBAEETSTTk0QFFg0TCjBsRDExKyw9CiMaChEeGBMPBQMSDwYFBQkFQC0tMjJDFSwqJA0LEQ8WBSY8OzthRkYCb2pGRf2HJk1SIwxRAQJDCCISHBYTBgMPBgUECkMCKiU0IAsLJTstJ0MpKDg0AYQPUUIIAwAAAAL/9/6VA/QDTgAmAIYAAAEUIyI1ETQnLgEnLgE1NDc2MzIXFhUUBiMiJyYjIgYVFBceARcWFQMUBwYjISInJjU0Njc2MzIXFhUUIyInJiMiBwYXFjMhMjY1ETQnJiMiBwYHFRQHBiMiJjU0NzY3JjcmJyYjIgcGFRQeAhcWFRQGIyInJicmNTQ3NjMyFzY3NjMyFxYVA/QjJD0ePB4nEyopQ2wpBBcPFQoXOyQtNh88Hz+vERo6/XojHBwBARvBq58aIwgDkqSHDwQEBg0ChhMLLi1BRC4nBm4JCg8UEUYGAQEFLS8+QS8vDBYfFA8UDwgMNh8fQ0NgfkMPEUZcXURE/rkkJAKYTkEaNBoiOBs5LylWBwkPFBUvLh8wMR05HEhQ/bYwHCYlJS4HDQZ7LwgYJAEqQRYSFBAaAd1CLS4xJkMOlEQEFg4RCy1kCgxALS8yMkMVLCokDQsRDxYFJjw7O2FGRm0WE0VERFsAAAAAAf/0/pUBpgG/AEsAACUUBwYjIicVFBY7ATInJjMyFxYUFRQGKwEiJjURNDYzMhcWMzI3NjU0JyYjIg8BBic0NzYVFgYjIiY1NDcVNBcrASc3PgE3NjMyFxYBpkREXkc7IjDAEwUFKh4EATAmwE9KKRUNBihQQC8wLi9CTC4NBAECAgIaBw8WCAEBAQEBAgoHRmxjQULdXUZFJusxIiYmHwUMBS0yTU4BHxAXBjQwMEFBLS05DwQBAgICAwcOFQ0MCwEBAQEBBAwJUUJBAAAAAAT/9P6VAaYDUABLAF4AbwB3AAAlFAcGIyInFRQWOwEyJyYzMhcWFBUUBisBIiY1ETQ2MzIXFjMyNzY1NCcmIyIPAQYnNDc2FRYGIyImNTQ3FTQXKwEnNz4BNzYzMhcWAxQHBgcGJy4BJyY1NDc2MzIXFgc0JyYjIg4CFRQXNjc2FzYHJiMiBxYzMgGmREReRzsiMMATBQUqHgQBMCbAT0opFQ0GKFBALzAuL0JMLg0EAQICAhoHDxYIAQEBAQECCgdGbGNBQik1NE9LNgQFAis0NE9MNjZIISEuGCgeEQYnPDwzBzUcIyQWHCEf3V1GRSbrMSImJh8FDAUtMk1OAR8QFwY0MDBBQS0tOQ8EAQICAgMHDhUNDAsBAQEBAQQMCVFCQQFZSjc0AgI2AwcEMkNPNTc4OEsuIyISHyoYEhInAgEvEkkfHhMAAAAABP/1//QBsAHBACcANAA+AEoAAAEUBwYHFhcWFRQHDgEHBicmJyYnLgM3Njc2NzYXFhceARcWBx4BJyYnJiMiBxYXFjMyNwcmJyYnBgcGFzYXJiMiBwYHFjMWNzYBsBQtNjgzBxAFCgdGayooGAgaKRwOAgZtBwUtPWE4BxYPBQECBVUgEx0rGxsQOBQUJSBgEhJUGyoFBDMoyR8kEBQ4ExUVTC8GASsYCSoJCzEGERQZBg0HSQMCDgIPDSozPB+EPwcBGQICPAgaEQcBAhQGJwsRCDsTBxZbAQgeSyxCQzBeSxYFETkGATIEAAAB//f+lwPAA1AAcQAABRQHBiMhIicmNRE0NzYzITIXFgcGByYjISIGFREUHgIzITI3NjUnBiMiJyYnBgcGJyY1NDcyFxYXFhcWNzY3NicmIyIHBgcGByInND8BNjcyFxYVFBcWFxY3Njc2JyYnIgcGIyInJjMyFxYXFgcGBxUDWDo7Uf4pVTg3NjZXAkpGLQILCwQiOf22OUMTIS4bAdc+ICABIBRcRxENT3J2QBYmHAUEBidUPy8vAQIvL0IiKy8NCRQjBQsQQHNgQUErK0pDLCwCAS4uPhQRBwkiAQJXX0FBAQJFEBOmUzc5OzpUAyxUODg5Ah0dAS5DOfzUGzAjFCkpKqwJRBMVagIBVR0PIQIdAgY3AQEwMkFBLS0WFxoUAiYFExVPAjw8XksxMgEBLy9FQDAvAgUEJSpERF9hQxENyAAF//T+lwdpA1EAdwCrALQA3gEBAAAlJicGBwYnJic0NzYXHgEXFhcWNzY3NicmIyIHBgcGIyI1NDcyFQcrATY/ATY3NhcWFRQXFhcWNzY3NicmIyIGIyInJjc2FxYXFgcGHQEUBwYjISInJjURNDc2MyEyFxQHBgcmIyEiBhURFB4CMyEyNzY9AQYjBgEWFRQHIicmJyYnJicmBwYHBhc2NzYzNhcWFRQHBgcGJyInIicuATc2NzYzMhceAR8BMxcHJiMiBxYzFjclFhcWBwYHBicmJzQ3MhcyFhcyNzY1NCcmJyYHDgEHBgcGJyYnJjc2MzITFgcGJzQnJicmBgcGFx4BFxYXFCMiJy4BJyY1NDc2NzYXFgQeEg1ScHU+FwEmGgcBBQQnU0AvLgIBLy9CISsvDQcWJgYBAQEBAQITQ25hQEIrK0tCLSwCAi8vPwsgCCIBAVVgQEACAkQXOjtQ/ilWODc2OFUCS0ctCwoEITr9tTlCEyEuGwHXPSMeICNd/UMHJAYOCQMnEh4yQy4uBAU0I1QgGUo+BxAIDkZrLiQXCTY4AwVBQWJeOwMMChACAUgfJFYZFRVJMgXxJQIBZD1NMiQLAyUKCwIUFjs3MRstTzEtCxsPHQsOGiABAThMf3YPBSUiAyEZHh4sAwk4BTMwEQMjDEAXHwhHLCs/NTIyOhEXbAEBViAMIAICHgEEBDUCATAwQkAuLRUXGxYmDAgBAwEGFk8CAjw8X0sxMQIBMC9DQTAwCCUoAQJFQ2FfRQwLx1M3OTs6VAMsVDg4OQUcGwEuQzn81BswIxQqJS2mCQIBVgoNJAEIBgkuDhUCAS4vQkIxURgIAkAEExQZDA5IAhARG2s/YkBAPgMPCxUC0hZPBgEy8ThJeUUpAgEXCxEiBAkHAi8rSS8pQQEBHwgbFCQdIwUGHhNUcAEXIwIDIxgRDQIBKBcxNQQeHAsTJiUNEwhBSz4qKwIBJCQACQAG/+8CQANOABkAJgAsADIAOQA/AEsAWQBlAAAlFAcGNRE0JyYnJgcGBwYHBicmNzY3NhcWFQEmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcCQCMkHhsgHR4cAgIiIQECNzI5OzIy/csFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhsVIgICJgKrHxQSAQEUEx8jAQEjOCwoAQIrKjn98hcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAACQAG/+4DxAHKACkANgA8AEIASQBPAFsAaQB1AAABFhcUBwYHBicmJzQ3MhcyFhcyNzY1NCcmJyYHBgcOAQcGJyYnJjc2MzIBJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXA5wnAWI9TjIkCwMkCwsCFBU7NzIbL00xLRgeDhUFDRseAgE4TH52/LMFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhsBWTlIekQpAgEXCxEhBQgIAi8sSC8pQQEBHxElEyEOIgQGHhNUcP75FxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAAKAAYAAAHGA00ADwAgAC0AMwA5AEAARgBSAGAAbAAAARYHBgcGJyYnJjc2NzYXFgc2JyYnJgcGBwYXFjMyPgIBJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAZcBNjRPSjY0AgE1N0tMNTVFASIgMC4gIAEBISEuGCkeEv67BQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYbApJNNTQCAjc2TUs3NwEBNzdNLiEhAgIiIy8uIyESHir+OBcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAALAAYAAAHGA1UAEQAhACkANgA8AEIASQBPAFsAaQB1AAABFgcGBwYnJicmJyY3Njc2FxYHNCcmJyYHBgcUFzY3Nhc2ByYnJgcWMzIDJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAZwBNjRPRzgIBCkCATU1TU40NUYhHzEtISABBic8OjYHNRwjIRkaIyH6BQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYbAplNNTMDAjcGBzRCSzc2AgI3OE0vISECASIhMRQQJwIBLxJJHQIBHxP+ixcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAACQAG/pgBxgHKAA0AGgAgACYALQAzAD8ATQBZAAATIjURNDc2HQEzMhcWIwEmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhftIyMlRSMCAif+tAUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWG/6YJAEWIwIBJvMkIwIaFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAAKAAb+kAHGAcoADQAXACQAKgAwADcAPQBJAFcAYwAAASI1ETQ3NhURMzIXFiMnFAcGNRE0NzYVJyY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwEZIyMkRiMBASXDJCMjJLUFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhv+kiQBKCECAST++yUiJCMBAiYBKCECASTUFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAAK//8AAAOYAcsAMgA7AEYATABSAFkAYABsAHoAhgAAARYVFAciJyYnJicmJyYHBgcGFzY3NjM2FxYVFAcGBwYnIiciJy4BNzY3NjMyFxYfATMXByYjIgcWMxY/ASY1NDY3FwYVFB8BJic3FhclBgcnNjcnBgcnPgE3BSYnNx4BHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAbEHJAcNCQMnEh8xQi8uBAU0I1QeG0k/BxAKDEdqLSUXCTY4AwNCRGBeOwUUEAIBSB8kVhkUFkkyggUCAzUEBCosGiwVJAEeGiweJBTyJBUsDiIWAREUJB4WIw0fAwI2AgMFNgWvDhgLCxkOCQsUCgoUCgoUCgoUCwkcFhQdAVYKDSMBBwYJMAwWAgEvL0JCMVEYCQFABBIUGQ8MSAESEBxqP2FBQT8GFhYC0hZOBwEyOhcbDRoLCREYGBCWHSogIxcaKh0tFyPoFyMgFiMOZyMXLQ4jFncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYACQAGAAABxgLeAA8AHAAiACgALwA1AEEATwBbAAABFhcUBiMiLwEmJzQ2MzIXAyY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwFjDAEVDw4I2wsDFQ8MC30FAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhsCLgoSDxYHqQkSDxcI/dwXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAAAAoABgAAAcYCvgATABsAKAAuADQAOwBBAE0AWwBnAAABFg4CIwYuAjU0PgIzNh4CBzYnJgcGFxYDJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAT0BDxokExQhGA8OGSEUEyMZEEcBGRQBARYW6QUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWGwJhFCIaEAEPGyMUEyEZDwEOGiITEwIBFhgBAf5rFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAAKAAb+/wHGAcoAEQAZACYALAAyADkAPwBLAFkAZQAABRQOAiMiLgI1NDYzMh4CBzQjIhUUMzIBJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAU4OGB8RER4XDS4lEh8XDj4YFRUY/vsFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhupESAYDw8aIRIkKg0XHhAVDx8BdBcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAMAAb/9QKpAcoADwAfACcALwA8AEIASABPAFUAYQBvAHsAAAEUDgIjIiY1NDY3Nh4CExQOAiMiJjU0NjcyHgInJiMiBwYXFhcmIyIHBhcWJSY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwKpDxkjEyc1MioSIBoQAg8ZIxMnNTIqEiAaEEQBFxQBARYWAgEXFAEBFhb9qgUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWGwFkFCMaEDgoKDMBAQ4ZIv7fFCMaEDgoKDMCDhki+RYVGAEC9BYVGAECeBcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAkABv7eAcYBygAMABIAGAAfACUAMQA/AEsAVwAANyY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwMVIzUjNTM1MxUzFQsFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhsOMUlJMUmyFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3Bgb9Y0lJMUlJMQAACQAGAAABxgNRACEALgA0ADoAQQBHAFMAYQBtAAABFgcGJzQnJicmBgcGFx4BFxYXFCMiJyYnJjU0NzY3NhcWASY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwFkBSYgBSAbHRwsBQk5BDMwEwEjDEAtEEgsKz81MjP+qwUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWGwLVIwIDIxgRDQIBJBsyNAQeHAsTJiUaDkBMPiorAgEkJf2qFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAAACQAG/pcCVgHKABMAIAAmACwAMwA5AEUAUwBfAAABFCMiLwEmPQE0NzYdARcRNDc2FQUmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcCVh0KBtkaIySSJSL9tQUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWG/68JQI7CBvxIQIBJNYoArQjAQEl7BcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAACQAA/pcCWQNLADEAPgBEAEoAUQBXAGMAcQB9AAAFFAcGKwEiJyYnJjURND4COwEyFxYdARQHBj0BNCcmKwEiFREUFjsBMjc2PQE0NzYVASY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJzY1NCYnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwJZOjpWyi8iGx47GjJJLuBfPBsjJBEpNeB8RTnKOCUmJCP+LAUCAzYDAQEDKiwaLBQkAR4ZLB8kFPIkFCwNIxYBEBQkHywZHwMCNQQCAjUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWG6FZNzgPDR07VwMcLkw2HVMmJg4iAgImDhAXMIX85DlLJSY2TiMCAicBBRcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMChEXCxQKCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAACgAG/pMBxgHKAA8AIAAtADMAOQBAAEYAUgBgAGwAAAUWBwYHBicmJyY3Njc2FxYHNicmIyIGBxQeAjMyPgIBJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAYwBMDBFQzAuAgEvL0ZELy8+Ah4eKi02AQ8cJBUVJBsQ/r8FAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhvIRS8vAQExMEREMS8CAjIxRSoeHjktFSUcEBAcJQGPFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAAJAAb+kwHGAcoADQAaACAAJgAtADMAPwBNAFkAAAEUKwEiJyY7ARE0NzYVJyY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwEJIF4fAQIiPx8g/gUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWG/6zIB8gAQIeAQIh3hcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAAAAH/9P/1BKgBxgB1AAABFhUUBwYjIicmJwYHBiMiJy4BJwYjIicmNTQzMhcWFxYzMjc2NTQnJiMiBwYHBiMiJjU0NzM2FScXByM2PwE2MzIXFgcUFxYzMjc2NTQnJiMiBwYjIjU0NzYzMhcWFwYXFjMyNzY1NCcmIyIHBiMiNTQ3NjMyBGdBRENcXEcQDg0SRF5cRgoQB0B/eD0WJR4EAwcoUkAvMC4vQiMsLA0LEg8ZBwEBAQECAwEEEUVuYURDAiwuQkEuLy8vPBcNCAkiGhUmWkc7BgMwMUBALy4uLz0VDwcJIxoYI1oBgUNfYERDRBIWGBRDRQoUCm1VHw0iHAIHNzAwQUEtLRYVHBUWDwoKAQIBAQMBBhZRQkBgRiwwLi9EQy8vBQMlGQkHRT1SUzAvLy5FRC4vBQQmGQgIAAAAAgAAAAEB2QHKABEAIwAAJTY3Njc2JyYnNRYXFhcWBwYHJxUmJyY3Njc2NxUGBwYXFhcWAQszJS0BAjEWQUw8QgICQz5NPk85RQECQj1LQBYyAgEtJkkIKC5APy8XFEoIO0JeXkM+B0hGBztFXV5CPAdLFBYvP0AuKQAAAAH/9P/1AaYBwQAyAAAlFAcGIyInJjU0MzIXFhcWMzI3NjU0JyYjIgcOAQcVBiMiJjU0NyI1NjU3PgE3NjMyFxYBpkREXn9ACyQZCAEIJ1VALzAuLkNQLQIFAwsTDxgGAgICAwgFR21hQkPcXUZEYREPIhoCCjYwMEBCLS45AgYEAxUWDwkJAgIBAQUKBlNDQgAAAAAB/97+lgFPAcEAGgAABRQHBiMiJyY3NjMyFRQXFjMyNzY1ETQzMhURAU9CNUA8PUEBASAkKiUjKiAoJCOeZTcwKi42ISQcFhIgJUACOyQk/cUAAAAB//T+kQGmAcEAOwAAJRQHBiMiJxMWFRQGIyInASY1NDc2MzIXFjMyNzY1NCcmIyIHDgEHFQYjIiY1NDciNTY1Nz4BNzYzMhcWAaZERF4mIcMGFhASCv77AxcXFAgEKFJALzAuLkNQLQIFAwsTDxgGAgICAwgFR21hQkPcXUZECv7JCwkPFA8BmwQGDw8QBDMwMEBCLS45AgYEAxUWDwkJAgIBAQUKBlNDQgAAAf/3/pEBpwHBADoAAAEUIxYVFAYjIic1JicmIyIHBhUUFxYzMjc2MzIXFhUUBwEGIyImNTQ3EwYjIicmNTQ3NjMyFx4BHwEUAaQCBRcPFQoCBy1QQy4tLy9AUScDChQWFwL+/AwRDxYGwR8mXUREQUJibEgFBwQCAVUCCggPFhUDAQs5Li1CQDEvMgUQDw8EBv5lDxQPCQsBNwpERl1fQ0NTBgoFAQEAA//0/pABwQG/ADEAOQA8AAAFFAcGIyInJjU0MzIVFBcWMzI3NjURNCcmIyIHHgEVFA4CIyInJjU0NzY3NjMyFxYVJTQjIhUUMzInHQEBwUREXl5DQyQjLi5BQC8wMC9AQi8qPBEeKBYsIiEECkFAWF9DRP7JJyYmJ0+KXURFQ0JhJCRCLi4vLkEBZT8vLy8DPC4XKB0RICEsDwtXOTlDRF0BJycnLwcEAAAAAAL/9//wAcEDLgA7AEMAACUUBwYjIicmNRE0NzYzMhcWFRQjIjU0JyYjIgcGFREUFxYzMjc2NycmIicmJyY1ND4CMzIXFhUUBxcWJzQjIhUUMzIBwUREXV5EQ0NEXl5EQyMkLy5BQC8vLy9AOS0tCW4CAgEjGB8RHicXLCEiGjwYgyYnJybWXkRERENfAXNeQ0RCQmEkJEMtLi8vQP6NPzEvJyc4JwEBBBkfLhgoHRAhHy0kHxUJYScnJgAAAf/3/pcBwQHBADUAACUOAQcGFRQXFhcWFRQGJyYnJicuATU0PwE2NTQnJiMiBwYVFBcWFRQjIicmNTQ3NjMyFxYVFAGtFCYTKwMEKB4YEyEdIQYBASpRDyswRzsuMQwDJhcJEEhDV2VFPn8lSiVVYBIQLQgGHBAWAgQfIS4LFAtlX6IiIj0sMSkqRyEfCAYjGCcuZkQ8SERULwAAAv/3//MBwwG/ACwAOwAAJRQHBgcGIyImNTQ3PgM1NCcmIyIHFhcWFRQHBiMiJy4DNTQ3NjMyFxYFJicmJwYVFBceARcWNzYBwyAfNwoKDxQPFCAXDDAwQE4uVhkPChUxEhAbKx8QQUJiYENE/toFDRE4BBkNHREKAQHTOj06JgcWDxILDSQqLBVCMjJAIVUyMSUXMQkQLzg/IWBGRkZG2CgsNhMQFS0rFiILBxIJAAAC//f+kAHBAb8AOwBDAAAFFAcGIyInJjURNDc2MzIXFhUUDwEWFRQHBiMiLgI1NDc2NzYyPwEmJyYjIgcGFREUFxYzMjc2NTQzMic0IyIVFDMyAcFDQ19fQ0NDQ19eQ0QYPBoiIisXJx4RHxohAQICbgktLTlALy8vL0BBLi8kI4MmJycmimFCQ0VDXgFlXkNDQ0RdGQkVHyUrISARHScXLx8XBgEBJzcmJy8vP/6bQC8vLi9BJMYnJyYAAAEANP/1AHsBwQAKAAA3FCMiNRE0MzIVEXsjJCQjGSQkAYQkJP58AAACADT/9QERAcEACQATAAAlFCMiNRE0MzIVAxQjIjURNDMyFQERIyQkI5YjJCQjGSQkAYQkJP58JCQBhCQkAAAAAv/0/pUBwANQACEAggAAARYHBicuASMiBgcGFx4BFxYVFCMiJy4BJyY1NDc2MzIXFhMUBwYjIicVFxYzMjc2MzIdARQGKwEiJjU0NzYzMhYVFCMiJyYnJhUGOwEyNj0BBiMiLwEmPQE0OwEWFxYzMjc2NTQnJiMiDwEWBwYjIiY1NDcwJzY0PwE2NzY3NjMyFxYBagIjIgMBNSIgKQQJOQUyMBQjDEAUHwpILCw+PS8tW0VEXl49DjpTUz4QDx5KUcAmMgMLQRgnIAgJDgEFCBfAMSM4T2lPHA4jJxUIJVZBLzAvLkNMMAoBAQsTDxgFAgEBAgEQHC02NGNCQgLVIwIDIxchJRkzMwUfGgsTJiULFAlATD0sLCcl/dheRUQ3MAsrPg4oukxPNygLCzgXFiMDBwICAyAkL3EiORULEnEjAgw0LzBBQS0uOQsCARUWDwgIAgEBAQIDEx8YGkJCAAP/9P6VAlUDUQBBAGkAcQAABRQHBiMhIicmNTQ3NjMyFxYVFAYjIicmIyIHBhUUMyEyNjURNCcmJyY1NDc2MzIXFhUUBiMiJyYjIgYVFBcWFxYVAxQjIjURNCcmIyIOAgc2FxYVFA4CIyInJicmJyY3Njc2MzIXFhUFNicmBxYXFgJVFyFB/oopHh0XJVZBMg0VEAwLHyYpEREeAXYbFzIOXlEqKkVfNAUWEBILIDUhLkhgEjKWIyQuLj8bMioeBl8xNQQLEQw7HR0sPAQGBAVFQVpaRET/AAQoHD0KNhrVQiMxIyMyKyZDKgoQDxYIGh0dGikhLQImTDUPSD1XNS8wVQwIDxQRNS8gODxRFj5S/eQkJAGTWC0uEiAtGwI2PUoPHRcODQ0sP1MNC19APUREcYszLiAENC0XAAAB//f+lQGnAcEANQAAARQjIjURBiMiJyY1NDc2MzIXHgEfARQXFCMWFRQGIyInNSYnJiMiBwYVFBcWMzI3NjMyFhURAaYjJDpJXUREQUJibEgFBwQCAgIFFw8VCgIHLVBDLi0vL0BRJwUQFSj+uSQkAWElREZdX0NDUwYKBQEBAgIKCA8WFQMBCzkuLUJAMS8yCBgQ/msAAAX/9f/0AuoDUAApAGMAcAB6AIYAACUUIyI1ETQnLgEnJicmNTQ3NjMyFxYVFCMiJyYjIgcGFRQXHgMXFhUDFCMiPQE0JyYjIgceARcUIx4BFRQHBgcWFxYVFAcOAQcGJyYnJicuAzc2NzY3NhcWFzY3NhcWFScmJyYjIgcWFxYzMjcHJicmJwYHBhc2FyYjIgcGBxYzFjc2AuojJDMdOx0nEQ0tLTw5MiwlFQoWOhMPLwgIHSQpFF6jIyQQFCEbFgUQCwECBRQtNjgzBxAFCgdGayooGAgaKRwOAgZtBwUtPUktI0U8KSTsIBMdKxsbEDgUFCUgYBISVBsqBQQzKMkfJBAUOBMVFUwvBhkkJAE4YC8VKhYeLSEfPCoqJiMgIhUuBhQxDxUWIxwYDDyT/sgkJPUiICwPBhINAgIUBRgJKgkLMQYRFBkGDQdJAwIOAg8NKjM8H4Q/BwEZAgIeGgMBODVCKCcLEQg7EwcWWwEIHkssQkMwXksWBRE5BgEyBAAAAAADAAD//wHKAcgAJwAvADcAACUUBwYjIiY1NDc2NzYnJicmBwYVFBcWFxQGIyInJjU0NzY3NjMyFxYHFAcGJyY3Mgc0JyIHFBcyAcpSCw8OFgo8AgIuLUVDLS1ACQEWDhEJUxQTH0BcZ0BBpD46BANBOSUUEQQVEuSCWAsWDg8KQWdFKysCAS4tQ2RECg8OFgtZgSgzLx48QD98PgIBQDcGPRAFFREEAAADAAD//wHNAcsAKgAyADoAACUWBwYHBicmNTQ3NjcyFxYXFAcGBwYXFjMyNzY3NCcmJyYnNDMyFx4DBxQHBicmNzIHNCciBxQXMgHMAUNCYmBDQx8xOA4LCQEPUwIBLy5DQy0tAhcQMgwCIwkKGywgEaY+OQUDQTklFBEEFRLrYUVDAgFGRmA9O10LDAoQDw05ZUIzMDAwRTcmHCcJESUFEDA5QBk+AgE/Ngg+EAYWEQQAAAABAAIAAAHSAcwAPQAANzQ3NjMyFQYHBhcWFxY3Njc2NzUuAycmJyYnJjc2FxYXFgcGJyY1NCcmBhUWFxYXFgcOARUGBwYnJicmAnQMByMDDVQBATAvRTAvLwsFBgYHBiMQNQECICAoJyAiAgIhJA8RIAU4Hg8iBwEBFEBAR2NERe+OSgUkFAk4ZEQzMgIBHx8sGAoNBwQDDQsnNCggIAEBGRgjJgEBJAoFAhEQHBkODxlGBQgCRTAwAQJERQAAAAABAAIAAAHQAcsAVgAANxY3Njc1LgEnJicmJyYnJjc2NzYXFhcWBwYnNCcmBgcWHwEWFxYHFAYHBgcGBwYnJi8BJjc2NzY3NCYHBgcUBwYnJjc2NzYXFgcGBwYHDgMHHgPuLjAvDAEGBgIeFw0zAwMgHyoiJB8CASQiAg8QIAEBPA8RDSAFAQESQEFITT88GgMHIxAdOgMiEAwBJCICASMeJycgIAEBNgsoBgcGBgUHHyo0SAEgICwYAg0LBQwHCyY1KB4fAgEaGCQjAgEmCgUDEhAcGQcGEBVKAggFRi4vAQEvL00IQCAPDhYfEBECBQokAQEmJBkWAgEgICg0JwgQAwQHDgwcLiITAAAAAgAAAAIBuQHNADMAPAAAJSMHDgEHBiMiJyYnJjY3NjM2MzYXFhcWFRQHBiciJyYnBhcWFxY3Njc2NzY3NjMWFRQPAScmByIHFjMyNwGvARAKDAM7XmFDQQUCODYJFicsakcNCBEIPkkbHlQjNQUGLS5DMR8SJwMJDgYkBwFVNEcVFRlWIyBzFQsPAz9AQGM/axsQEgFIDg0aExEFQQIJGFEyQUQuLgECFg0vCQYIAiIOCgHfMgEGTxcAAAACAAAAAgMXAc0ASgBTAAAlFhceARcWNzY3Njc2NzYzFhUUDwIjBw4BBwYjIicuAScHBiMiJyYnJjY3NjM2MzYXFhcWFRQHBiciJyYnBhcWFxY3Njc2NzY3NicmByIHFjMyNwGVDxUIEAgvQjEfEicDCQ4GJAcBAgEQCgwDO15gRAcGARM7XmFDQQUCODYJFicsakcNCBEIPkkbHlQjNQUGLS5DMR8SJwMJDjM0RxUVGVYjILMCFggSCjECAhYNLwkGCAIiDgoBAxULDwM/QQgJARQ/QEBjP2sbEBIBSA4NGhMRBUECCRhRMkFELi4BAhYNLwkGCKIyAQZPFwAJAAb/9wNzAcoAMQA+AEQASgBRAFcAYwBxAH0AACUmNzYnFjcyNzY3Jjc0JyYnJicmNzYzMhcWBxQnJjUmIyIHBhUWFxYXFhcWBwYHBicmJSY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwHgBSQkAzU2LzAwCwEBHjAFMwEBPRIbJx8mAiQjAhASDBYDOCAMGAYGCRJAP0pr/esFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhtHBRESBTUBHh0sCQoiCxQEJjU6IgkTFyQmAgIiDAYNDR8XDA8QKx4QSTEyAgG4FxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYACQAG//4FIgHKAE0AWgBgAGYAbQBzAH8AjQCZAAAlPgE3Njc2FhUWFxY3Njc2NyY3NCcmJyYnJjc2MzIXFgcGJyY1JiMiBwYVFhcWFxYXFgcGBwYnJicGIyInJicmNjMyFx4BFxYXFjMyNzYlJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXA0UDBgIFIREUBC4uPzEuMAsCAh4xBDICAz8TGiYgJgICIiMBERMKFwM5Hw0WCAUJEkFCRn9DSnhdQjENAiEQDwYEBwQPDi4/QTAQ/NQFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhu0ChULFwUCFAY/Ly8BAhweLAoIJAkUBCU3OiEKFBckJgICIg0HDA4gFgwPECseEEcyMgECbW9DMTsKEwkIDwghDi4xECsXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAACgAG/pcBxgHKACMALwA8AEIASABPAFUAYQBvAHsAAAEGBwYnJicmNzYzMhcWFxYHBgcGJwYHBhcWFxY3NjcyFxYXBicWMzI3Iy4BJyYjIgMmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcBcTdWTDU2AQI7NEtpNgkDAgg4PGcuFQEBKCEyNTQHEgsLBwMFzRhHISMKAQQEJzkavwUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWG/7RNwECLS1JRy8tThgSGAgpAQFbGSIuGxgCAjAQAQsEEg6xPxUBBAUnARMXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAAAAoABv6YAosBygAxADoARwBNAFMAWgBgAGwAegCGAAABBgcGBwYnBgcGJyYnJjc2MzIXFhcWBwYHBicGBwYXFhcWNzY3MhcWFxYzFjc2NzIXFiUmIyIHFjMyNwEmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcCiwUQOFdKOTFTSjc2AQI6OEhpNgoCAQs7NWYvFQEBKCIxMzYHEwsNByciHDQ2BxILCwb+tzcmFxwaTBUm/r0FAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhv+9Q4WNwEBMjABAS0tSUgvLE8XExELLAIBXBkiLhsZAgEvEAINFQ8PAS8QAgwEcSgJRBIBTRcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAH8qP6V/0z/3wBCAAADBiMiJicmNTQzMhYzFjc2NTQnJiMiBwYHFBUUBwYjIjU0NzY3NDUuASMiBhUUFxYVFCMiJy4BNTQ2MzIXNjMyFhUU6TxBCxUQFx4FEAM2MB8fJjQ0JBwFWgkHHxE2AwJFNDNIQQ8fCAYuNG1NZzM0ZExs/sYxAgUIFSAFBykbKywbHx8XLAQFaTIEIREKHEAHBys5PS4/JgoPIAQZVypHYUdHWklJAAL94v6V/07/3wAqADUAAAcUBwYjIicmJyY3NjMyFhc2NTQuAiMiBwYjIiY1NDc1Ijc+ATc2MzIXFgcmIyIHNh8BFjMysjc4TGw2CQMDCTk9MFMVFhMiLRo2NQkQDBUDAgIDCQY6Vk83NYsZRyMiBgQKJTwYxUguMFAVFhoGKjArGiIVJRsQLxEUDgYIAQICCgc5LzCkQBYBAQooAAH98v6X/2X/3QAiAAAHFAcGIyI1NDc2NTQnJiMiBhUUFxYVFCMiJyY1NDc2MzIXFptECgsgCzAhIzc2RTALIQoLRDszSE83N8ZaQQghDAsqQS0aHjgtPi0LDSAIPl1MMCcrKwAB/WH+lQAI/+MAXwAABxYVFAcGIyInBgcGJyYnJjc+ATc+ATc+ATcmJyYVFAciNTQ+AjMyFxYHBgcGDwEGFRQHFhcWMzI2NzUmJyY1NDMyFxYdARYXFjMyNzY1NCcmByIGIyI1NDc2Mz4BMzItNTY4SWQ1NWc4MjIRDggHHAQCCwkSGAMGDCEgHw8aIBEgHBwDAyoUHwIJAQcmIyg0RwMENhIeCAddBRslNzElICAuNwQPBB4VCBQFCgVETi1ISi0tR0gBAiUiLRIgGBgDAQQDBQwOAQcQDR4CIAwWEQkWFiAuGQUNAQkJDgMXFBM5Kw5AHQkSIQQyagkpFyEgGi0rGygHBCAUCQQBAQAAAf36/pj/Jf/pADIAAAcjNRYXFCMiJzUnJiMmBwYVFBcWMxY3PgE3NjMyFxQHBiMiJyY1NDc2NzYfAR4BHwEUM90BAQIaDgYIIzMvHx8fHy85HAEDAgQUGAEPJ1ZBLy8sK0hNLwgBAgIBAWgBBAocDwMJKQEiIy4tIyQBJgIEBBMZDBY8MjJFQjIwAgI9CwEDAgEBAAP99P6Z/17/4wAPACAAKAAABxQHBiMiJyY1NDc2MzIXFgc2NTQnJiMiBxYVFAcWMjMyJzQnBhUUFzaiMzhJSTY3NDtPUC4uWx4hHDIRDzAuBQwFNFo0HSAxzkErLS0tSkgtMTc3gBkmKiciBCk+PSgBZi0ZGikuHRwAAAAD/V/+lwAA/98AOQBMAFQAABUUBwYHBicGIyInLgEnJjU0NzYzMh4CFzY3JjQ1PAE3JiMiBwYjIiY1NDciNTc2NzYzMhc2MzIXFgc0LgIjIgceARUUBxYzMj4CBSYnJgcWMzI2OE1hODhiVTgFDQoKEDE2Fi4nIAkZCQEBGWE4MQgQDRUEAQEEDThWYzc3Yk43Nj4UIi0aXxkBAQMbXBouIxT+bg8nKCQnPRDERy0vAQFFRTcHEgsKCQwNKQwWIBMRIQcMBQULB0wtERQNCAcBAQYNN0NFLi9GFiYaD04HCwULDU8OGyZMHw0NEykAAAAC/gj+lv+P/90AOABAAAAHFgcGLwEGJyYnJjU0NzYzMhcmJyYjIg4CFRQXFjMyNzYzMhUUBwYjIicmNTQ3NjMyFxYXFhUUBycmJyYHBhcygxIZGBQRIjMkHRwdGiArJQYKKTIaLCITIyU2JxcJCh8vISJONzc4N0tFPQoPEgpXEQsmCAcpGfAZExMaGBABARoZHR4UER8QBx8QGyUVKR0hDwYfGg8KMTBFRC8uKwgcIBoSFAcPAw4UDgIAAAP+JP6TACoBwQAbACwANgAAExQjIi8BBiMiJyY1NDc2MzIXFhUUBxcRNDMyFQE2NTQnJiMiBxYVFAcWMjMyJzQnBhUUFzY3NioeCAetNUZINDUzOktMLS0IaCQk/v8eIBovEg8uLQULBTNWMhodDxEP/ronAy8pKyxFRSwxNTY/FxYaArMlJf1YGCYnJiAEJzs6KAFjKhoXMi0XBxoYAAAB/qT+mQAG/+EAPQAAExQrASI9ATQzMhcWMzI+AjU0JyYjIg8BFAYjIic0NyczIzc+ATc2NzIXFhUWBwYHIicVFBY7ATI1NDMyFwYy1FslCAMVMhIhGhAdHSMtHAcLBxMDAwICAgIBBgYmQjIrKQIuJzMoJBUb1AkVEQP+xy5LSxMEGQsVHREiFxIcBQQIEgQFAQEBBgUmAR0eMzAjHQESMBkPEhIPAAAB/Yn+lwAI/+sARgAAExQHBiMhIicmPQE3FhcWNzY3NicmJyYHJzYzMhcWFRQXFhcWNzY3NicmJyYHJzYzMhcWFRQHBiMiJwYHBicVFDMhMj0BMxUIHRki/jQaHyAyIU8rJSgCAiwiLk4iNC53QjE8LSYpKycmAQIrIS8dHBcnKUIyOzw1PmE1MWQ8LyIByyI2/tMdEg0SEhpqDzIBARUYKSkYEwIBNA9JHCM8JxgUAQEWFykpGBICAQskCxwjPDwiHjs5AQIaOBgWICAAAAAAAv4z/pT/U//zADQAVAAABxQHLwEmJxUuASczJiciBhUUFjMyPwEVNjMyFxQHBiMiLgI1ND4CMzIXFh8BFDYVHgEXAyI1NwYHBicmJzQzMhcWMzI3Iz4BNzYzMh0BMzIXFCPbHwcCAwEDBAIBFighKywgJhgDBw8YAw4lPxsuIxQTIy8bGyEVEQoBAgECIRkBFSM3MgkBGQUMHC0jDgEDBQMNDBYaGAMbVRcEBwEBBwMDBQMVAyoiHy4dAgEQGwcRLRQiLhscLyETDgoRDwICAgMHAv7nGRIGAQMjBwscBxULAgMDCxsnGBoAAAAB/aT+lAAA/98AUgAAASImPQE0NzYzMhYVFAcGIyImNTQ3NjU0JyYjIh0BFBY7ASY1NDc2MzIXFhUUBw4BByI1BiMiNTQ3NhcUIwYnPwE2NTQnJiMiBwYVFBcWFRQGKwH+Gzo9Ih4oLTkhCAwKEQgTGw0VJRwkpx81NElIMjE9BAkHAQgIGg4BAgEBAQMKLCIiMjIlJSYHEgzN/pQ6PYsiFRI7LjAiCRELCwgSGiIOBxeLJRssOUY1MzIxS1M2AggFAQUbDQsBAQEBAQIKJDo0IiIkJDA7IAMODyEAAf4z/o//N//uAEkAAAU0Njc2Nz4BMzIWFRQOAiMGJicVFjMyNzI3Mh0BMjYzIzYXFCsBIj0BDgEHBiIHBi8BJj0BNDsBMhcWFzI2NzQmJyIGBw4BByL+MwIBBywMIQ43RxMiLhsOJAsZIBYzBgMZAg0DARkCEjQZBhcKAwkDNhkPDxkGCQsWJh8sAi4fDh0SBgwFGloFBgUTFwcHRjcaLyIUAQkHDA4VARojAwMYHxkaAgUCAQIBEAsKGjsYCRcDLh8gKgIQDQQLBQAAAf26/pf/NP/mADIAAAcUBwYnJicmNTwBNzY3NjU0IyIGIyI1NDc2FxYXFAcGBwYXFjMyPgI1NCcmNTQzMhcWzDg6UUMwNQEIKSkbCB0DJyQqLDcCKCwGBSEdKBgrIBM5FCQJCWG8STIyAQIkJzcFCQUgDw8YDgImIAMCFBcrHRMVHx4SEQ8bJBU9IAsUJgQ0AAAAAAL9a/6XAE3/4ABcAHAAABcWFRYHBiMiJyY3MhcWNzY3NicmIyIHBgcOAQ8BBgcUFxYXFRQOAiMGJyY9ATQ3NjU0LwEmLwEmJyYjIgcGBwYXFjcyNzIXFgcGJyYnNDc2NzYfATY3Nhc3Njc2BSY1NDc1NCciHQEWFRQHFRQzMjUsHwJOLjZPAQEkCAQzKiwBAS8ZIycdCREBAgIGBwMKBQEPGSASJBscBAsLAQgBAQseHhY5IQ8CATItLQkDIwEBSjkyTgEgNGI+KgoZMSscDCo/Xv74Dg4SFA8PFBJmJjdcLhooIwIBDhkaMy4cEBQBEQEFBQoLFBkQCAkUEh8XDgIbGSQVDgUQFgcYBwYGAg8LCigSHC4dGgsBJSUCAR4vWTMoQAICHAYhAgEmCBsBAegcKhohEwoDDRQeHCMeDQ8PAAAAAAL9WP6UAAH/4AA4AEgAABcUBwYnIic3FxYUIwY1NDcyFQYjJyI1FzI2NTQnJiMiBwYHHgEVFAcGIyInJjU0NzYzMhc2MzIXFgUmNy4BIyIOAhUUFjMyNgE6O1cLCgICAgIfHwICAQMBGUNHIiYyNSUdBwEBNjhMTDc3NTVQZTU6YEk6OP6KAQEHRC4bLSESRzQuRMZDMTIFAwEBAQIBHx8BAQICAwM8KikeHx8YHAUIBEguLy8uSEMvL0dILzBVEBMlLQ8bJBUuOjAAAAP9XP6WAAX/3wAXAC0ARQAAFxQHBiMiJwYjIicmNTQ3NjMyFzYzMhcWBzQuAiMiBgcWFBUcAQceATMyPgIlLgMjIg4CFRQeAjMyNjcuATU0NgU2NlBiNzdkTTY2NTVPZTY3YlE1Nj4TIi4bLkMHAQEHRC0aLiIU/soEFyEqFhosIRMTISwaLUcIAQEBxEguMEZGMC5IRDAvRUUuL0YXJRoOLCIGCwUFCwUmLRAbJi0SHBULEBskFRcmGxAtJgULBQULAAAB/fv+lf9D/+YALwAAAQYnLgE1NDMyFxY3Njc2JwYrASI1NDsDNicmIyIHBiMiJjc2FxYXFhUUBxYVFP6geCUBAR8SCRNSXAcEKgQGGh8fGgQDMAcLWFkKBRkPFQUYiD4sOR0b/pgDUQQGAx0SKwICIxwGAR4hBhstLRoXEF4BAhcgNiQaHR5gAAAAAAP95v6X/1z/3wAPACEAKQAABxQHBiMiJyY1NDc2MzIXFgc0LgIjIg4CFRQXNjMyFzYHJiMiBxYzMqQ3OE1ONjY2NU9ONzc/FCIuGRosIhMJLERGLwo1HC4oHSAnJcNILjAwLkhELy8uLkYWJRsOEBskFRUVLy8TPRodEQAC/fv+i/+C/98AQQBPAAADFCMiJwYnJicmNTQ3NjMyFhceARc1JicuAScmJyY1NDc2MzIXHgEXFhUUIyIvAS4BIyIHBhcWFxYXFhcWBx4BFxYnJicmIyIHBhQeARcWN34eCTg5U2QeGhIZMypXKgMMCQQjByEcQBcJFRwsKh0CBAIWHgQQAwQWFhMMAgIHVCgiIAUDCwgQCBGmFTodFhEKBxQrJCEi/qwhIBgFBiMdIx0YJDEfAgkFCRsUBAgFDCMQEx4UGBQCBAIHFSEGAwIQCQIBDREHHx0qGBkFCAQOHhEjEg0JFBAMAwMGAAH98f6X/2f/4AAlAAAHFAcGIyInJjU0NzYzMhUUBwYVFB4CMzI+AjU0JyY1NDMyFxaZNTZQTzY2YgUJHw9BEyItGhotIhNBEyEIB2PBSDAwMDBIZDgEIBAJJ0AXJxwQEBwnF0IjCRIhBDMAAAL97/6X/2X/6QAhACoAAAcUBwYHBicmNTQ2NzYzMhUUBwYVFBYzMjcmJyY1NDc2FxYHNCcGFxYXPgGbNDRSTzc2My0HCB8PQEc2NiNDEQUtEBlkPjATCgssAQG/SDAwAQEwMEoqVxkEIBAJJUAwPB8YPhYSNykXDTNoMSYcIiEMBQoAAAL98f6V/2f/4AAdAC8AAAcUBwYHBicmNTQ+Ajc2MzIHBhYzMjYnJjMyFx4BBzQnBiMiJwYVFB4CMzI+Apk1NlBQNTYNGCMWDAgZAQIeFBIaAQEZCggwNz8gHEFAIhkTIS0bGi0iE8FILzABAjEwSRYuKSMMBSIUICAUIgQVWDAuHkA9ICkXJxwQEBwnAAAAAAH9Z/6V//v/3wBOAAADKgEnIicmNTQzMhYzFjc2NTQnJiMiBwYHFRQHBiMiJy4BJzQzJjU0NjMyHwEWMzI2Ny4DIyIHMAcGIyImNzY3NjMyFzYzMhcWFRQHBrgFCwUVCBUeBA8FNy0iIiYxMigcBDU2UVY6AwkGAQQVDhAIAi07NkUCARUhLRlCHggMEg8SBQEPMGJkODRkSjc4Njj+lQEECRQgBAYmGyotHCAgFygIRDAwOQQKBgIFCQ0VEgEtNjMWJRoOIgYXGBEJEjlISC4vSkYuLwAAAAL98f6X/2f/4AAoADAAAAcUBwYjIicmJyY1ND4CNzYzMhUUBwYVFBc2MzIXNjU0JyY1NDMyFxYHJiMiBxYzMpk1NVEoLS8UIw4ZJBcHCB4OQgQvSUkvBEETIQgHY2ghMjIiIzEwwEgwMREVICw3FSwqJAwEIBAJJz8QDC4vDRBAJAkSIQQ1uCQkGgAAAf1m/pgAAP/cAEQAAAcWFRQHBiMiJwYHIicmNTQ2NzYzMhUUBwYVFB4CMz4BNzUmJyY1NDMyFxYdARYXFjMyNzY1NCcmByIGIyI1NDc+ATMyNTU1NkllMzFmSzY2NC0IBx0PQBQhLBgzRAMDNRIfCAhYBRwlMjMlHx8wNAMQBB4WEBUKQFMtR0gsLUZEAjAuSCpUGQUfEQklPhcnHBABNi0MPh0JESIFMGgKKxYfHxktKhwnBwQeFwYFAQAC/i3+kv94/+sAJQAtAAADIj0BNCYjIgYHMh4CFRQjIicmJyY3Njc2MzIWFxYdATMyFRQjJzYnJgcWFxbTFjcmITQHHCweEBo4KiQEAwMDJyc5J0gPCx8WFroBFhEmCB4Q/pIVujEzJh0RHSYVLiciMAYHMiQkKyQYI6cVE4EeGBMCIBcNAAAAAfzX/pf/d//oAEMAAAEiJyYnJjU0JyYjIgYVFBcWNzI2MzIVFAcGIwYjIicmNTQ2MzIXFhcWBwYWMzI3NjU0JyYHIgcGJjU0NzYzNhcWFRQG/sRLOCgMBSIoMTJFHy02BREEHxwKDgoLQjk2bEpJOCUOBwEDTDQxJR8fKzgGCREYFg8QTEI1af6XLiE8BgkzHiE8LSoaKAcFIBgIAgEwK0hKXC4gOQoKL0MgGywsGyUEAwQUDxQIBQk4LUlJWgAAAf0K/pj/YP/qAEcAAAcWFRQGIyInBiMiJyYnJjYzMhceARUWMzI2NTQmIyIOASMiNTQ3NgcVByI0PwE2MzIWFRQWMzI2NTQnJg8BIgYjIjU0NzY3NtAwYkNeLzhTVy0HCAYUDA4FBwUePC1GRC8ZQRAOHgUDAQIBAw0xUUddRDAuQyApMgcDCwUZHwQJSlAvREVgTk49BhEOEwoOAgEnRC8tQSAjGwgIAQMBAQEEDzpXQThDQjAvICgGAQMZGAMBAQoAAAH9av6PAAH/3gBGAAADBiciJyY1NDMiFhcWNzY1NCcmIyIHBgcVFAYHBiMiJyY1NDc2MzIVFAcGFRQeAjMyNzY3NSYnJjU0MzIXFhc2MzIXFhUUM0JNBxUUHgIMDjQvIyUmMTgmFwYcGThMRjU1QQoJHgkwEyEsGTMpIAMDMAgeCgkaETZcSTU1/so7CggIDRkDAgcqIC4wISEpGiUKIEATMCwsS1o+CBkLBy1IGyocDyAaLBQ+LQgKGgkbH0UvMERDAAAC/Yf+lwAA/+YATABWAAAVFAcGIyInJjc2MzYXNjc0LgInIgcGFxYHBgcWFxYHDgEjIicmNx4BMzI3NicmJzUyNzYnJicmBw4BBxYXByI1NDc+ATc2FzY3NhcWBy4BJyIHFxY3Njc4S0IBAywoNyURAwMUIy8bEi4uBAUjIBITHiMFFl42dTMCMQ5BKj8nBxMsLzAqEgUjQ08fAQEBARoYNk4UKxdTNzdUSzc2TwsRCEIWExQnD8NJLi8ZMyMhAQcKFBoqHhEBFRYMDBcUBAQUGhEuN2IEGSQpMAYNGAExGAwILgEBIwIEAgMGJS48FAUFAQI5NQIBLi+NAwMBNgcIEQcAAAH/Mv/1AHEDUAAeAAA3FCMiNRE0LgIjIgcGFRQjIicmNDU0NzYzMhcWFRFxIyQPGSARHx0dJB8CATM0OjoyMhkkJAKrDxkSCxQUHCURCQkBOCkrKSk6/VUAAAAAAf+B//QBYwG+ADIAAAEWFRQHBiMiJicmNTQ2MzIXFhcWMzI3NjU0JyYjIgcOAQcGBwYnJjU0Nz4BNz4BNzYzMgE9JmFBSh0vCg4WDwoLAQoREDAwQxstTzQqCxsPHQsOGiEHBxkRECISPkd3AV47RntDKw4ICxEPFwkBAwUhLVUxJ0IeCBsUJB0kBgYeDwsNJhoXIw0pAAAC/g0B3P98A1AADwAgAAADFAcGBwYnJjU0NzYzMhcWBzQnJiMiDgIVFBcWMzI3NoQ1Nk1ONDU1NE5MNjZIISEuGCkdESEfLy4hIQKVSjc0AgI3OEpNNzc4OEsuIyISHyoYLiIiIiIAAAP+DQHc/3wDUAARACIAKgAAAxQHBgcGJyYnJjU0NzYzMhcWBzQnJiMiDgIVFBc2NzYXNgcmIyIHFjMyhDU2TUs2CAMrNTROTDY2SCEhLhgpHREGJzw8Mwc1HCMjFhoiHwKVSjc0AgI2BggyQ003Nzg4Sy4jIhIfKhgSEicCAS8SSR8eEwAAAAAB/qX+lf9X//QADQAAASI1ETQzMh0BMzIVFCP+ySQkJEYkJP6VJAEXJCTzJSMAAAAC/l3+lf+M//IADQAXAAABIjURNDMyHQEzMhUUIycUIyI1ETQzMhX+/iQkJEYkJMMkJCQk/pUkARUkJPElIyQkJAEVJCQAAAAC//X/9AGtAcEANQBBAAABFhUUIyImJyYnJiMiBwYHBhc2NzYzMhcWFRQHDgEHBicmJyYnLgM3Njc2FxYXHgEfATMXByYjIgcGBxYzFjc2AaYHJAkVAiwNHTNDLi0FBDMjVCAZSj4HEAUKB0ZrKigYCBopHA4CBUFDYGE4AwwKEAIBSB8kEBQ4ExUVTC8GAUsKDSUPCDMJFy4sREMwURgIPgYRFBkGDQdJAwIOAg8NKjM8H2FBQgICPAMPCxUC0hYFETkGATIEAAH+JwH1/0sC5wAQAAADFhUUBiMiLwEmNTQ2MzIfAcMOFw4LC9sOFw4LC9sCNgsQDxcIqQsQDxcIqQAAAAL+agIS/xMCvAARABkAAAMUDgIjIi4CNTQ2MzIeAgc0IyIVFDMy7Q4XHxERHhcOLyUSHxcNPRgVFRgCaREfGA8PGiESIysNFx4RFg8gAAAAAv6V/wT/Pv+uABAAGAAABxQHBiMiLgI1NDYzMh4CBzQjIhUUMzLCGRoiER4XDi8lEh8XDT0YFRUYpSIaGw8aIRIjKw0XHhEWDx8AAAQABv/5AK8BtwARABkAKgAyAAA3FA4CIyIuAjU0NjMyHgIHNCMiFRQzMhMUBwYjIi4CNTQ2MzIeAgc0IyIVFDMyrw4XHxERHhcOLiYSHxcNPhcVFRc+GhohER4XDi4mEh8XDT4XFRUXUBEfGA8PGSESJCoNFx4QFQ8fAS0gHBsPGiESIysNFx4RFg8fAAAAAAH+QgHi/3kDUAAhAAADFgcGJy4BIyIGBwYXHgEXFhUUIyInLgEnJjU0NzYzMhcWigMkIgMBNSIgKQQJOQUyMBQjDEAUHwpILCw+PS8tAtUjAgMjFyElGTMzBR8aCxMmJQsUCUBMPSwsJyUAAAH/Cf6PACoBwQAUAAATFCMiLwEmNRE0MzIVERcRNDMyFREqHgUK2RskJJIkI/60JQI7BxwBIiUl/vooArokJP0XAAAAAv4J/oz/bv/0AA8AIQAABxQHBgciJyY1NDc2MzIXFgc0JyYjIg4CFRQeAjMyNzaSMzVLSDU1NDRKSjU0RSAgLhcnHRERHScXLiAgwEo0NQE1NUpMNDQ1NEsrIyERHigYFygeEiEiAAAB/j3+lf7w//IADgAAARQrASI1NDsBNTQzMhUR/vAkaiUlRyMk/rkkIyXxJCT+6wAB/zr/9QFzA1AALgAAAQYnJjU0JyYjIg4CHQEUBxEUIyI1ETQuAiMiBwYVFCMiJyY3NjMWFzYzMhcWAXICISMdHR4RHxkPASMkDxogER8cHSQhAQI1NDhLMzRFOzI0AsIjAQIjGxQVCxQZDTkHA/2YJCQCqw8ZEgsVFBslIjkqKwI3OSsqAAAAAf/3/pUCUANQADUAAAUUBwYrASInJicmNRE0NzY7ATIXFh0BFCMiPQE0JyYrASIHBhURFB4COwEyNzY9ATQzMh0BAlAzOl7JMh8aHzswOVrfYDwbIyQOKTnfPCIeEyIuG8k/IyIkI6NRN0APDB87VwMiXTU7UyclDiQkDg8TNSUhP/zeHjEiEyklMk8kJE8AAf/3/pUD7gNQADIAAAUUBwYjISInJjURNDc2MyEyFxYdARQjIj0BNCcmIyEiBhURFB4CMyEyNzY9ATQzMh0BA+4wOV39k1U4NzQ1WgJjXzsfIyQSJDz9nTxAEyEuGwJtPyAfJSOoRDpFOzpUAy5POjtQLEIOJSUOKxgzQzn80hwvIxMqJitWJSVWAAAB//f+lQJQA08AJgAAEwYHBhURFB4COwEyNzY9ATQzMh0BFAcGKwEiJyYnJjURNDc2NxWiKxseEyIuG8k/IyIkIzM6XskyHxofOzAzSAMHBh4hP/zeHjEiEyklMk8kJE9RN0APDB87VwMiXTUzB0gAAf/3/pUD7gNQACgAAAUUBwYjISInJjURNDc2MyEyFRQjISIGFREUHgIzITI3Nj0BNDMyHQED7jA5Xf2TVTg3NDVaAQwlJf70PEATIS4bAm0/IB8lI6hEOkU7OlQDLk86OyMlQzn80hwvIxMqJitWJSVWAAAB//f+lQJQA1AAKgAAEyInJicmNRE0NzY7ATIXFh0BFCMiPQE0JyYrASIHBhURFB4COwEWFRQjvDIfGh87MDla32A8GyMkDik53zwiHhQiLhqWIiD+lQ8MHztXAyJdNTtTJyUOJCQODxM1JSE//N4eMSITIhMTAAAB//f+lQPWA1AAJwAAEyInJjURNDc2MyEyFxYdARQjIj0BNCcmIyEiBhURFB4CMyEWFRQju1U4NzQ1WgJjXzsfIyQSJDz9nTxAEyEuGwF9IBz+lTs6VAMuTzo7UCxCDiUlDisYM0M5/NIcLyMTIxMSAAAAAAL9l/6M/3z/7wAeAC8AAAcUBwYHIicmJwYnJicmNTQzMhcWFxYzFj8BNjMyFxYHNCcmIyIOAhUUFxYzMjc2hDMzSkMxMQkXEi4lCyMNCg4FCgsSCyAxZUk0M0QfIC0XJxwRIB8sLSAfw0gzNAIuLEILAgMoCw0iCg4BCQIRKVU0NUksIiERHigYLCAhIR8AAAL+Mf6PACoBqAAcAC4AABMUIyImLwEGBwYnJjU0NzYzMhcWFRQHFxE0MzIVAzQnJiMiDgIVFBcWMzI+AiocBAYErDNCRzQzMzFKSTQzD2ciId8fIC0XJh0QICAqFigdEf6zJAEBLi0BATU0SEg1MzQ1RyYhGwKnIiL9uywgIBEdJxctICARHicAAAAAAv///pACWQNVADQASAAABRQHBgciByMGJyYnJjURNDc2OwEyFxYdARQjIj0BNCcmKwEiBwYVERQWOwEmNTQ3NjMyFxYHNC4CIyIOAhUUHgIzMj4CAjExMkcLCa4yHxweOzE4WuBfPBwkJA0pOeA8IR9JNj4kMDJGRjIyQw8cJhYWJhsPEBsmFRUmHBC/RTEyAQMFDw0ePVUDK180O1MmJg4kJA4RETUlIEH81TxILjtHMzIyNEYVJh4REB0nFhUmHRAQHSYAAAACAAD+lwPfA0wANQBFAAAFFAcGByImJxUhIicmNRE0NzYzITIXFh0BFCMiPQE0JyYjISIGFREUHgIzISY1NDc2MzIXFgc0LgIjIgYVFBcWMzI3NgPFMjJJCRII/c9VODc0NVoCY187HyMkEiQ8/Z08QBMhLhsByh8xMUdJMTNCERwoFis8HiApKyAgvkYyMgEBAQI7OlQDKU46O1AsQg4kJA4sFzRFN/zXHDAjFCs6SDQzNDVGFicdET0uKx8gICAAAAAAAf3X/pQAKQHGAC0AABMUIyImLwEmPQEHDgEHBicmLwEmNTQ2MzIXHgEXFjM2PwE2MzIdARcRNDMyFREpHgQGBOYaMg0wEg8dHxBBCRcOEQoLFwsmDAsMcw4WIJ8kIv65JQEBPwgbTl8WIwECEA8TUAoMDxUNDx4OLAIT1BcptysCuSUl/RgAAAL9vv6UACoBugArADwAABMUIyImLwEGBwYnJicGJyYnJjU0MzIXFhcWMxY/ATYzMhcWFRQHFxE0MzIVAzQnJiMiBhUUHgIzMj4CKhwEBgSnLkNAMC8JFRIrJgohDAsNBQsKEAsfMWBGMjIOZSAh2h4gKi06EBwmFRUmHRD+tyMBAS0rAgEsLD8KAgEoCg0gCQwDCAEPKFIyNEUkIRoCtyEh/agqICA9LRUnHRERHScAAv/3/pUCUANQAEcAXQAABRQHBgciJxUjIicmJyY1ETQ3NjsBMhcWHQEUIyI9ATQnJisBIgcGFREUHgI7ASYnBicmJyY1NDMyFx4BFxYzMj8BNjMyFxYHNC4CIyIGFRQXHgEXMxUWMzI+AgIvLjBBFA+xMh8aHzswOVrfYDwbIyQOKTnfPCIeEyIuG08UBRcQKCMJHwwKBQkCCgkPCx4uW0EvLz0QGyMUKjgdBQ0HCQ8UFCMbEMpBLjABBAUPDB87VwMiXTU7UyclDiQkDg8TNSUhP/zeHjEiEx8mCAEBJwkMHgcHBwIHDiVOMDFCFCQcETorKB0HCQQEBxAcJAAAAAL/9/6RA9YDUABCAFcAAAUUBwYHIichIicmNRE0NzYzITIXFh0BFCMiPQE0JyYjISIGFREUHgIzISYnBicmJyY1NDMyFxYXFjMWPwE2MzIXFgc0JyYjIg4CFRQXFhczFRYzMjc2A8UyM0cWEP3IVTg3NDVaAmNfOx8jJBIkPP2dPEATIS4bAdAYBBYSLCYJIQ0KDQULChELIDJgSDIyQx8gKhcmHBAeDw4UDA4qIB/CRzIzAQQ7OlQDLk86O1AsQg4lJQ4rGDNDOfzSHC8jEyMpCgEBKQoOIQoOAQgCEClTMzVGKyAhER0oFiwfDwcHBCAfAAAB/ksB7f8mA1IAOQAAAwYjIicmNTQ3JjU0NzYzMhcWFRQGIyIuAiMmBwYXFhcWFxYVFAYjIicmLwEiBhUUFjMyNzYzMhUU3xpDLiIpJRUVGS8xHgkWDQgLCwsHDQcPDQ0jJAofFg0NCA4DGhgaIRQTDgwQJQIgMxojOTceHCEhHR8hCwwOFwcJCAEIEg0SBAUKEBkPFwgNAQYZExgbEhIjBwAAAAP9hgHB/8kDQwAPAEsAXAAAAxQHBgcGJyY1NDc2MzIXFgUGIyInJjU0NyY1ND4CMzIXFhUUBiMiLgIjJgcGFxYXFhcWFRQGIyInLgEvASIGFRQWMzI3NjMyFRQlNCcmIyIOAhUUFxYzMjc2NzU0T000NTU0TU02Nf6TGkQuIComFwwYIxgwHgoXDQgMCgsHDQcNCg4kIgofFQ0OCAcIARkZGR8WFAwKEyQBISEjLRgoHhEhHy8uIiECiUo3NQICODhKTDc3NzjgMxoiOjgdHSERIRoQIAoODhUHCAcCCRINDwUEDBIYDxYHBwYBBhkTFxwSEyMIiy0jIxIfKhguIiIiIgAABP19Adz/rwNmABIATQBeAGYAAAMUBwYHBicuAScmNTQ3NjMyFxYFBiMiJyY1NDcmNTQ3NjMyFxYVFAYjIi4CIyIHBhcWFxYXFhUUBiMiJy4BLwEiBhUUFjMyNzYzMhUUJTQnJiMiDgIVFBc2NzYXNgcmIyIHFjMyUTU2TUs2BAUCKzU0Tkw2Nv6JFjwpGyUhExMXJyoaCBINBgoKCwYPBAUHCiAfCRoSDAoJBwYBFRYWHBETCgsNIAEsISEuGCkdEQYnPDwzBzUcIyQWHCAgApVKNzQCAjYDBwQyQ003Nzg4yjccJUE5Ix0nJR0jJA4MEBcICggSEQkQBgQNFBoQGAkHBwEGGxQaHhMUJgp1LiMiEh8qGBISJwIBLxJJHx4TAAAAAAP93QHF/4MDDQARAEcATwAAAxQHBiMiLgI1ND4CMzIXFgcGIyInJjU0NyY1ND4CMzIXFhUUIyInJgcGFxYXHgEXFgcWBwYnLgEnBhUUFjMyNzYzMhUUNzQjIhUUMzJ9HR0lEyIZDw4ZIhQnHBzdGj8qICYgEwsWIRYvHAolDQ0VCwsJCBcIEQgZBQECDxMIFA4bHBASBQoVJpIYFhYYAmolHx0QGiMUEyIZDhwbmjAXIDc1FxcgDx8ZEB8KDyIMFRINCgwFAQICBCABBSAEAgQEEhQVFgsWIwpuFRUaAAAABP2IAdz/ywNQABEAIQApADoAAAMUBwYjIi4CNTQ+AjMyFxYnFAcGBwYnJjU0NzYzMhcWFzQjIhUUMzInNCcmIyIOAhUUFxYzMjc2NR0dJRMiGQ8OGSIUJxwc1DU0T000NjU0Tkw2No0YFhYY1SAhLxgoHhEhHy8vISACaiUfHRAaIxQTIhkOHBsGSjc0AgI3N0tNNzc4OHYVFRpFLiMiEh8qGC4iIiIjAAAAAAH+Nf9V/uEAMQAOAAAFFCsBIjU0OwE1NDMyHQH+4SViJSVAIiWGJSMkcSQkkwAAAAL9+P6V/5MAHQANABsAAAMiNRE0MzIVETMyFRQjJxQrASI1NDsBETQzMhX8IyMkRyQkxCRqJSVHIyT+lSQBQSMj/uMlIyQkIyUBHSMjAAAAAf/s/pUAtwHBAA0AABMiNRE0MzIVETMyFRQjDyMlImAkJP6VJALkJCT9QCUjAAAAAv/s/pUBTQHBAA0AFwAAEyI1ETQzMhURMzIVFCMnFCMiNRE0MzIVpSMlIl8lJfUiJSUi/pUkAuQkJP1AJSMkJCQC5CQkAAAAAf/0/pUDJgHDAF4AAAEWFRQHBiMiJy4BJwYjIicVFBY7ATIVFCsBIiY1ETQ2MzIXFjMyNzY3JjcmJyYjIg8BFgYjIiY1NDcmNTMVIjU3Njc2MzIXFh0BFhcWMzI3NjU0JyYjJgYHIjU0NjMyAuVBRERcXEYJDwdFfEo4IjA5JCQ5T0onFQ0HJ1I9Ly8EAQEELi4/TDALARYJDxgGAgICAgESRW5hQkMGJjBAQC8vLy87DBoPIS4mWQF+Q15gRERGCBULbiXqMSIkJE1OASAQFwU2LS8+Cgo8KSk5CwkPFg8ICAIBAQEBARhRQkBgC0EkMS8vQ0IvLwIIAiUXEgAAAAEAAP6VAiwBzQBMAAAlFgcGBwYnFRQWMyEyNTQ3NjMyFxwBFxQGIyEiNRE0NzYzMhcWMzI3Njc2JyYjIg8BBiM0NxcWDgIjIjU0NxU1FyMnNz4BNzY3NhcWAbIBRENgO0chMQE6DgEGHh0FAS4o/saaGBMUDAcpT0ItLQICLy9CSzAMBAECAQEGCgwEJAcCAwEBAgoIRW1gQkLqXkZDAgIp+DEjFCEDFB4ICwQqNJsBKxIMCQU1Ly9EQS0tOg8CAQIBAwcHBCMMCwICAgIBBAwJUAEBQkEAAAEAAP6RAbMBywBDAAATLgEnJicmNRE0NzYzMhcWMzI3Njc2JyYjIg8BBiM0NxcWDgIjIjU0NxU1FyMnNzY3Njc2FxYXFgcGBwYnFRQXFhcHmA0XCzIQJRgVEg4FKU9CLS0CAi8vQkswDAMCAgEBBgoMBCQHAgMBAQUPRmxgQkICAURDYEY8EAo4BP6RAQQCDBAlTwExEA4IBDUvL0NBLS05DwMCAgEDCAYFJAwKAQEBAQEJEFEBAUNBYF9EQwIBKP4yEAsHQwAAAv/v/+EB1AG/AEMAVQAAJRYVFAYjIi8BBicmJyY1NDc2MzIXFhcWFzYnJicuAScmJyY1NDc2MzIXFhcWFRQjIicuAScmIyIHBhcWFx4BFxYXFg8BLgEnLgEnJiMiBwYVFBcWFxYBxBAXDQoNOURrfCgeFSBCNy9LIQUaBAEILRIuFlAaDBsjNSUVCiMPGwwRBgsFDhoWEAsIDDAcOhdJCwUTdggLAxAxIiMdGAoJDhZaOSQKEw4YCCkjCAczKCskIy8hMx4GEw0OMxoLCQQULRMZJB0iCQgZCxciCwQIBAwOCwoWCgYODi5UKSIEBwkCDiUXFxIOERMTHAYFAAAAAAL/9P8xAb8BwQAnAC8AAAUUIyI1ETQnJiMiDgIHNhcWFRQOAiMiJyYnJicmNzY3NjMyFxYVBTYnJgcWFxYBvyMkLi4/GzIqHgZfMTUECxEMOx0dLDwEBgQFRUFaWkRE/wAEKBw9CjYaqiUlAXJYLS4SIC0bAjY9Sg8dFw4NDSw/Uw0LX0A9RERxizMuIAQ0LRcAAAAB//H+qQHCAcEAaAAAJRQHBgcRMzIVFCsBIjURJicmJyYnJj8BNjc2NTQnJgcGFRQjIjU0NzYzMhcWBwYHDgEHDgEHHQEWFxYXNSMiPQE0MzIXFjc2NTQnJgcGIyI1NDc2FxYXFgcVPgE1NCcmNTQ2MzIXFhcWAcIwL0oIJCQsI0g9PxAFAgYWAwwVVBcNDREiJCIgJygeHwECNggaEwQPCAQvLjUYIhwIDBIWGBkYFQgGIBwwH0wDAzgtNVISFA8IDDYgHuJNQD8X/vElIiQBKAIxMz8HCycjBBIPGyIQCwQBAwskJCQZFx8fKDclBQ0IAwwaAwkpIiMBGSQWJQUIBwYKBQQEBwIlHQYKBxM6OBglFFMwazAMEg8XBSE+OwAC//f+lQGnAb8ANABrAAABFCsBPQEWFRQGIyInJjcnJiMiBwYVFBcWMzI3Njc2MzIVFAcGIyInJjU0NzYzMhcWHwMDFAYrASImNTQ3NjMyFxYVFCMiJyYjIgYVFDsBMjY9AQYHBicmJyY1NDYzMhcWFxY/AjYzMhUBpQMBBhcPFQoCAgkwTEQuLS8xP1QlBQUJGSQVOnpeRERCQmI0Ni0cCwYBDEpRpS0uDxc+KzIUIwkIKRMRDRSlMSM7QjoqITMOFQ8KLCU+LhM4Hw4KHQFTAgIBCwgPFhUBAgs5Li1BQDEvNAUJGiIQH1JERl1fQkIaGB8NCQL92kxPMiYcIC8aCxMmBRcaCxQkL24dAQMSDioLEQ0WISABAgkaDwgnAAAD//j+mQOsAb8AZQCrAMMAAAEWFRQHBiMiJyY1NDMyFjMyNzY1NCcmIyIPAQYHFAcGFRQXFh0BFAcGIyInJj0BNDc2NTQuAjUuAScmJyYjIgcGFRQXFjMyNjMyFRQOAiMiJyY1NDc2MzIXHgEXNjMyFzc2MzIDFCsBFhcWDwEGKwEiPQE0NzY1NCcmIyIGFRQfARQnNDcWFRQjIiYnJicmNTQ3NjMyFxYVFAczNzYnJicmNTQ7ATU0MzIVJSY3Njc1NCYjIgYdARYVFAcVFBYzMjY1A4UnYjtEJR4ZIgklCjAoQxsvTzUoGAkQBRERBSAgKywfHwISBgcGBAkGBB0qM1AtHEMuMgkgCCEOFx4PSkBgJ0N3ST4HCgMfP0AfEj5KdycmmwICBSQSCxFrJRobJCQtMEIoCwMBGSMLERAaExg3N05JOzsTEwUSAgERBSatJiT+3BgDARQWDw8VFBQVDw8WAV07Rn1EJgoHGyQKHC1XMidEHxAJGQYKIBwlJAgIHSwjISEiLR4JBiInCRQUEAUFDwoFFB5CJjJRLSIJJA0RCQMrQnlFO2ApBQgCOjoNLP3DJgYhTSsUDCUKDxoZJCUdGzMqLBsIBQMCAQcbKAoLEiInKEcwMDIyQysgCBQuIx0PCB8OJSXFKzguJiMPFxcPJC0nNSoXEBoaEAAAAAT/+P6XA6wBvwBlAH0A2QDtAAABFhUUBwYjIicmNTQzMhYzMjc2NTQnJiMiDwEGBxQHBhUUFxYdARQHBiMiJyY9ATQ3NjU0LgI1LgEnJicmIyIHBhUUFxYzMjYzMhUUDgIjIicmNTQ3NjMyFx4BFzYzMhc3NjMyASY3Njc1NCYjIgYdARYVFAcVFBYzMjY1BRYVFgcGIyI1JjcyFxY3Njc2JyYjIgcGBwYPAQYHFBcWFxUUDgIjBicmPQE0NzY1NC8BJic1JicmIyIHBgcGFxY3MjcyFxYHBicmJzQ3Njc2HwE2NzYXNzY3NgUmNTQ3NTQnIh0BFhUUBxUUMzI1A4UnYjtEJR4ZIgklCjAoQxsvTzUoGAkQBRERBSAgKywfHwISBgcGBAkGBB0qM1AtHEMuMgkgCCEOFx4PSkBgJ0N3ST4HCgMfP0AfEj5Kd/61GAMBFBYPDxUUFBUPDxYBPh8CTi81UAEkCAMzKywBAS8ZJCYdCw8BBQUJAQoDAg8YIRIkGxsECwsCBgMLHx0XOSEOAgIyLi0JAyIBAks3NE0BHzRiPioKGjArHQwqPl/++A8PExMPDxMTAV07Rn1EJgoHGyQKHC1XMidEHxAJGQYKIBwlJAgIHSwjISEiLR4JBiInCRQUEAUFDwoFFB5CJjJRLSIJJA0RCQMrQnlFO2ApBQgCOjoNLP67KzguJiMPFxcPJC0nNSoXEBoaEMsmN1wuGigkAQEOGRozLhwQFAIQAQoKDhEZEAYLFBIfFw4CGxkkFQ4FEBYHGAcGBgIQCgooExsvHBkKASUmAQEeL1k1JkACAhwGIQIBJggbAQLpGysbIBMKAw0UHhwjHg0PDwAC//3+swH/AcMANQB9AAABFCsBPQEWFRQGIyInJjcnJiMiBwYVFBcWMzI3NjsBMhUUBwYjIicmNTQ3NjMyFxYfARYfARYDIicGKwEiPQEGIyImNTQ2MzIXFhUUIy4DByIVFBczMjc2MzIVFh0BMzIXNQYjIicmNTQ2MzIXFjMyPwE2MzIdATMyFRQjAZ4DAQUWDxMKAgIKLklCLCwuLj5MKwwPDSMiPWNbQkJAQF80MiwbCwIEAQEhDgsHGTEiDxU5SkQ3RxcKIQkKCQkGUicXHhgHCSIBDg0KNEZgWxAUDwgLR05PHBwODhw+IyMBWgEBAQwGDxYVAQILNywsPz8uLjQMIiIBQEJDW1xAQBkXHg4DBAIC/WUIFiMcAzAtKDQVDQ0hAQUFAwEZFwELBCkEBhkI1BQ5ChANGAYsFBYLJv4jIgACAAH+kAHQAcsAUQC6AAAFFisBIj0BDgEHBi8BJj0BNDsBMhcWFxY3IzY3NicuAScmBgcOAQc1BgciJzQ2NxU0NzY3FTYzMhceARUWBw4BBwYnFRYzMjc6ATcyHQEyNjM2JTQ2Nz4BNz4BMzIXFhUUDgIjBiYnFRYzMjY3MjceAR0BMjcyNjMjNhYXFCsBIj0BFyIGIw4BBzcGIyIGIwcjBiYvASY1JzMmPQE0OwEyFhceARcyNjc0JiciBgcOAQc2BgcyDwEuAScBzwEoXScQMyJgKBwYJg4RDChGOioBKgIBKhYxHx84FwIEAgwWJQQBAQQBEERmW0EfIgJDIU4tRC0pQStbAgkGIg4ZDSX+YwEDBh0QCh4ONSMkEyItGhAaERscCicXBwIJDgoDAQIDAg0LAxIzFwUBAQEGEAcBBQYDAgIEBxoeCRIHAgIPGAcBDgMLHxEgKgErIA4XDggGBQQFBAIDBAoQA/UlJeoFCgYDGxIQEmElDSsCASgpNjUmFBECARoXAggCARQBJQcGBQIHAQUQAUo7HUsqVT4cIQEDIQsZIwEl9gEBagIGBwkUCQMHHyAuFygeEgEECQsMCggBAgoKNgECAQwGHRY2BgIBAwEBAwECAQYCCgECAgwTMxgHAwgMASgbGyYBCwkEBwcEAQMDBAMKCgAABP34/pX/kwNQAA8AIAAuADwAAAMUBwYHBicmNTQ3NjMyFxYHNCcmIyIOAhUUFxYzMjc2AyI1ETQzMhURMzIVFCMnFCsBIjU0OwERNDMyFYU1NE9NNDU1NE1NNjVHISEvGCgeESEfLy8hITAjIyRHJCTEJGolJUcjJAKVSjc0AgI3OEpNNzc4OEsuIyISHyoYLiIiIiL8LiQBQSMj/uMlIyQkIyUBHSMjAAAAAAL/mP8LAEH/tQAPABcAABcUBwYjIicmNTQ2MzIeAgc0IyIVFDMyQRsaICEaGS8lER8XDj4XFRUXniMZGx0cIyQqDhYfEBUPHwAAAAAC/xT/Ff+9/8AADwAXAAAHFA4CIyIuAjU0NjMyFgc0IyIVFDMyQw4XHxERHhcOLyUkMT0YFRUYkxEgGA8PGiESJCsyIRUPHwABAAf/6wHhAboAEwAANyM1MzcjNSE3FwczFSMHIRUhBydMRYF3+AE0bSxEUY12AQP+wVstS0B9QHIqSEB9QGAqAAAAAAgABgAAAcYBygAMABIAGAAfACUAMQA/AEsAADcmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcLBQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYbshcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAJ/6r+lQJOAcoARABPAFUAWwBhAGgAdACCAI4AAAEGIyImJyY1NDMyFjMWNzY1NCcmIyIHBgcVFAcGIyI1NDc2NzUuASMiDgIVFBcWFRQjIicuATU0NzYzMhc2MzIXFhUUASY1NDY3FwYVFB8BJic3FhclBgcnNjcnBgcnNjcFJic3HgEfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcCGTtDCxUQFx4FEAM1MR8fJTQ0JRwFWggIHxE2BANFNBosIRNADx8HBy00NzZMZzM1ZEs2N/2+BQIDNQQEKiwZKxUkAR4aLB4kFPIkFSsZLAERFCQeFiMNHwMCNgIDBTYFrw4YCwsYDggLFAoKFAoKFAoKFAsIGxYUHf7GMQIFCBUgBQcpGyssGx8fFi0JajEEIRALHEAOKzkQHScXQCULDiAEGVcqRzEwR0ctLUlJAb8XGw0aCwkRGBgQlh0qICMXGiodLRcj6BcjICodZyMXLQ4jFncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAoABv6VAcYBygAqADcARABKAFAAVwBdAGkAdwCDAAAFFAcGIyInJicmNzYzMhYXNjU0LgIjIgcGIyImNTQ3NSI3PgE3NjMyFxYHJiMiBzYXHgEXFjMyAyY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwGTNzZOazYJAwMIOj0wUxQWEyEtGzU1Cw4NFQQCAgIJBjpWUTU1ixlGJCIHBAEEBCU8GOUFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhvFSC4wUBUWGgYqMCsaIhUlGxAvERQOBwcBAgIKBzkvMKRAFgEBAQUEKAHeFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAkABv6XAcYBygAkADEANwA9AEQASgBWAGQAcAAABRQHBiMiNTQ3NjU0JyYjIg4CFRQXFhUUIyInJjU0NzYzMhcWASY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwGVQwoLIAsvICM4Gi0hEzALIAoLRDozSVA1N/52BQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYbxltACCEMCypBLRoeDxomFj4tCw0gCD5dTDAnKysBKxcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAJ/5/+lQJGAcoAXwBqAHAAdgB9AIQAjwCdAKkAAAUWFRQHBiMiJwYHBicmJyY3PgE3PgE3PgE3JicmFRQHIjU0PgIzMhcWBwYHBg8BBhUUBxYXFjMyNjc1JicmNTQzMhcWHQEWFxYzMjc2NTQnJgciBiMiNTQ3NjM+ATMyJSY1NDY3FwYVFB8BJic3FhclBgcnNjcnBgcnPgE3BSYnNx4BHwEUByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcCEDY3OEljNjVnODIxEQ4IBxwEAgsJEhcDBQwhIB8PGiARHxwcAwMqFR0CCQEHJSMoNEcDBDYRHQgHXQUbJzUxJSAgLjcEDwMfFggTBQsFQ/42BQIDNQQEKiwaLBUkAR4aLB8kFfIkFSwNIxYBERUkHxYjDR8GNQIDBTUGsA4XCwsZDgkKFAsKEwsLEwoLFAoJHBYVG04tSEotLUdIAQIlIi0SIBgYAwEEAwUMDgEHEA0eAiAMFhEJFhYgLhkFDQEJCQ4DFxQTOSsOQB0JEiEEMmoJKRchIBotKxsoBwQgFAkEAQHPFxsNGgsJERgYEJYdKiAjFxoqHS0XI+gXIyAWIw5nIxctDiMWdxoYCgoTCxMWCRf6AgMDAjYDAQEDAVIDAgIDNwYGAAAACQAG/pgBxgHKADIAPwBFAEsAUgBYAGQAcgB+AAAFIzUWFRQjIic1JyYjJgcGFRQXFjMWNz4BNzYzMhUUBwYjIicmNTQ3Njc2HwEeAR8BFDMBJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAWkBAxkQBAkjMjAfHx8fMDkbAQMCBhMYDilUQi8uKytJTC8IAQICAQL+oAUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWG2gBBQkcDwMJKQEiIy4tIyQCJwIEBBMZCxc8MjFGQzEwAgE8CwEDAgEBARkXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgALAAb+kgHGAcoADwAgACgANQA7AEEASABOAFoAaAB0AAAFFAcGIyInJjU0NzYzMhcWBzY1NCcmIyIHFhUUBxYyMzInNCcGFRQXNgMmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcBjjQ4SUg3NjQ7T08uL1wfIRwyEQ8wLwYLBTNZMx0gMKgFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhvUQistLS1KSC0xNzh+GSUrJiIEKT48KQFmLBoaKS8bGwGpFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAv/i/6ZAiYBygA1AEUATQBYAF4AZABrAHIAfQCLAJcAAAUWBwYHBicGBwYnLgEnJic0NzY3Nhc2NzQnNDY3JiMiBwYHIicmJzQ3Jzc2NzYzMhc2NzYXFgc2JyYjIgcUFxQHFhcWNzYFJicmBxYXMgMmNTQ2NxcGFRQfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzceAR8BFAcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAiQCPDVLXjk4YE48BA4KCQEQLzZnKxYMAgEBGl82MQgQDAsJAQMBAQQONFdhNzViTTU3PQEpIzFdGQIDGV0xJiT+dRAmJSUlPRJBBQIDNQQEKiwaLBUkAR4aLB8kFfIkFSwNIxYBERUkHxYjDR8GNQIDBTUGsA4XCwsZDgkKFAsKEwsLEwoLFAoJHBYVG8NHLykCAUNDAQE4BhILCAsNDCgBAVcRIQ4KBwwFSiwPAwsHDwcIAQIDDzdDQgICLS1JLh0YSw4KDgpMAQIdGjIfDA0SKAEB2hcbDRoLCREYGBCWHSogIxcaKh0tFyPoFyMgFiMOZyMXLQ4jFncaGAoKEwsTFgkX+gIDAwI2AwEBAwFSAwICAzcGBgAAAAoABv6XAcYBygA5AEIATwBVAFsAYgBoAHQAggCOAAABFAYjIi8BBiMiJyY1NDc2NzYXJicmIyIHBhUUFxYXFjc2NzIVFAcGIwYnJicmNzY3NhcWFxYXFgcWJy4BJyYHBhcyASY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwGZFwoOChEdOCIdHR4YISgnBQsoMy4jJygmLyUXBwsgLh4lSDg3AgI8NEhEPQ4ODQICDB91Bg0IJQkGKRf+9gUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWG/7+DBQPGA8ZGB8gEg8CASAOCB8aHSwvHBoBAhEBBR8aDwoCLy5JRy4pAgItCiAgExEUJSsFCgQMEw4CAYAXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgALAAb+kAIpAcoAGwAsADYAQwBJAE8AVgBcAGgAdgCCAAABFCMiLwEGIyInJjU0NzY3NhcWFxQHFxE0NzYVAzY3NicmJyIGBxYVFAcyFzInNCcGBwYXNjc2AyY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwIpHQwEqTRFRDQ1NzRJRS4tAgdmJCP8HAEBIRYyBhAKLiwPBS9SMRgBAh8QDg+qBQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYb/rYmAy4pKSxFRy0qAQExMEcXFhkCryICAib9XRclIykdAgICJTs3KQFhKRkYLywXCBgYAY8XGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAAAAkABv6VAfIBygA9AEoAUABWAF0AYwBvAH0AiQAAARQjISI9ATQzMhcWMzI+AjU0JyYjIg8BFAYjIic0NyczIzc+ATc2NzIXFhUWBwYHIicVFBYzITI1NDMyFwEmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcB8jP+ylwkBwUVNBIiGhAeHSMuHgYNBRUBAgEBAQEBBgUoRDMqKwIuKDQqJBUcATYIFxED/hkFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhv+wy5MUBMDGQsVHRIjFxIbBgQIEwIGAQEBBwUnAR4eNTIjHAISNRkRExMPAeQXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAACf+U/pQCEwHKAEYAUgBYAF4AZQBsAHgAhgCSAAABFAcGIyEiJyY9ATcWFxY3Njc2JyYnJgcnNjc2FxYVFBcWFxY3Njc2JyYnJgYHJzYzMhcWFRQHBiMiJwYHBicVFDMhMj0BMwEmNTQ3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3HgEfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcCEx4ZIv41HB0gMh9QLSMoAgIsJCxLJDQseEAzPC0iLCcrJwECLCQsEBsNFycoRDE8PTU/YjMxZDswIwHKIjf9+gYGNQMBAQMqLBosFSQBHhosHyQV8iQVLA0jFgERFiMfFiMNHgMCNQIDBTUFrw4YCwsYDgkKFAoLEwoKEwsKFAoJGxYWG/7RHxAOERQabA8yAQIWFioqFxUBATQOSAIBHSM9JRkUAQEWFiopGBMBAQYFJAscIz07Ih47OQECGjoYFiABwRUdGxcJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0OIxZ3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAACQAG/poBxgHKADEAPgBEAEoAUQBXAGMAcQB9AAAFFAcGIyInJicmNzY3NicqAQciBgciJyY3NhcWFxQHBgcGFxYXFjc2NTQnJic0MzIXFgEmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcBlzs7Tz0yNwMBMygCAR0BCwoDCQYkAgIlJzA1BCoqBgUmGyUtJSU6EgMlBwxi/nQFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhu5Ty8vIyU8MRINGg0CAQEBJiECBBQYKxkYGhohEg4BAR0cLD4fCxQoBTUBARcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAAAAoABv6QAcYBygAsAEkAVgBcAGIAaQBvAHsAiQCVAAAFFAcnLgEnJiciBhUUHgIzMjcyNzYzMhcUBwYjBicmNTQ+AjMyFxYfAR4BAyI1NwYHBicmJzQzMhcWMzI/ATYzMh0BMzIXFCMBJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAVgVBgMIBRwnJTENGB8SKBoBBQQNEgMMIkA0JSQTIS4bHR0UFAYCAhkTARgoOS8IARMGBSYtJREPCAgRIhMBFP6YBQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYbVxICBQQJBRcCLyUSHxgOHgUMFAYQKwElJTQbLSESDQoUCAQH/uITJwgCAiEECxUDGgwMBxY5ExMCIhcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAAAAn/w/6bAh8BygBRAFsAYQBnAG4AdQCAAI4AmgAAEyImPQE0NzYzMhYVFAcGIyImNTQ3NjU0JyYjIh0BFBY7ASY1NDc2MzIXFhUUBw4BByI1BiMiNTQ3NhcUIwYnPwE2NTQnJiMiBwYVFBcWFRQGIwMmNTQ3FwYVFB8BJic3FhclBgcnNjcnBgcnPgE3BSYnNx4BHwEUByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhc5OT0iHCktOSAJCwsRCBMbDRQlGySnHzQ0SkgyMj4ECQcBCAgaDgECAQEBAwosIyIxMSYkJgcTDPoGBjUEBCosGiwVJAEeGiwfJBXyJBUsDSMWAREWIx8WIw0fBjUCAwU1BrAOFwsLGQ4JChQLChMLCxMKCxQKCRwWFRv+nDk9iyIVEjsuLyMJEQsKChAbIg4HF4slGyw5RzQzMjJKUzUDCAUBBRsPCQEBAQEBAwkkOjMjIiMkMTogBA4PIAIWFR0bFwkRGBgQlh0qICMXGiodLRcj6BcjIBYjDmcjFy0OIxZ3GhgKChMLExYJF/oCAwMCNgMBAQMBUgMCAgM3BgYACQAG/pMBxgHKAEMAUABWAFwAYwBpAHUAgwCPAAAXNDcnFDY3Njc2MzIXFhUWBwYjBicVFjMyNzI2NzIdATc2FxQrASI9AQYHBi8BJj0BNDsBMhcWFzI+AjU0JiciDwEiAyY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyF3UDAgEBCicZITUjJAElJTMrHBooGTYCBAQPGhMBDDYTES42FxANEwcLBBkrEh8XDjMjHyUWEW0FAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhtSAwUCBAEFFhQNIiM1MyQlAhgfERUBARUrAwMTGBImBQoBDwwKFT0TBxwCDhgfEiMxAR8TARgXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAACv9g/pkCVgHKAF0AcQB8AIIAiACPAJYAoQCvALsAAAUWFRYHBiMiJyY3MhcWNzY3NicmIyIHBgcOAQ8BBgcUFx4BFxUUDgIjBicmPQE0NzY1NC8BJi8BJicmIyIHBgcGFxY3MjcyFxYHBicmJzQ3Njc2HwE2NzYXNzY3NgUmNTQ3NTQnIh0BFhUUBxUUMzI1AyY1NDY3FwYVFB8BJic3FhclBgcnNjcnBgcnPgE3BSYnNx4BHwEUByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcCNCEBUS84UAIBJQkDMy0tAgIxGiUoHQwOAQMDBQkBCgICAQ8aIhIkHBwEDAwCBgMBCx8hFDoiEAECNC4vCAQiAgJONzVPAiE0ZT4sCxwvLh0MKUJh/vEPDxQTEBATFOIFAgM1BAQqLBosFSQBHhosHyQV8iQVLA0jFgERFSQfFiMNHwY1AgMFNQawDhcLCxkOCQoUCwoTCwsTCgsUCgkcFhUbXSk3Xi8bKSUBAQ4aGTUwHRAUAxACBQQLDhIaEAQJBRQSIBgOAhsbJBUQBA8YCRcHBgYDDgwLKhQbMB0bDAEmJgIBHjBcNSlCAgIdByMCAScIHAEB7xwsGiMUCgMNFR8dJB8NDw8BvxcbDRoLCREYGBCWHSogIxcaKh0tFyPoFyMgFiMOZyMXLQ4jFncaGAoKEwsTFgkX+gIDAwI2AwEBAwFSAwICAzcGBgAACv90/pcCIAHKACkAOgBHAE0AUwBaAGAAbAB6AIYAAAU0JyYnJgYHFBcUBwYjIicmJyY3Njc2FzYzMhcWFxYHBicmJzQ2MxY3NiUmNyYnJicmBwYVFBcWFxY2AyY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwHfJyMxKkYNATU1T0o3NgICOzRMYzg4YUs3OQEBQz1OLAISEy4mQv7KAQEHJiEsLiUoKCIxK0aUBQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYbxC4dGAICMCUMBUUvMC4sSkgvKAIBR0cuLklLLioCARcRDgIUISkQEicXEgIBGhwuLxwaAgIuAbAXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAL/4j+lAIvAcoAFwApADsARQBLAFEAWABfAGoAeACEAAAFFgcGIyInBgcGJyYnJjc2NzYXNjMyFxYHNCcmJyYHFBcUBxYXFhcWNzYlJiciDgIVBhcWFxY2NzQnNAMmNTQ3FwYVFB8BJic3FhclBgcnNjcnBgcnPgE3BSYnNx4BHwEUByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcCLQI6N0xhNzhiSTc2AQE5NEtiODhgTTU4PicjMmEXAgIJJB8sLycm/swQahgsIRQCJiYvK0QLAa0GBjUEBCosGiwVJAEeGiwfJBXyJBUsDSMWAREWIx8WIw0fBjUCAwU1BrAOFwsLGQ4JChQLChMLCxMKCxQKCRwWFRvGSi0uRkQCAS8uSUkuKQICRkQpLUwyGhcBAVALCgsKKBYUAQEdHURMAw0aJRgvGx0BASwoEAUQAWgVHRsXCREYGBCWHSogIxcaKh0tFyPoFyMgFiMOZyMXLQ4jFncaGAoKEwsTFgkX+gIDAwI2AwEBAwFSAwICAzcGBgAACQAG/pMBxgHKAC0AOgBAAEYATQBTAF8AbQB5AAATBic0JzQ3NhcWMzI3NiciByMiJyY7ATYnJicmBwYHIicmNzYzMhcWFxYHFhUUASY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyF9F4JAMfEwkUUF0GBiwGAxsdAQIgITAHCVpWDAUaDgoIARiIPS03AgEeHP6YBQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYb/pcEUwQJHAEBEykkGwgBHiEFHCsCAi8ZAQwIEl0YIDciGxwgYQIXFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYACwAG/pYBxgHKAA8AHwAoADUAOwBBAEgATgBaAGgAdAAABRYHBiMiJyYnJjc2NzYXFgc2JyYnJgcGFRQXNjc2FzYHJicmBx4BFxYDJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAZYBPDdLSzc3AQE7M01MNzk+ASojMjElJQopR0UwCjYaLycfESMTIvAFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhvFSDIrLi5JSS4qAgEsLkoxGxcBAhscLxYTLQIBMBM+GgEBHwgIAQEB4BcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAACgAG/owBxgHKADcARQBSAFgAXgBlAGsAdwCFAJEAAAEUByInBicmJzQ+AjMyFxYXNSYnLgEnJicmJzQzMhceARcWFxQHIi8BJiMiBxUWFxYXFgcWFxYnLgEnJiciBwYXFhcWNwMmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcBpx4HOzpTmwIOGiIUQmsGEgQjBSMeQBcHAl4rHQIEAhMDHgYPAx0TEg0HW14LBAsMFBCmDCccHRgQChoyGykjIPYFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhv+rSABIBcECGITIRgOUAYLChsUAgkGDCMPFEsUAgQCBBghAQcDEQoCChUYVBQeBQ0LHAoaEBEBDB4SCgMCBQHYFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYACQAG/pUBxgHKACMAMAA2ADwAQwBJAFUAYwBvAAAFFgcGIyInJicmNzYzMhcWBwYHBhcWFxY3NjU0JyYnNDcyFxYBJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAZQCOThMTDg2AgFjBQodAgEQQAIBKCMzNCMlQRECIQgHYv54BQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYbwkwuLy8uTGI7BCAQCiVCMB8aAgIfHy9AJgcUHwIENQELFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYACgAG/pUBxgHKACIAKwA4AD4ARABLAFEAXQBrAHcAAAUUBwYHBicmJyY2NzYzMhcWBwYVFBcWFxY3JicmJzQ3NhcWBzQnBhcWFzY0ASY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwGTNjVPTTc2AgI3KwUKHAIBEEAoIjQxKEQQBAEtEBlkPy8UCgotAf64BQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYbwEwuLQICMC5NL1QXBCAQCSVAMh4bAQEhGD0JHzcpGA0zaTEnHSEhDQIKAXoXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAAAAoABv6SAcYBygAdAC4AOwBBAEcATgBUAGAAbgB6AAAFFgcGBwYnJic0PgI3PgE3MhUUFjMyNTQ3MhceAQc0JwYjIicGBwYXFhcWPgIBJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAZQCNTNTUDM1AQ0ZIhQECgcXGhYqGgULMDU9IB0/QCEXAgEnJDEZLSIT/rUFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhvGRDAwAgIuL0sYLigiDAECASEWHjQfAgMUVTQsID88HyouIBoBAQ8dJwGPFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAAACf+q/poCMQHKAEoAVQBbAGEAZwBuAHoAiACUAAABIiYnJic0MzIXFjc2NzYnJiMiBwYHFRQHBiMiJyYnNyY1NDc2NzIfARYXFjY3JicmJyYHIgcGByImNz4BNzY3Nhc2MzIXFhUUBwYBJjU0NjcXBhUUHwEmJzcWFyUGByc2NycGByc2NwUmJzceAR8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwGBCRYTEgIeCw01LR4CAigiLzAoGQY5NUpTOwgKAgULCQ8QCAIqPDNEAwEnIy9BHQQECxMNEgMBCAcuYmE3NGJLNTY1NP5EBQIDNQQEKiwZKxUkAR4aLB4kFPIkFSsZLAERFCQeFiMNHwMCNgIDBTYFrw4YCwsYDggLFAoKFAoKFAoKFAsIGxYUHf6aAQYGFSAFBSUZKy0dHB8UKghILSw4DAgBBwgMCwcDEgEqAgI2My0cFwECIwYWARcSAg0LNgICSEYtLUlGLSwCFhcbDRoLCREYGBCWHSogIxcaKh0tFyPoFyMgKh1nIxctDiMWdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAAAAAKAAb+mAHGAcoAJgAuADsAQQBHAE4AVABgAG4AegAABRYHBgcGJicmJyY3Njc2MzIXFgcGBxQXNjc2FzY1NCcmJzQ3MhcWByYjIgcWFxYBJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAZoCOjdMK1UZIQICHB0qBwgdAgIQQQEEMElIMARCEgEiBgliaCAyMyIhNC/+/QUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWG7xLLy4CAiQjLTgmLy8XBCARCCc/EAwsAQIwCxJBIwsQIAEDNbokJBgCAgHcFxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAAJ/4/+mgIwAcoASQBVAFsAYQBoAG8AewCJAJUAAAEiJwYjIicmNTQ3Njc2NzIWFxQHBhUUFxYzMj4CNzYnJicmJzQzMhcWFxUWFxYzMjc2NzYnJiMiBiMiJzQ3MjcyNzIXFhcUBwYBJjU0NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNx4BHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAXlfOjVlSzU3GxsoBAsKEAEMQyYlMxgsIhUCAQECOgwBHAgHVwEEHyY1MiUlAQIoJy8IGgYaAhUIEw4FRjc2Ajc4/ksFBTYDAQEDKiwaLBQkAR8aLB8kFfMkFCwNIxYBERYjHxYiDh4DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhv+mk9NMTFEKywqFgQBDwwKCidGMyEhEB0pGAQKQyMHDRoELmkKLhwiHx80LyEhBhoRCAQBLy5HRy0uAhcXGxkZCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctDiMWdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAKAAb+lgHGAcoAIwArADgAPgBEAEsAUQBdAGsAdwAAASInNTQnJgYHMhYVFCMiLgInJjc2NzY3NhYXFh0BMzIXFCMnNicmIxYXFgMmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcBRhMCWx80CDY9GBouJBcDAgICJyg1JkUQCh4UAha1AhYPJwcfDaIFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhv+lhS6XwMBJx05LSwRHywaBAg1ISACASsjFSWnFBOCHhYRHhgLAZYXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAAAAn/iP6YAiQBvwAFAAsAFAAcACUALAAzADwAgQAAEwYHIzY3EzY3FwYHNzY3PgE3FwYHATY3FQYHBgc3LgEnJic1FhcFHgEXByYnFx4BFxUmJyUuAScmJzcWFwMGJyYnJjU0JiciDgIVBhcWNzI3MhUUBwYHKgEHIicmJyY3Njc2FxYVBhcWFxY+AjU2JyYHIgcGJjU0NzYXFhcWBwZfDxNGBDK4ITkyOlJsCQkKCwFGAzj+vDlSFSMQEMwIDwglFk4+/swEEw4zNAR7DiwfSz0BNgMGAgwPMTYFTkU8KA0ERzMYKiATAR8tNQsPHx0IDwYKBEQ1NQECODBNjiUFASckMhgpIBMBKiYyCAcPGjVNQDMCATQ1AT0YPlI5/skIHTI0A3wPERMcCwFNPgFFMwZHARQHDAMFCAQTAUcDNrkPKxwyOk5rCBMKRgQ3tQoRCCYRMjtR/bIBLx89BAo0PQEOHCYYKBsnBgUgFwgCAQEvL0RJLyoCAogIDDAhHAIBDhsmFzAdGwEDBBIRGwUINitKSissAAAAAAn/tv6UAhEBygBPAFoAYABmAG0AdAB/AI0AmQAABRQHBgcGJwYHBicmJyY3Mh4CMRcWFxY3Njc2LgIjIgcGBwYjIjU0NxcVByM/ATY3NhcWFx4BFxY3NjU0JyYjIgcjIgciNTQ3MjYzMhcWASY1NDY3FwYVFB8BJic3FhclBgcnNjcnBgcnPgE3BSYnNx4BHwEUByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcCETEvRVwyN1VVLwYJCSMHCwcEAx08LiIhAgERICsYFiIhCAcQHQUBAQECDi5TRS8vAQJBMy8hIiAlKAYKBwgLGSAGEApDMDD9+wUCAzUEBCosGiwVJAEeGiwfJBXyJBUsDiIWAREVJB8WIw0fBjUCAwU1Bq8OGAsLGQ4JChQLChMLCxMKCxQKCRwWFB3ERTAvAgJRTgEBPgcRHAUICQgCJwEBIyExGCgeEA8OExMcCAcBAgEFEDgCASosRDlBAgIjIjAwICMCBBoWBQMxMgE0FxsNGgsJERgYEJYdKiAjFxoqHS0XI+gXIyAWIw5nIxctDiMWdxoYCgoTCxMWCRf6AgMDAjYDAQEDAVIDAgIDNwYGAAn/nP6aAjgBygBEAE4AVABaAGEAaABzAIEAjQAAASImJyY1NDciFhcWNzY1NCYnJgYHFRQHBiMiJyYnJjc2MzIXFAcGBwYeAjMWNzY3NSYnJic0NzIXFhc2MzIXFhcWBwYBJjU0NxcGFRQfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzceAR8BFAcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAYsMFBQTHQUMEjEyI0g0MUQHNzdMTDEyAQJCCAwcAQktAgESISwaMSwgBAMxBgIdCwkaETZeSDU0AgE2Nf4/BgY1BAQqLBosFSQBHhosHyQV8iQVLA0jFgERFiMfFiMNHwY1AgMFNQawDhcLCxkOCQoUCwoTCwsTCgsUCgkcFhUb/poBBwYQFgIDAggsHjAzPwECOzAKRi4vLS1JXD0IGQkJLEoaKR0QAiMYLhM+LgYMGAIJGCNHLy5IQzAwAhgVHRsXCREYGBCWHSogIxcaKh0tFyPoFyMgFiMOZyMXLQ4jFncaGAoKEwsTFgkX+gIDAwI2AwEBAwFSAwICAzcGBgAACv/C/pkCTAHKAEYAUABdAGMAaQBvAHUAgQCPAJsAAAUWBwYHIicmNzY3Nhc+ATc0JicmBwYXFgcGBxYXFgcGBwYnJjcWFxY3NicmJzUyNzYnJicmDwEUFwciJyY3NjcyFzY3MhcWBy4BJyIHFxY3NgEmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnNjcFJic3Fh8BFAYHJzY1NCYnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwJKAj01TUIDAy0oOSYSAQECSTkQMTAEBCMZGRUdIgQze3U1BTUbXz8pBhEnNzMpEQQjRU8gBBsXNgIBUCwsUzo8U0w4N1ELEQhEFhQUKQz+NgUCAzUCAQECKiwZLBQkAR4aKx8kFPIkFCwZLAERFCQfLBkfAwI1BAICNQWvDhgLCxgOCAsUCgoUCgoUCgoUCwgbFhQdvUwvLQIaNiIfAgIIBA8LNjwDARUXCw0YEAgDFxgSZQECZAQaSwICMQcOFAUxGQoKLQIBIwgDByUvPRQLATo5AS8ukQQDATYICBEEAd0XGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgKh1nIxctHSp3DRkMChEXCxQKCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAAAAkABgAAAcYDRwA1AEIASABOAFUAWwBnAHUAgQAAEwYnJicmNyYnJjY3NhcWFxQjIi4CIyIHBhYXFhcWFxYGIyInLgEvASIHFBcWNzYzMhUUBwYBJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIX3C4iJgICJhMCAjYqMR0HAiMGDAwLBhEHCRUkJAsdAgEVDwkNAgkFGi0FNREPChMmBBv+6wUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWGwHgARsfPjUgGSQqMQECIgsNJAcJCA0MHQYECxIYDhcIAwYEBywxAwIUEyMIBzT+0BcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAv/nwAAAeMDRAAPAEUAVgBjAGkAbwB2AHwAiACWAKIAAAEWBwYHBicmJyY3Njc2FxYFBgcGLgI1NDcmNTQ2MzIWFxQHBgciJiciBwYXFhcWFxYXFgYjIicmLwEiBxQXFjc2MzIXFCU2JyYnJgcGBwYXFjMyPgIBJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAeIBNjRPSzU0AgI1NU5MNTX+lRlFGiwgEiYWNyceNgQLDA0PFQwKCgwKDSMjCh0CAhYODAoFCxktBTURDwoTIgIBIQEiIi4tISABASEhLhgoHhL+cgUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWGwKJTTU0AgI3Nk1NNjUCATc34jECAREgLBo1IBokKDQdGwsOCQEVAQcRDg4GBAwSGA8WBwYIBiwwAwIUEyMGiS4iIQIBIiMvLSMiEh4q/kEXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLR0qdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAL//MAAAHHAw0AEgBBAEkAVgBcAGIAaQBwAHwAigCWAAABFgcGByIuAic0PgIzMh4CBSImNTQ3JjU0NjcyFhcUIyInJgcGFxYXFhcWDwEGJy4BJwYVFDMyNzY3NhUUBwY3NicmBwYXFgEmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNx4BHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAXQBHRsoFCEYDwEOGSIUEyMZEP7uMzwfEjIlHjMEJQ0MFA0KCQgWIQcSBQEPEwkVDBorEgULFCYhGqgCGRQBAhcX/uAFAgM2AwEBAyosGiwUJAEfGiwfJBXzJBQsDSMWAREWIx8WIg4eAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYbAmonHRsCDxsjFBMhGQ8OGSK4OzM0GBYhJTEBHRsiDBIPCg0LBgMEBxsGHQECBAQSFCsLFQECJR8SEKUUAQEWGAIC/mQXGw0aCwkKFAsLFAmWHSogIxcaKh0tFyPoFyMgFiMOZyMXLQ4jFncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAAADP+UAAAByANnABEAQwBUAFwAZwBtAHMAeQCAAIwAmgCmAAABFAcGBwYnJicmJyY3Njc2FxYFBgcGJyYnJjcmNTQ2NzYWFxQjIicmJyIHBhcWFxYVFCMiLgIjJyIVFDMyNzY3MhcUJTQuAiMmBwYHFBc2NzYXNgcmJyYHFjMyASY1NDY3FwYVFB8BJic3FhclBgcnNjcnBgcnNjcFJic3HgEfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcByDY0Tko3CQIpAgE1NU5MNTb+ihY8MR0ZAgIjEionHCsEHwsKBhAPBAs4HwgZHQYLCAcBFSssEQwJDx4CASwRHikYLx8gAQUnPTk2BzUbJCEZGiIi/twFAgM1BAQqLBkrFSQBHhosHiQU8iQVKxksAREUJB4WIw0fAwI2AgMFNgWvDhgLCxgOCAsUCgoUCgoUCgoUCwgbFhQdApVNNDQCAjYGCDJDTjU2AgE3Ocs2AQEqJDU5Ix4mKDwBASQbJwwLAxIkDAUMEhwoBwkIBi84ExICJgp1GCofEgEiITEQFCcCAS8SSR0CAR8T/o8XGw0aCwkRGBgQlh0qICMXGiodLRcj6BcjICodZyMXLQ4jFncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYADP+4AAAB8wM4ABMAIwArADwARwBNAFMAWgBhAGwAegCGAAABFA4CByIuAic0PgIzMh4CJxYHBgcGJyYnJjc2NzYXFhc2JyYHBhcWJzYnJicmBwYHBhcWMzI+AgMmNTQ2NxcGFRQfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzceAR8BFAcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAfMPGiMTEyEZDwEOGiITEyMZEMwCNjZNTDQ2AQE1NE9NNDWGAhoTAgIXFsoCIyAvLSEhAQEhIS8YKB4S1AUCAzUEBCosGiwVJAEeGiweJBTyJBUsDiIWAREUJB4WIw0fBjUCAwU1Bq8OGAsLGQ4JChQLChQKChQKCxQKCRwWFB0CUBMiGhABDxsjExMiGQ8OGSMZTTQ0AgE1OUpONTUCAjc4eRQCARcXAgFGLyEhAgEiIjAtIyISHyn+ThcbDRoLCREYGBCWHSogIxcaKh0tFyPoFyMgFiMOZyMXLQ4jFncaGAoKEwsTFgkX+gIDAwI2AwEBAwFSAwICAzcGBgAACgAG/pcBxgHKAA0AGwAoAC4ANAA7AEEATQBbAGcAAAEiNRE0NzYdATMyFxYjJxQrASInJjsBNTQ3NhUnJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXARUjIyVFIwICJ8IlaiMCAidHIyWyBQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYb/pckARkiAgEl9iQjJCQjJPYiAgEl3hcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAkABv/zAzMDUQAtADoAQABGAE0AUwBfAG0AeQAAARQjIicmJyYnJgcGHQEUBxEUBwY1ETQuAiMmBwYHBgcGJyY3Njc2FzYzMhcWASY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwMzIiIBAhsaIiAbHQEjJBAZIRAcHxgGBR4fAgE0MTpJNTRGOjIz/NgFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhsCwiIlGhQUAgIWFRw5BwP9mCICAiYCqw8ZEgsBFhAnGwIBIzkrKAIBOjkqKv22FxsNGgsJChQLCxQJlh0qICMXGiodLRcj6BcjIBYjDmcjFy0dKncNGQwKChMLExYJGfgCAwMCNgMBAQMBUgMCAgM3BgYAAAAKAAb+kQIeAcoAGwApADYAPABCAEkATwBbAGkAdQAAARQjIi8BBgcGJyYnJjc2NzYXFhcUBxcRNDc2FQM2JyYjIgYHBhcWMzI2ASY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3Fh8BFAYHJz4BNTQnNxYHDgEjIiYnNx4BMzI2NxEuASMiBgcnNjMyFwIeHAkEqS9DRzEwAgIyMkhGMjECDmUhIdwBHx4sLTkBAh8fKy06/soFAgM2AwEBAyosGiwUJAEeGSwfJBXzJBQsDSMWAREWIx8sGR8DAjUCAwU1Ba8OGAsLGA4JChQKChQKChQKChQKCRsWFhv+tCMDLCoCAzQySEgyMgEBMzVGICUbArYgAgEj/aorIB89LSwfHz0BoBcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAr/v/6VAccBygAdACsANgA8AEIASABPAFsAaQB1AAAFFgcGBwYnJicGIyYnJic0NzIeAhcyPwE2NzYXFgc2JyYjIgYHBhcWMzI2ASY1NDY3FwYVFB8BJic3FhclBgcnNjcnBgcnNjcFJic3HgEfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcBkwEyMkc/MTAJFBMrJQcDIQkPDg4JEQsfMWBGMjBAAiAeLC05AQIfHystOv68BQIDNQQEKiwZKxUkAR4aLB4kFPIkFSsZLAERFCQeFiMNHwMCNgIDBTYFrw4YCwsYDggLFAoKFAoKFAoKFAsIGxYUHcBHMTEBASwsPwkCKAcQHgIJCwsBDilRAQEzMkksIB89LiweHzwBnxcbDRoLCREYGBCWHSogIxcaKh0tFyPoFyMgKh1nIxctDiMWdw0ZDAoKEwsTFgkZ+AIDAwI2AwEBAwFSAwICAzcGBgAACv///o8CWQNRADUARwBTAFkAXwBmAG0AeACEAI8AAAUUBwYHKgEvASInJicmNRE0NzY7ATIXFh0BFCMiPQE0JyYrASIHBhURFBYzFyY1NDc2MzIXFgc0LgIjIgYVFB4CMzI+AgEmNTQ3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3HgEfARQHJz4BNTQnNxYHBiMiJzceATMyNjcRLgEjIgcnNjMyFwI3MTBJBQoFtDIfHB47MTha4F88HCQkDSk54DwhH0k2RSQxMkRHMjFCEBsnFi04EBslFRYmHBD+qQYGNQMBAQMqKxssFiMBHhsrHyMW8iQVLA0jFgERFyIfFiMNHwY1AgMFNQawGxYWGwkKFAoLEwoKEwsSFgkbFhYbyEUxMgEBAQ8NHj1VAydeNDtTJScOJCQOERE1JR9B/Nk8SAouO0gyMjIySBUmHhE9LRUmHRAQHSYBiBUdGxcJChQLCxQJlhwsHyQWGywcLRYk6BciHxYjDmYiFy0OIxZ3GhgKChMLExYJF/kGBjUDAQEDAVMCAwU2BgYAAAAACv+t/o4CHAG/AAYADAAVAB0AJgAsADMAPABmAHQAABMOAQcjNjcTNjcXBgc3Njc+ATcXBgcBNjcVBgcGBzcuAScmJzUWFwUWFwcmJxceARcVJiclLgEnJic3FhcTFCMiLwEGBwYnJicGIyYnJic0NzIeAjMyPwE2NzYXFhcUBxcRNDc2FQM2JyYjIgYHBhcWMzI2YAgSCEcEMrgjNzI6UmwJCQoLAUcEOP68O1AWIhAPywgPCCUWTj7+zQgcMjUEew4sH0s9ATYCBgIODjE3BVwdCQSpLUU9MzEHFhIrJQcDIQkPDg4JEQsfMl9HMjACDmUiIdwCIB4tLTkBAR4fKy07AT0NKx5SOf7JCB0yNAN8DxETHAsBTT4BRTMGRwITCAsDBQgEEwFHAza5HzcyOU9rCBMKRgQ3tQoRCCcQMjxQ/cskAywqAgEsLD8IAicHER8CCwwKDilRAQEzMkkgJRoCuiEBASP9pSwgHz4tKyAfPQAACf/J/osCIwHKACsANgA8AEIASQBQAFsAaQB1AAABFCMiLwEmPQEHBgcGJy4BLwEmJzQ3NjcyFxYXFhc2PwE2NzYdARcRNDc2FQUmNTQ2NxcGFRQfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzceAR8BFAcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIXAiMdCgTbGVQcLhAfDBQIPQYDCwsODxcNER8PCwuTDRUelyIi/ekFAgM1BAQqLBosFSQBHhosHyQV8iQVLA0jFgERFSQfFiMNHwY1AgMFNQawDhcLCxkOCQoUCwoTCwsTCgsUCgkcFhUb/q8kAzsHGouSMAcBEQcOCEwLCw4LBgMbFBYmAwMR/xUCASjtKgK/IQEBI+gXGw0aCwkRGBgQlh0qICMXGiodLRcj6BcjIBYjDmcjFy0OIxZ3GhgKChMLExYJF/oCAwMCNgMBAQMBUgMCAgM3BgYAAAoABf6VAl4DRABAAFIAXwBlAGsAcgB5AIUAkQCbAAATIicmJyY1ETQ+AjsBMhcWHQEUBwY9ATQnJisBIhURFBcWOwEmJwYjJicmJzQ3Mh4CMzI/ATY3NhcWFxQHBgc3NicmIyIOAhUGFxYzMj4CASY1NDY3Fw4BFRQWHwEmJzcWFyUGByc2NycGByc+ATcFJic3HgEfARQHJz4BNTQnNx4BBwYjIic3HgEzMjY3ESYjIgcnNjMyF8ovIh0cOxoySS7fYDwbIyQSKTXffCUlMz0hBxcTLykHAyMKEA8QChILIjRoTTU2ATY0TG4BISEwGCgdEQIhIi0YKR4S/pMFAwI2AwEBAyorGywWIgEfGysfIxbzIxUsDSMWAREXIh8WIg4eBTUCAwU1AgOvGxYWGwkKFAoKFAoWEhIWCRsWFhv+lQ8OHTtXAxYuTDYdUiYmDiMCAicOEBUyhvzqPSQlKjMJASwIESECCw0LDyxWAgI3OUxMNDYDuS8jIRIfKhgvICISHikBhhYbDRoMCgkVCwoUCpYcLB8kFhssHC0WJOgXIh8WIw5mIhctDiMWdxgZCQoTCxMWCgwZ7QYGNgMBAQMBUwUFNgYGAAsABv6nAcYBygAKADAAOABFAEsAUQBYAF4AagB4AIQAABciJjU0NhceARUUFwYjIjU0NzY1NC4CIyIOAhUUFxYVFCMiJyY1NDc2MzIXFhUUJwYVFjMyNSYDJjU0NjcXDgEVFBYfASYnNxYXJQYHJzY3JwYHJz4BNwUmJzcWHwEUBgcnPgE1NCc3FgcOASMiJic3HgEzMjY3ES4BIyIGByc2MzIX5xcgIRYYHx4FCSARQRMhLRkaLCATQA4eCAdfNDVOTTU1thEBEBEB7AUCAzYDAQEDKiwaLBQkAR4ZLB8kFfMkFCwNIxYBERYjHywZHwMCNQIDBTUFrw4YCwsYDgkKFAoKFAoKFAoKFAoJGxYWG+0fGBYiAQEiFjVoBCESCCRAFicbEBAbJxZAJQkQHwQ1ZEYwLy8wRmh8Ag4TExABWBcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAACwAG/qMBxgHKAAoAMAA4AEUASwBRAFgAXgBqAHgAhAAABRQGBwYmNTQ2MzIXFAcGIyInJjU0NzYzMhUUBwYVFB4CMzI+AjU0JyY1NDMyFxYHNCMiBxQXMgMmNTQ2NxcOARUUFh8BJic3FhclBgcnNjcnBgcnPgE3BSYnNxYfARQGByc+ATU0JzcWBw4BIyImJzceATMyNjcRLgEjIgYHJzYzMhcBJR8YFyAfGDd/NTVNTzQ1YAUKHg9AEyEsGhktIRNBESAJBWGlERABEQ/yBQIDNgMBAQMqLBosFCQBHhksHyQV8yQULA0jFgERFiMfLBkfAwI1AgMFNQWvDhgLCxgOCQoUCgoUCgoUCgoUCgkbFhYbuhYhAQIiFxcgNEYvMDAvRmQ2BCAQCSVAFyYbEBAbJhdBIwkRIQQzaxISDwIBfhcbDRoLCQoUCwsUCZYdKiAjFxoqHS0XI+gXIyAWIw5nIxctHSp3DRkMCgoTCxMWCRn4AgMDAjYDAQEDAVIDAgIDNwYGAAAD/e/+o/9e/+cACgAwADgAAAUiJjU0NhceARUUFwYjIjU0NzY1NC4CIyIOAhUUFxYVFCMiJyY1NDc2MzIXFhUUJwYVFjMyNSb+qRggIRcXHx8HCCASQBMhLRkaLCATPw8eCQVhNjVNTjU0tREBEBEC8R8YFiIBASIVNmgEIRIIJEAWJxsQEBsnFkAlCRAfBDRlRjAvLzBGZ3wCDxMTEQAAA/3z/qP/Yv/nAAoAMAA4AAAFFAYHBiY1NDYzMhcUBwYjIicmNTQ3NjMyFRQHBhUUHgIzMj4CNTQnJjU0MzIXFgc0IyIHFBcy/uMfFxchIBg2fzQ1Tk40NmEFCR4PPxMgLBoZLSETQBIgCAdgpBEQAREPuhYhAQIiFxcgNEcuMDAuR2Q2BCAQCSY/FyYbEBAbJhdCIgkRIQQzaxISDwIAAAH/VwAAAO0ByAAxAAAnJjc2JxY3Mjc2NyY3NCcmJyYnJjc2MzIXFgcUJyY1JiMiBwYVFhcWFxYXFgcGBwYnJqUEJCUFNjUwMC8LAQEeLwUzAgE9ERwmICUBJCMBERMKFwQ4Hg4ZBAYJEkBASWtQBBISBTcCHh4sCQoiCxQDJjU6IgoUFiUmAgIiDQcMDiAWCxARKh4QSDEyAQIAAAH/kP/+AtsBxgBNAAA3PgE3Njc2FhUWFxY3Njc2NyY3NCcmJyYnJjc2MzIXFgcGJyY1JiMiBwYVFhcWFxYXFgcGBwYnJicGIyInJicmNjMyFx4BFxYXFjMyNzb+AwYCBSERFAYsLj8xLi8MAgIeMAUyAgM/ERwmICYCAiIjAREUChYEOB8NFwYEBxFBQkd/Q0p3XkIwDQMhEA8GBAcEDw4uQEIuELQKFQsXBQIUBj8vLwECHB4sCggkCRQEJTc6IQoUFyQmAgIiDQcMDiAWDA8RKh4QRzIyAQJtb0MxOwoTCQgPCCEOLjEQAAAAAv3w/pn/Xf/jACMALwAAAwYHBicmJyY3NjMyFxYXFgcGBwYnBgcGFxYXFjc2NzIXFhcGJxYzMjcjLgEnJiMiwzdaSjg2AgI8NkpqOAkDAwg3QGguFQIBKSIxNzQHEwsLBwUHzxhIIyILAQQEJjwV/tM3AgEtLExIMC1QGBIYCCkCAl4YJC4dGAICMBACCwQTD7RAFgEFBScAAAAAAv3i/pYASv/eADEAOgAAEwYHBgcGJwYHBicmJyY3NjMyFxYXFgcGIwYnBgcGFxYXFjc2NzIXFhcWMxY3NjcyFxYlJiMiBxYzMjdKBRA4Vkw5MVNLNzYCAjs3Smk3CQMBDDs1aC4WAQEpIzE1NAcTCg4JJyIcNTQHEgsLBv61OCQWHhtLFSf+9A4WOAEBMjABAS0sS0gvLU8YEw4OLQJcGiEtHRgCAjAPAgwUEQ4CMBABCwRxKQlFEgAAAAAC//n+mgHJAb8AYgBqAAAFFA8BBicmJxUUFjsBMicmDwEGIyI1NDYzMhceARUUDgIrASImPQE0MzIXFhcWPwE1BiMiJyY1NDc2MzIXFh8BFh8BFhcUIxYVFAYjIicmNycmIyIHBhUUFxYzMjc2NzMyFQMGIyc1MzUzAcgPHVNpTzYiMcMWBwEHAhMJICgWRAsBAQ0YIBPDUUsfEA5AU1I9D0BeX0VFQ0NjNTYsHgsDBAEBAgMFGBAUCgICCjBNRS4uMDBBVCYKFiclnQQZRCc6HBEMFTwDASFkMSQhBAQBCiMXFzgFDAYUIxoPT06tKA0+AQEtCjI4RUVfYENDGhchDgMGAQICAQoGEBYVAQILOi4uQkIwMDUMAiP+vRwBN0AAAAAAAf56/tz/Pv+fAAsAAAUVIzUjNTM1MxUzFf71MUpKMUnbSUkxSUkxAAT+Fv6Y/3X/4wAoADQAPABEAAAHFAcGDwEiNTQ3Mjc2NzQvAiIHBg8BFAYjIiY1NDc2MzIXNzYzMhcWBxQGIyImNTQ2MzIWJyYjIgcGMzIXNCciFRQXMoshDRcnHx8bDw8CEQ8VERQGAgY2Ki44Hx8oKiABICklHyEkKBoZJyMdGyeiCSoxAQI0J3gMDw8MfigcDQYHGhYBEgsQEQ4KBA4EBxEqNzMrKxsbHwYZGSbKGigpGR0kJpIoLS17DQIPDQEAAAAEACT/8gLNAqQACgANABEAIwAAJSMVIzUjNRMzETMnBzMFIwEzASM1PgE1ETQjIgc1NxcRFBYXAs03RrTLLzd9h4f+ajEBxS7+Z78gFQgHJHYOFyJaWloxAQr+/rOzoQKy/moYAg0RAQEcDRc3A/6pFA4CAAMAHv/yAukCpAADABUANgAAFyMBMwEjNT4BNRE0IyIHNTcXERQWHwE2NzY3Njc+ATMyFhUUBg8BMzI2NxcHIzU3NjU0JiMiB5UxAcUu/oa/IBUIByR2Dhci5QcCAw0ODA0yHzpJJi5WeRQUCxEl/WJRKx43Hw4Csv5qGAINEQEBHA0XNwP+qRQOAgcVBgcWGAkKFDowJUQtVQoRB1UXa1cyHy1GAAAAAAQADv/yAs0CpAAKAA0AEQA+AAAlIxUjNSM1EzMRMycHMwUjATMBNCcmKwEHNT4BNTQmIyIGByc+ATMyHgIVFAcWFRQHBiMiNTQ2MzIXFjMyNgLNN0a0yy83fYeH/moxAcUu/lYgIBkFFzYqHhocJBgZGz4wFigdET1XODhSUxQPDxoZGx8lWlpaMQEK/v6zs6ECsv7WLBQVAx0SJRsWGxghEjMsDRYeEicrI046JycyEBUUEyUAAAADACj/8gLZAqQALAA+AEIAACU0JyYrAQc1PgE1NCYjIgYHJz4BMzIeAhUUBxYVFAcGIyI1NDYzMhcWMzI2JSM1PgE1ETQjIgc1NxcRFBYXAyMBMwKIICAZBRc0Kx8bGSQXGRo/LhYoHRE8Vzg4UlMUDw8aGRsfJf5fvyAVCAckdg4XIlIxAcUucSwUFQMdEiUbFxoZIBIzLA0WHhInKyNOOicnMhAVFBMlvBgCDREBARwNFzcD/qkUDgL+zAKyAAADAAj/8gLQAqQALABPAFMAACU0JyYrAQc1PgE1NCYjIgYHJz4BMzIeAhUUBxYVFAcGIyI1NDYzMhcWMzI2ATY3Njc2Nz4BMzIWFRQPATMyNjcXByM1Nz4DNTQmIyIHEyMBMwJ/ICAZBRc0Kx8bGSQXGRo/LhYoHRE8Vzg4UlMUDw8aGRsfJf2JBwIDDQ4MDTIfOklUVnkUFAsRJf1iEh4VDCseNx90MQHFLnEsFBUDHRIlGxcaGSASMywNFh4SJysjTjonJzIQFRQTJQHbFQYHFhgJChQ6MEpSVQoRB08RaxMpJiEMHy1G/dMCsgAAAAADACj/8gLGAqQAKQA7AD8AACUUDgIjIjU0NjMyFxYzMj4CNTQmJyYjIjU0NjU3MzI3FwcGKwEHHgElIzU+ATURNCMiBzU3FxEUFhcDIwEzAsYdNEotUxQPDxoZGxAeGA4yOT8NDAFLmBIOBhsCDocYZGb+Ib8gFQgHJHYOFyJSMQHFLpIjOCkWMhAVFBMMFBsPKjYSFAUBAwGTDwRCBiURSjYYAg0RAQEcDRc3A/6pFA4C/swCsgAAAwAA//ACzgKkACIATABQAAARNjc2NzY3PgEzMhYVFA8BMzI2NxcHIzU3PgM1NCYjIgcBFA4CIyI1NDYzMhcWMzI+AjU0JicmIyI1NDY1NzMyNxcHBisBBx4BBSMBMwcCAw0ODA0yHzpJVFZ5FBQLESX9YhIeFQwrHjcfArUdNEotUxQPDxoZGxAeGA4yOT8NDAFLmBIOBhsCDocYZGb9yTEBxS4CLRUGBxYYCQoUOjBKUlUKEQdPEWsTKSYhDB8tRv5rIzgpFjIQFRQTDBQbDyo2EhQFAQMBkw8EQgYlEUreArIAAwAM/+4CwgKkACwAVgBaAAATNCcmKwEHNT4BNTQmIyIGByc+ATMyHgIVFAcWFRQHBiMiNTQ2MzIXFjMyNgUUDgIjIjU0NjMyFxYzMj4CNTQmJyYjIjU0NjU3MzI3FwcGKwEHHgEFIwEz0CAgGQUXNCsfGxkkFxkaPy4WKB0RPFc4OFJTFA8PGhkbHyUB8h00Si1TFA8PGhkbEB4YDjI5Pw0MAUuYEg4GGwIOhxhkZv3TMQHFLgF6LBQVAx0SJRsXGhkgEjMsDRYeEicrI046JycyEBUUEyXTIzgpFjIQFRQTDBQbDyo2EhQFAQMBkw8EQgYlEUrcArIAAAAABAAD/+wCuAKkAAoADQA3ADsAAAEjFSM1IzUTMxEzJwczARQOAiMiNTQ2MzIXFjMyPgI1NCYnJiMiNTQ2NTczMjcXBwYrAQceAQUjATMBNDdGtMsvN32HhwIBHTRKLVMUDw8aGRsQHhgOMjk/DQwBS5gSDgYbAg6HGGRm/d0xAcUuAWhaWjEBCv7+s7P+5SM4KRYyEBUUEwwUGw8qNhIUBQEDAZMPBEIGJRFK2gKyAAAEACj/8ALDAqQAEgAgADIANgAABSImNTQ+AjcVBgc2MzIWFRQGJyIOAhUUFjMyNjU0JiUjNT4BNRE0IyIHNTcXERQWFwMjATMCL0hWFT5xXKMZHChASk9SCRYTDSwmHSEr/p+/IBUIByR2DhciUjEBxS4QYFEjT0YzBxwWfBxHPEBO6gkUHhU/QDItNjo0GAINEQEBHA0XNwP+qRQOAv7MArIAAAQAFf/oAsQCsgApADwASgBOAAABFA4CIyI1NDYzMhcWMzI+AjU0JicmIyI1NDY1NzMyNxcHBisBBx4BASImNTQ+AjcVBgc2MzIWFRQGJyIOAhUUFjMyNjU0JgUjATMBMB00Si1TFA8PGhkbEB4YDjI5Pw0MAUuYEg4GGwIOhxhkZgEASFYVPnFcoxkcKEBKT1IJFhMNLCYdISv+TDEBxS4BoCM4KRYyEBUUEwwUGw8qNhIUBQEDAZMPBEIGJRFK/gJgUSNPRjMHHBZ8HEc8QE7qCRQeFT9AMi02OuACsgAABQAo//IC2AKkAB4AKAA0AEYASgAAJRYXFhUUBiMiLgI1NDc2NyYnJjU0NjMyHgIVFAYHBhUUFjMyNTQnNz4BNTQmIyIGFRQWISM1PgE1ETQjIgc1NxcRFBYXAyMBMwJkRhcXUkweMiQUDw80NBAOTj0dMSQUKXsmKCBFQAIgHiwjHico/s2/IBUIByR2DhciUjEBxS7mJxscJTA5DxwlFx0UEiYoFRYdKzgNGCETHSpEJi0lLDwmKFcSJBgeIh4ZFygYAg0RAQEcDRc3A/6pFA4C/swCsgAABQAB//IC6QKkACwASwBVAGEAZQAAEzQnJisBBzU+ATU0JiMiBgcnPgEzMh4CFRQHFhUUBwYjIjU0NjMyFxYzMjYFFhcWFRQGIyIuAjU0NzY3JicmNTQ2MzIeAhUUBgcGFRQWMzI1NCc3PgE1NCYjIgYVFBYBIwEzxSAgGQUXNCsfGxkkFxkaPy4WKB0RPFc4OFJTFA8PGhkbHyUBsEYXF1JMHjIkFA8PNDQQDk49HTEkFCl7JiggRUACIB4sIx4nKP5qMQHFLgF6LBQVAx0SJRsXGhkgEjMsDRYeEicrI046JycyEBUUEyV1JxscJTA5DxwlFx0UEiYoFRYdKzgNGCETHSpEJi0lLDwmKFcSJBgeIh4ZFyj+5AKyAAAFABT/8gLpArIAKQBIAFIAXgBiAAABFA4CIyI1NDYzMhcWMzI+AjU0JicmIyI1NDY1NzMyNxcHBisBBx4BARYXFhUUBiMiLgI1NDc2NyYnJjU0NjMyHgIVFAYHBhUUFjMyNTQnNz4BNTQmIyIGFRQWASMBMwEvHTRKLVMUDw8aGRsQHhgOMjk/DQwBS5gSDgYbAg6HGGRmAUZGFxdSTB4yJBQPDzQ0EA5OPR0xJBQpeyYoIEVAAiAeLCMeJyj+ajEBxS4BoCM4KRYyEBUUEwwUGw8qNhIUBQEDAZMPBEIGJRFK/wAnGxwlMDkPHCUXHRQSJigVFh0rOA0YIRMdKkQmLSUsPCYoVxIkGB4iHhkXKP7kArIAAAAABQAw//IC6gKkAAMAEAAvADkARQAAFyMBMwcDIxMjIg4CByc3IQEWFxYVFAYjIi4CNTQ3NjcmJyY1NDYzMh4CFRQGBwYVFBYzMjU0Jzc+ATU0JiMiBhUUFpUxAcUu/I5Hhn8OFBIRDAwWARUBG0YXF1JMHjIkFA8PNDQQDk49HTEkFCl7JiggRUACIB4sIx4nKA4Csg7+eAFbAwkRDQVf/kMnGxwlMDkPHCUXHRQSJigVFh0rOA0YIRMdKkQmLSUsPCYoVxIkGB4iHhkXKAAAAAUAHwCKAqoCNgA/AEsAXABpAHgAACUOAysBISInIic+Azc+ATc+ATc2NTQnDgEHBiMiJzY3Njc+ATMyFhcWFxYXBiMiJy4BJw4BFRQXHgEXFic2MzIXNy4BIyIGBxciDgIVFBYzMj4CNTQnJiUGBwYjIicmJx4BMzIlDgMHIi4CJxYzMjYCPgEBAwUECP54CwEIAw0UDwwGCQYJBQYCCgMYFwcpQB0cCBgbWC1YLS1XLFkbGAgcHUAoBxcZAQEEAg4LFuo8Dw88ESMuCwsuI18VJBsPNyoUIxsPHR0BHggIByE7FBIFGSsTLv4tBAYTKSYOEAoHBBUjHDPIFRkNAwE9JjcnGwoPCAkFBwILDgwSBRIUCQVEFBURCAkJCBEVE0UFCRQSBQwPAwsFAxANGD8DAykEBAQERg8aIxQqOA8bIxQnHR0GFwQEBQYUBAMHCgsGAwECBgwLBwMAAAEAIf/yAtACwQAkAAA3NDc2MzIfARYzMjc+ATc2Nz4BOwEVDgEHBgcOASMiJicuAScmISYnFw8IHQoKCBEqWC5bOxlaLQQ5gEqUUAgcJR8SBg4ZCxbdGhQVFEcZHkiGQoI1FxkQPKFnz5QPCQYMHDwgQQAAAQAA/7sCPQKUAFAAACUOAQ8BFBYVFAYjIicuAScOAQcGIyInJjU0NjU0LgIxND4CNz4BNy4BNSY1NDYzMhczPgEzHgEXFhc+ATc2MzIXPgEzMBcWFRQHBgcOAQcB4wIRBwgEFwkMLhclDjljKhEjCw0NBwMDAwMLFBILWU4DB0oVBwUKEAsEBggZEiMHTlIEBAIJGggLAg4OEA8zGTohigQTCAgDCAIHEkUjOxpRmUkMFhQLARYDAQgJBwMGECIgFHNfChECxggJGQcLBwwtID8RXF4EAxQHDRITAQMREUAgTC4AAQBqAAAClwLBADAAACUeARcWFx4BFx4BFxYzFSE1Njc2NwYjIicmNTQ+Ajc+ATceARceARceARUUBwYjIgGRBSgWFyARGgkJFg0DA/4fVC1VCjVRNiUlGis2HCVIExFHJRMlESMtJiU1Ue40UBUVDwgKAgIEAgEUFA8dOHZQJSU1IzkyLhkha0NCaiERHxAfSi40JiUAAAABAB8AAALhAsEARwAAARQeAhceARceARcVITU+ATc+ATc+AzU2NwYHDgEjIicmNTQ3NjMyHgIXJjU0NzYzMhcWFRQHPgE3NjMyFxYVFAcGIyIBjgECBAMGEREXVkX+HC1GFxghCQQIBQQBAhkOG08mPy8uKSo7DR0aEwUnLi5CQC8vJw4eCRMUPCgqLi8/bgElDxcVFg8cJRcfLgwUFAgdDw8vEgkZGhgJEh4tESIoLy9EQC8uCAoKAjczPy0tLS0/MzcGDwMGLi9ARC8vAAAAAQBI//ICuALAACEAAAE+ATMyFxYVFA4CBw4DBy4DJy4BJy4BNTQ3NjMyAYEUWTY8LCwgMj8fFSokHAgJHiUqFRUpFCg0Kis+dwIwS0UqKjsqUlBQJxo8QUQhIkdDPhsbNBo0ZzA+LCsAAAAAAQB5//IChwLBAA8AAAEWFx4BFw4BBwYHLgEnPgEBfxhcLkocI0spUCElg15hggLBL34/XB8oXzdvO0Kzc3WzAAIAUwAAAsUC0wADAAYAABMBAxMDEwFTAnLR0eyr/f8BaQFq/pb+lwFpASj+2AAAAAIAUwAAAsUC0wADAAYAABMJARMzIQFTAnL9jtEcAVb9/wLT/pb+lwFpASgAAf9Z//IBTAKkAAMAAAcjATN2MQHFLg4CsgAAAAH/+P6VA9cDUAAnAAATIicmNRE0NzYzITIXFh0BFCMiPQE0JyYjISIGFREUHgI7ARYVFCO8VTg3NDVaAmNfOx8jJBIkPP2dPEATIS4btSAc/pU7OlQDLk86O1AsQg4lJQ4rGDNDOfzSHC8jEyMTEgAC/ND+lP95/+AAOABIAAAHFAcGJyInNxcWFCMGNTQ3MhUGIyciNRcyNjU0JyYjIgcGBx4BFRQHBiMiJyY1NDc2MzIXNjMyFxYFJjcuASMiDgIVFBYzMjaHOjtXCwoCAgICHx8CAgEDARlDRyImMjUlHQcBATY4TEw3NzU1UGU1OmBJOjj+igEBB0QuGy0hEkc0LkTGQzEyBQMBAQECAR8fAQECAgMDPCopHh8fGBwFCARILi8vLkhDLy9HSC8wVRATJS0PGyQVLjowAAAD/O7+lv9+/98AFwAtAEUAAAcUBwYjIicGIyInJjU0NzYzMhc2MzIXFgc0LgIjIgYHFhQVHAEHHgEzMj4CJS4DIyIOAhUUHgIzMjY3LgE1NDaCNTNNXzU1YEo0NDMzTGE0NV9NMzU8EyAsGi1ABwEBB0EsGSwhE/7WBBYgKRUZKiASEiAqGStFCAECAsRILjBGRjAuSEQwL0VFLi9GFyUaDiwiBgsFBQsFJi0QGyYtEhwVCxAbJBUXJhsQLSYFCwUFCwAAAAHCApYAHgACAAYAJgAmACsANQA8AEkAVQACAAYATgBVAFUAWwBnAGcAbQBzACECeQAAAAEAAAABW6a+cdqnXw889QKLA4QAAAAAx1F/SgAAAADKXwAS/Kj+iwdpA2cAAAAIAAAAAAAAAAAAAQAAA4T+cABaB5z8qPy0B2kAAQAAAAAAAAAAAAAAAAAAAWwAAgAAAAAAAP+DADIAAAAAAAAAAAAAAAAAAAAAAAAAAAFtAAAAAQACAAMABAAFAAYABwAIAAkACgALAAwADQAOAA8AEAARABIAEwAUABUAFgAXABgAGQAaABsAHAAdAB4AHwAgACEAIgAjACQAJQAmACcAKAApACoAKwAsAC0ALgAvADAAMQAyADMANAA1ADYANwA4ADkAOgA7ADwAPQA+AD8AQABBAEIAQwBEAEUARgBHAEgASQBKAEsATABNAE4ATwBQAFEAUgBTAFQAVQBWAFcAWABZAFoAWwBcAF0AXgBfAGAAYQDwALIAswC2ALcAtAC1AQIBAwEEAQUBBgEHAQgBCQEKAQsBDAENAQ4BDwEQAREBEgETARQBFQEWARcBGAEZARoBGwEcAR0BHgEfASABIQEiASMBJAElASYBJwEoASkBKgErASwBLQEuAS8BMAExATIBMwE0ATUBNgE3ATgBOQE6ATsBPAE9AT4BPwFAAUEBQgFDAUQBRQFGAUcBSAFJAUoBSwFMAU0BTgFPAVABUQFSAVMBVAFVAVYBVwFYAVkBWgFbAVwBXQFeAV8BYAFhAWIBYwFkAWUBZgFnAWgBaQFqAWsBbAFtAW4BbwFwAXEBcgFzAXQBdQF2AXcBeAF5AXoBewF8AX0BfgF/AYABgQGCAYMBhAGFAYYBhwGIAYkBigGLAYwBjQGOAY8BkAGRAZIBkwGUAZUBlgGXAZgBmQGaAZsBnAGdAZ4BnwGgAaEBogGjAaQBpQGmAacBqAGpAaoBqwGsAa0BrgGvAbABsQGyAbMBtAG1AbYBtwG4AbkBugG7AbwBvQG+Ab8BwAHBAcIBwwHEAcUBxgHHAcgByQHKAcsBzAHNAc4BzwHQAdEB0gHTAdQB1QHWAdcB2AHZAdoB2wHcAd0B3gHfAeAB4QHiAeMB5AHlAeYB5wHoAekA9QD0APYB6gHrAewB7QHuAe8B8AHxAfIB8wH0AfUB9gH3AfgB+QH6AfsB/AH9Af4AvAH/AgACAQJrYQNraGECZ2EDZ2hhA25nYQJjYQNjaGECamEDamhhA255YQRubnlhA3R0YQR0dGhhA2RkYQRkZGhhA25uYQJ0YQN0aGECZGEDZGhhAm5hAnBhA3BoYQJiYQNiaGECbWECeWECcmECbGECd2ECc2ECaGEDbGxhBWEubGV0BWkubGV0BmlpLmxldAV1LmxldAZ1dS5sZXQFZS5sZXQFby5sZXQGYXUubGV0DmFhLnZvdy5hbHQuZG90CmFhLnZvdy5kb3QJaS52b3cuZG90CmlpLnZvdy5kb3QJdS52b3cuZG90CnV1LnZvdy5kb3QJZS52b3cuZG90CmFpLnZvdy5kb3QMYW51c3ZhcmEuZG90C2F1a215aXQuZG90C3Zpc2FyZ2EuZG90CnZpcmFtYS5kb3QIYXNhdC5kb3QKeWEubWVkLmRvdApyYS5tZWQuZG90CndhLm1lZC5kb3QKaGEubWVkLmRvdAhncmVhdC5zYQZtLnplcm8FbS5vbmUFbS50d28HbS50aHJlZQZtLmZvdXIGbS5maXZlBW0uc2l4B20uc2V2ZW4HbS5laWdodAZtLm5pbmULc2VjdGlvbi5zaWcRbGl0dGxlc2VjdGlvbi5zaWcMbG9jYXRpdmUuc2lnDWNvbXBsZXRlZC5zaWcSYWZvcmVtZW50aW9uZWQuc2lnDGdlbml0aXZlLnNpZwZwYS5zaGEGZ2Euc2hhBXIudm9sBnJyLnZvbAVsLnZvbAZsbC52b2wJci52b3cuZG90CnJyLnZvdy5kb3QJbC52b3cuZG90CmxsLnZvdy5kb3QGa2Euc3ViB2toYS5zdWIGZ2Euc3ViB2doYS5zdWIHbmdhLnN1YgZjYS5zdWIHY2hhLnN1YgZqYS5zdWIHamhhLnN1YgdueWEuc3ViCG5ueWEuc3ViB3R0YS5zdWIIdHRoYS5zdWIHZGRhLnN1YghkZGhhLnN1YgdubmEuc3ViBnRhLnN1Ygd0aGEuc3ViBmRhLnN1YgdkaGEuc3ViBm5hLnN1YgZwYS5zdWIHcGhhLnN1YgZiYS5zdWIHYmhhLnN1YgZtYS5zdWIGeWEuc3ViBnJhLnN1YgZsYS5zdWIGc2Euc3ViBmhhLnN1YgVhLnN1YgphYS52b3cuYWx0BmFhLnZvdwVpLnZvdwZpaS52b3cFdS52b3cGdXUudm93BWUudm93BmFpLnZvdwhhbnVzdmFyYQdhdWtteWl0B3Zpc2FyZ2EEYXNhdAZ5YS5tZWQGd2EubWVkBmhhLm1lZAthYS5hbHQuYXNhdAZyYS5tZWQLcmEubWVkLndpZGURcmEubWVkLnVwcGVyc2hvcnQWcmEubWVkLnVwcGVyc2hvcnQud2lkZRFyYS5tZWQubG93ZXJzaG9ydBdyYS5tZWQubG93ZXJzaG9ydDEud2lkZQl3YS5oYS5tZWQJeWEud2EubWVkCXJhLndhLm1lZA5yYS53YS5tZWQud2lkZQl5YS5oYS5tZWQMeWEud2EuaGEubWVkDHJhLndhLmhhLm1lZBFyYS53YS5oYS5tZWQud2lkZQVraW56aQtraW56aS5pLnZvdwxraW56aS5paS52b3cOa2luemkuYW51c3ZhcmEOaS52b3cuYW51c3ZhcmEKaGEubWVkLmFsdAxoYS5tZWQudS5zaWcKdS5zaWcubG9uZwt1dS5zaWcubG9uZwhubnlhLmFsdAlueWEuc2hvcnQHbnlhLmFsdAZuYS5hbHQGcmEuYWx0DGRkYS5kZGhhLmxpZwx0dGEudHRoYS5saWcLbm5hLmRkYS5saWcLbm5hLm5uYS5saWcLdHRhLnR0YS5saWcLZGRhLmRkYS5zdWIGaGEuaS51DGF1a215aXQuYWx0MgxhdWtteWl0LmFsdDEEendzcAR6d25qA3p3agV1MjA2MAV1MjI2MAxkb3R0ZWRjaXJjbGUKa2Euc3ViLmRvdAtraGEuc3ViLmRvdApnYS5zdWIuZG90C2doYS5zdWIuZG90C25nYS5zdWIuZG90CmNhLnN1Yi5kb3QLY2hhLnN1Yi5kb3QKamEuc3ViLmRvdAtqaGEuc3ViLmRvdAtueWEuc3ViLmRvdAxubnlhLnN1Yi5kb3QMZGRoYS5zdWIuZG90C3R0YS5zdWIuZG90DHR0aGEuc3ViLmRvdAtkZGEuc3ViLmRvdAtubmEuc3ViLmRvdAp0YS5zdWIuZG90C3RoYS5zdWIuZG90CmRhLnN1Yi5kb3QLZGhhLnN1Yi5kb3QKbmEuc3ViLmRvdApwYS5zdWIuZG90C3BoYS5zdWIuZG90CmJhLnN1Yi5kb3QLYmhhLnN1Yi5kb3QKbWEuc3ViLmRvdAp5YS5zdWIuZG90CnJhLnN1Yi5kb3QKbGEuc3ViLmRvdApzYS5zdWIuZG90CmhhLnN1Yi5kb3QJYS5zdWIuZG90CWtpbnppLmRvdA9raW56aS5pLnZvdy5kb3QSa2luemkuYW51c3ZhcmEuZG90EGtpbnppLmlpLnZvdy5kb3QSaS52b3cuYW51c3ZhcmEuZG90EGhhLm1lZC51LnNpZy5kb3QPYWEuYWx0LmFzYXQuZG90DXlhLndhLm1lZC5kb3QNd2EuaGEubWVkLmRvdA1yYS53YS5tZWQuZG90EHlhLndhLmhhLm1lZC5kb3QNeWEuaGEubWVkLmRvdBByYS53YS5oYS5tZWQuZG90DmdhLnNoYS5zdWIuZG90DnBhLnNoYS5zdWIuZG90CmdhLnNoYS5zdWIKcGEuc2hhLnN1YgVyLnZvdwZyci52b3cFbC52b3cGbGwudm93CmxsYS5oYS5tZWQGdmlyYW1hB3MudGEud2EIb25ldGhpcmQJdHdvdGhpcmRzB3VuaTIxNTUHdW5pMjE1Ngd1bmkyMTU3B3VuaTIxNTgHdW5pMjE1OQd1bmkyMTVBCW9uZWVpZ2h0aAx0aHJlZWVpZ2h0aHMLZml2ZWVpZ2h0aHMMc2V2ZW5laWdodGhzBXBob25lB3VuaTI3MTMHdW5pMjcxNwVzcGFkZQRjbHViBWhlYXJ0B2RpYW1vbmQHbGVmdGFycghyaWdodGFychdyYS5tZWQubG93ZXJzaG9ydDIud2lkZQp0YS5zdWIuYWx0C3RoYS5zdWIuYWx0AAAAcgVeAAEAAAAAAAAAIAAKAAEAAAAAAAEACAAqAAEAAAAAAAIABwAyAAEAAAAAAAMAFgA5AAEAAAAAAAQACABPAAEAAAAAAAUAHQBXAAEAAAAAAAYACAB0AAEAAAAAAAcAJwB8AAEAAAAAAAgACwCjAAEAAAAAAAkABwCuAAEAAAAAAAsAHAC1AAEAAAAAAA0CcQDRAAEAAAAAAA4AJQNCAAEAAAAAABMALANnAAEAAAAAAQAACQOTAAEAAAAAAQEABQOcAAEAAAAAAQIADgOhAAEAAAAAAQMACQOvAAEAAAAAAQQACAO4AAEAAAAAAQUABgPAAAEAAAAAAQYACQPGAAEAAAAAAQcAEAPPAAEAAAAAAQgACQPfAAEAAAAAAQkACgPoAAEAAAAAAQoACQPyAAEAAAAAAQsADgP7AAEAAAAAAQwACAQJAAEAAAAAAQ0ACQQRAAEAAAAAAQ4AEAQaAAEAAAAAAQ8ACQQqAAEAAAAAARAACgQzAAEAAAAAAREACQQ9AAEAAAAAARIADgRGAAEAAAAAARMACARUAAEAAAAAARQACQRcAAEAAAAAARUAEARlAAEAAAAAARYACQR1AAEAAAAAARcACgR+AAEAAAAAARgACQSIAAEAAAAAARkADgSRAAEAAAAAARoACASfAAEAAAAAARsACQSnAAEAAAAAARwAEASwAAEAAAAAAR0ACQTAAAEAAAAAAR4ACgTJAAEAAAAAAR8ACQTTAAEAAAAAASAADgTcAAEAAAAAASEACATqAAEAAAAAASIACQTyAAEAAAAAASMAEAT7AAEAAAAAASQACQULAAEAAAAAASUACgUUAAEAAAAAASYACQUeAAEAAAAAAScADgUnAAEAAAAAASgACAU1AAEAAAAAASkACQU9AAEAAAAAASoAEAVGAAEAAAAAASsACQVWAAEAAAAAASwACgVfAAEAAAAAAS0ACQVpAAEAAAAAAS4ADgVyAAEAAAAAAS8ACAWAAAEAAAAAATAACQWIAAEAAAAAATEAEAWRAAEAAAAAATIACQWhAAEAAAAAATMACgWqAAEAAAAAATQACQW0AAEAAAAAATUADgW9AAEAAAAAATYACAXLAAEAAAAAATcACQXTAAEAAAAAATgAEAXcAAEAAAAAATkACQXsAAEAAAAAAToACgX1AAEAAAAAATsACQX/AAEAAAAAATwADgYIAAEAAAAAAT0ACAYWAAEAAAAAAT4ACQYeAAEAAAAAAT8AEAYnAAEAAAAAAUAACQY3AAEAAAAAAUEACgZAAAEAAAAAAUIACQZKAAEAAAAAAUMADgZTAAEAAAAAAUQACAZhAAEAAAAAAUUACQZpAAEAAAAAAUYAEAZyAAEAAAAAAUcACQaCAAEAAAAAAUgACgAAAAMAAQQHAAIAEAaLAAMAAQQHABMAWAabAAMAAQQJAAAAQAbzAAMAAQQJAAEAEAczAAMAAQQJAAIADgdDAAMAAQQJAAMALAdRAAMAAQQJAAQAEAd9AAMAAQQJAAUAOgeNAAMAAQQJAAYAEAfHAAMAAQQJAAcATgfXAAMAAQQJAAgAFgglAAMAAQQJAAkADgg7AAMAAQQJAAsAOghJAAMAAQQJAA0E4giDAAMAAQQJAA4ASA1lAAMAAQQJABMAWA2tAAMAAQQKAAIADA4FAAMAAQQKABMAWA4RAAMAAQQMAAIADA5pAAMAAQQMABMAWA51AAMAAQQTAAIAEg7NAAMAAQQTABMAWA7fAAMAAQQZABMAWA83AAMAAQQkAAIADg+PAAMAAQQkAA0AjA+dAAMAAQQkAA4ASBApAAMAAQQkABMAWBBxU3dhcCBSYU1lZENvcHlyaWdodCAoYykgTXlhbm1hciBOTFAsIDIwMDUuTXlhbm1hcjNSZWd1bGFyTXlhbm1hcjM6VmVyc2lvbiAxLjM1OE15YW5tYXIzVmVyc2lvbiAxLjM1OCBKYW51YXJ5IDUsIDIwMTFNeWFubWFyM015YW5tYXIzIGlzIGEgdHJhZGVtYXJrIG9mIE15YW5tYXIgTkxQLk15YW5tYXIgTkxQU3VuIFR1bmh0dHA6Ly93d3cubXlhbm1hcm5scC5uZXQubW1OT1RJRklDQVRJT04gT0YgTElDRU5TRSBBR1JFRU1FTlQNCg0KVGhpcyBmb250IGlzIGZyZWUgc29mdHdhcmU7IHlvdSBjYW4gcmVkaXN0cmlidXRlIGl0IGFuZC9vciBtb2RpZnkgaXQgdW5kZXIgdGhlIHRlcm1zIG9mIEdOVSBMZXNzZXIgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBhcyBwdWJsaXNoZWQgYnkgdGhlIEZyZWUgU29mdHdhcmUgRm91bmRhdGlvbjsgZWl0aGVyIHZlcnNpb24gMi4xIG9mIHRoZSBMaWNlbnNlLCBvciAoYXQgeW91ciBvcHRpb24pIGFueSBsYXRlciB2ZXJzaW9uLgpUaGlzIGxpYnJhcnkgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCwgYnV0IFdJVEhPVVQgQU5ZIFdBUlJBTlRZOyB3aXRob3V0IGV2ZW4gdGhlIGltcGxpZWQgd2FycmFudHkgb2YgTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLj8gU2VlIHRoZSBHTlUgTGVzc2VyIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy4KWU91IHNob3VsZCBoYXZlIHJlY2VpdmVkIGEgY29weSBvZiBHTlUgTGVzc2VyIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgYWxvbmcgd2l0aCB0aGlzIGZvbnQ7IGlmIG5vdCwgdmlzaXQgaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2xncGwuaHRtbFRoZSBxdWljayBicm93biBmb3gganVtcHMgb3ZlciB0aGUgbGF6eSBkb2cuTm8gQ2hhbmdlS2luWmlLaW5aaSBMaWdhdHVyZVNob3J0IE55YUFsdGEgTnlhTG9uZ3ZVVXNlIExvbmd1U3RhY2tlZCBMaWdhdHVyZVNob3J0IE55YVN3YXAgUmFNZWRObyBDaGFuZ2VLaW5aaSBMaWdhdHVyZUFsdGEgTnlhVXNlIExvbmd1U3RhY2tlZCBMaWdhdHVyZVNob3J0IE55YVN3YXAgUmFNZWRObyBDaGFuZ2VLaW5aaSBMaWdhdHVyZUFsdGEgTnlhVXNlIExvbmd1U3RhY2tlZCBMaWdhdHVyZVNob3J0IE55YVN3YXAgUmFNZWRObyBDaGFuZ2VLaW5aaSBMaWdhdHVyZUFsdGEgTnlhVXNlIExvbmd1U3RhY2tlZCBMaWdhdHVyZVNob3J0IE55YVN3YXAgUmFNZWRObyBDaGFuZ2VLaW5aaSBMaWdhdHVyZUFsdGEgTnlhVXNlIExvbmd1U3RhY2tlZCBMaWdhdHVyZVNob3J0IE55YVN3YXAgUmFNZWRObyBDaGFuZ2VLaW5aaSBMaWdhdHVyZUFsdGEgTnlhVXNlIExvbmd1U3RhY2tlZCBMaWdhdHVyZVNob3J0IE55YVN3YXAgUmFNZWRObyBDaGFuZ2VLaW5aaSBMaWdhdHVyZUFsdGEgTnlhVXNlIExvbmd1U3RhY2tlZCBMaWdhdHVyZVNob3J0IE55YVN3YXAgUmFNZWRObyBDaGFuZ2VLaW5aaSBMaWdhdHVyZUFsdGEgTnlhVXNlIExvbmd1U3RhY2tlZCBMaWdhdHVyZVNob3J0IE55YVN3YXAgUmFNZWRObyBDaGFuZ2VLaW5aaSBMaWdhdHVyZUFsdGEgTnlhVXNlIExvbmd1U3RhY2tlZCBMaWdhdHVyZVNob3J0IE55YVN3YXAgUmFNZWRObyBDaGFuZ2VLaW5aaSBMaWdhdHVyZUFsdGEgTnlhVXNlIExvbmd1U3RhY2tlZCBMaWdhdHVyZVNob3J0IE55YQBTAHQAYQBuAGQAYQByAGQAVABoAGUAIABxAHUAaQBjAGsAIABiAHIAbwB3AG4AIABmAG8AeAAgAGoAdQBtAHAAcwAgAG8AdgBlAHIAIAB0AGgAZQAgAGwAYQB6AHkAIABkAG8AZwAuAEMAbwBwAHkAcgBpAGcAaAB0ACAAKABjACkAIABNAHkAYQBuAG0AYQByACAATgBMAFAALAAgADIAMAAwADUALgBNAHkAYQBuAG0AYQByADMAUgBlAGcAdQBsAGEAcgBNAHkAYQBuAG0AYQByADMAOgBWAGUAcgBzAGkAbwBuACAAMQAuADMANQA4AE0AeQBhAG4AbQBhAHIAMwBWAGUAcgBzAGkAbwBuACAAMQAuADMANQA4ACAASgBhAG4AdQBhAHIAeQAgADUALAAgADIAMAAxADEATQB5AGEAbgBtAGEAcgAzAE0AeQBhAG4AbQBhAHIAMwAgAGkAcwAgAGEAIAB0AHIAYQBkAGUAbQBhAHIAawAgAG8AZgAgAE0AeQBhAG4AbQBhAHIAIABOAEwAUAAuAE0AeQBhAG4AbQBhAHIAIABOAEwAUABTAHUAbgAgAFQAdQBuAGgAdAB0AHAAOgAvAC8AdwB3AHcALgBtAHkAYQBuAG0AYQByAG4AbABwAC4AbgBlAHQALgBtAG0ALwBOAE8AVABJAEYASQBDAEEAVABJAE8ATgAgAE8ARgAgAEwASQBDAEUATgBTAEUAIABBAEcAUgBFAEUATQBFAE4AVAANAAoADQAKAFQAaABpAHMAIABmAG8AbgB0ACAAaQBzACAAZgByAGUAZQAgAHMAbwBmAHQAdwBhAHIAZQA7ACAAeQBvAHUAIABjAGEAbgAgAHIAZQBkAGkAcwB0AHIAaQBiAHUAdABlACAAaQB0ACAAYQBuAGQALwBvAHIAIABtAG8AZABpAGYAeQAgAGkAdAAgAHUAbgBkAGUAcgAgAHQAaABlACAAdABlAHIAbQBzACAAbwBmACAARwBOAFUAIABMAGUAcwBzAGUAcgAgAEcAZQBuAGUAcgBhAGwAIABQAHUAYgBsAGkAYwAgAEwAaQBjAGUAbgBzAGUAIABhAHMAIABwAHUAYgBsAGkAcwBoAGUAZAAgAGIAeQAgAHQAaABlACAARgByAGUAZQAgAFMAbwBmAHQAdwBhAHIAZQAgAEYAbwB1AG4AZABhAHQAaQBvAG4AOwAgAGUAaQB0AGgAZQByACAAdgBlAHIAcwBpAG8AbgAgADIALgAxACAAbwBmACAAdABoAGUAIABMAGkAYwBlAG4AcwBlACwAIABvAHIAIAAoAGEAdAAgAHkAbwB1AHIAIABvAHAAdABpAG8AbgApACAAYQBuAHkAIABsAGEAdABlAHIAIAB2AGUAcgBzAGkAbwBuAC4ACgBUAGgAaQBzACAAbABpAGIAcgBhAHIAeQAgAGkAcwAgAGQAaQBzAHQAcgBpAGIAdQB0AGUAZAAgAGkAbgAgAHQAaABlACAAaABvAHAAZQAgAHQAaABhAHQAIABpAHQAIAB3AGkAbABsACAAYgBlACAAdQBzAGUAZgB1AGwALAAgAGIAdQB0ACAAVwBJAFQASABPAFUAVAAgAEEATgBZACAAVwBBAFIAUgBBAE4AVABZADsAIAB3AGkAdABoAG8AdQB0ACAAZQB2AGUAbgAgAHQAaABlACAAaQBtAHAAbABpAGUAZAAgAHcAYQByAHIAYQBuAHQAeQAgAG8AZgAgAE0ARQBSAEMASABBAE4AVABBAEIASQBMAEkAVABZACAAbwByACAARgBJAFQATgBFAFMAUwAgAEYATwBSACAAQQAgAFAAQQBSAFQASQBDAFUATABBAFIAIABQAFUAUgBQAE8AUwBFAC4APwAgAFMAZQBlACAAdABoAGUAIABHAE4AVQAgAEwAZQBzAHMAZQByACAARwBlAG4AZQByAGEAbAAgAFAAdQBiAGwAaQBjACAATABpAGMAZQBuAHMAZQAgAGYAbwByACAAbQBvAHIAZQAgAGQAZQB0AGEAaQBsAHMALgAKAFkATwB1ACAAcwBoAG8AdQBsAGQAIABoAGEAdgBlACAAcgBlAGMAZQBpAHYAZQBkACAAYQAgAGMAbwBwAHkAIABvAGYAIABHAE4AVQAgAEwAZQBzAHMAZQByACAARwBlAG4AZQByAGEAbAAgAFAAdQBiAGwAaQBjACAATABpAGMAZQBuAHMAZQAgAGEAbABvAG4AZwAgAHcAaQB0AGgAIAB0AGgAaQBzACAAZgBvAG4AdAA7ACAAaQBmACAAbgBvAHQALAAgAHYAaQBzAGkAdAAgAGgAdAB0AHAAOgAvAC8AdwB3AHcALgBnAG4AdQAuAG8AcgBnAC8AYwBvAHAAeQBsAGUAZgB0AC8AZwBwAGwALgBoAHQAbQBsAFQAaABlACAAcQB1AGkAYwBrACAAYgByAG8AdwBuACAAZgBvAHgAIABqAHUAbQBwAHMAIABvAHYAZQByACAAdABoAGUAIABsAGEAegB5ACAAZABvAGcALgBOAG8AcgBtAGEAbABUAGgAZQAgAHEAdQBpAGMAawAgAGIAcgBvAHcAbgAgAGYAbwB4ACAAagB1AG0AcABzACAAbwB2AGUAcgAgAHQAaABlACAAbABhAHoAeQAgAGQAbwBnAC4ATgBvAHIAbQBhAGwAVABoAGUAIABxAHUAaQBjAGsAIABiAHIAbwB3AG4AIABmAG8AeAAgAGoAdQBtAHAAcwAgAG8AdgBlAHIAIAB0AGgAZQAgAGwAYQB6AHkAIABkAG8AZwAuAFMAdABhAG4AZABhAGEAcgBkAFQAaABlACAAcQB1AGkAYwBrACAAYgByAG8AdwBuACAAZgBvAHgAIABqAHUAbQBwAHMAIABvAHYAZQByACAAdABoAGUAIABsAGEAegB5ACAAZABvAGcALgBUAGgAZQAgAHEAdQBpAGMAawAgAGIAcgBvAHcAbgAgAGYAbwB4ACAAagB1AG0AcABzACAAbwB2AGUAcgAgAHQAaABlACAAbABhAHoAeQAgAGQAbwBnAC4ATgBhAHYAYQBkAG4AbwBUAGgAZQAgAHUAcwBlACAAbwBmACAAdABoAGkAcwAgAGYAbwBuAHQAIABpAHMAIABnAHIAYQBuAHQAZQBkACAAcwB1AGIAagBlAGMAdAAgAHQAbwAgAEcATgBVACAARwBlAG4AZQByAGEAbAAgAFAAdQBiAGwAaQBjACAATABpAGMAZQBuAHMAZQAuAGgAdAB0AHAAOgAvAC8AdwB3AHcALgBnAG4AdQAuAG8AcgBnAC8AYwBvAHAAeQBsAGUAZgB0AC8AZwBwAGwALgBoAHQAbQBsAFQAaABlACAAcQB1AGkAYwBrACAAYgByAG8AdwBuACAAZgBvAHgAIABqAHUAbQBwAHMAIABvAHYAZQByACAAdABoAGUAIABsAGEAegB5ACAAZABvAGcALgAAAAADAAAAAwAAABwAAQAAAAAAnAADAAEAAAJSAAQAgAAAABwAEAADAAwAfgDXECEQJxAyEFkgDSAUIBkgHSBgImAlzP//AAAAIADXEAAQIxApEDYgCyATIBggHCBgImAlzP///+P/i/Bp8GjwZ/Bk4QjgVOBL4Engtt6320wAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgG2AAAAAADWAAEAAAAAAAAAAAAAAAAAAAABAAIAAgAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAADAAQABQAGAAcACAAJAAoACwAMAA0ADgAPABAAEQASABMAFAAVABYAFwAYABkAGgAbABwAHQAeAB8AIAAhACIAIwAkACUAJgAnACgAKQAqACsALAAtAC4ALwAwADEAMgAzADQANQA2ADcAOAA5ADoAOwA8AD0APgA/AEAAQQBCAEMARABFAEYARwBIAEkASgBLAEwATQBOAE8AUABRAFIAUwBUAFUAVgBXAFgAWQBaAFsAXABdAF4AXwBgAGEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEXAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZwBoAGUAZgBjAGQABACAAAAAHAAQAAMADAB+ANcQIRAnEDIQWSANIBQgGSAdIGAiYCXM//8AAAAgANcQABAjECkQNiALIBMgGCAcIGAiYCXM////4/+L8GnwaPBn8GThCOBU4EvgSeC23rfbTAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAFtAQIADAAAAAAAAgABAAAACQAAAQABRwAAAAAAAgAAAAAAAQAAB/8AADT8AAAANwAAACYAAQAAAAAAAf////8AAQABAAAAAP////4ACAACAAAAAv////8ACAADAAAAAP////0AAQAAAAAAAf////8AAQABAAAAAP////4ABQAAAAAABP////8ABQABAAAAAP////sAQAAAAAAAAP////cAQAABAAAACP////8AAQAAAAAAAf////8AAQABAAAAAP////4AAQABAAAAEP////8AAQACAAAAAP///+8AAQAAAAAAAf////8AAQABAAAAAP////4ABQAAAAAABP////8ABQABAAAAAP////sACAABAAAAIP////8ACAACAAAAAP///98ACAACAAAAAv////8ACAADAAAAAP////0ABQABAAAAQP////8ABQACAAAAAP///78ABQAAAAAABP////8ABQABAAAAAP////sABQABAAAAQP////8ABQACAAAAAP///78AAQAAAAAAAf////8AAQABAAAAAP////4ABQAAAAAABP////8ABQABAAAAAP////sABQABAAAAQP////8ABQACAAAAAP///78ABQACAAAAgP////8ABQADAAAAAP///38ACAACAAAAAv////8ACAADAAAAAP////0AAQAAAAAAAf////8AAQABAAAAAP////4ABQADAAABAP////8ABQAEAAAAAP///v8AAQAAAAAAAf////8AAQABAAAAAP////4ABQAAAAAABP////8ABQABAAAAAP////sAAQAAAAAAAf////8AAQABAAAAAP////4AGwABAAACAP////8AGwACAAAAAP///f8AGwAAAAAEAP////8AGwABAAAAAP//+/8AAQAAAAAAAf////8AAQABAAAAAP////4AAAABAAAAAAAAAAAAAAEIAAAAAgAAAAEAAAAHAAAAHAAAAIYAAAC+AAAA6AAAAPQAAAD6AAgAbQAyAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAYABQAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAgAAAAAAAQABAAMAAQABAAEABAABAAEABQABAAEABgABAAAAAAAAAABAAAAAAAOAAAAAAAIAAAAAAACgAAAAAAMAAAAAAAKAAAAAP///Yz///2O///+VAAAAAAAAATkAAAIYAAAAAQAAAAEAAAAGAAAAFAAAAbwAAAHgAAACAAAIAGkA0QAFAAUABQAFAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAFAAEAAQAFAAUAAQAFAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAQAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAIAAAABAAMAAAAA/////wACgAD/////AAIAAP////8AAAAAAAD//wAAAAQACAE5AAEA/AAAAJAAAAABAAAAAQAAAAYAAAAUAAAAPAAAAGAAAAB4AAgAjgARAAQAAQABAAEABAAEAAEABAAEAAQABAABAAQAAQAFAAEABAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAQAAAAEAAgAAAAD/////AAIAAP////8AAAAA//8AAAAAAAQACACcAAEA6AAAAIQAAAABAAAAAQAAAAYAAAAUAAAALgAAAFIAAABsAAgAkgAKAAQABAABAAEABAAEAAQABAAEAAUAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAEAAAABAAIAAAAA/////wACAAD/////AAAAAP//AAAAAAAAAAQACACbAAEA5wAAARgAAAABAAAAAgAAAAYAAAAUAAAAtAAAANgAAAD4AAgAmwBNAAQAAQABAAUAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAgAAAAEAAwAAAAD/////AAKAAP////8AAgAA/////wAAAAAAAAAAAAAABAAIAJsABADnAAAAAADpAAAAAAB8AAAAAQAAAAEAAAAGAAAAFAAAACgAAABMAAAAZAAIAJQABwAEAAEABAABAAEAAQAFAAAAAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAABAAAAAQACAAAAAP////8AAgAA/////wAAAAD//wAAAAAABAAIAJoAAQDmAAAAdAAAAAEAAAABAAAABgAAABQAAAAgAAAARAAAAFwACACUAAMABAABAAUAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAEAAAABAAIAAAAA/////wACAAD/////AAAAAP//AAAAAAAEAAgAlgABAOIAAACQAAAAAQAAAAEAAAAGAAAAFAAAADYAAABaAAAAdAAIAJIADgAFAAUAAQABAAEAAQAEAAEAAQABAAEAAQABAAQAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAEAAAABAAIAAAAA/////wACAAD/////AAAAAP//AAAAAAAAAAQACACSAAIA3gDfAAAAAAGEAAAAAQAAAAQAAAAHAAAAFAAAAQgAAAEyAAABVAAIAGkAdwAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAEAAQABAAEAAQAEAAEABgAGAAYABgAGAAYABgAGAAYAAQABAAEABgAFAAUABQAFAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAEAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAQAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAACAAAAAQACAAMAAAAA/////wACgAD/////AAIAAP////8AAAAA//8AAAAAAAAABAAIAJIADQDeAN8A4ADhAOIA4wDkAOUA5gAAAAAAAADpAAAAGAAAAAQAAAAIAAgA4gACAQMBBAAAAAAAdAAAAAEAAAABAAAABgAAABQAAAAeAAAAQgAAAFwACAChAAIABAAFAAAAAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAABAAAAAQACAAAAAP////8AAgAA/////wAAAAD//wAAAAAAAAAEAAgAogABAOwAAAB8AAAAAQAAAAEAAAAGAAAAFAAAACIAAABGAAAAYAAIAJ8ABAAEAAQABQAFAAAAAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAABAAAAAQACAAAAAP////8AAgAA/////wAAAAD//wAAAAAAAAAEAAgAoQACAOsA7AAAAAABEAAAAAEAAAABAAAABgAAABQAAAC0AAAA2AAAAPAACABpAE0ABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABQAFAAUABQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABAAEAAAAAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAABAAAAAQACAAAAAP////8AAgAA/////wAAAAD//wAAAAAABAAIAJ8ABADqAO4A6wDsAAAAAAKMAAAAAgAAABAAAAAfAAAAHAAAAIwAAAFGAAAB/AAAAgQAAAJKAAgAaQA1AAUABgAHAAgAAQAJAAoACwAMAAEAAQANAA4ADwAQABEAEgATABQAFQAWABcAGAAZABoAGwABAAEAHAABAB0AAQABAB4AAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQABAAMAAQABAAQABQAGAAcACAAJAAoACwAMAA0ADgAPABAAEQASABMAFAAVABYAFwAYABkAGgAbABwAHQAAAAAAAAAAQAAAAAACgAAAAAACAAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAoAAAAAAAP///l7///4UAAAABAAIAAwAAAAQABQAGAAcAAAAAAAgACQAKAAsADAANAA4ADwAQABEAEgATABQAFQAWAAAAAAAXAAAAGAAAAAAAGQAAARkBGgEbARwBHgEfASABIQElASYBJwEkASgBKQEqASsBLAEtAS4BLwEwATEBMgE1ATYBOAAAAAACUAAAAAEAAAABAAAABgAAABQAAAG6AAAB3gAAAfgACABpANAABAAEAAQABAABAAQABAAEAAQABAABAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAEAAQAEAAEABAAEAAEABAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAUABQAFAAUAAQAFAAUABQAFAAEAAQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQABAAEABQAFAAEABQAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAQAAAAEAAgAAAAD/////AAIAAP////8AAAAA//8AAAAAAAAABAAIARkAIAC+AL8AwADBAAAAwwDEAMUAxgAAAAAAzADJAMoAywDNAM4AzwDQANEA0gDTANQA1QDWANcAAAAAANoA2wAAAN0AAAAAAWgAAAABAAAAAQAAAAYAAAAUAAABEgAAATYAAAFQAAgAcwB8AAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABQABAAQAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAEAAAABAAIAAAAA/////wACAAD/////AAAAAP//AAAAAAAAAAQACADsAAEBAQAAAbgAAAABAAAABAAAAAcAAAAUAAABUgAAAXwAAAGcAAgAaQCcAAQABAAEAAQABAAEAAQABAABAAEAAQABAAEAAQABAAEABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAABAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAQABAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAFAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABgAGAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAgAAAAEAAgADAAAAAP////8AAoAA/////wACAAD/////AAAAAP//AAAAAAAEAAgBAwACAOIA4wAAAAABaAAAAAEAAAAgAAAABgAAABQAAAEKAAABLgAAAVAACAByAHgABAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABQABAAUAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAIAAAABAAMAAAAA/////wACgAD/////AAIAAP////8AAAAAAAD//wAAAAAABAAIAHIAAQEGAAABIAAAAAEAAAACAAAABgAAABQAAADCAAAA5gAAAQgACAByAFQABAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABQAFAAUAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAIAAAABAAMAAAAA/////wACgAD/////AAIAAP////8AAAAAAAD//wAAAAAABAAIAHIAAQEHAAABbAAAAAEAAABAAAAABwAAABQAAAEKAAABNAAAAVQACAB9AHgABAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABgAGAAYABgAGAAYABgAGAAYABgABAAEAAQABAAEAAQABAAEABQABAAYABgABAAEAAQABAAEABQAGAAYABgABAAYAAQABAAEAAQABAAYAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAACAAAAAQACAAMAAAAA/////wACgAD/////AAIAAP////8AAAAAAAD//wAAAAQACAB9AAEBCAAAAdgAAAABAAAABAAAAAgAAAAUAAABKAAAAWgAAAGQAAgAggCHAAQAAQAEAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABgABAAEAAQABAAEAAQABAAEAAQABAAEABQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAcAAQABAAEAAQAEAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAIAAQADAAAAAAADAAAAAQADAAMABAAAAAD/////AAIAAP////8AA4AA/////wADAAD/////AAAAAAAAAAAAAAAEAAgA7AAYAQIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAETAAAAAAF8AAAAAQAAAEAAAAAHAAAAFAAAARgAAAFCAAABZAAIAIQAfwAEAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABgAGAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABQABAAYABgABAAEAAQABAAEAAQAGAAEABQABAAYAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAGAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAgAAAAEAAgADAAAAAP////8AAoAA/////wACAAD/////AAAAAAAA//8AAAAAAAQACACEAAEBCQAAAYAAAAABAAAAQAAAAAcAAAAUAAABHgAAAUgAAAFoAAgAcwCCAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABQABAAEAAQABAAEAAQABAAEAAQABAAYAAQABAAEAAQABAAEAAQABAAYAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAACAAAAAQACAAMAAAAA/////wACgAD/////AAIAAP////8AAAAAAAD//wAAAAQACABzAAEBBQAAARwAAAACAAAAAQAAAAcAAAAcAAAAtgAAAOAAAAEAAAABCAAAAQwACACeAEoABQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAEAAEAAQABAAEAAQABAAEAAQAGAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAACAAAAAAABAAEAAwABAAEABAADAAAAAAAAAABAAAAAAAKAAAAAAAIAAAAAAACgAAAAAAA///9iv///IwAAAAAA7QAAAAABlAAAAAEAAAAEAAAABwAAABQAAAEcAAABRgAAAWgACABpAIEABAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABgABAAEAAQABAAEAAQABAAEABgABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAUAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAACAAAAAQACAAMAAAAA/////wACgAD/////AAIAAP////8AAAAA//8AAAAAAAAABAAIAJYACgDiAAAAAAAAAAAAAAAAAAAAAADqAAAAAAGcIAAAAAAAAAQAAAAHAAAAEAAAAT4AAAF2AAgAaQCUAAUABQAFAAUAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAUAAQABAAUABQABAAUAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAYAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAQAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAACAAAAAQADAAMABQAFAAQABQABAAUABQAAAAAAAoAAAAIAAAADIAAAAwAAAAAAAQAAAAACAAAAAAAAAABAAAAACQAAABAAAAFWAAAB1AAIAGkAoAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAEAAQABAAEAAQAEAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAEAAQAAQABAAEAAQABAAEAAQABAAYABgAGAAYAAQAGAAYABgAGAAEAAQAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgABAAEABgAGAAEABgABAAEAAQABAAEAAQAIAAEAAQABAAEAAQAHAAcABwABAAcAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAFAAEAAQABAAEABwABAAEAAQABAAEABAAEAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAQACAAMABAAFAAAAAAACAAAAAQAAAAMABAAFAAAAAAADAAAAAQAAAAAABAAFAAAAAAAAAAAAAQAAAAAABAAFAAcABwAGAAcABwAHAAcABwAHAAAAAAACgAAAAwAAAAQAAAAFAAAABiAAAAYAAAAAQAIAAAHgAAAAAAAAAIAAAAAIAAAAEAAAAVYAAAG2AAgAaQCgAAQABAAEAAEABAAEAAQABAABAAEAAQABAAEAAQABAAEABAABAAQABAAEAAQABAAEAAEABAABAAQAAQABAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAQABAABAAEAAQABAAEAAQABAAEABgAGAAYABgABAAYABgAGAAYAAQABAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAEAAQAGAAYAAQAGAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABwABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAUAAQABAAEAAQABAAEAAQABAAEAAQABAAQAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAEAAgADAAQAAAAAAAIAAAABAAAAAwAEAAAAAAADAAAAAQAAAAAABAAGAAYABQAGAAYABgAGAAYAAAAAAAKAAAADAAAABAAAAAUgAAAFAAAAAEACAAAAAAGYAAAAAQAAAAIAAAAGAAAAFAAAASYAAAFWAAABgAAIAGkAhgAFAAEAAQABAAEAAQAFAAEAAQABAAEAAQABAAEAAQABAAUABQABAAEAAQABAAEAAQABAAEAAQABAAUAAQAFAAUAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAQAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAIAAAABAAQABAAEAAMABAACAAIAAAAA/////wACgAD/////AAIAAP////8AAwAA/////wAAAAAAAP//AAAAAAAEAAgA7gABAO8AAAPMAAAAAgAAAAEAAAATAAAAHAAAARQAAAKQAAADQAAAA4AAAAOoAAgAdAB5AAQAAQAHAAEACQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABQAGAAoACAALAAwAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAA4AAQASAAEAAQABAA8AAQABAAEAEAANABEAAAAAAAAAAAACAAAAAAADAAAABAAAAAAABQAIAAYAAAAHAAkAAAAAAAAAAAAAAAIAAAAAAAMAAAAEAAAAAAAFAAgABgAAAAcACQAAAAEAAQAKAAEAAQALAAwAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAA0AAQABAAEAAQABAA4AAQABAAEAAQABAAEAAQABAAEAAQABAAEADwABAAEAAQABAAEAAQABABAAEQABAAEAAQABAAEAAQABAAEAAQASAAEAAQABAAEAAQABAAEAAQABAAEAEwABAAEAAQABAAEAAQABABQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAFQABAAEAAQABAAEAFgABAAEAAQABAAEAAQABAAEAAQABABcAAQABAAEAGAABAAEAAQAZAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAaAAEAAQABABsAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAHAAAAAAAAAAAQAAAAAACgAAAAAADgAAAAAAEgAAAAAAFgAAAAAAGgAAAAAAHgAAAAAAIgAAAAAAJgAAAAAACAAAAAAAAoAAAAAAAoAAAAAADAAAAAAAAoAAAAgAEAAAAAAAAoAAABAAAoAAABAAFAAAAAAAAoAAABgAGAAAAAAAAoAAACAAHAAAAAAAAoAAACgAAoAAACgAIAAAAAAAAoAAADAAJAAAAAAAAoAAADgAAP///N7///44///83v///jj///zq///+QP///Hr///zw///8lv///LD///yK///8lP///JL///yY///8wv///JwAAAAEAAAAAAAIAAAAAAAEAAwAAAAUAAAAGAAAAAQAHAAAACQAAAAoBDgELAQoBDAENAVABAAD1APgA9AECAAAAAAHYAAAAAQAAAQAAAAAHAAAAFAAAAVoAAAGSAAABvAAIAGkAoAAFAAUABQABAAUABQAFAAUAAQABAAEAAQABAAEAAQABAAUABQAFAAUABQAFAAUABQABAAUAAQAFAAUAAQAFAAUAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAFAAUAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAYAAQABAAQABAABAAEAAQABAAYAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAFAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAgAAAAEAAwAAAAAAAAADAAAAAQAAAAQAAAAA/////wACgAD/////AAIAAP////8AAwAA/////wAAAAAAAP//AAAAAAAEAAgA7gACAPIA8wAAAAAB4AAAAAEAAAEAAAAACAAAABQAAAFaAAABmgAAAcQACABpAKAABQAFAAUAAQAFAAUABQAFAAEAAQABAAEAAQABAAEAAQAFAAUABQAFAAUABQAFAAUAAQAFAAEABQAFAAEABQAFAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABQAFAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAcABwABAAEAAQAHAAEAAQABAAEAAQABAAEAAQAEAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEABwABAAEAAQABAAYAAQABAAEAAQABAAEABQAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAACAAAAAQADAAEAAAAAAAAAAwAAAAEAAAADAAQAAAAA/////wACgAD/////AAIAAP////8AAwAA/////wAAAAAAAP//AAAAAAAEAAgA7gACAPAA8QAAAAAAxAAAAAEAAAABAAAABwAAABQAAABUAAAAfgAAAKAACADgAB0ABgAGAAEAAQABAAEABgABAAEAAQAFAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAEAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAQAAAAEAAgADAAAAAP////8AAgAA/////wACAAD/////AAAAAP//AAAAAAAAAAQACADgAAcA/QD+AAAAAAAAAAAA/wAAAKAgAAAAAAAABAAAAAcAAAAQAAAAQgAAAHoACADqABYABQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABAAGAAYABgAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAIAAAABAAIAAwAFAAUABAAFAAEABQAFAAAAAAACgAAAAgAAAAMgAAADAAAAAAACAAAAAAB4AAAAAQAAAAEAAAAGAAAAFAAAACIAAABGAAAAYAAIAPwABAAFAAQABAAEAAAAAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAABAAAAAQACAAAAAP////8AAgAA/////wAAAAD//wAAAAAAAAAEAAgA/AABARMAAAGUAAAAAQAAAgAAAAAHAAAAFAAAAToAAAFkAAABfAAIAIQAkAAEAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEABQAFAAUAAQAFAAUABQAGAAEAAQAEAAQABQABAAEAAQABAAEAAQABAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAEAAQABAABAAEAAQABAAQAAQABAAEAAQABAAEAAQABAAEABQAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAEAAAABAAEAAgAAAAD/////AAIAAP////8AAAAA//8AAAAAAAQACADnAAEBEQAAAVQAAAABAAAEAAAAAAcAAAAUAAAA+gAAASQAAAE8AAgAfQBwAAQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAUABQAEAAQAAQAFAAUABgABAAUAAQABAAQAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAABAAAAAQABAAIAAAAA/////wACAAD/////AAAAAP//AAAAAAAEAAgA5wABARIAAAEgAAAAAQAAAAEAAAAGAAAAFAAAAMgAAADsAAABBAAIAHkAVwAEAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAUABQAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAQAAAAEAAgAAAAD/////AAIAAP////8AAAAA//8AAAAAAAQACADOAAIBawFsAAAAAQGRAZAABQAAAooCuwAvAIwCigK7/10B3wAxAQIAAAICBgMFBAUCAwTlk6r/wgD9/wNQGygAAAAAR05VIABAACAlzAMg/zgAAAOEAZBgAQH/3/8AAAAAAAEAAAAMAAAAIgAAAAIAAwAAAOUAAQDmAOYAAwDnAWwAAQAEAAAAAQAAAAAAAQAAAAoAHgAuAAFteW1yAAgABAAAAAD//wABAAAAAWtlcm4ACAAAAAIAAAABAAMACAAQABgAAgAAAAEAGAAIAAAAAQCWAAIAAAABAUoAAQBGAAAAAQANACQAJAAkACQAJAAkACQAJAAkACQAJAAkACQACADB/0YAxP9GAMr/RgDN/0YAzv9GAM//RgDW/0YA3P9GAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAADAAAAAgAUAEgAAQCOAAEAAAACAAIACACEAIQAAAC+AL4AAQDBAMEAAgDEAMQAAwDKAMoABADNAM8ABQDWANYACADaANwACQACAAsAagBrAAAAbQBuAAIAcABwAAQAdwB3AAUAewCAAAYAggCCAAwAhACEAA0AhgCGAA4AjwCPAA8AtAC1ABABCAEJABIAAgAHAL4AvgAAAMEAwQABAMQAxAACAMoAygADAM0AzwAEANYA1gAHANoA3AAIAAEAxgAEAAAADAB0ACIAIgAiACIAIgAiACIAIgAiACIAIgAUAGoAMgBrADIAbQAyAG4AMgBwADIAdwAyAHsAMgB8ADIAfQAyAH4AMgB/ADIAgAAyAIIAMgCEADIAhgAyAI8AMgC0ADIAtQAyAQgAMgEJADIAFABqAJYAawCWAG0AlgBuAJYAcACWAHcAlgB7AJYAfACWAH0AlgB+AJYAfwCWAIAAlgCCAJYAhACWAIYAlgCPAJYAtACWALUAlgEIAJYBCQCWAAIACACEAIQAAAC+AL4AAQDBAMEAAgDEAMQAAwDKAMoABADNAM8ABQDWANYACADaANwACQAAAAEAAAAKACAAqgABbXltcgAIAAQAAAAA//8AAgAAAAEAAmNsaWcADmxpZ2EAgAAAADcAAAABAAIAAwAEAAUABgAHAAgACQAKAAsADAANABAAEQASABMAFAAVABYAFwAYABkAGgAbABwAHQAeAB8AIAAhACIAIwAkACUAJgAnACgAKQAqACsALAAtAC4ALwAwADEAMgAzADQANQA2ADcAOAAAAAMADgAPABUAbgDeAOYA8AD4AQABCAEQARgBIAEoATABOAFAAUgBUAFYAWABaAF2AYQBjAGcAaQBrAG6AcIBygHSAdoB5gHuAgACCAIQAhwCJAIsAjYCQAJKAlICWgJiAm4ChAKMApYCugLEAtAC+gMEAyYDNAM8A0QDTgNYA2ADaANwA3gDgAOIA5ADmAOgA6gDsAO4A8ADyAPQA9gD4APoA/AD+AQABAgEEAQYBCAEKAQwBDgEQARIBFAEWARgBGgEcAR4BIAEiASQBJgEoASoBLAEuATABMgE0ATYBOAE6ATwBPgABgAAAAEEIgAGAAAAAgRUBGYABgAAAAEEsgAGAAAAAQT2AAYAAAABBSgABgAAAAEFbAAGAAAAAQWqAAYAAAABBeQABgAAAAEGFgAGAAAAAQZIAAYAAAABBnoABgAAAAEGrAAGAAAAAQboAAYAAAABBwAABAAAAAEHHgAEAAAAAQf+AAYAAAABCQ4ABgAAAAQJSgleCXAJhAAGAAAABAn+ChAKJAo4AAYAAAABCxIABgAAAAULNAtGC1oLbAuAAAQAAAABC9oABgAAAAEMNAAGAAAABAxgDHIMhgyYAAYAAAABDPgABgAAAAENRgAGAAAAAQ2UAAYAAAABDeIABgAAAAMOKA48DlAABgAAAAEOqgAGAAAABg7wDwQPGA8sD0IPWAAGAAAAAQ/kAAYAAAABEDwABgAAAAMQjBCgELYABgAAAAERHgAGAAAAARFcAAYAAAACEZoRrgAGAAAAAhIcEjIABgAAAAISnhKyAAYAAAABEyAABgAAAAETXgAGAAAAARN8AAYAAAADE8oT3BPwAAYAAAAIFF4UdBSKFKIUuhTSFOoVBAAGAAAAAResAAYAAAACF9IX7gAGAAAADxi4GNIY7BkGGSAZOBlQGWgZgBmYGbAZyBngGfgaEgAGAAAAAh7AHtwABgAAAAMfpB+8H9QABgAAABIgvCDSIOgg/iEUISwhRCFaIXAhhiGcIbQhzCHkIfwiFCIsIkIABgAAAAInjCemAAYAAAAOKH4olCiqKMAo1ijsKQIpHCk2KU4pZil+KZYprgAGAAAABC4+LlAuZC54AAQAAAABLv4ABgAAAAEygAAGAAAAAjKyMsQABgAAAAIzTDNeAAEAAAABM7AAAQAAAAEztgABAAAAATO8AAEAAAABM8IAAQAAAAEzyAABAAAAATPOAAEAAAABM9QAAQAAAAEz3gABAAAAATPkAAEAAAABM+oAAQAAAAEz8AABAAAAATP2AAEAAAABNBQAAgAAAAE0GgABAAAAATQoAAEAAAABNHgAAQAAAAE0ggABAAAAATSMAAEAAAABNJYAAQAAAAE0oAABAAAAATSmAAIAAAABNKwAAgAAAAE26AACAAAAATigAAIAAAABOlgAAgAAAAE8UgACAAAAAT3IAAIAAAABP8IAAgAAAAFBOAACAAAAAUMyAAIAAAABRKgAAgAAAAFGogACAAAAAUgYAAIAAAABSY4AAgAAAAFLiAACAAAAAUz+AAIAAAABTnQAAgAAAAFQLAAEAAAAAVA6AAIAAAABUEwABAAAAAFSvgABAAAAAVLOAAQAAAABUtQABAAAAAFS9AAEAAAAAVZ8AAQAAAABVpAABAAAAAFW6AAEAAAAAVmKAAQAAAABWaoAAQAAAAFZ0gABAAAAAVnmAAEAAAABWewAAQAAAAFZ8gADAAEAGAABABIAAAABAAAAOQABAAEAnAACAAUAjgCOAAAAkgCTAAEAlQCYAAMAmgCbAAcAngCeAAkAAwABACwAAQAmAAAAAQAAADoAAwACAE4ASAABABQAAAABAAAAOgABAAEAngACAAcAaQCKAAAAjwCQACIAmwCbACQAnwCfACUAogCjACYAtAC1ACgBBwEJACoAAQABAJgAAQACAJIAkwADAAEAGAABABIAAAABAAAAOwABAAEAmwACAAgAaQCKAAAAjwCQACIAkgCTACQAlQCaACYAowCjACwAtAC1AC0A6QDpAC8BBwEJADAAAwABABgAAQASAAAAAQAAADwAAQABAJQAAgAFAGkAigAAAI8AkAAiAJ8ApAAkALQAtQAqAQcBCQAsAAMAAQAYAAEAEgAAAAEAAAA9AAEAAQCWAAIACABpAIoAAACNAI0AIgCPAJAAIwCfAKMAJQC0ALUAKgDgAOAALADpAOkALQEHAQkALgADAAEAGAABABIAAAABAAAAPgABAAEAmgACAAcAaQCKAAAAjwCQACIAnwCjACQAtAC1ACkA4ADgACsBAwEDACwBBwEJAC0AAwABABoAAQASAAAAAQAAAD8AAQACAJIAkwACAAYAaQCKAAAAjwCQACIAmACYACQAnwCtACUAtAC1ADQBBwEJADYAAwABABgAAQASAAAAAQAAAEAAAQABAJcAAgAFAGkAigAAAI8AkAAiAJ8AowAkALQAtQApAQcBCQArAAMAAQAYAAEAEgAAAAEAAABBAAEAAQCVAAIABQBpAIoAAACPAJAAIgCfAKMAJAC0ALUAKQEHAQkAKwADAAEAGAABABIAAAABAAAAQgABAAEAmQACAAUAaQCKAAAAjwCQACIAnwCjACQAtAC1ACkBBwEJACsAAwABABgAAQASAAAAAQAAAEMAAQABAJgAAgAFAGkAigAAAI8AkAAiAJ8AowAkALQAtQApAQcBCQArAAMAAQAiAAEAEgAAAAEAAABEAAIAAgCfAKIAAAC2ALkABAACAAUAaQCKAAAAjwCQACIAowCjACQAtAC1ACUBBwEJACcAAwAAAAEAEgABABgAAQAAAEUAAQABAHIAAQACAOcA6QADAAEAGgABABQAAQAgAAEAAABGAAEAAQDnAAEAAQDeAAEAAQDpAAEAqAAKABoAJgA8AFIAaAByAH4AiACSAJ4AAQAEATkAAwDpAJ0AAgAGAA4BCwADAJ0AdQEOAAMAnQB0AAIABgAOAQoAAwCdAHcBDwADAJ0AdgACAAYADgEMAAMAnQB2AQ0AAwCdAHgAAQAEAU4AAgDsAAEABAFQAAMAeQDrAAEABAD0AAIAogABAAQA7QACAOkAAQAEAPkAAwChAKIAAQAEAQIAAgEDAAIACgBtAG0AAAB0AHQAAQB2AHYAAgB4AHgAAwCJAIkABACdAJ0ABQChAKEABgDeAN4ABwDqAOoACADsAOwACQABARIAAQAIACEARABKAFAAVgBcAGIAaABuAHQAegCAAIYAjACSAJgAngCkAKoAsAC2ALwAwgDIAM4A1ADaAOAA5gDsAPIA+AD+AQQBGQACAGkBGgACAGoBGwACAGsBHAACAGwBHQACAG0BHgACAG4BHwACAG8BIAACAHABIQACAHEBIgACAHIBIwACAHMBJQACAHQBJgACAHUBJwACAHYBJAACAHcBKAACAHgBKQACAHkBKgACAHoBKwACAHsBLAACAHwBLQACAH0BLgACAH4BLwACAH8BMAACAIABMQACAIEBMgACAIIBMwACAIMBNAACAIQBNQACAIUBNgACAIcBNwACAIgBRgACALQBRwACALUAAQABAJ0AAwABACIAAQASAAAAAQAAAEcAAgACARkBNwAAAUYBRwAfAAIABQBpAIoAAACPAJAAIgCjAKMAJAC0ALUAJQEHAQkAJwADAAIAXABWAAEATgAAAAEAAABIAAMAAQBOAAEAOgAAAAEAAABIAAMAAgCAAF4AAQAoAAAAAQAAAEgAAwACAHgAcgABABQAAAABAAAASAABAAIAnwDfAAEAAQBpAAEAAQDpAAIABQBqAGsAAABtAG0AAgB7AHsAAwB+AH4ABACGAIYABQACAAUAagBrAAAAbQBtAAIAewB7AAMAfgB+AAQAhgCGAAUAAQABAOQAAQABAHsAAgACAL4A3AAAAUgBSQAfAAMAAQBWAAEATgAAAAEAAABJAAMAAgCyAHIAAQA8AAAAAQAAAEkAAwACAOoApAABACgAAAABAAAASQADAAIA4gDcAAEAFAAAAAEAAABJAAEAAgEDAQQAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAABAAEA4AACAAsAagBrAAAAbQBuAAIAcABwAAQAdwB3AAUAewCAAAYAggCCAAwAhACEAA0AhgCGAA4AjwCPAA8AtAC1ABABCAEJABIAAQABAOAAAQABAH0AAQABAOkAAwABABoAAQASAAAAAQAAAEoAAQACAKIA7AACAAIAcgBzAAAA7gDuAAIAAwAAAAEAXgABAGYAAQAAAEsAAwAAAAEATAACAGQAagABAAAASwADAAAAAQA4AAEAXAABAAAASwADAAAAAQAmAAIAbAByAAEAAABLAAMAAAABABIAAQBkAAEAAABLAAEAAgByAH0AAgACAOIA4gAAAOoA7AABAAEAAQDgAAEAAQDiAAIABQC+ANwAAADjAOMAHwDuAO4AIAECAQIAIQFIAUkAIgABAAEA6QABAAEA4gABAAEBUAABAEwABQAQABoALAA2AEIAAQAEAQAAAgDmAAIABgAMAPUAAgChAPgAAgCiAAEABAD0AAIAogABAAQBEAADAOABAwABAAQA9gACAKEAAgADAOAA4AAAAOoA7AABAO4A7gAEAAMAAAABABIAAQAYAAEAAABMAAEAAQBzAAIABAC+ANwAAADqAOsAHwD0APQAIQFIAUkAIgADAAAAAQBMAAEAUgABAAAATQADAAAAAQA6AAIASABOAAEAAABNAAMAAAABACYAAQBAAAEAAABNAAMAAAABABQAAgBiAGgAAQAAAE0AAQABAIQAAQACAOIA6gABAAEA4AABAAEA4gACAAgAvgDcAAAA4wDjAB8A7gDuACAA9QD2ACEA+AD4ACMBAgECACQBEAEQACUBSAFJACYAAQABAOkAAQABAOIAAwABAEQAAQAWAAIASgBQAAEAAABOAAIABwBpAHAAAAB3AIgACACKAIoAGgCPAI8AGwCjAKMAHAC0ALUAHQEIAQkAHwABAAEBOQABAAEA7gABAAEA5AADAAEARAABABYAAgBKAFAAAQAAAE8AAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAEAAQE5AAEAAQDuAAEAAQDgAAMAAQBEAAEAFgACAEoAUAABAAAAUAACAAcAaQBwAAAAdwCIAAgAigCKABoAjwCPABsAowCjABwAtAC1AB0BCAEJAB8AAQABATkAAQABAO4AAQABAOYAAwABAEIAAQAUAAEASAABAAAAUQACAAcAaQBwAAAAdwCIAAgAigCKABoAjwCPABsAowCjABwAtAC1AB0BCAEJAB8AAQABATkAAQABAO4AAwAAAAEAPAACAGoAcAABAAAAUgADAAAAAQAoAAIAYgBoAAEAAABSAAMAAAABABQAAgBaAGAAAQAAAFIAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAEAAQD2AAEAAQDgAAEAAQD2AAEAAQDhAAEAAQD2AAEAAQDlAAMAAAABABQAAgBCAEgAAQAAAFMAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAEAAQD2AAEAAQDkAAMAAAABAH4AAgCsALIAAQAAAFQAAwAAAAEAagACAKQAqgABAAAAVAADAAAAAQBWAAIAnACiAAEAAABUAAMAAAABAEIAAwCUAJoAoAABAAAAVAADAAAAAQAsAAMAkACWAJwAAQAAAFQAAwAAAAEAFgADAIwAkgCYAAEAAABUAAIABwBpAHAAAAB3AIgACACKAIoAGgCPAI8AGwCjAKMAHAC0ALUAHQEIAQkAHwABAAEA7gABAAEA4AABAAEA7gABAAEA4QABAAEA7gABAAEA5QABAAEA7gABAAEBAQABAAEA4AABAAEA7gABAAEBAQABAAEA4QABAAEA7gABAAEBAQABAAEA5QADAAAAAQAWAAMARABUAFoAAQAAAFUAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAIAAgC+ANwAAAFIAUkAHwABAAEA7gABAAEA5AADAAAAAQAUAAIAQgBSAAEAAABWAAIABwBpAHAAAAB3AIgACACKAIoAGgCPAI8AGwCjAKMAHAC0ALUAHQEIAQkAHwACAAIAvgDcAAABSAFJAB8AAQABAO4AAwAAAAEAQAACAG4AdAABAAAAVwADAAAAAQAsAAMAZgBsAHIAAQAAAFcAAwAAAAEAFgADAGIAaABuAAEAAABXAAIABwBpAHAAAAB3AIgACACKAIoAGgCPAI8AGwCjAKMAHAC0ALUAHQEIAQkAHwABAAEA7gABAAEA5AABAAEA7gABAAEBAQABAAEA5AABAAEA7gABAAEA9AABAAEA5AADAAAAAQASAAEAQAABAAAAWAACAAcAaQBwAAAAdwCIAAgAigCKABoAjwCPABsAowCjABwAtAC1AB0BCAEJAB8AAQABAPYAAwAAAAEAEgABAEAAAQAAAFkAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAEAAQDuAAMAAQBYAAEAKgABAF4AAQAAAFoAAwABAFAAAQAWAAIAVgByAAEAAABaAAIABwBpAHAAAAB3AIgACACKAIoAGgCPAI8AGwCjAKMAHAC0ALUAHQEIAQkAHwABAAEBOQABAAEA5AABAAEBOQACAAQA6gDsAAAA9AD1AAMA+AD5AAUBAQEBAAcAAQABAOQAAwABAFgAAQAqAAIAXgB6AAEAAABbAAMAAQBqAAEAFAABAHAAAQAAAFsAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAEAAQE5AAIABADqAOwAAAD0APUAAwD4APkABQEBAQEABwABAAEA4AABAAEBOQABAAEA4AADAAEAWAABACoAAQBeAAEAAABcAAMAAQBQAAEAFgACAFYAcgABAAAAXAACAAcAaQBwAAAAdwCIAAgAigCKABoAjwCPABsAowCjABwAtAC1AB0BCAEJAB8AAQABATkAAQABAOEAAQABATkAAgAEAOoA7AAAAPQA9QADAPgA+QAFAQEBAQAHAAEAAQDhAAMAAQBAAAEAEgAAAAEAAABdAAIABwBpAHAAAAB3AIgACACKAIoAGgCPAI8AGwCjAKMAHAC0ALUAHQEIAQkAHwABAAEBOQADAAIAIAAaAAEAFAAAAAEAAABeAAEAAQCYAAEAAQE5AAEAAQETAAMAAQAoAAMAFgAcACIAAAABAAAAXwABAAEA/AABAAEAAAABAAEA5gACAAcAaQBwAAAAdwCIAAgAigCKABoAjwCPABsAowCjABwAtAC1AB0BCAEJAB8AAwAAAAEAOgABAGIAAQAAAGAAAwAAAAEAKAACAFYAcgABAAAAYAADAAAAAQAUAAIAZAB0AAEAAABgAAIABgBpAIoAAACPAJAAIgCjAKMAJAC0ALUAJQEHAQ8AJwFOAU4AMAABAAEA5AACAAQA6gDsAAAA9AD1AAMA+AD5AAUBAQEBAAcAAQABAOQAAgACAL4A3AAAAUgBSQAfAAEAAQDkAAMAAAACAMAAxgACAMwBDAABAAAAYQADAAAAAgCqALAAAgEAAUYAAQAAAGEAAwAAAAIAlACaAAMBOgFAAYAAAQAAAGEAAwAAAAIAfACCAAMBcgF4Ab4AAQAAAGEAAwAAAAIAZABqAAMBsAG2AfYAAQAAAGEAAwAAAAIATABSAAMB6AHuAjQAAQAAAGEAAwAAAAIANAA6AAQCJgIsAjICcgABAAAAYQADAAAAAgAaACAABAJiAmgCbgK0AAEAAABhAAEAAQE5AAEAAQEVAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAACAAEA/AD/AAAAAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAIAAQD8AP8AAAABAAEA5AACAAoAaQBpAAAAbABsAAEAbwBvAAIAeAB6AAMAgQCBAAYAgwCDAAcAhQCFAAgAhwCIAAkAigCKAAsAowCjAAwAAgABAPwA/wAAAAEAAQDkAAIACwBqAGsAAABtAG4AAgBwAHAABAB3AHcABQB7AIAABgCCAIIADACEAIQADQCGAIYADgCPAI8ADwC0ALUAEAEIAQkAEgACAAEA/AD/AAAAAQABAPEAAgAKAGkAaQAAAGwAbAABAG8AbwACAHgAegADAIEAgQAGAIMAgwAHAIUAhQAIAIcAiAAJAIoAigALAKMAowAMAAIAAQD8AP8AAAABAAEA8AACAAsAagBrAAAAbQBuAAIAcABwAAQAdwB3AAUAewCAAAYAggCCAAwAhACEAA0AhgCGAA4AjwCPAA8AtAC1ABABCAEJABIAAgABAPwA/wAAAAEAAQDkAAEAAQDxAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAACAAEA/AD/AAAAAQABAOQAAQABAPAAAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAIAAQD8AP8AAAADAAAAAQAWAAMAHAAiACgAAQAAAGIAAQABATkAAQABARMAAQABAJgAAQABAPwAAwADAF4AWABSAAQAOAA+AEQATAAAAAEAAABjAAMAAwCOAIgAggAEABwAIgAoADAAAAABAAAAYwABAAEBFQABAAEA7gABAAIA9AEBAAEAAQDkAAEAAQDkAAEAAQDvAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAABAAEA5AABAAEA7gACAAsAagBrAAAAbQBuAAIAcABwAAQAdwB3AAUAewCAAAYAggCCAAwAhACEAA0AhgCGAA4AjwCPAA8AtAC1ABABCAEJABIAAwADAewB5gHgAAMBdAGQAcQAAAABAAAAZAADAAMCJAIeAhgAAwFaAXYBqgAAAAEAAABkAAMAAwJWAlACSgADAUABXAGQAAAAAQAAAGQAAwADAogCggJ8AAMBJgFCAXYAAAABAAAAZAADAAICugK0AAMBDAEoAVwAAAABAAAAZAADAAIC7gLoAAMA9AEQAUQAAAABAAAAZAADAAIDHAMWAAMA3AD4ASwAAAABAAAAZAADAAIDSgNEAAMAxADgARQAAAABAAAAZAADAAIDfgN4AAMArADIAPwAAAABAAAAZAADAAIDiAOCAAMAlACwAOQAAAABAAAAZAADAAIDtgOwAAMAfACYAMwAAAABAAAAZAADAAID6gPkAAMAZACAALQAAAABAAAAZAADAAIEGAQSAAMATABoAJwAAAABAAAAZAADAAMEkgRMBEYAAwA0AFAAhAAAAAEAAABkAAMAAwTIBIgEggADABoANgBqAAAAAQAAAGQAAgAEAAAAAAAAAO4A7gABAPwA/wACARUBFQAGAAIACAAAAAAAAAC+ANwAAQDqAOwAIADuAO4AIwD0APYAJAD4APkAJwEBAQEAKQFIAUkAKgACAAQA4ADhAAAA5ADkAAIA5gDmAAMA7gDuAAQAAQABAOQAAQABAO4AAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAEAAQDkAAEAAQDvAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAABAAEA5AABAAEA9wACAAoAaQBpAAAAbABsAAEAbwBvAAIAeAB6AAMAgQCBAAYAgwCDAAcAhQCFAAgAhwCIAAkAigCKAAsAowCjAAwAAQABAOQAAQABAPYAAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAEAAQDuAAIACwBqAGsAAABtAG4AAgBwAHAABAB3AHcABQB7AIAABgCCAIIADACEAIQADQCGAIYADgCPAI8ADwC0ALUAEAEIAQkAEgABAAEA7wACAAoAaQBpAAAAbABsAAEAbwBvAAIAeAB6AAMAgQCBAAYAgwCDAAcAhQCFAAgAhwCIAAkAigCKAAsAowCjAAwAAQABAOQAAgAKAGkAaQAAAGwAbAABAG8AbwACAHgAegADAIEAgQAGAIMAgwAHAIUAhQAIAIcAiAAJAIoAigALAKMAowAMAAEAAQDkAAIACwBqAGsAAABtAG4AAgBwAHAABAB3AHcABQB7AIAABgCCAIIADACEAIQADQCGAIYADgCPAI8ADwC0ALUAEAEIAQkAEgABAAEA5AACAAQAcQB2AAAAiQCJAAYAkACQAAcBBwEHAAgAAQABAPEAAgAKAGkAaQAAAGwAbAABAG8AbwACAHgAegADAIEAgQAGAIMAgwAHAIUAhQAIAIcAiAAJAIoAigALAKMAowAMAAEAAQDwAAIACwBqAGsAAABtAG4AAgBwAHAABAB3AHcABQB7AIAABgCCAIIADACEAIQADQCGAIYADgCPAI8ADwC0ALUAEAEIAQkAEgABAAEA8wACAAoAaQBpAAAAbABsAAEAbwBvAAIAeAB6AAMAgQCBAAYAgwCDAAcAhQCFAAgAhwCIAAkAigCKAAsAowCjAAwAAQABAPIAAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAEAAQDkAAIACwBqAGsAAABtAG4AAgBwAHAABAB3AHcABQB7AIAABgCCAIIADACEAIQADQCGAIYADgCPAI8ADwC0ALUAEAEIAQkAEgACAAEA/AD/AAAAAQABAOQAAgAKAGkAaQAAAGwAbAABAG8AbwACAHgAegADAIEAgQAGAIMAgwAHAIUAhQAIAIcAiAAJAIoAigALAKMAowAMAAIAAQD8AP8AAAADAAMAXABWAFAABAA4AD4ARABKAAAAAQAAAGUAAwADAJIAjACGAAQAHAAiACgALgAAAAEAAABlAAEAAQEVAAEAAQDuAAEAAQEBAAEAAQDkAAEAAQDkAAEAAQDuAAIACwBqAGsAAABtAG4AAgBwAHAABAB3AHcABQB7AIAABgCCAIIADACEAIQADQCGAIYADgCPAI8ADwC0ALUAEAEIAQkAEgABAAEA5AABAAEA7gACAAoAaQBpAAAAbABsAAEAbwBvAAIAeAB6AAMAgQCBAAYAgwCDAAcAhQCFAAgAhwCIAAkAigCKAAsAowCjAAwAAwACAHYAcAADAEgATgBqAAAAAQAAAGYAAwACAKQAngADADAANgBSAAAAAQAAAGYAAwACANgA0gADABgAHgA6AAAAAQAAAGYAAQABARUAAgAEAOoA7AAAAPQA9QADAPgA+QAFAQEBAQAHAAEAAQDkAAEAAQDkAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAABAAEA5AACAAsAagBrAAAAbQBuAAIAcABwAAQAdwB3AAUAewCAAAYAggCCAAwAhACEAA0AhgCGAA4AjwCPAA8AtAC1ABABCAEJABIAAQABAOQAAgAEAHEAdgAAAIkAiQAGAJAAkAAHAQcBBwAIAAMAAgHsAeYAAgGcAdAAAAABAAAAZwADAAICHAIWAAIBhgG6AAAAAQAAAGcAAwACAlICTAACAXABpAAAAAEAAABnAAMAAgJeAlgAAgFaAY4AAAABAAAAZwADAAMCngJeAlgAAgFEAXgAAAABAAAAZwADAAMC3AKWApAAAgEsAWAAAAABAAAAZwADAAIDDgLOAAIBFAFIAAAAAQAAAGcAAwACAz4C/gACAP4BMgAAAAEAAABnAAMAAgN0Ay4AAgDoARwAAAABAAAAZwADAAIDqgNkAAIA0gEGAAAAAQAAAGcAAwADA+ADoAOaAAIAvADwAAAAAQAAAGcAAwADBCQD3gPYAAIApADYAAAAAQAAAGcAAwADBCgEIgQcAAIAjADAAAAAAQAAAGcAAwADBFwEVgRQAAIAdACoAAAAAQAAAGcAAwADBJYEkASKAAIAXACQAAAAAQAAAGcAAwADBMoExAS+AAIARAB4AAAAAQAAAGcAAwACBP4E+AACACwAYAAAAAEAAABnAAMAAgUuBSgAAgAWAEoAAAABAAAAZwACAAgAvgDcAAAA6gDsAB8A9AD1ACIA+AD5ACQA/AD/ACYBAQEBACoBFQEVACsBSAFJACwAAgADAOAA4AAAAOQA5AABAO4A8QACAAEAAQDkAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAABAAEA5AACAAsAagBrAAAAbQBuAAIAcABwAAQAdwB3AAUAewCAAAYAggCCAAwAhACEAA0AhgCGAA4AjwCPAA8AtAC1ABABCAEJABIAAQABAOQAAgAEAHEAdgAAAIkAiQAGAJAAkAAHAQcBBwAIAAEAAQDkAAIAAgEKAQ8AAAFOAU4ABgABAAEA5AACAAoAaQBpAAAAbABsAAEAbwBvAAIAeAB6AAMAgQCBAAYAgwCDAAcAhQCFAAgAhwCIAAkAigCKAAsAowCjAAwAAgABAPwA/wAAAAEAAQDkAAIACwBqAGsAAABtAG4AAgBwAHAABAB3AHcABQB7AIAABgCCAIIADACEAIQADQCGAIYADgCPAI8ADwC0ALUAEAEIAQkAEgACAAEA/AD/AAAAAgAKAGkAaQAAAGwAbAABAG8AbwACAHgAegADAIEAgQAGAIMAgwAHAIUAhQAIAIcAiAAJAIoAigALAKMAowAMAAEAAQD9AAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAABAAEA/gACAAsAagBrAAAAbQBuAAIAcABwAAQAdwB3AAUAewCAAAYAggCCAAwAhACEAA0AhgCGAA4AjwCPAA8AtAC1ABABCAEJABIAAQABAP0AAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAEAAQD+AAEAAQDkAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAACAAIAvgDcAAABSAFJAB8AAQABAOQAAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAIAAgC+ANwAAAFIAUkAHwABAAEA5AABAAEA7wACAAoAaQBpAAAAbABsAAEAbwBvAAIAeAB6AAMAgQCBAAYAgwCDAAcAhQCFAAgAhwCIAAkAigCKAAsAowCjAAwAAQABAOQAAQABAO4AAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAEAAQDkAAEAAQDzAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAABAAEA5AABAAEA8gACAAsAagBrAAAAbQBuAAIAcABwAAQAdwB3AAUAewCAAAYAggCCAAwAhACEAA0AhgCGAA4AjwCPAA8AtAC1ABABCAEJABIAAQABAPEAAgAKAGkAaQAAAGwAbAABAG8AbwACAHgAegADAIEAgQAGAIMAgwAHAIUAhQAIAIcAiAAJAIoAigALAKMAowAMAAEAAQDwAAIACwBqAGsAAABtAG4AAgBwAHAABAB3AHcABQB7AIAABgCCAIIADACEAIQADQCGAIYADgCPAI8ADwC0ALUAEAEIAQkAEgADAAQAmgCQAFAASgACADQAOgAAAAEAAABoAAMABADcANIAjACGAAIAGgAgAAAAAQAAAGgAAQABAO4AAgACAOAA4QAAAOYA5gACAAEAAQDxAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAACAAEA/AD/AAAAAQABAAAAAQABAPAAAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAIAAQD8AP8AAAABAAEAAAADAAIBZAFeAAIBSAFOAAAAAQAAAGkAAwACAZQBjgACATIBOAAAAAEAAABpAAMAAgHKAcQAAgEcASIAAAABAAAAaQADAAICAAH6AAIBBgEMAAAAAQAAAGkAAwACAjACKgACAPAA9gAAAAEAAABpAAMAAgJgAloAAgDaAOAAAAABAAAAaQADAAQC4ALWApYCkAACAMQAygAAAAEAAABpAAMABAMiAxgC0gLMAAIAqgCwAAAAAQAAAGkAAwADA1QDFAMOAAIAkACWAAAAAQAAAGkAAwADA5gDUgNMAAIAeAB+AAAAAQAAAGkAAwADA9wDlgOQAAIAYABmAAAAAQAAAGkAAwADBCAD2gPUAAIASABOAAAAAQAAAGkAAwADBGQEHgQYAAIAMAA2AAAAAQAAAGkAAwADBKIEYgRcAAIAGAAeAAAAAQAAAGkAAQABARUAAgACAO4A7wAAAPYA9wACAAEAAQDvAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAABAAEA7gACAAsAagBrAAAAbQBuAAIAcABwAAQAdwB3AAUAewCAAAYAggCCAAwAhACEAA0AhgCGAA4AjwCPAA8AtAC1ABABCAEJABIAAQABAPAAAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAEAAQDxAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAABAAEA9wACAAoAaQBpAAAAbABsAAEAbwBvAAIAeAB6AAMAgQCBAAYAgwCDAAcAhQCFAAgAhwCIAAkAigCKAAsAowCjAAwAAQABAPYAAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAEAAQDxAAIACgBpAGkAAABsAGwAAQBvAG8AAgB4AHoAAwCBAIEABgCDAIMABwCFAIUACACHAIgACQCKAIoACwCjAKMADAACAAEA/AD/AAAAAQABAAAAAQABAPAAAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAIAAQD8AP8AAAABAAEAAAABAAEA7wACAAoAaQBpAAAAbABsAAEAbwBvAAIAeAB6AAMAgQCBAAYAgwCDAAcAhQCFAAgAhwCIAAkAigCKAAsAowCjAAwAAgACAL4A3AAAAUgBSQAfAAEAAQDvAAIACwBqAGsAAABtAG4AAgBwAHAABAB3AHcABQB7AIAABgCCAIIADACEAIQADQCGAIYADgCPAI8ADwC0ALUAEAEIAQkAEgACAAIAvgDcAAABSAFJAB8AAQABAO4AAgALAGoAawAAAG0AbgACAHAAcAAEAHcAdwAFAHsAgAAGAIIAggAMAIQAhAANAIYAhgAOAI8AjwAPALQAtQAQAQgBCQASAAIAAgC+ANwAAAFIAUkAHwABAAEA8AACAAsAagBrAAAAbQBuAAIAcABwAAQAdwB3AAUAewCAAAYAggCCAAwAhACEAA0AhgCGAA4AjwCPAA8AtAC1ABABCAEJABIAAgACAL4A3AAAAUgBSQAfAAEAAQDxAAIACwBqAGsAAABtAG4AAgBwAHAABAB3AHcABQB7AIAABgCCAIIADACEAIQADQCGAIYADgCPAI8ADwC0ALUAEAEIAQkAEgACAAIAvgDcAAABSAFJAB8AAQABAPEAAgAKAGkAaQAAAGwAbAABAG8AbwACAHgAegADAIEAgQAGAIMAgwAHAIUAhQAIAIcAiAAJAIoAigALAKMAowAMAAIAAgC+ANwAAAFIAUkAHwADAAEAXgABAE4AAAABAAAAagADAAIAXABWAAEAPAAAAAEAAABqAAMAAgBqAGQAAQAoAAAAAQAAAGoAAwACAHgAcgABABQAAAABAAAAagACAAIA4ADhAAAA5gDmAAIAAgABAP0A/wAAAAEAAQD9AAIABADqAOwAAAD0APUAAwD4APkABQEBAQEABwABAAEA/gACAAQA6gDsAAAA9AD1AAMA+AD5AAUBAQEBAAcAAQABAP8AAgAEAOoA7AAAAPQA9QADAPgA+QAFAQEBAQAHAAEDMgBAAIYAugDEAM4A2ADiAOwA9gEAAQoBFAEeASgBMgE8AUYBUAFaAWQBbgF4AYIBjAGWAaABqgG0Ab4ByAHSAdwB5gHwAfoCBAIOAhgCIgIsAjYCQAJKAlYCYAJqAnQCfgKIApICnAKmArACugLEAs4C2ALiAuwC9gMAAwoDFAMeAygABgAOABQAGgAgACYALgEBAAIBAQD0AAIA9ADmAAIA5gDiAAIA4gDkAAMAAADkAOQAAgDkAAEABABpAAIAAAABAAQAagACAAAAAQAEAGsAAgAAAAEABABsAAIAAAABAAQAbQACAAAAAQAEAG4AAgAAAAEABABvAAIAAAABAAQAcAACAAAAAQAEAHEAAgAAAAEABAByAAIAAAABAAQAcwACAAAAAQAEAHQAAgAAAAEABAB1AAIAAAABAAQAdgACAAAAAQAEAHcAAgAAAAEABAB4AAIAAAABAAQAeQACAAAAAQAEAHoAAgAAAAEABAB7AAIAAAABAAQAfAACAAAAAQAEAH0AAgAAAAEABAB+AAIAAAABAAQAfwACAAAAAQAEAIAAAgAAAAEABACBAAIAAAABAAQAggACAAAAAQAEAIMAAgAAAAEABACEAAIAAAABAAQAhQACAAAAAQAEAIYAAgAAAAEABACHAAIAAAABAAQAiAACAAAAAQAEAIkAAgAAAAEABACKAAIAAAABAAQAjwACAAAAAQAEAJAAAgAAAAEABACjAAIAAAABAAQAtAACAAAAAQAEALUAAgAAAAEABADgAAIAAAABAAQA5wADARUA6QABAAQA6gACAAAAAQAEAOsAAgAAAAEABADsAAIAAAABAAQA9AACAAAAAQAEAPUAAgAAAAEABAD4AAIAAAABAAQA+QACAAAAAQAEAPwAAgAAAAEABAD9AAIAAAABAAQA/gACAAAAAQAEAP8AAgAAAAEABAEBAAIAAAABAAQBBwACAAAAAQAEAQgAAgAAAAEABAEJAAIAAAABAAQBCgACAAAAAQAEAQsAAgAAAAEABAEMAAIAAAABAAQBDQACAAAAAQAEAQ4AAgAAAAEABAEPAAIAAAABAAQBTgACAAAAAgAOAAAAAAAAAGkAigABAI8AkAAjAKMAowAlALQAtQAmAOAA4AAoAOcA5wApAOoA7AAqAPQA9QAtAPgA+QAvAPwA/wAxAQEBAQA1AQcBDwA2AU4BTgA/AAMAAQAYAAEAEgAAAAEAAABrAAEAAQCdAAIABQBpAIoAAACPAJAAIgCjAKMAJAC0ALUAJQEHAQkAJwADAAEALAABACYAAAABAAAAbAADAAIAdgBIAAEAFAAAAAEAAABsAAEAAQDnAAIABwCEAIQAAAC+AL4AAQDOAM4AAgDqAOsAAwD0APQABQECAQQABgEJAQkACQACAAcAhACEAAAAvgC+AAEAzgDOAAIA6gDrAAMA9AD0AAUBAgEEAAYBCQEJAAkAAgAEAOAA4gAAAOQA5gADAOwA7AAGARMBEwAHAAMAAQAsAAEAJgAAAAEAAABtAAMAAgBGADAAAQAUAAAAAQAAAG0AAQABAOcAAgADAH0AfQAAAOIA4wABAOwA7AADAAIAAwB9AH0AAADiAOMAAQDsAOwAAwACAAMA4ADhAAAA5QDmAAIA6QDpAAQAAgAIAAEA6AABAAEAnAACAAgAAQDpAAEAAQCeAAIACAABAOcAAQABAJsAAgAIAAEA4AABAAEAlAACAAgAAQEDAAEAAQCWAAIACAABAOYAAQABAJoAAgAKAAIA3gDfAAEAAgCSAJMAAgAIAAEBBAABAAEAlwACAAgAAQDhAAEAAQCVAAIACAABAOUAAQABAJkAAgAIAAEA5AABAAEAmAACABYACADqAO4A6wDsAUoBSwFMAU0AAgACAJ8AogAAALYAuQAEAAIACAABAQYAAQABAHIAAQAQAAEACAADAOkA5wEVAAEAAQDnAAIASAAhAL4AvwDAAMEAwgDDAMQAxQDGAMcAyADMAMkAygDLAM0AzgDPANAA0QDSANMA1ADVANYA1wDYANkA2gDbANwBSAFJAAIAAgEZATcAAAFGAUcAHwACAAoAAgDqAJMAAQACAJ8A3wACAAoAAgDiAOMAAQACAQMBBAACAAoAAgEBAQEAAQACAKIA7AACAAoAAgEHAQgAAQACAHIAfQACAAgAAQEFAAEAAQBzAAIACAABAQkAAQABAIQAAQIWACEASABWAGQAcgCAAI4AnACqALgAxgDUAOIA8AD+AQwBGgEoATYBRAFSAWABbgF8AYoBmAGmAbQBwgHQAd4B7AH6AggABgEVAOQA8QBpAPwAAAAGARUA5ADwAGoA/AAAAAYBFQDkAPAAawD8AAAABgEVAOQA8QBsAPwAAAAGARUA5ADwAG0A/AAAAAYBFQDkAPAAbgD8AAAABgEVAOQA8QBvAPwAAAAGARUA5ADwAHAA/AAAAAYBFQDkAPAAdwD8AAAABgEVAOQA8QB4APwAAAAGARUA5ADxAHkA/AAAAAYBFQDkAPEAegD8AAAABgEVAOQA8AB7APwAAAAGARUA5ADwAHwA/AAAAAYBFQDkAPAAfQD8AAAABgEVAOQA8AB+APwAAAAGARUA5ADwAH8A/AAAAAYBFQDkAPAAgAD8AAAABgEVAOQA8QCBAPwAAAAGARUA5ADwAIIA/AAAAAYBFQDkAPEAgwD8AAAABgEVAOQA8ACEAPwAAAAGARUA5ADxAIUA/AAAAAYBFQDkAPAAhgD8AAAABgEVAOQA8QCHAPwAAAAGARUA5ADxAIgA/AAAAAYBFQDkAPEAigD8AAAABgEVAOQA8ACPAPwAAAAGARUA5ADxAKMA/AAAAAYBFQDkAPAAtAD8AAAABgEVAOQA8AC1APwAAAAGARUA5ADwAQgA/AAAAAYBFQDkAPABCQD8AAAAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAEBkgAhAEgAUgBcAGYAcAB6AIQAjgCYAKIArAC2AMAAygDUAN4A6ADyAPwBBgEQARoBJAEuATgBQgFMAVYBYAFqAXQBfgGIAAQBFQDxAGkA/QAEARUA8ABqAP0ABAEVAPAAawD9AAQBFQDxAGwA/QAEARUA8ABtAP0ABAEVAPAAbgD9AAQBFQDxAG8A/QAEARUA8ABwAP0ABAEVAPAAdwD9AAQBFQDxAHgA/QAEARUA8QB5AP0ABAEVAPEAegD9AAQBFQDwAHsA/QAEARUA8AB8AP0ABAEVAPAAfQD9AAQBFQDwAH4A/QAEARUA8AB/AP0ABAEVAPAAgAD9AAQBFQDxAIEA/QAEARUA8ACCAP0ABAEVAPEAgwD9AAQBFQDwAIQA/QAEARUA8QCFAP0ABAEVAPAAhgD9AAQBFQDxAIcA/QAEARUA8QCIAP0ABAEVAPEAigD9AAQBFQDwAI8A/QAEARUA8QCjAP0ABAEVAPAAtAD9AAQBFQDwALUA/QAEARUA8AEIAP0ABAEVAPABCQD9AAIABwBpAHAAAAB3AIgACACKAIoAGgCPAI8AGwCjAKMAHAC0ALUAHQEIAQkAHwABAZIAIQBIAFIAXABmAHAAegCEAI4AmACiAKwAtgDAAMoA1ADeAOgA8gD8AQYBEAEaASQBLgE4AUIBTAFWAWABagF0AX4BiAAEARUA8QBpAP8ABAEVAPAAagD/AAQBFQDwAGsA/wAEARUA8QBsAP8ABAEVAPAAbQD/AAQBFQDwAG4A/wAEARUA8QBvAP8ABAEVAPAAcAD/AAQBFQDwAHcA/wAEARUA8QB4AP8ABAEVAPEAeQD/AAQBFQDxAHoA/wAEARUA8AB7AP8ABAEVAPAAfAD/AAQBFQDwAH0A/wAEARUA8AB+AP8ABAEVAPAAfwD/AAQBFQDwAIAA/wAEARUA8QCBAP8ABAEVAPAAggD/AAQBFQDxAIMA/wAEARUA8ACEAP8ABAEVAPEAhQD/AAQBFQDwAIYA/wAEARUA8QCHAP8ABAEVAPEAiAD/AAQBFQDxAIoA/wAEARUA8ACPAP8ABAEVAPEAowD/AAQBFQDwALQA/wAEARUA8AC1AP8ABAEVAPABCAD/AAQBFQDwAQkA/wACAAcAaQBwAAAAdwCIAAgAigCKABoAjwCPABsAowCjABwAtAC1AB0BCAEJAB8AAQHUACEASABUAGAAbAB4AIQAkACcAKgAtADAAMwA2ADkAPAA/AEIARQBIAEsATgBRAFQAVwBaAF0AYABjAGYAaQBsAG8AcgABQEVAPEAaQD8AAAABQEVAPAAagD8AAAABQEVAPAAawD8AAAABQEVAPEAbAD8AAAABQEVAPAAbQD8AAAABQEVAPAAbgD8AAAABQEVAPEAbwD8AAAABQEVAPAAcAD8AAAABQEVAPAAdwD8AAAABQEVAPEAeAD8AAAABQEVAPEAeQD8AAAABQEVAPEAegD8AAAABQEVAPAAewD8AAAABQEVAPAAfAD8AAAABQEVAPAAfQD8AAAABQEVAPAAfgD8AAAABQEVAPAAfwD8AAAABQEVAPAAgAD8AAAABQEVAPEAgQD8AAAABQEVAPAAggD8AAAABQEVAPEAgwD8AAAABQEVAPAAhAD8AAAABQEVAPEAhQD8AAAABQEVAPAAhgD8AAAABQEVAPEAhwD8AAAABQEVAPEAiAD8AAAABQEVAPEAigD8AAAABQEVAPAAjwD8AAAABQEVAPEAowD8AAAABQEVAPAAtAD8AAAABQEVAPAAtQD8AAAABQEVAPABCAD8AAAABQEVAPABCQD8AAAAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAEBUAAhAEgAUABYAGAAaABwAHgAgACIAJAAmACgAKgAsAC4AMAAyADQANgA4ADoAPAA+AEAAQgBEAEYASABKAEwATgBQAFIAAMA9wBpARUAAwD2AGoBFQADAPYAawEVAAMA9wBsARUAAwD2AG0BFQADAPYAbgEVAAMA9wBvARUAAwD2AHABFQADAPYAdwEVAAMA9wB4ARUAAwD3AHkBFQADAPcAegEVAAMA9gB7ARUAAwD2AHwBFQADAPYAfQEVAAMA9gB+ARUAAwD2AH8BFQADAPYAgAEVAAMA9wCBARUAAwD2AIIBFQADAPcAgwEVAAMA9gCEARUAAwD3AIUBFQADAPYAhgEVAAMA9wCHARUAAwD3AIgBFQADAPcAigEVAAMA9gCPARUAAwD3AKMBFQADAPYAtAEVAAMA9gC1ARUAAwD2AQgBFQADAPYBCQEVAAIABwBpAHAAAAB3AIgACACKAIoAGgCPAI8AGwCjAKMAHAC0ALUAHQEIAQkAHwABAdQAIQBIAFQAYABsAHgAhACQAJwAqAC0AMAAzADYAOQA8AD8AQgBFAEgASwBOAFEAVABXAFoAXQBgAGMAZgBpAGwAbwByAAFAAAA5AD3AGkBFQAFAAAA5AD2AGoBFQAFAAAA5AD2AGsBFQAFAAAA5AD3AGwBFQAFAAAA5AD2AG0BFQAFAAAA5AD2AG4BFQAFAAAA5AD3AG8BFQAFAAAA5AD2AHABFQAFAAAA5AD2AHcBFQAFAAAA5AD3AHgBFQAFAAAA5AD3AHkBFQAFAAAA5AD3AHoBFQAFAAAA5AD2AHsBFQAFAAAA5AD2AHwBFQAFAAAA5AD2AH0BFQAFAAAA5AD2AH4BFQAFAAAA5AD2AH8BFQAFAAAA5AD2AIABFQAFAAAA5AD3AIEBFQAFAAAA5AD2AIIBFQAFAAAA5AD3AIMBFQAFAAAA5AD2AIQBFQAFAAAA5AD3AIUBFQAFAAAA5AD2AIYBFQAFAAAA5AD3AIcBFQAFAAAA5AD3AIgBFQAFAAAA5AD3AIoBFQAFAAAA5AD2AI8BFQAFAAAA5AD3AKMBFQAFAAAA5AD2ALQBFQAFAAAA5AD2ALUBFQAFAAAA5AD2AQgBFQAFAAAA5AD2AQkBFQACAAcAaQBwAAAAdwCIAAgAigCKABoAjwCPABsAowCjABwAtAC1AB0BCAEJAB8AAQFQACEASABQAFgAYABoAHAAeACAAIgAkACYAKAAqACwALgAwADIANAA2ADgAOgA8AD4AQABCAEQARgBIAEoATABOAFAAUgAAwDxAGkBFQADAPAAagEVAAMA8ABrARUAAwDxAGwBFQADAPAAbQEVAAMA8ABuARUAAwDxAG8BFQADAPAAcAEVAAMA8AB3ARUAAwDxAHgBFQADAPEAeQEVAAMA8QB6ARUAAwDwAHsBFQADAPAAfAEVAAMA8AB9ARUAAwDwAH4BFQADAPAAfwEVAAMA8ACAARUAAwDxAIEBFQADAPAAggEVAAMA8QCDARUAAwDwAIQBFQADAPEAhQEVAAMA8ACGARUAAwDxAIcBFQADAPEAiAEVAAMA8QCKARUAAwDwAI8BFQADAPEAowEVAAMA8AC0ARUAAwDwALUBFQADAPABCAEVAAMA8AEJARUAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAEB1AAhAEgAVABgAGwAeACEAJAAnACoALQAwADMANgA5ADwAPwBCAEUASABLAE4AUQBUAFcAWgBdAGAAYwBmAGkAbABvAHIAAUAAADkAPMAaQEVAAUAAADkAPIAagEVAAUAAADkAPIAawEVAAUAAADkAPMAbAEVAAUAAADkAPIAbQEVAAUAAADkAPIAbgEVAAUAAADkAPMAbwEVAAUAAADkAPIAcAEVAAUAAADkAPIAdwEVAAUAAADkAPMAeAEVAAUAAADkAPMAeQEVAAUAAADkAPMAegEVAAUAAADkAPIAewEVAAUAAADkAPIAfAEVAAUAAADkAPIAfQEVAAUAAADkAPIAfgEVAAUAAADkAPIAfwEVAAUAAADkAPIAgAEVAAUAAADkAPMAgQEVAAUAAADkAPIAggEVAAUAAADkAPMAgwEVAAUAAADkAPIAhAEVAAUAAADkAPMAhQEVAAUAAADkAPIAhgEVAAUAAADkAPMAhwEVAAUAAADkAPMAiAEVAAUAAADkAPMAigEVAAUAAADkAPIAjwEVAAUAAADkAPMAowEVAAUAAADkAPIAtAEVAAUAAADkAPIAtQEVAAUAAADkAPIBCAEVAAUAAADkAPIBCQEVAAIABwBpAHAAAAB3AIgACACKAIoAGgCPAI8AGwCjAKMAHAC0ALUAHQEIAQkAHwABAVAAIQBIAFAAWABgAGgAcAB4AIAAiACQAJgAoACoALAAuADAAMgA0ADYAOAA6ADwAPgBAAEIARABGAEgASgBMAE4AUABSAADAPMAaQEVAAMA8gBqARUAAwDyAGsBFQADAPMAbAEVAAMA8gBtARUAAwDyAG4BFQADAPMAbwEVAAMA8gBwARUAAwDyAHcBFQADAPMAeAEVAAMA8wB5ARUAAwDzAHoBFQADAPIAewEVAAMA8gB8ARUAAwDyAH0BFQADAPIAfgEVAAMA8gB/ARUAAwDyAIABFQADAPMAgQEVAAMA8gCCARUAAwDzAIMBFQADAPIAhAEVAAMA8wCFARUAAwDyAIYBFQADAPMAhwEVAAMA8wCIARUAAwDzAIoBFQADAPIAjwEVAAMA8wCjARUAAwDyALQBFQADAPIAtQEVAAMA8gEIARUAAwDyAQkBFQACAAcAaQBwAAAAdwCIAAgAigCKABoAjwCPABsAowCjABwAtAC1AB0BCAEJAB8AAQHUACEASABUAGAAbAB4AIQAkACcAKgAtADAAMwA2ADkAPAA/AEIARQBIAEsATgBRAFQAVwBaAF0AYABjAGYAaQBsAG8AcgABQAAAOQA7wBpARUABQAAAOQA7gBqARUABQAAAOQA7gBrARUABQAAAOQA7wBsARUABQAAAOQA7gBtARUABQAAAOQA7gBuARUABQAAAOQA7wBvARUABQAAAOQA7gBwARUABQAAAOQA7gB3ARUABQAAAOQA7wB4ARUABQAAAOQA7wB5ARUABQAAAOQA7wB6ARUABQAAAOQA7gB7ARUABQAAAOQA7gB8ARUABQAAAOQA7gB9ARUABQAAAOQA7gB+ARUABQAAAOQA7gB/ARUABQAAAOQA7gCAARUABQAAAOQA7wCBARUABQAAAOQA7gCCARUABQAAAOQA7wCDARUABQAAAOQA7gCEARUABQAAAOQA7wCFARUABQAAAOQA7gCGARUABQAAAOQA7wCHARUABQAAAOQA7wCIARUABQAAAOQA7wCKARUABQAAAOQA7gCPARUABQAAAOQA7wCjARUABQAAAOQA7gC0ARUABQAAAOQA7gC1ARUABQAAAOQA7gEIARUABQAAAOQA7gEJARUAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAEBUAAhAEgAUABYAGAAaABwAHgAgACIAJAAmACgAKgAsAC4AMAAyADQANgA4ADoAPAA+AEAAQgBEAEYASABKAEwATgBQAFIAAMA9wBpARUAAwD2AGoBFQADAPYAawEVAAMA9wBsARUAAwD2AG0BFQADAPYAbgEVAAMA9wBvARUAAwD2AHABFQADAPYAdwEVAAMA9wB4ARUAAwD3AHkBFQADAPcAegEVAAMA9gB7ARUAAwD2AHwBFQADAPYAfQEVAAMA9gB+ARUAAwD2AH8BFQADAPYAgAEVAAMA9wCBARUAAwD2AIIBFQADAPcAgwEVAAMA9gCEARUAAwD3AIUBFQADAPYAhgEVAAMA9wCHARUAAwD3AIgBFQADAPcAigEVAAMA9gCPARUAAwD3AKMBFQADAPYAtAEVAAMA9gC1ARUAAwD2AQgBFQADAPYBCQEVAAIABwBpAHAAAAB3AIgACACKAIoAGgCPAI8AGwCjAKMAHAC0ALUAHQEIAQkAHwABAVAAIQBIAFAAWABgAGgAcAB4AIAAiACQAJgAoACoALAAuADAAMgA0ADYAOAA6ADwAPgBAAEIARABGAEgASgBMAE4AUABSAADAO8AaQEVAAMA7gBqARUAAwDuAGsBFQADAO8AbAEVAAMA7gBtARUAAwDuAG4BFQADAO8AbwEVAAMA7gBwARUAAwDuAHcBFQADAO8AeAEVAAMA7wB5ARUAAwDvAHoBFQADAO4AewEVAAMA7gB8ARUAAwDuAH0BFQADAO4AfgEVAAMA7gB/ARUAAwDuAIABFQADAO8AgQEVAAMA7gCCARUAAwDvAIMBFQADAO4AhAEVAAMA7wCFARUAAwDuAIYBFQADAO8AhwEVAAMA7wCIARUAAwDvAIoBFQADAO4AjwEVAAMA7wCjARUAAwDuALQBFQADAO4AtQEVAAMA7gEIARUAAwDuAQkBFQACAAcAaQBwAAAAdwCIAAgAigCKABoAjwCPABsAowCjABwAtAC1AB0BCAEJAB8AAQHUACEASABUAGAAbAB4AIQAkACcAKgAtADAAMwA2ADkAPAA/AEIARQBIAEsATgBRAFQAVwBaAF0AYABjAGYAaQBsAG8AcgABQEVAOQAaQD8AAAABQEVAOQAagD8AAAABQEVAOQAawD8AAAABQEVAOQAbAD8AAAABQEVAOQAbQD8AAAABQEVAOQAbgD8AAAABQEVAOQAbwD8AAAABQEVAOQAcAD8AAAABQEVAOQAdwD8AAAABQEVAOQAeAD8AAAABQEVAOQAeQD8AAAABQEVAOQAegD8AAAABQEVAOQAewD8AAAABQEVAOQAfAD8AAAABQEVAOQAfQD8AAAABQEVAOQAfgD8AAAABQEVAOQAfwD8AAAABQEVAOQAgAD8AAAABQEVAOQAgQD8AAAABQEVAOQAggD8AAAABQEVAOQAgwD8AAAABQEVAOQAhAD8AAAABQEVAOQAhQD8AAAABQEVAOQAhgD8AAAABQEVAOQAhwD8AAAABQEVAOQAiAD8AAAABQEVAOQAigD8AAAABQEVAOQAjwD8AAAABQEVAOQAowD8AAAABQEVAOQAtAD8AAAABQEVAOQAtQD8AAAABQEVAOQBCAD8AAAABQEVAOQBCQD8AAAAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAEBUAAhAEgAUABYAGAAaABwAHgAgACIAJAAmACgAKgAsAC4AMAAyADQANgA4ADoAPAA+AEAAQgBEAEYASABKAEwATgBQAFIAAMBFQBpAP0AAwEVAGoA/QADARUAawD9AAMBFQBsAP0AAwEVAG0A/QADARUAbgD9AAMBFQBvAP0AAwEVAHAA/QADARUAdwD9AAMBFQB4AP0AAwEVAHkA/QADARUAegD9AAMBFQB7AP0AAwEVAHwA/QADARUAfQD9AAMBFQB+AP0AAwEVAH8A/QADARUAgAD9AAMBFQCBAP0AAwEVAIIA/QADARUAgwD9AAMBFQCEAP0AAwEVAIUA/QADARUAhgD9AAMBFQCHAP0AAwEVAIgA/QADARUAigD9AAMBFQCPAP0AAwEVAKMA/QADARUAtAD9AAMBFQC1AP0AAwEVAQgA/QADARUBCQD9AAIABwBpAHAAAAB3AIgACACKAIoAGgCPAI8AGwCjAKMAHAC0ALUAHQEIAQkAHwABAVAAIQBIAFAAWABgAGgAcAB4AIAAiACQAJgAoACoALAAuADAAMgA0ADYAOAA6ADwAPgBAAEIARABGAEgASgBMAE4AUABSAADARUAaQD+AAMBFQBqAP4AAwEVAGsA/gADARUAbAD+AAMBFQBtAP4AAwEVAG4A/gADARUAbwD+AAMBFQBwAP4AAwEVAHcA/gADARUAeAD+AAMBFQB5AP4AAwEVAHoA/gADARUAewD+AAMBFQB8AP4AAwEVAH0A/gADARUAfgD+AAMBFQB/AP4AAwEVAIAA/gADARUAgQD+AAMBFQCCAP4AAwEVAIMA/gADARUAhAD+AAMBFQCFAP4AAwEVAIYA/gADARUAhwD+AAMBFQCIAP4AAwEVAIoA/gADARUAjwD+AAMBFQCjAP4AAwEVALQA/gADARUAtQD+AAMBFQEIAP4AAwEVAQkA/gACAAcAaQBwAAAAdwCIAAgAigCKABoAjwCPABsAowCjABwAtAC1AB0BCAEJAB8AAQGSACEASABSAFwAZgBwAHoAhACOAJgAogCsALYAwADKANQA3gDoAPIA/AEGARABGgEkAS4BOAFCAUwBVgFgAWoBdAF+AYgABAEVAGkA/AAAAAQBFQBqAPwAAAAEARUAawD8AAAABAEVAGwA/AAAAAQBFQBtAPwAAAAEARUAbgD8AAAABAEVAG8A/AAAAAQBFQBwAPwAAAAEARUAdwD8AAAABAEVAHgA/AAAAAQBFQB5APwAAAAEARUAegD8AAAABAEVAHsA/AAAAAQBFQB8APwAAAAEARUAfQD8AAAABAEVAH4A/AAAAAQBFQB/APwAAAAEARUAgAD8AAAABAEVAIEA/AAAAAQBFQCCAPwAAAAEARUAgwD8AAAABAEVAIQA/AAAAAQBFQCFAPwAAAAEARUAhgD8AAAABAEVAIcA/AAAAAQBFQCIAPwAAAAEARUAigD8AAAABAEVAI8A/AAAAAQBFQCjAPwAAAAEARUAtAD8AAAABAEVALUA/AAAAAQBFQEIAPwAAAAEARUBCQD8AAAAAgAHAGkAcAAAAHcAiAAIAIoAigAaAI8AjwAbAKMAowAcALQAtQAdAQgBCQAfAAEAEAABAAgAAwCYAPwAAAABAAEAmAABABQAAQAIAAEABAD/AAMAAADmAAEAAQD8AAECUgAxAGgAcgB8AIYAkACaAKQArgC4AMIAzADWAOAA6gD0AP4BCAESARwBJgEwAToBRAFOAVgBYgFsAXYBgAGKAZQBngGoAbIBvAHGAdAB2gHkAe4B+AICAgwCFgIgAioCNAI+AkgABAAAAOQAaQEVAAQAAADkAGoBFQAEAAAA5ABrARUABAAAAOQAbAEVAAQAAADkAG0BFQAEAAAA5ABuARUABAAAAOQAbwEVAAQAAADkAHABFQAEAAAA5ABxARUABAAAAOQAcgEVAAQAAADkAHMBFQAEAAAA5AB0ARUABAAAAOQAdQEVAAQAAADkAHYBFQAEAAAA5AB3ARUABAAAAOQAeAEVAAQAAADkAHkBFQAEAAAA5AB6ARUABAAAAOQAewEVAAQAAADkAHwBFQAEAAAA5AB9ARUABAAAAOQAfgEVAAQAAADkAH8BFQAEAAAA5ACAARUABAAAAOQAgQEVAAQAAADkAIIBFQAEAAAA5ACDARUABAAAAOQAhAEVAAQAAADkAIUBFQAEAAAA5ACGARUABAAAAOQAhwEVAAQAAADkAIgBFQAEAAAA5ACJARUABAAAAOQAigEVAAQAAADkAI8BFQAEAAAA5ACQARUABAAAAOQAowEVAAQAAADkALQBFQAEAAAA5AC1ARUABAAAAOQBBwEVAAQAAADkAQgBFQAEAAAA5AEJARUABAAAAOQBCgEVAAQAAADkAQsBFQAEAAAA5AEMARUABAAAAOQBDQEVAAQAAADkAQ4BFQAEAAAA5AEPARUABAAAAOQBTgEVAAIABgBpAIoAAACPAJAAIgCjAKMAJAC0ALUAJQEHAQ8AJwFOAU4AMAABABIAAQAIAAEABAAAAAIBFQABAAEBOQACAAgAAQAAAAEAAQE5AAEAIgABAAgAAgAGABAA9AAEAO4A9ADkAQEABADuAQEA5AABAAEBFQABA3QABwAUAGYAcgCIAJ4AtADKAAgAEgAaACIAKgAyADoAQgBKAOoAAwDqAOQA6wADAOsA5ADsAAMA7ADkAPQAAwD0AOQA9QADAPUA5AD4AAMA+ADkAPkAAwD5AOQBAQADAQEA5AABAAQA9AADAPQA5AACAAYADgD8AAMAAADuAPwAAwAAAOQAAgAGAA4A/QADAAAA7gD9AAMA7gDgAAIABgAOAP4AAwAAAO4A/gADAO4A4QACAAYADgD/AAMAAADuAP8AAwDuAOYARACKAJIAmgCiAKoAsgC6AMIAygDSANoA4gDqAPIA+gECAQoBEgEaASIBKgEyAToBQgFKAVIBWgFiAWoBcgF6AYIBigGSAZoBogGqAbIBugHCAcoB0gHaAeIB6gHyAfoCAgIKAhICGgIiAioCMgI6AkICSgJSAloCYgJqAnICegKCAooCkgKaAqIAAAADAO4A5AAAAAMA9gDkAL4AAwC+AOQAvwADAL8A5ADAAAMAwADkAMEAAwDBAOQAwgADAMIA5ADDAAMAwwDkAMQAAwDEAOQAxQADAMUA5ADGAAMAxgDkAMcAAwDHAOQAyAADAMgA5ADJAAMAyQDkAMoAAwDKAOQAywADAMsA5ADMAAMAzADkAM0AAwDNAOQAzgADAM4A5ADPAAMAzwDkANAAAwDQAOQA0QADANEA5ADSAAMA0gDkANMAAwDTAOQA1AADANQA5ADVAAMA1QDkANYAAwDWAOQA1wADANcA5ADYAAMA2ADkANkAAwDZAOQA2gADANoA5ADbAAMA2wDkANwAAwDcAOQBSAADAUgA5AFJAAMBSQDkAL4AAwC+AO4AvwADAL8A7gDAAAMAwADuAMEAAwDBAO4AwgADAMIA7gDDAAMAwwDuAMQAAwDEAO4AxQADAMUA7gDGAAMAxgDuAMcAAwDHAO4AyAADAMgA7gDJAAMAyQDuAMoAAwDKAO4AywADAMsA7gDMAAMAzADuAM0AAwDNAO4AzgADAM4A7gDPAAMAzwDuANAAAwDQAO4A0QADANEA7gDSAAMA0gDuANMAAwDTAO4A1AADANQA7gDVAAMA1QDuANYAAwDWAO4A1wADANcA7gDYAAMA2ADuANkAAwDZAO4A2gADANoA7gDbAAMA2wDuANwAAwDcAO4BSAADAUgA7gFJAAMBSQDuAAIABAAAAAAAAADuAO4AAQD8AP8AAgEVARUABgABABYAAQAIAAEABAEBAAQA7gEBAOQAAQABARUAAQBaAAEACAAIABIAGgAiACoAMgA6AEIASgDqAAMA6gDkAOsAAwDrAOQA7AADAOwA5AD0AAMA9ADkAPUAAwD1AOQA+AADAPgA5AD5AAMA+QDkAQEAAwEBAOQAAQABARUAAQJ2AC4AYgBsAHYAgACKAJQAngCoALIAvADGANAA2gDkAO4A+AECAQwBFgEgASoBNAE+AUgBUgFcAWYBcAF6AYQBjgGYAaoBvAHOAeAB8gIEAhYCKAIyAjwCRgJYAmICbAABAAQAvgACAOQAAQAEAL8AAgDkAAEABADAAAIA5AABAAQAwQACAOQAAQAEAMIAAgDkAAEABADDAAIA5AABAAQAxAACAOQAAQAEAMUAAgDkAAEABADGAAIA5AABAAQAxwACAOQAAQAEAMgAAgDkAAEABADJAAIA5AABAAQAygACAOQAAQAEAMsAAgDkAAEABADMAAIA5AABAAQAzQACAOQAAQAEAM4AAgDkAAEABADPAAIA5AABAAQA0AACAOQAAQAEANEAAgDkAAEABADSAAIA5AABAAQA0wACAOQAAQAEANQAAgDkAAEABADVAAIA5AABAAQA1gACAOQAAQAEANcAAgDkAAEABADYAAIA5AABAAQA2QACAOQAAQAEANoAAgDkAAEABADbAAIA5AABAAQA3AACAOQAAgAGAAwA6gACAOQA6gACAOAAAgAGAAwA6wACAOQA6wACAOAAAgAGAAwA7AACAOQA7AACAOAAAgAGAAwA9AACAOQA9AACAOAAAgAGAAwA9QACAOQA9QACAOAAAgAGAAwA+AACAOQA+AACAOAAAgAGAAwA+QACAOQA+QACAOAAAgAGAAwA/AACAOQA/AACAO4AAQAEAP0AAgDvAAEABAD+AAIA8AABAAQA/wACAPEAAgAGAAwBAQACAOQBAQACAOAAAQAEAAAAAgDkAAEABAFIAAIA5AABAAQBSQACAOQAAgAIAL4A3AAAAOoA7AAfAPQA9QAiAPgA+QAkAPwA/wAmAQEBAQAqARUBFQArAUgBSQAsAAEAIgABAAgAAwAIAA4AFAAAAAIA4AAAAAIA4QAAAAIA5gABAAEA7gABACoAAQAIAAQACgAQABYAHAAAAAIA7gAAAAIA7wAAAAIA9gAAAAIA9wABAAEBFQACAAwAAwAAAAAAAAACAAIA4ADhAAAA5gDmAAIAAgAIAAEBTwABAAEAnQACAAgAAQERAAEAAQDnAAIACAABARIAAQABAOcAAAABAAAAAwAAAAAAAAABAAIAAAAwAAAA+AAIAAIAAAA4AAAA+gBAAAIAAABAgAAA/AAAAUMAAQFGAAEBRwACAUQAAAFCAAEBRQAAAAH//wAC');

    doc.addFont('mm3.ttf', 'custom', 'normal');

    doc.setFont('custom');
    doc.text(title, 15, 22);
    var elem = document.getElementById("tbl-data");
    var res = doc.autoTableHtmlToJson(elem);
    doc.autoTable(res.columns, res.data, {
        startY: 30,
        styles: {overflow: 'hidden',font: 'custom'},
        columnStyles: {text: {columnWidth: 'auto'}}

    });
    $("#loader").hide();
    doc.save(new Date()+'_export.pdf');
}    

$('#btn_export').click(function () {
    $("#loader").show();   
    exportTableToPDF();
});


var tableToExcel = (function() {
 
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table style="font-family:Myanmar3">{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
     document.getElementById(table).style.fontFamily = "Myanmar3";
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()


///Area Report.....................
function executeAreaReport() { // execute selected report
  
  // get data from corresponding fields
  //var id_res = "14938601",
  var id_res = resource_id,   
    id_unit = $("#units").val();
   
  var  start_date = $("#start_date").val();
  if (!id_res) {
    $("#loader").hide();
    msg("Select resource");
    return;
  } // exit if no resource selected
  if (!id_unit) {
    $("#loader").hide();
    msg("Select unit");
    return;
  } // exit if no unit selected
   if (!start_date) {
    $("#loader").hide();
    msg("Select from date");
    return;
  }

   if (!$('#no_of_hours').val()) {
    $("#loader").hide();
    msg("Select No. of hours");
    return;
  }
  var sess = wialon.core.Session.getInstance(); // get instance of current Session
  renderer = sess.getRenderer();
  var res = sess.getItem(id_res); // get resource by id
 
  var from_to =calculateTime();
  var from =from_to[0];
  var to =from_to[1];

  console.log(new Date(from *1000));
  console.log(new Date(to *1000));

  // specify time interval object
  var interval = {
    "from": from,
    "to": to,
    "flags": wialon.item.MReport.intervalFlag.absolute
  };


  $("#btn_generate").prop("disabled", true); 

 /* var ride_begin ="gz"+resource_id+"_"+$('#ride_begin').val();
  var ride_end ="gz"+resource_id+"_"+$('#ride_end').val();*/

  
  
  //grouping
  var gp="{\"grouping\":\" {\\\"type\\\":\\\"day\\\"}\" }";
  var gp_geo = "{\"rides\":{\"ride_begin\":\""+ride_begin+",\",\"ride_end\":\""+ride_end+",\",\"flags\":2}}";
  //var pt="{\"geozones_ex\":""}";


  var c="ride_name,ride_name_from,ride_name_to,driver,time_begin,time_end,duration,duration_ival,mileage,max_speed,fuel_consumption_fls,avg_fuel_consumption_fls,fuel_level_begin";

  var template1 = {// fill template object
    "id": 0,
    "n": "unit_rides",
    "ct": "avl_unit",
    "p":"",
    "tbl": [{
        "n": "unit_rides",
        "l": "unit_rides",
        "c": c,
        "cl":c,
        "s": "",
        "sl": "",
        "p": gp_geo,
        "sch": {
          "f1": 0,
          "f2": 0,
          "t1": 0,
          "t2": 0,
          "m": 0,
          "y": 0,
          "w": 0
        },
        "f": 4496
      },
      {
        "n": "unit_engine_hours",
        "l": "unit_engine_hours",
        "c": "duration_stay,fuel_avg_consumption_fls_idle,time_begin,time_end",
        "cl": "duration_stay,fuel_avg_consumption_fls_idle,time_begin,time_end",
        "s": "",
        "sl": "",
        "p": "",
        "sch": {
          "f1": 0,
          "f2": 0,
          "t1": 0,
          "t2": 0,
          "m": 0,
          "y": 0,
          "w": 0
        },
        "f": 0
      },
      {
        "n": "unit_thefts",
        "l": "unit_thefts",
        "c": "thefted,time_begin",
        "cl": "thefted,time_begin",
        "s": "",
        "sl": "",
        "p": "",
        "sch": {
          "f1": 0,
          "f2": 0,
          "t1": 0,
          "t2": 0,
          "m": 0,
          "y": 0,
          "w": 0
        },
        "f": 0
      },
      {
        "n": "unit_fillings",
        "l": "unit_fillings",
        "c": "filled,time_end",
        "cl": "filled,time_end",
        "s": "",
        "sl": "",
        "p": "",
        "sch": {
          "f1": 0,
          "f2": 0,
          "t1": 0,
          "t2": 0,
          "m": 0,
          "y": 0,
          "w": 0
        },
        "f": 0
      },
      
 ]
  };

  

  renderer.setLocale(wialon.util.DateTime.getTimezoneOffset(),'en',function(){

  //for template
  res.execReport(template1, id_unit, 0, interval, // execute selected report

  function(code, data) { // execReport template
    
    if (code) {
        $("#loader").hide();
      console.log(wialon.core.Errors.getErrorText(code));
      return;
    }
     // exit if error code
     if(!data.getTables().length){
       $("#loader").hide();
       msg('There is no data generated.');
       $('#btn_generate').attr('disabled',false);
       return;
     }
     else{
      console.log("trips");
      console.log(data);
      showReportResultExtra(data);      

    } //else of trip
  });//end of trip 

  });//end of set timezone
  
}

function showReportResultExtra(result) { // show result after report execute
  trip_table=[];
  engine_table=[];
  theft_table=[];
  filling_table=[];
  trip_total=[];
  engine_total =[];
  theft_total=[];
  filling_total=[];

  var tables = result.getTables(); // get report tables
  if (!tables) return false; // exit if no tables
  var count =tables.length;
  var row_counts=0;

  for (var n = 0; n < tables.length; n++)
  {
    row_counts = row_counts + parseInt(tables[n].rows);
  }
 
  for (var i = 0; i < tables.length; i++) { // cycle on tables

    var table_name = tables[i].label;                
    var index =0;
    var trip_index=0;
    var engine_index = 0;
    var theft_index=0;
    var fill_index=0;
    var total_index=0;

    if(table_name == 'unit_rides')
       trip_total = tables[i].total;
    if(table_name == 'unit_engine_hours')
       engine_total = tables[i].total;
    if(table_name == 'unit_thefts')
       theft_total = tables[i].total; 
    if(table_name == 'unit_fillings')
       filling_total = tables[i].total;
  
   (function(i,table_name){
   result.getTableRows(i, 0, tables[i].rows, // get Table rows
     function( code, rows) { // getTableRows callback
      $('#loader').hide();
        if (code) {console.log(wialon.core.Errors.getErrorText(code)); return;} // exit if error code

        for(var l in rows) { // cycle on table rows
          if (typeof rows[l].c == "undefined") continue; // skip empty rows

          if(table_name == 'unit_rides'){

              var tmp_array=[];
              for (var k = 0; k < rows[l].c.length; k++) // add ceils to table
              {
                tmp_array[k] = getTableValue(rows[l].c[k]);
              }
              trip_table[trip_index]= tmp_array;
              trip_index++; 
          }
          else{
            var tmp_data='';
          for (var k = 0; k < rows[l].c.length; k++) // add ceils to table
          {            
            tmp_data += getTableValue(rows[l].c[k]) +'&&';            
          } 

            //add separate table
            if(table_name == 'unit_engine_hours'){
               var arr_tmp=tmp_data.split('&&');
               var duration_stay =arr_tmp[0];
               var fuel_avg_consumption_fls_idle =arr_tmp[1]; 
               var time_begin =arr_tmp[2]; 
               var time_end =arr_tmp[3]; 

               
               engine_table[engine_index] = {"duration_stay":duration_stay,
                                  "fuel_avg_consumption_fls_idle":fuel_avg_consumption_fls_idle,
                                  "time_begin":time_begin,
                                  "time_end":time_end,
                                  };
              engine_index++;                    
               

            }
            if(table_name == 'unit_thefts'){

               var arr_tmp=tmp_data.split('&&');
               var thefted =arr_tmp[0];
               var time_begin =arr_tmp[1]; 
               
               theft_table[theft_index] = {"thefted":thefted,
                                  "time_begin":time_begin,
                                 
                                  };
              theft_index++;  

            }
            if(table_name == 'unit_fillings'){

               var arr_tmp=tmp_data.split('&&');
               var filled =arr_tmp[0];
               var time_end =arr_tmp[1]; 
              
               
               filling_table[fill_index] = {"filled":filled,
                                  "time_end":time_end,
                                 
                                  };
              fill_index++; 

            }
          }          

            total_index++;                          

              if(total_index == row_counts){

               showAreaReportData();
                
              }
        }
        
      }
    );
  })(i,table_name);
    
  }  
  
}

function showAreaReportData(){

 
  if(trip_table.length <= 0){
    msg('There is no data generated.');
    $("#loader").hide();
    $('#btn_generate').attr('disabled',false);
    return;
  }


  var unit_name =$('#units option:selected').text();

  var html = "<div class='wrap'>";
  html +="<table id='tbl-data'summary='Code page support in different versions of MS Windows.' rules='groups' frame='hsides' border='1' class='table table-striped' style='width:100%'>";
  html +="<caption style='font-size:23px;font-family:Myanmar3'>Custom Report For "+unit_name+"</caption>";
  html +="<caption></caption>";
  html +="<tr style='background-color:rgb(211,211,211);'><td>No.</td>";
  html += "<td>Grouping</td>"; 
  html +="<td>Ride</td>";
  html +="<td>Ride From</td><td>Ride To</td>";
  html +="<td>Driver</td>";
  html +="<td>Beinning</td>";
  html +="<td>End</td>";
  html +="<td>Ride Duration</td>";
  html +="<td>Total Time</td>";
  html +="<td>Mileage</td>";
  html +="<td>Max speed</td>";
  html +="<td>Initial Fuel Level</td>";
  html +="<td>Filled</td>";
  html +="<td>Consumed by FLS</td>";
  html +="<td>Stolen (Fuel Theft)</td>";
  html +="<td>Avg consumption by FLS</td>";
  html +="<td>Idling</td>";
  html +="<td>Consumed by FLS in idle run</td>"; 
  html +="</tr>";

  var theft_t=0;
  var fill_t=0;
  var idle_total=0;
  var idle_comsum=0;
  for (var i =0;i< trip_table.length;i++) {

    html += "<tr>";
    html +="<td>"+trip_table[i][0]+"</td>";
    html +="<td>-</td>";
    html +="<td>"+trip_table[i][1]+"</td>";
    html +="<td>"+trip_table[i][2]+"</td>";
    html +="<td>"+trip_table[i][3]+"</td>";
    html +="<td>"+trip_table[i][4]+"</td>";
    html +="<td>"+trip_table[i][5]+"</td>";
    html +="<td>"+trip_table[i][6]+"</td>";
    html +="<td>"+trip_table[i][7]+"</td>";
    html +="<td>"+trip_table[i][8]+"</td>";
    html +="<td>"+trip_table[i][9]+"</td>";
    html +="<td>"+trip_table[i][10]+"</td>";
    html +="<td>"+trip_table[i][11]+"</td>";
    html +="<td>"+trip_table[i][13]+"</td>";
    

    var time_begin =new Date(trip_table[i][5]).getTime();
    var time_end =new Date(trip_table[i][6]).getTime();

    //fill fuel filling
    if(filling_table.length>0){

      var fill=0;
      for(var k=0;k<filling_table.length;k++)
      {
          var f_time_end =new Date(filling_table[k].time_end).getTime();

          if(f_time_end >= time_begin && f_time_end <= time_end){
             var tmp_fill = filling_table[k].filled;
             tmp_fill =tmp_fill.replace('lt');

             fill =parseFloat(tmp_fill)+parseFloat(fill);
             fill_t =parseFloat(tmp_fill)+parseFloat(fill_t);
          }
             
       
      }
      html +="<td>"+fill.toFixed(2)+" lt</td>";     
    }
    else{
       html +="<td>-</td>";
    }
    //end fill fuel filling

   
    //fill fuel theft
    if(theft_table.length>0){

      var theft=0;
      for(var k=0;k<theft_table.length;k++)
      {
          
          var t_time_begin =new Date(theft_table[k].time_begin).getTime();

          if(t_time_begin >= time_begin && t_time_begin <= time_end){
             var tmp_theft = theft_table[k].thefted;

             tmp_theft =tmp_theft.replace('It');

             theft =parseFloat(tmp_theft)+parseFloat(theft);
             theft_t =parseFloat(tmp_theft)+parseFloat(theft_t);
             
          }
             
       
      }
      html +="<td>"+theft.toFixed(2)+" lt</td>";     
    }
    else{
       html +="<td>-</td>";
    }
    //end fill fuel theft

    html +="<td>"+trip_table[i][12]+"</td>";

    if(engine_table.length>0){

      var idle=0;
      var fls=0;
      console.log(engine_table.length);
      for(var k=0;k<engine_table.length;k++)
      {
          var i_time_begin = new Date(engine_table[k].time_begin).getTime();
          var i_time_end = new Date(engine_table[k].time_end).getTime();


          if((i_time_begin >= time_begin) && (i_time_end <=time_end)){

  

               var str_idle = engine_table[k].duration_stay;
               var tmp_fls =engine_table[k].fuel_avg_consumption_fls_idle;

               console.log(str_idle);
               console.log(tmp_fls);

               //sum times
               var a1 = str_idle.split(':');
               var seconds1 = (+a1[0]) * 60 * 60 + (+a1[1]) * 60 + (+a1[2]); 
               idle =parseInt(seconds1) + parseInt(idle);
               idle_total =parseInt(seconds1) + parseInt(idle_total);
               //end sum times

               tmp_fls =tmp_fls.replace('lt');
               fls =parseFloat(tmp_fls)+parseFloat(fls);
               idle_comsum =parseFloat(tmp_fls)+parseFloat(idle_comsum);
            
            
             
          }
             
       
      }

      html +="<td>"+sformat(idle)+"</td>";     
      html +="<td>"+fls.toFixed(2)+" lt</td>";     
      
    }
    else{
       html +="<td>-</td>";
       html +="<td>-</td>";
       
    }

    
    
    html +="</tr>";


    

  }//end of loop

  //add total
  html += "<tr style='background-color:rgb(211,211,211);'><td>"+trip_total[0]+"</td>";
   html +="<td>Total</td>";
  html +="<td>"+trip_total[1]+"</td>";
  html +="<td>"+trip_total[2]+"</td>";
  html +="<td>"+trip_total[3]+"</td><td>"+trip_total[4]+"</td>";
  html +="<td>"+trip_total[5]+"</td>";
  html +="<td>"+trip_total[6]+"</td>";
  html +="<td>"+trip_total[7]+"</td>";
  html +="<td>"+trip_total[8]+"</td>";
  html +="<td>"+trip_total[9]+"</td>";
  html +="<td>"+trip_total[10]+"</td>";
  html +="<td>"+trip_total[11]+"</td>";
  html +="<td>"+trip_total[13]+"</td>";

   if(fill_t >0)
    html +="<td>"+fill_t.toFixed(2)+" lt</td>";
  else 
    html +="<td>-</td>";  
  
  if(theft_t >0)
    html +="<td>"+theft_t.toFixed(2)+" lt</td>";
  else 
    html +="<td>-</td>"; 

  html +="<td>"+trip_total[12]+"</td>";

  if(idle_total >0)
    html +="<td>"+sformat(idle_total)+"</td>";
  else 
    html +="<td>-</td>";  
  if(idle_comsum >0)
    html +="<td>"+idle_comsum.toFixed(2)+" lt</td>";
  else 
    html +="<td>-</td>";

    
 
  html +="</tr>";

  //add Remarks
  html +="<tr><td colspan='18'></td></tr>";
  html +="<tr><td colspan='2'>Remarks:</td><td colspan='17'>"+$('#remarks').val();+"</td></tr>";

   html +="</table></div>";
   $('#div-data').html(html);
  $('#report-modal').modal('show');
  $('#btn_generate').attr('disabled',false);
}

function sformat(s) {
    
     var days=   Math.floor(s / 60 / 60 / 24); // DAYS
     var hours=   Math.floor(s / 60 / 60) % 24; // HOURS
     var min =   Math.floor(s / 60) % 60; // MINUTES
     var sec=   s % 60; // SECONDS

     hours = (hours < 10)? '0'+hours:hours;
     min = (min < 10)? '0'+min:min;
     sec = (sec < 10)? '0'+sec:sec;
     
     if(days ==0){
      var dhms =hours+":"+min+":"+sec;
      return dhms;
     }
     days =(days <=1)? days+" day": days+" days";     

     var dhms =days +":"+hours+":"+min+":"+sec;
     return dhms;
}




