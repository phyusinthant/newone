(function ($) {
    "use strict";

    var OrderQty = function (options) {
        this.init('order_qty', options, OrderQty.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(OrderQty, $.fn.editabletypes.abstractinput);

    $.extend(OrderQty.prototype, {
        /**
         Renders input from tpl

         @method render()
         **/
        render: function() {
            this.$input = this.$tpl.find('input');
            this.$select = this.$tpl.find('select');
        },

        /**
         Default method to show value in element. Can be overwritten by display option.

         @method value2html(value, element)
         **/
        value2html: function(value, element) {

             if(!value) {
                $(element).empty();
                return; 
            }
            //console.log(element);
            var tpl='<button>delete</button>';
            var html = $('<div>').text('Start:'+value.start_time+' - End:'+value.end_time).html();
            $(element).html(html); 
           
        },

        /**
         Gets value from element's html

         @method html2value(html)
         **/
        html2value: function(html) {
            /*
             you may write parsing method to get value by element's html
             e.g. "Moscow, st. Lenina, bld. 15" => {autoload_qty: "Moscow", outlet_qty: "Lenina", total_qty: "15"}
             but for complex structures it's not recommended.
             Better set value directly via javascript, e.g. 
             editable({
             value: {
             autoload_qty: "Moscow", 
             outlet_qty: "Lenina", 
             total_qty: "15"
             }
             });
             */
            return null;
        },

        /**
         Converts value to string.
         It is used in internal comparing (not for sending to server).

         @method value2str(value)
         **/
        value2str: function(value) {
            var str = '';
            if(value) {
                for(var k in value) {
                    str = str + k + ':' + value[k] + ';';
                }
            }
            return str;
        },

        /*
         Converts string to value. Used for reading value from 'data-value' attribute.

         @method str2value(str)  
         */
        str2value: function(str) {
            /*
             this is mainly for parsing value defined in data-value attribute. 
             If you will always set value by javascript, no need to overwrite it
             */
            return str;
        },

        /**
         Sets value of input.

         @method value2input(value)
         @param {mixed} value
         **/
        value2input: function(value) {
            if(!value) {
                return;
            }
            //console.log(value);
            this.$select.filter('#start_time').html(value.start_timehtml);
            this.$select.filter('#end_time').html(value.end_timehtml);
            this.$select.filter('#start_time').val(value.start_time);
            this.$select.filter('#end_time').val(value.end_time);
        },

        /**
         Returns value of input.

         @method input2value()
         **/
        input2value: function() {
            //console.log(this.$select.filter('#start_time').val());
            return {
                
                start_timehtml: this.$select.filter('#start_time').html(),
                end_timehtml: this.$select.filter('#end_time').html(),
                start_time: this.$select.filter('#start_time').val(),
                end_time: this.$select.filter('#end_time').val(),
                //dept: this.$select.filter('#dept').val()
            };
        },

        /**
         Activates input: sets focus on the first field.

         @method activate()
         **/
        activate: function() {
            //this.$input.filter('[name="outlet_qty"]').focus();
        },

        /**
         Attaches handler to submit form in case of 'showbuttons=false' mode

         @method autosubmit()
         **/
        autosubmit: function() {
             this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
        }
    });

    var template='<div class="">'+
        '<div class="col-md-6">'+
        '<label>Select Department :</label><br/>'+
        '<select id="dept"><option>AA</option><option>BB</option></select>'+
        '</div>'+
        '<div class="col-md-3">'+
        '<label>Select Start Time :</label>'+
        '</div>'+
        '<div class="col-md-3">'+
        '<label>Select End Time :</label>'+
        '</div>'+
        '<div class="col-md-3">'+
        '<select id="start_time"></select>'+
        '</div>'+
        '<div class="col-md-3">'+
        '<select id="end_time"></select>'+
        '</div>'+
        '<div class="col-md-6" style="">'+        
        '</div>'+
        '</div>';
       

    OrderQty.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: template,
        inputclass: '',
    });

    $.fn.editabletypes.order_qty = OrderQty;

}(window.jQuery));