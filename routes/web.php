<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::post('/language-chooser', 'LanguageLocalizationController@changeLanguage');

Route::post('/language/', array(
    'before' => 'csrf',
    'as' => 'language-chooser',
    'uses' => 'LanguageLocalizationController@changeLanguage')
);


Route::group(['middleware' => ['auth']], function() {

    Route::get('/home', ['as'=>'home.index','uses' => function () {
        return view('/backend/pages/dashboard');
    }]);

    Route::get('/', function () {
        return view('/backend/pages/dashboard');
    });


    Route::get('/profile', 'ProfileController@index');

    Route::get('users',
        ['as'=>'users.index','uses'=>'UserController@index',
        'middleware' => ['permission:user-list|user-create|user-edit|user-delete']]);

    Route::get('users/create',
        ['as'=>'users.create','uses'=>'UserController@create',
        'middleware' => ['permission:user-create']]);

    Route::post('users/create',
        ['as'=>'users.store','uses'=>'UserController@store',
        'middleware' => ['permission:user-create']]);

    Route::get('users/{id}',
        ['as'=>'users.show','uses'=>'UserController@show',
        'middleware' => ['permission:user-list|user-create|user-edit|user-delete']]);

    Route::get('users/{id}/edit',
        ['as'=>'users.edit','uses'=>'UserController@edit',
        'middleware' => ['permission:user-edit']]);

    Route::patch('users/{id}',
        ['as'=>'users.update','uses'=>'UserController@update',
        'middleware' => ['permission:user-edit']]);

    Route::delete('users/{id}',
        ['as'=>'users.destroy','uses'=>'UserController@destroy',
        'middleware' => ['permission:user-delete']]);
 
    Route::get('users/get-data/datatable',
        ['as'=>'users.getData','uses'=>'UserController@getData',
        'middleware' => ['permission:user-list|user-create|user-edit|user-delete']]);

    Route::delete('users/delete-data/multi-destroy', 
        ['as'=>'users.multiDestroy','uses'=>'UserController@multiDestroy',
        'middleware' => ['permission:user-delete']]);

    Route::get('roles',
        ['as'=>'roles.index','uses'=>'RoleController@index',
        'middleware' => ['permission:role-list|role-create|role-edit|role-delete','useractionlog:Manage Role']]);

    Route::get('roles/create',
        ['as'=>'roles.create','uses'=>'RoleController@create',
        'middleware' => ['permission:role-create','useractionlog']]);

    Route::post('roles/create',
        ['as'=>'roles.store','uses'=>'RoleController@store',
        'middleware' => ['permission:role-create']]);

    Route::get('roles/{id}',
        ['as'=>'roles.show','uses'=>'RoleController@show',
        'middleware' => ['permission:role-list|role-create|role-edit|role-delete']]);

    Route::get('roles/{id}/edit',
        ['as'=>'roles.edit','uses'=>'RoleController@edit',
        'middleware' => ['permission:role-edit']]);

    Route::patch('roles/{id}',
        ['as'=>'roles.update','uses'=>'RoleController@update',
        'middleware' => ['permission:role-edit']]);

    Route::delete('roles/{id}',
        ['as'=>'roles.destroy','uses'=>'RoleController@destroy',
        'middleware' => ['permission:role-delete']]);

    Route::get('roles/{id}/assign-permission',
        ['as'=>'roles.assignPermission','uses'=>'RoleController@assignPermission',
        'middleware' => ['permission:role-assign-permission']]);

    Route::patch('roles/{id}/assign-permission',
        ['as'=>'roles.assignPermission','uses'=>'RoleController@storeRolePermission',
        'middleware' => ['permission:role-assign-permission']]);

    Route::get('roles/get-data/datatable',
        ['as'=>'roles.getData','uses'=>'RoleController@getData',
        'middleware' => ['permission:role-list|role-create|role-edit|role-delete']]);

    Route::delete('roles/delete-data/multi-destroy', 
        ['as'=>'roles.multiDestroy','uses'=>'RoleController@multiDestroy',
        'middleware' => ['permission:role-delete']]);

    #Customer
    Route::get('customers',
        ['as'=>'customers.index','uses'=>'CustomerController@index',
        'middleware' => ['permission:customers-list|customers-create|customers-edit|customers-delete']]);

    Route::get('customers/create',
        ['as'=>'customers.create','uses'=>'CustomerController@create',
        'middleware' => ['permission:customers-create']]);

    Route::post('customers/create',
        ['as'=>'customers.store','uses'=>'CustomerController@store',
        'middleware' => ['permission:customers-create']]);

    Route::get('customers/{id}',
        ['as'=>'customers.show','uses'=>'CustomerController@show',
        'middleware' => ['permission:customers-list|customers-create|customers-edit|customers-delete']]);

    Route::get('customers/{id}/edit',
        ['as'=>'customers.edit','uses'=>'CustomerController@edit',
        'middleware' => ['permission:customers-edit']]);

    Route::patch('customers/{id}',
        ['as'=>'customers.update','uses'=>'CustomerController@update',
        'middleware' => ['permission:customers-edit']]);

    Route::delete('customers/{id}',
        ['as'=>'customers.destroy','uses'=>'CustomerController@destroy',
        'middleware' => ['permission:customers-delete']]);
 
    Route::get('customers/get-data/datatable',
        ['as'=>'customers.getData','uses'=>'CustomerController@getData',
        'middleware' => ['permission:customers-list|customers-create|customers-edit|customers-delete']]);

    Route::delete('customers/delete-data/multi-destroy', 
        ['as'=>'customers.multiDestroy','uses'=>'CustomerController@multiDestroy',
        'middleware' => ['permission:customers-delete']]);
});



