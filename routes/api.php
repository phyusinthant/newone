<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('authenticate', 'JwtAuthenticateController@authenticate');

// Route to create a new role
Route::post('role', 'JwtAuthenticateController@createRole');
// Route to create a new permission
Route::post('permission', 'JwtAuthenticateController@createPermission');
// Route to assign role to user
Route::post('assign-role', 'JwtAuthenticateController@assignRole');
// Route to attache permission to a role
Route::post('attach-permission', 'JwtAuthenticateController@attachPermission');

Route::post('authenticate', 'JwtAuthenticateController@authenticate');


// API route group that we need to protect
Route::group(['middleware' => ['ability:admin,user-create']], function()
{
    // Protected route
    Route::get('users', 'JwtAuthenticateController@index');

});


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/push_notification_api', function () {

	$pns = new App\Services\NoviPushNotification();

	$androidDeviceToken = array("ffPAP7PVekM:APA91bF7Rr04H4COyzBPDmYrRQfdaE_Mwi8DPM03rK0KI4EvrRE4QNKxKzOTUKz1bXjAaZ0QFjMcTfDO-kLKRkPt2DPmPGkBuRjvFOBIONxj-G6NVaLGAAwsfcY7yFQfD7mAYSM7x9KP");

	$data = array("notiID" => "1" , "type" => "test");

	$result = $pns->send("Test title 12345", "Test body 12345", $androidDeviceToken, [], $data);
	
	// $result = $pns->sendAsyn("Test title 123", "Test body 123", $androidDeviceToken, []);

    return $result;
});


