<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Item' => 'Item',
    'Item Management' => 'Item Management',
    'Your last login was' => 'Your last login was',
   
    
    'User' => 'User',
    'User Management' => 'User Management',
    'Create New User'=>'Create New User',
    'Name'=>'Name',
    'Email'=>'Email',
    'Roles'=>'Roles',
    'Search'=>'Search',
    'Action'=>'Action',
    'Password'=>'Password',
    'Confirm Password'=>'Confirm Password',
    'Role'=>'Role',
    'City'=>'City',
    'Save'=>'Save',
    'Back to Listing'=>'Back to Listing',
    'Are you sure'=>'Are you sure ',
    'You will not be able to recover this item!'=>'You will not be able to recover this item!',
    'to delete single'=>'to delete single',
    'Yes, delete it!'=>'Yes, delete it!',
    'No, cancel plx!'=>'No, cancel plx!',
    'Failed'=>'Failed',
    'Invalid Parameters'=>'Invalid Parameters',
    'Cancelled'=>'Cancelled',
    'It is safe :)'=>'It is safe :)',

    'Role Management' => 'Role Management',
    'Create New Row' => 'Create New Row',
    'Description' => 'Description',
    'Display Name' => 'Display Name',
    'Permission' => 'Permission',
    'Assign Permission to Role' => 'Assign Permission to Role',
    'Assgin Permission' => 'Assgin Permission',

    'Permission Management' => 'Permission Management',

    'Location' => 'Location',
    'Location Management' => 'Location Management',

    'Customer' =>'Customer',
    'Customer Management'=>'Customer Management',
    'Create New Customer'=>'Create New Customer',
    
    'Phone' => 'Phone',
    'Address' => 'Address',
    'Status' => 'Status'
];
