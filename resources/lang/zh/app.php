<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'User' => 'User',
    'User Management' => 'User Management',
    'Create New User'=>'အသစ္ထည့္ရန္',
    'Name'=>'အမည္',
    'Email'=>'Email',
    'Roles'=>'အမ်ိဴးအစားမ်ား',
    'Search'=>'ရွာေဖြရန္',
    'Action'=>'လုပ္ေဆာင္ရန္',
    'Password'=>'Password',
    'Confirm Password'=>'Confirm Password',
    'Role'=>'အမ်ိဴးအစား',
    'City'=>'ျမိဴ႕',
    'Save'=>'သိမ္းရန္',
    'Back to Listing'=>'ေနာက္သို႕ျပန္သြားရန္',
    'Are you sure'=>'ေသခ်ာပါသလား',
    'You will not be able to recover this item!'=>'You will not be able to recover this item!',
    'to delete single'=>'to delete single',
    'Yes, delete it!'=>'Yes, delete it!',
    'No, cancel plx!'=>'No, cancel plx!',
    'Failed'=>'Failed',
    'Invalid Parameters'=>'Invalid Parameters',
    'Cancelled'=>'Cancelled',
    'It is safe :)'=>'It is safe :)',

    'Role Management' => 'Role Management',
    'Create New Row' => 'အသစ္ထည့္ရန္',
    'Description' => 'Description',
    'Display Name' => 'အမည္',
    'Permission' => 'ခြင့္ျပဴရန္',
    'Assign Permission to Role' => 'Assign Permission to Role',
    'Assgin Permission' => 'Assgin Permission',


    'Client'=>'ကုန္သည္',
    'Client Management'=>'ကုန္သည္မ်ား',
    'Create New Client'=>'ကုန္သည္အသစ္ထည့္ရန္',
    'Status'=>'Status',
    'Edit'=>'ျပင္ရန္',

    'City'=>'ျမိဴ႕',
    'City Management'=>'ျမိဴ႕မ်ား',
    'Create New City'=>'ျမိဳ႕အသစ္ထည့္ရန္',

    'Driver'=>'ဒရိုင္ဘာ',
    'Driver Management'=>'ဒရိုင္ဘာမ်ား',
    'Create New Driver'=>'ဒရိုင္ဘာအသစ္ထည့္ရန္',

    'Vehicle'=>'ကား',
    'Vehicle Management'=>'ကားမ်ား',
    'Create New Vehicle'=>'ကားအသစ္ထည့္ရန္',
    'Vehicle No'=>'ကားနံပါတ္',
    'Weight'=>'ကားတန္ခ်ိန္',
    'Item Weight'=>'ကား ကုန္တန္ခ်ိန္',
    'Total Item Weight'=>'စုစုေပါင္း ကုန္တန္ခ်ိန္',
    'Balance'=>'က်န္ အေလးခ်ိန္',
    'Spare'=>'စပါယ္ယာ',

    'Item' => '项目',
    'Item Management' => '物品管理',
    'Your last login was' => '你的最后一次登录是',

     'Track' =>'Track',
   
    
   
];
