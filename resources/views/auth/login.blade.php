
@extends('backend.master.master_without_sidebar')

@section('contents')
<!-- begin #page-container -->
<div class="login-cover">
    <div class="login-cover-image"><img src="adminlte_assets/dist/img/login-bg/bg-8.jpg" data-id="login-cover-image" alt="" /></div>
    <div class="login-cover-bg"></div>
</div>

<!-- begin login -->
    <div class="login login-v2">
        <div class="login-content">
            <form action="{{ route('login') }}" role="form"  method="POST" class="margin-bottom-0">
            {{ csrf_field() }}

                <div class="form-group m-b-20">
                    <input type="email" id="email" name="email" class="form-control input-lg" placeholder="Email Address" value="{{ old('email') }}" required autofocus/>

                    {{-- <input id="email" type="email" class="form-control input-lg" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus> --}}

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group m-b-20">
                    <input type="password" name="password" class="form-control input-lg" placeholder="Password" required />

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                </div>
                <div class="checkbox m-b-20">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}/> Remember Me
                    </label>
                </div>
                <!-- <div class="m-t-20 m-b-20">
                   Forget Password <a href="{{ route('password.request') }}">here</a> to reset your password.
                </div> -->
                <div class="login-buttons">
                    <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
                </div>
            </form>
        </div>
    </div> <!-- end .row -->
    <!-- end login -->
</div>
@stop
