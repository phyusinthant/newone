@extends('backend.master.master_without_sidebar')

@section('contents')
<div class="login-cover">
    <div class="login-cover-image"><img src="adminlte_assets/dist/img/login-bg/bg-8.jpg" data-id="login-cover-image" alt="" /></div>
    <div class="login-cover-bg"></div>
</div>
<!-- begin #page-container -->
<div class="login login-v2">
    <!-- begin login -->
    <div class="login-content">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" class="form-horizontal" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Send Password Reset Link
                    </button>
                </div>
            </div>
            <div style='margin-top: 20px' class="pull-right">
                <a href="{{ route('login') }}"> Go back to Login </a>
            </div>
        </form>
    </div>
    <!-- end login -->
</div>
@stop
