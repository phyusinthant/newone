@extends('backend.master.master_with_side_bar')

@section('title')
	PIB Master Blade
@stop
<!-- add new css file -->
@section('css')

@stop
<!-- add new css file  -->

@section('contents')
	  <!-- Begin Row -->
  <div class="row">
    <div class="panel panel-danger">
      <div class="panel-heading">
          <h3 class="text-center white">Whoops. An unexpected error has occured!</h3>
      </div>
      <div class="panel-body">
         <h4 class="text-center">Please contact to your system administrator! Your Error code is</h4>
         <br />
         <div class="text-center"><button type="button" class="btn btn-warning">{{$error_id}}</button> <!--  btn btn-primary --></div><!--  .text-center -->
      </div>
    </div>
  </div>
  
@stop

<!-- add new js file -->
@section('js')

@stop
<!-- add new js file  -->

