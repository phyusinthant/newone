@if(session()->has('flash_message'))

	<script>

		swal({
			title: "{{ session('flash_message.title') }}",
			text: "{{ session('flash_message.message') }}",
			type: "{{ session('flash_message.level') }}",
			@if(\Config::get('flash.isTimer'))
				timer: {{\Config::get('flash.timer')}},
			@endif
			showConfirmButton: {{\Config::get('flash.showConfirmButton')}},
			confirmButtonText: '{{\Config::get('flash.confirmButtonText')}}',
		});

	</script>

@endif



@if(session()->has('flash_message_overlay'))

	<script>

		swal({
			title: "{{ session('flash_message_overlay.title') }}",
			text: "{{ session('flash_message_overlay.message') }}",
			type: "{{ session('flash_message_overlay.level') }}",
			confirmButtonText: 'Okay'
		});

	</script>

@endif