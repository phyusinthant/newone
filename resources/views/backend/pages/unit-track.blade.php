
@extends('backend.master.master_with_side_bar')

@section('css')
<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.css" />
<link rel="stylesheet" href="/adminlte_assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="/css/track.css">
<link href="/plugins/vis/vis.min.css" rel="stylesheet">

<link href="/adminlte_assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<!-- ================== END PAGE LEVEL STYLE ================== -->
@stop

@section('contents')
<!-- begin #content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Home</a></li>
            <li class="active">@lang('app.Track')</li>
        </ol>

        <h4 class="bold"> @lang('app.Track') <small> </small></h4>

        <div id="footer" class="footer" style="margin-left: 5px"></div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <!-- start content panel -->
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">@lang('app.Track')</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <div class="row">
                                   <div class="col-md-12">
                                       <input type="hidden" id="api_key" value="{{$member->api_key}}">
                                       <input type="hidden" id="devices" value="{{$member->devices}}">
                                       Select unit:<select id="units" class="form-control"><option></option></select>
                                   </div>   

                               </div>

                               <div class="row">
                                   <div class="col-md-12">
                                       Select track color:
                                       <div id="cp2" class="">
                                          <input type="text" id="color" class="form-control" value=""/>
                                      </div>               
                                  </div>   

                              </div>

                              <div class="row">
                               <div class="col-md-12">
                                  Select From Date:
                                  <div class='input-group date start_date' id='datetimepicker1'>
                                    <input type='text' id='start_date' name='start_date'
                                    class="form-control"
                                    value="" 
                                    data-parsley-required='true'/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>   

                        </div>

                        <div class="row">
                           <div class="col-md-12">
                               No. of Hours:
                               <input type="number" id="no_of_hours" class="form-control" onkeypress="return isNumber(event);">
                           </div>   

                       </div>

                       <div class="row">
                           <div class="col-md-12">
                               Report Remarks:
                               <textarea class="from-control" name="" id="remarks" cols="30" rows="5"></textarea>
                           </div>   

                       </div>

                       <div class="row">
                           <div class="col-md-12">
                               Report Type:
                               <input type="radio" id="report_trip"  name="report_type" value="Trip" checked="true"> Trip
                               <input type="radio" id="report_area" name="report_type" value="Area"> Ride
                           </div>   

                       </div>

                       <div id="div-geo" style="display:none;">
                       <!--  <div class="row" >
                          <div class="col-md-12">
                              Select From Geofence:
                              <select id="ride_begin" class="form-control"><option></option></select>
                          </div>   
                       
                                              </div> 
                       
                                              <div class="row" >
                          <div class="col-md-12">
                              Select To Geofence:
                              <select id="ride_end" class="form-control"><option></option></select>
                          </div>   
                       
                                              </div> -->
                   </div>

                   <div class="row">
                       <div class="col-md-12">
                           <br>
                           <button id="build" type="button" class="btn btn-info">Show Track</button>
                           <button id="btn_generate" type="button" class="btn btn-info">Generate Report</button>
                       </div>   

                   </div> 

                   <div class="row m-t" >
                    <div id="div-info" class="col-md-12" style="display:none;">
                        <table id="tracks">
                            <thead>

                                <td>Info</td>                
                                <td>Delete</td>
                            </thead>
                        </table>
                    </div>
                </div>       

            </div>
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div id="map"></div>
            </div>
        </div>

        @include('backend.pages.timeline')
        @include('backend.pages.export-report')
    </div> <!-- .panel-body -->
</div> <!-- .panel-inverse -->
</div> <!-- .col-md-12.col-lg-12 -->
</div> <!-- .row -->
<div id="loader"></div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- end #content -->
@stop

@section('js')

<script type="text/javascript" src="https://hst-api.wialon.com/wsdk/script/wialon.js"></script>
<script src="/adminlte_assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="/adminlte_assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.js"></script>
<script type="text/javascript" src="/plugins/vis/vis.min.js"></script>
<script src="/js/track.js"></script>


<script>
    $('.start_date').datetimepicker({
        todayHighlight: true,
        format: 'yyyy/mm/dd hh:ii',
        autoclose: true,
    });

    $('#color').colorpicker(
      {format :'hex'}
    );

    $(document).on("change","#no_of_hours", function(){
        if(parseInt($("#no_of_hours").val()) < 0){
            $("#no_of_hours").val("0");
        }
    });

    function isNumber(evt) 
    {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && charCode != 46 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    } 
</script>
@stop