
@extends('backend.master.master_without_sidebar')

@section('contents')
<!-- begin #page-container -->
<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8 col-xs-offset-4 col-sm-offset-4 col-md-offset-2 col-lg-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Blank</div>
            <div class="panel-body">
                <form action="index.html" method="POST" class="form-horizontal">
                    <label class="control-label">Name <span class="text-danger">*</span></label>
                    <div class="row row-space-10">
                        <div class="col-md-6 m-b-15">
                            <input type="text" class="form-control" placeholder="First name" required />
                        </div>
                        <div class="col-md-6 m-b-15">
                            <input type="text" class="form-control" placeholder="Last name" required />
                        </div>
                    </div>
                    <label class="control-label">Email <span class="text-danger">*</span></label>
                    <div class="row m-b-15">
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="Email address" required />
                        </div>
                    </div>
                    <label class="control-label">Re-enter Email <span class="text-danger">*</span></label>
                    <div class="row m-b-15">
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="Re-enter email address" required />
                        </div>
                    </div>
                    <label class="control-label">Password <span class="text-danger">*</span></label>
                    <div class="row m-b-15">
                        <div class="col-md-12">
                            <input type="password" class="form-control" placeholder="Password" required />
                        </div>
                    </div>
                    <div class="checkbox m-b-30">
                        <label>
                            <input type="checkbox" required /> By clicking Sign Up, you agree to our <a href="#">Terms</a> and that you have read our Data Policy. </a>.
                        </label>
                    </div>
                    <div class="register-buttons">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Sign Up</button>
                    </div>
                    <div>
                        Already a member? Click <a href="login_v3.html">here</a> to login.
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end login -->
</div>
@stop
