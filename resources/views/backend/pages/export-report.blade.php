<div id="report-modal" class="modal" tabindex="" role="dialog" data-backdrop="static"
>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
        <div class="row">
          <div class="col-md-12" id="div-modal" style="padding: 1em; font-size:90%;line-height: 1.5em;">
          
            <div id="div-title"></div>  
            <div id="div-data">
                  
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        
         <button type="button" id="btn_export" class="btn btn-info">Export PDF</button>
         <button type="button" id="btn_exportxls" class="btn btn-info" onclick="tableToExcel('tbl-data','report')">Export Excel</button>
         <button type="button" id="btn_map" class="btn btn-info">Get Map</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>