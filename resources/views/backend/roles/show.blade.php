@extends('backend.master.master_with_side_bar')

@section('title')
	Role
@stop
<!-- add new css file -->
@section('css')

@stop
<!-- add new css file  -->

@section('contents')
	<div class="content-wrapper">
        <section class="content-header">
			<!-- begin breadcrumb -->
		    <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="{{ route('roles.index') }}">Role</a></li>
				<li class="active">Show Role</li>
			</ol>
		    <!-- end breadcrumb -->

		    <h4 class="bold content-header">Show Role <small> </small></h4>

		    <div id="footer" class="footer" style="margin-left: 5px"></div>
		</section>

		<section class="content">
			<!-- start content panel -->
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<h4 class="panel-title">Show Role</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Name:</strong>
				                {{ $role->display_name }}
				            </div>
				        </div>
				        <div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Description:</strong>
				                {{ $role->description }}
				            </div>
				        </div>
				        <div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Permissions:</strong>
				                @if(!empty($rolePermissions))
									@foreach($rolePermissions as $v)
										<label class="label label-success">{{ $v->display_name }}</label>
									@endforeach
								@endif
				            </div>
				        </div>
				        <div class="row">
						    <div class="col-lg-12 margin-tb">
						        <div class="pull-left">
						        </div>
						        <div class="pull-right">
						            <a class="btn btn-warning" href="{{ route('roles.index') }}"> Back to Listing</a>
						        </div>
						    </div>
						</div>
					</div>
				
				</div>
			</div>
			<!-- end content panel -->
		</section> <!-- end section .content -->
    </div> <!-- end div.wrapper -->
@stop

<!-- add new js file -->
@section('js')

@stop
<!-- add new js file  -->

