@extends('backend.master.master_with_side_bar')

@section('title')
	@lang('app.Role')
@stop

<!-- add new css file -->
@section('css')

@stop
<!-- add new css file  -->
 
@section('contents')
	<div class="content-wrapper">
        <section class="content-header">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">@lang('app.Role')</li>
			</ol>
			<!-- end breadcrumb -->

			<h4 class="bold content-header"> @lang('app.Role Management') <small> </small></h4>

			<div id="footer" class="footer" style="margin-left: 5px"></div>

			<div class="row m-b-10">
			    <div class="col-lg-12">
			        <div>
			        	@permission('role-create')
			            <a class="btn btn-success" href="{{ route('roles.create') }}"><i class="fa fa-plus"></i> @lang('app.Create New Row') </a>
			            @endpermission

			        </div>
			    </div>
			</div>
		</section>

		<section class="content">
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<!-- start content panel -->
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4 class="panel-title"> @lang('app.Role Management') </h4>
						</div>
						<div class="panel-body">

							@include('backend.shared.success')

							<table id="data-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
							            
										<th>@lang('app.Name')</th>
										<th>@lang('app.Description')</th>
										<th width="400px">@lang('app.Action')</th>
						            </tr>
						        </thead>
						        <tfoot>
						            <tr>
						            	
										<th>@lang('app.Name')</th>
										<th>@lang('app.Description')</th>
										<th width="400px">@lang('app.Action')</th>
						            </tr>
						        </tfoot>
						    </table>

						</div>
					</div>
					<!-- end content panel -->
				</div>
		        <!-- end col-12 -->
		    </div>
		    <!-- end row -->
		</section> <!-- end section .content -->
    </div> <!-- end .content-wrapper -->

<!-- begin Div to put Delete Form based on Single Delete, Multi Delete or All Delete -->
<div id="deleteAction"></div>
<!-- end Div to put Delete Form based on Single Delete, Multi Delete or All Delete -->

<!-- begin delete action template -->
<script id="deleteActionTemplate" type="text/html">
	<form id="deleteForm" name="deleteForm" method="POST" action="{actionlink}" style="display:none">
			<input name="_method" type="hidden" value="DELETE">
			<input name="_token" type="hidden" value="{{ csrf_token() }}">
			<input name="action" type="hidden" value="{action}">
			<input name="ids" type="hidden" value="{ids}">
	</form>
</script>
<!-- end delete action template -->

<!-- begin datatable action column template -->
<script id="datatableActionTemplate" type="text/html">

	@permission('role-edit')
		<a class="btn btn-warning btn-icon btn-circle" href="roles/{id}/edit">
			<i class="fa fa-edit"></i>
		</a>&nbsp;
	@endpermission

	@permission('role-assign-permission')
		<a class="btn btn-info btn-icon btn-circle" href="roles/{id}/assign-permission">
			<i class="fa fa-key"></i>
		</a>&nbsp;
	@endpermission

	@permission('role-delete')
		<a class="btn btn-danger btn-icon btn-circle" href="#" onClick="confirmDelete('delete_single','roles/{id}','Role')">
			<i class="fa fa-times"></i>
		</a>&nbsp;
	@endpermission

</script>
<!-- end datatable action column template -->


@endsection

<!-- add new js file -->
@section('js')

<script src="/js/datatablescript.js"></script>

<script type="text/javascript">

// Array holding selected row IDs
var rows_selected = [];

$(document).ready(function() {

	var datatableActionTemplate = $("#datatableActionTemplate").html();

	//Begin Datatable
    var table = $('#data-table').DataTable( {
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "ajax": "roles/get-data/datatable",
        dom:            "Bfrtip",
        scrollY:        true,
        scrollX:        true,
        scrollCollapse: true,
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],                
        buttons: [
            { extend: 'pageLength', className: 'btn-sm'},
            { extend: 'colvis', className: 'btn-sm' }
        ],
        fixedColumns:   {
            leftColumns: 2
        },
        fixedHeader: {
            header: true,
            headerOffset: $('#header').height()
        },

        "columns": [
           
            { data: 'display_name', name: 'display_name' },
            { data: 'description', name: 'description' },
            { data: 'id', name: '' },
            
        ],
        "columnDefs": [
        	
	        {
	            // The `data` parameter refers to the data for the cell (defined by the
	            // `data` option, which defaults to the column being worked with, in
	            // this case `data: 0`.
	            "render": function ( data, type, row ) {

					return datatableActionTemplate.formatUnicorn({"id":data});
	            },
	            "searchable": false,
                "orderable": false,
	            "targets": 2
	        },
	    ],

    } );
    //End Datatable


} );
//End Document Ready

</script>

@stop
<!-- add new js file  -->

