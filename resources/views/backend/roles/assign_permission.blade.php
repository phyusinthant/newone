@extends('backend.master.master_with_side_bar')

@section('title')
    @lang('app.Role')
@stop

<!-- add new css file -->
@section('css')

@stop
<!-- add new css file  -->
 
@section('contents')
	<div class="content-wrapper">
        <section class="content-header">
			<!-- begin breadcrumb -->
		    <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="{{ route('roles.index') }}">@lang('app.Role')</a></li>
				<li class="active">@lang('app.Assign Permission to Role')</li>
			</ol>
		    <!-- end breadcrumb -->

		    <h4 class="bold content-header">@lang('app.Assign Permission to Role') <small> </small></h4>

		    <div id="footer" class="footer" style="margin-left: 5px"></div>
		</section>

		<section class="content">
			<!-- start content panel -->
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<h4 class="panel-title">@lang('app.Assign Permission to Role')</h4>
				</div>
				<div class="panel-body">
					
					@include('backend.shared.errors')

					<!-- begin of form -->
		            <form id="rolesForm" name="rolesForm" action="{{ route('roles.assignPermission', $role->id) }}" method="POST">
		                <input type="hidden" name="_method" value="PATCH">
		                {!! csrf_field() !!}

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
					            <div class="form-group">
					                <strong>@lang('app.Name'):</strong>
					                {{ $role->display_name }}
					            </div>
					        </div>
					        <div class="col-xs-12 col-sm-12 col-md-12">
					            <div class="form-group">
					                <strong>@lang('app.Description'):</strong>
					                {{ $role->description }}
					            </div>
					        </div>
					        <div class="col-xs-12 col-sm-12 col-md-12">
					            <div class="form-group">
					                <strong>@lang('app.Permission'):</strong>
					                <br/>

					                <div class="grid">

					                @foreach($uiPermissionGroups as $grouping => $uiPermissions)
										<div class="ui_permission">
											<div class="panel panel-success">
												<div class="panel-heading">
													<h4 class="panel-title">{{$grouping}}</h4>
												</div>
												<div class="panel-body bg-green text-white">
													@foreach($uiPermissions as $uiPermission)

														@if(in_array($uiPermission->permission_id, $rolePermissions)) 
		                            						<input type="checkbox" name="uiPermissions[]" value="{{$uiPermission->id}}" checked/> {{ $uiPermission->display_name }}
														@else
															<input type="checkbox" name="uiPermissions[]" value="{{$uiPermission->id}}"/> 
														@endif

														{{ $uiPermission->display_name }}
									                	
									                	<br>
									                @endforeach
												</div>
											</div>
					                	</div>
					                @endforeach

					                </div>
									
					            </div>
					        </div>
					        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
									
					        </div>
					        <div class="row">
							    <div class="col-lg-12 margin-tb">
							        <div class="pull-left">
							        </div>
							        <div class="pull-right">
							            <a class="btn btn-warning" href="{{ route('roles.index') }}"> @lang('app.Back to Listing')</a>
							            <button type="submit" class="btn btn-success">@lang('app.Assgin Permission')</button>
							        </div>
							    </div>
							</div>
						</div>
					</form>
		            <!-- end of form -->
				</div>
			</div>
			<!-- end content panel -->
		</section> <!-- end section .content -->
    </div> <!-- end div.wrapper -->

@endsection


<!-- add new js file -->
@section('js')
	<script type="text/javascript" src="{{URL::asset('js/gridify.js')}}"></script>

	<script type="text/javascript">
	    window.onload = function(){
	        var options =
	        {
	            srcNode: '.ui_permission',  // grid items (class, node)
	            margin: '20px',             // margin in pixel, default: 0px
	            width: '230px',             // grid item width in pixel, default: 220px
	            max_width: '',              // dynamic gird item width if specified, (pixel)
	            resizable: true,            // re-layout if window resize
	            transition: 'all 0.0s ease' // support transition for CSS3, default: all 0.5s ease
	        }
	        document.querySelector('.grid').gridify(options);
	    }
	</script>


	<script type="text/javascript">

	$(document).ready(function() {

	    $('#rolesForm').preventDoubleSubmission();

	} );

	//End Document Ready

	</script>

@stop
<!-- add new js file  -->

