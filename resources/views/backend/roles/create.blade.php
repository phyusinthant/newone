@extends('backend.master.master_with_side_bar')

@section('title')
    @lang('app.Role')
@stop

<!-- add new css file -->
@section('css')

@stop
<!-- add new css file  -->
 
@section('contents')
    <div class="content-wrapper">
        <section class="content-header">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li><a href="javascript:;">Home</a></li>
                <li><a href="{{ route('roles.index') }}"> @lang('app.Role')</a></li>
                <li class="active"> @lang('app.Create New Row')</li>
            </ol>
            <!-- end breadcrumb -->

            <h4 class="bold content-header">@lang('app.Create New Row') <small> </small></h4>

            <div id="footer" class="footer" style="margin-left: 5px"></div>
        </section>

        <section class="content">
            <!-- start content panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">@lang('app.Create New Row')</h4>
                </div>
                <div class="panel-body">
                    
                    @include('backend.shared.errors')

                    <!-- begin of form -->
                    <form name="rolesForm" id="rolesForm" method="POST" action="{{ route('roles.store') }}">
                        {!! csrf_field() !!}

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.Name'):</strong>
                                    <input type="text" 
                                            name="name" 
                                            value="{{ old('name') }}" 
                                            placeholder="Name" 
                                            class="form-control"
                                            data-parsley-required="true"
                                            data-parsley-maxlength="255"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.Display Name'):</strong>
                                    <input type="text" 
                                            name="display_name" 
                                            value="{{ old('display_name') }}" 
                                            placeholder="Display Name" 
                                            class="form-control"
                                            data-parsley-required="true"
                                            data-parsley-maxlength="255"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.Description'):</strong>
                                    <textarea name="description" 
                                                placeholder="Description" 
                                                class="form-control" 
                                                rows="5"
                                                data-parsley-required="true"
                                                data-parsley-maxlength="255"/>{{ old('description') }}</textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 margin-tb">
                                    <div class="pull-left">
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-warning" href="{{ route('roles.index') }}"> @lang('app.Back to Listing') </a>
                                        <button type="submit" class="btn btn-success">@lang('app.Save')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- end of form -->
                </div>
            </div>
            <!-- end content panel -->
        </section> <!-- end section .content -->
    </div> <!-- end .content-wrapper -->

@endsection


<!-- add new js file -->
@section('js')

<script type="text/javascript">

$(document).ready(function() {

    $('#rolesForm').parsley();
    $('#rolesForm').preventDoubleSubmission();

} );

//End Document Ready

</script>

@stop
<!-- add new js file  -->
