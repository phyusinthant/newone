@extends('backend.master.master_with_side_bar')

@section('title')
    @lang('app.User')
@stop
<!-- add new css file -->
@section('css')

@stop
<!-- add new css file  -->

@section('contents')
    <div class="content-wrapper">
        <section class="content-header">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li><a href="javascript:;">Home</a></li>
                <li><a href="{{ route('users.index') }}">@lang('app.User')</a></li>
                <li class="active">@lang('app.Create New User')</li>
            </ol>
            <!-- end breadcrumb -->

            <h4 class="bold">@lang('app.Create New User') <small> </small></h4>

            <div id="footer" class="footer" style="margin-left: 5px"></div>
        </section> <!-- end section content-header -->

        <section class="content">
            <!-- start content panel -->
            <div class="panel panel-inverse">

                <!-- start content heading panel -->
                <div class="panel-heading">
                    <h4 class="panel-title">@lang('app.Create New User')</h4>
                </div>
                <!-- end content heading panel -->

                <!-- start content body panel -->
                <div class="panel-body">

                    @include('backend.shared.errors')

                    <!-- start form -->
                    <form name="usersForm" id="usersForm" method="POST" action="{{ route('users.store') }}">
                        {!! csrf_field() !!}
                        
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.Name'):</strong>
                                    <input type="text" 
                                            name="name" 
                                            value="{{ old('name') }}" 
                                            placeholder="Name" 
                                            class="form-control"
                                            data-parsley-required="true"
                                            data-parsley-maxlength="255"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.Email'):</strong>
                                    <input type="text" 
                                            name="email" 
                                            value="{{ old('email') }}" 
                                            placeholder="Email" 
                                            class="form-control"
                                            data-parsley-required="true"
                                            data-parsley-maxlength="255"
                                            data-parsley-type="email"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.Password'):</strong>
                                    <input type="password" 
                                            id="password"
                                            name="password" 
                                            value="{{ old('password') }}" 
                                            placeholder="Password" 
                                            class="form-control"
                                            data-parsley-required="true"
                                            data-parsley-maxlength="255"
                                            />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.Confirm Password'):</strong>
                                    <input type="password" 
                                            id="confirm-password"
                                            name="confirm-password" 
                                            value="{{ old('password') }}" 
                                            placeholder="Confirm Password" 
                                            class="form-control"
                                            data-parsley-required="true"
                                            data-parsley-maxlength="255"
                                            data-parsley-equalto="#password"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.Role'):</strong>
                                    <select name="roles[]" 
                                            multiple='multiple' 
                                            class="form-control" 
                                            data-parsley-required="true">

                                        @foreach($roles as $roleId => $roleName)
                                            <option value="{{ $roleId }}" > {{ $roleName }} </option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.City'):</strong>
                                    <select name="city_id" 
                                            class="form-control" 
                                            data-parsley-required="true">        

                                        @foreach($cities as $key => $value)
                                            <option value="{{ $key }}" > {{ $value }} </option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 margin-tb">
                                    <div class="pull-left">
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-warning" href="{{ route('users.index') }}"> @lang('app.Back to Listing')</a>
                                        <button type="submit" class="btn btn-success">@lang('app.Save')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- end form -->
                </div>
                <!-- end content body panel -->
            </div>
            <!-- end content panel -->
        </section> <!-- end section .content -->
    </div> <!-- end div.wrapper -->
@stop

<!-- add new js file -->
@section('js')

<script type="text/javascript">

$(document).ready(function() {

    $('#usersForm').parsley();
    $('#usersForm').preventDoubleSubmission();

} );

//End Document Ready

</script>

@stop
<!-- add new js file  -->

