@extends('backend.master.master_with_side_bar')

@section('title')
	User
@stop
<!-- add new css file -->
@section('css')

@stop
<!-- add new css file  -->

@section('contents')
	<div class="content-wrapper">
        <section class="content-header">
			<!-- begin breadcrumb -->
		    <ol class="breadcrumb pull-right">
		        <li><a href="javascript:;">Home</a></li>
		        <li><a href="{{ route('users.index') }}">User</a></li>
		        <li class="active">Show User</li>
		    </ol>
		    <!-- end breadcrumb -->

		    <h4 class="bold content-header"> Show User <small> </small></h4>

		    <div id="footer" class="footer" style="margin-left: 5px"></div>
		</section> <!-- end section content-header -->

		<section class="content">
			<!-- start content panel -->
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<h4 class="panel-title">Show User</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Name:</strong>
				                {{ $user->name }}
				            </div>
				        </div>
				        <div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Email:</strong>
				                {{ $user->email }}
				            </div>
				        </div>
				        <div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Roles:</strong>
				                @if(!empty($user->roles))
									@foreach($user->roles as $v)
										<label class="label label-success">{{ $v->display_name }}</label>
									@endforeach
								@endif
				            </div>
				        </div>
				        <!-- <div class="row">
					    	<div class="col-lg-12 margin-tb">
						        <div class="pull-left">
						            <h2></h2>
						        </div>
						        <div class="pull-right">
						            <a class="btn btn-warning" href="{{ route('users.index') }}"> Back to Listing</a>
						        </div>
					    	</div>
						</div> -->
					</div>

				</div>
			</div>
			<!-- end content panel -->
		</section> <!-- end section .content -->
    </div> <!-- end div.wrapper -->
@stop

<!-- add new js file -->
@section('js')

@stop
<!-- add new js file  -->
