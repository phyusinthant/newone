<!DOCTYPE html>
<html lang="en">
<head>
    @section('meta')
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
    @show

    <title>
        @yield('title')
    </title> 

     @include('backend.master.partials._css')

</head>
<body>
    <!-- begin #page-container -->
    <div class="container">
        <!-- begin #content -->
        <div id="content" class="content">
            @yield('contents')
        </div> 
        <!-- end of content -->
    </div><!--  end of page-container -->
    @include('backend.master.partials._js')
    @yield('js')

    @include('flash')
    
</body>
</html>
