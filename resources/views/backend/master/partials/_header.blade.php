<!-- begin #header -->
<header class="main-header">

    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>L</b>TE</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">{{ config('app.name', 'AdminLTE') }}</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <!-- begin header navigation right -->
            <ul class="nav navbar-nav">
                <!-- Language Menu -->
                <li class="dropdown navbar-language">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if( App::getLocale() == 'en')
                            <span class="flag-icon flag-icon-us" title="us"></span>
                            <span class="name">EN</span> <b class="caret"></b>
                        @endif
                        @if( App::getLocale() == 'zh')
                            <span class="flag-icon flag-icon-mm" title="us"></span>
                            <span class="name">MM</span> <b class="caret"></b>
                        @endif
                    </a>
                    <ul class="dropdown-menu">
                        <li class="arrow"></li>
                        <li @if( App::getLocale() == 'en') {{'active'}} @endif>
                            <a href="#" onClick="switchLanguage('en')" ><span class="flag-icon flag-icon-us" title="us"></span> English</a>
                        </li>
                        <li @if( App::getLocale() == 'zh') {{'active'}} @endif>
                            <a href="#" onClick="switchLanguage('zh')"><span class="flag-icon flag-icon-mm" title="cn"></span> Myanmar</a>
                        </li>
            
                    </ul>
                </li> 
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{ asset("/adminlte_assets/dist/img/user2-160x160.jpg") }}" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{ asset("/adminlte_assets/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">

                            <p>
                              Hello {{ Auth::user()->name }}
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            @if (Auth::guest())
                              <div class="pull-left">
                                <a href="{{ route('login') }}" class="btn btn-default btn-flat">Login</a>
                              </div>
                            @else
                            <div class="pull-left">
                                <a href="{{ url('users/'.Auth::user()->id) }}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                Logout
                                </a>
                            </div>
                            @endif
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<!-- end #header -->

