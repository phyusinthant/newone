
<!-- ================== BEGIN BASE JS ================== -->
<script src="{{ asset("/adminlte_assets/plugins/jquery/jquery-latest.min.js") }}"></script>
<!-- <script type="text/javascript" src="//code.jquery.com/jquery-latest.min.js"></script> -->
<script src="{{ asset("/adminlte_assets/plugins/jquery/jquery-migrate-1.1.0.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/bootstrap/js/bootstrap.min.js") }}"></script>
<script src="/js/jspdf.min.js"></script>
<script src="/js/jspdf.customfonts.min.js"></script>
<script src="/js/jspdf.customfonts.debug.js"></script>
<!-- <script src="/js/default_vfs.js"></script> -->
<script src="/js/jspdf.plugin.autotable.min.js"></script>
<script src="/js/standard_fonts_metrics.js"></script>

<!--[if lt IE 9]>
<script src="assets/crossbrowserjs/html5shiv.js"></script>
<script src="assets/crossbrowserjs/respond.min.js"></script>
<script src="assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="{{ asset("/adminlte_assets/plugins/slimscroll/jquery.slimscroll.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/jquery-cookie/jquery.cookie.js") }}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{ asset("/adminlte_assets/plugins/DataTables/media/js/jquery.dataTables.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Scroller/js/dataTables.scroller.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/FixedColumns/js/dataTables.fixedColumns.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Buttons/js/buttons.colVis.min.js") }}"></script>

<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js") }}"></script>
<script src="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js") }}"></script>


<script src="{{ asset("/adminlte_assets/dist/js/app.js") }}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<!-- ================== BEGIN Utility JS ================== -->

<script src="{{ asset("/sweetalert/sweetalert.min.js") }}"></script>    
<script src="{{ asset("/js/parsley.js") }}"></script>   
{{-- <script src="{{ asset("/js/custom.js") }}"></script> --}}
<script src="{{ asset("/js/helpers.js") }}"></script>

<!-- ================== END Utility JS ================== -->

<script>
    $(document).ready(function() {
    	
    });
</script>
