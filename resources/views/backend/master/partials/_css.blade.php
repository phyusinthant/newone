
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{{ asset("/adminlte_assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css") }}" rel="stylesheet" />
    <link href="{{ asset("/adminlte_assets/plugins/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" />
    <link href="{{ asset("/adminlte_assets/plugins/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" />
    <link href="{{ asset("/adminlte_assets/dist/css/AdminLTE.min.css") }}" rel="stylesheet" />
    <link href="{{ asset("/adminlte_assets/dist/css/skins/skin-blue.min.css") }}" rel="stylesheet" id="theme" />
    <link href="{{ asset("/css/style.css") }}" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== END PAGE CSS STYLE ================== -->
    <link href="{{ asset("/adminlte_assets/plugins/flag-icon/css/flag-icon.css") }}" rel="stylesheet" />
    <!-- ================== END PAGE CSS STYLE ================== -->
    
    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="{{ asset("/adminlte_assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css") }}" rel="stylesheet" />
    <link href="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Scroller/css/scroller.bootstrap.min.css") }}" rel="stylesheet" />
    <link href="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css") }}" rel="stylesheet" />
    <link href="{{ asset("/adminlte_assets/plugins/DataTables/extensions/FixedColumns/css/fixedColumns.bootstrap.min.css") }}" rel="stylesheet" />
    <link href="{{ asset("/adminlte_assets/plugins/DataTables/extensions/FixedHeader/css/fixedHeader.bootstrap.min.css") }}" rel="stylesheet" />
    <link href="{{ asset("/adminlte_assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css") }}" rel="stylesheet" />

    <!-- ================== END PAGE LEVEL STYLE ================== -->
    
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{ asset("/adminlte_assets/plugins/pace/pace.min.js") }}"></script>
    <!-- ================== END BASE JS ================== -->

    <link rel="stylesheet" type="text/css" href="{{ asset("/sweetalert/sweetalert.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("/css/parsley.css") }}">
    
    <style>
        body {
      font-family: "Myanmar3";
      color: #555;
    }
    </style>    
    @yield('css')