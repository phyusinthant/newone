        <!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ asset("/adminlte_assets/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
            </div>
            <div class="info">
                @if(isset(Auth::user()->name)) 
                {{ Auth::user()->name }}
                @endif
                <small> 
                    @if(isset(Auth::user()->roles()->first()->display_name)) 
                    {{ Auth::user()->roles()->first()->display_name }} 
                    @endif
                </small>
                <!-- Status -->
                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
        </div>

<!-- search form (Optional) -->
 <!--  <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
    </div>
</form> -->
<!-- /.search form -->

<!-- begin sidebar nav -->
<ul class="sidebar-menu">
    <li class="treeview header">Menu</li>
    @if(isset(Auth::user()->roles()->first()->name) && (Auth::user()->roles()->first()->name == 'admin'))
    <li class="treeview">
        <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <ul class="treeview-menu">
            <!-- <li
            @if( 'home.index' == Route::currentRouteName()
            || '' == Route::currentRouteName()
            ) 
            class="active"
            @endif 
            ><a href="{{ url('/home') }}"><i class="fa fa-circle"></i>Home </a></li> -->

            @permission('user-list')
            <li
            @if('users.index' == Route::currentRouteName()
            || 'users.create' == Route::currentRouteName()    
            || 'users.edit' == Route::currentRouteName()    
            || 'users.show' == Route::currentRouteName()    
            ) 
            class="active"
            @endif 
            ><a href="{{ route('users.index') }}"><i class="fa fa-circle"></i>Users </a></li>
            @endpermission

            @permission('customers-list')
                <li 
                     @if('customers.index' == Route::currentRouteName()
                        || 'customers.create' == Route::currentRouteName()
                        || 'customers.edit' == Route::currentRouteName()
                        || 'customers.show' == Route::currentRouteName()
                     ) 
                        class="active"
                    @endif 
                ><a href="{{ route('customers.index') }}"><i class="fa fa-circle"></i>@lang('app.Customer')</a></li>
            @endpermission

            @permission('role-list')
                <li 
                    @if('roles.index' == Route::currentRouteName()
                        || 'roles.create' == Route::currentRouteName()
                        || 'roles.edit' == Route::currentRouteName()
                        || 'roles.show' == Route::currentRouteName()
                        || 'roles.assignPermission' == Route::currentRouteName()
                    ) 
                        class="active"
                    @endif 
                ><a href="{{ route('roles.index') }}"><i class="fa fa-circle"></i>Roles </a></li>
            @endpermission
        </ul>
    </li>
    @endif
</ul>
<!-- end sidebar nav -->
</section>
<!-- /.sidebar -->
</aside>