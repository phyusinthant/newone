<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>

    @section('meta')
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
    @show

    <title>
        @yield('title')
    </title> 

    @include('backend.master.partials._css')
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <!-- begin #page-container -->
    <div class="wrapper">

        @include('backend.master.partials._header')
        @include('backend.master.partials._leftbar')
        <!-- begin #content -->
        <!-- <div id="content" class="content"> -->
            @yield('contents')
        <!-- </div>  -->
        <!-- end of content -->

        <!-- begin #footer -->
        <div id="footer" class="footer">
            
        </div>
        <!-- end #footer -->

    </div>
    <!--  end of page-container -->


    @include('backend.master.partials._js')

    @yield('js')

    @include('flash')
    
</body>
</html>
