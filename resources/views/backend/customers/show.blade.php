@extends('backend.master.master_with_side_bar')

@section('title')
	Customer
@stop
<!-- add new css file -->
@section('css')

@stop
<!-- add new css file  -->

@section('contents')
	<div class="content-wrapper">
        <section class="content-header">
			<!-- begin breadcrumb -->
		    <ol class="breadcrumb pull-right">
		        <li><a href="javascript:;">Home</a></li>
		        <li><a href="{{ route('customers.index') }}">Member</a></li>
		        <li class="active">Show Member</li>
		    </ol>
		    <!-- end breadcrumb -->

		    <h4 class="bold content-header"> Show Member <small> </small></h4>

		    <div id="footer" class="footer" style="margin-left: 5px"></div>
		</section> <!-- end section content-header -->

		<section class="content">
			<!-- start content panel -->
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<h4 class="panel-title">Show Member</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Name:</strong>
				                {{ $customer->name }}
				            </div>
				        </div>
				        <div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Email:</strong>
				                {{ $customer->email }}
				            </div>
				        </div>
				        <div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Roles:</strong>
				                <label class="label label-success">{{ $customer->display_name }}</label>
				            </div>
				        </div>
				        @if(!empty($customer->phone))
				        <div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Phone:</strong>
				                {{ $customer->phone }}
				            </div>
				        </div>
				        @endif
				        @if(!empty($customer->address))
				        <div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Address:</strong>
				                {{ $customer->address }}
				            </div>
				        </div>
				        @endif
				        <div class="col-xs-12 col-sm-12 col-md-12">
				            <div class="form-group">
				                <strong>Status:</strong>
				                @if(!empty($customer->status))
									@foreach($status as $key=>$value)
					                	@if($key == $customer->status)
										<label class="label label-success">{{ $value }}</label>
										@endif
									@endforeach
								@endif
				            </div>
				        </div>

				        <div class="row">
					    	<div class="col-lg-12 margin-tb">
						        <div class="pull-left">
						            <h2></h2>
						        </div>
						        <div class="pull-right">
						            <a class="btn btn-warning" href="{{ route('customers.index') }}"> Back to Listing</a>
						        </div>
					    	</div>
						</div>
					</div>

				</div>
			</div>
			<!-- end content panel -->
		</section> <!-- end section .content -->
    </div> <!-- end div.wrapper -->
@stop

<!-- add new js file -->
@section('js')

@stop
<!-- add new js file  -->
