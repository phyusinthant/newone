@extends('backend.master.master_with_side_bar')

@section('title')
	@lang('app.Customer')
@stop

<!-- add new css file -->
@section('css')
@stop
<!-- add new css file  -->

@section('contents')
	<div class="content-wrapper">
        <section class="content-header">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
					<li><a href="javascript:;">Home</a></li>
					<li class="active">@lang('app.Customer')</li>
			</ol>
			<!-- end breadcrumb -->

			<h4 class="bold"> @lang('app.Customer Management') <small> </small></h4>

			<div id="footer" class="footer" style="margin-left: 5px"></div>

			<div class="row m-b-10">
			    <div class="col-lg-12">
			        <div>
			        	@permission('customers-create')
			            <a class="btn btn-success" href="{{ route('customers.create') }}"> <i class="fa fa-plus"></i> @lang('app.Create New Customer')</a>
			            @endpermission

			        </div>
			    </div>
			</div>
		</section>

		<section class="content">
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<!-- start content panel -->
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4 class="panel-title">@lang('app.Customer Management')</h4>
						</div>
						<div class="panel-body">
							
							@include('backend.shared.success')
							
							<div class="panel panel-inverse" data-sortable-id="form-stuff-5">
				                <div class="panel-body">
					                <form method="POST" id="search-form" class="form-inline" role="form">
										<div class="form-group m-r-10">
											<label for="name">@lang('app.Name')</label>
											<input type="text" class="form-control" name="name" id="name" placeholder="search name">
										</div>
										<div class="form-group m-r-10">
											<label for="roles">@lang('app.Roles')</label>
											<select name="roles" class="form-control">
												<option value="" > </option>
						                        @foreach($roles as $roleId => $roleName)
						                            <option value="{{ $roleId }}" > {{ $roleName }} </option>
						                        @endforeach
						                    </select>
										</div>
										<div class="form-group">
											<button type="submit" class="btn btn-inverse m-r-5" id="btnSearch">@lang('app.Search')</button>
										</div>
									</form>
				                </div>
				            </div>

							<table id="data-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
		  								<th>@lang('app.Name')</th>
		  								<th>@lang('app.Email')</th>
		  								<th>@lang('app.Role')</th>
		  								<th width="280px">@lang('app.Action')</th>
						            </tr>
						        </thead>
						        <tfoot>
						            <tr>
		  								<th>@lang('app.Name')</th>
		  								<th>@lang('app.Email')</th>
		  								<th>@lang('app.Role')</th>
		  								<th width="280px">@lang('app.Action')</th>
						            </tr>
						        </tfoot>
						    </table>
						</div>
					</div>
					<!-- end content panel -->
				</div>
		        <!-- end col-12 -->
		    </div>
		    <!-- end row -->
		</section> <!-- end section .content -->
    </div> <!-- end .content-wrapper -->
<div id="deleteAction"></div>
<!-- begin delete action template -->
<script id="deleteActionTemplate" type="text/html">
	<form id="deleteForm" name="deleteForm" method="POST" action="{actionlink}" style="display:none">
			<input name="_method" type="hidden" value="DELETE">
			<input name="_token" type="hidden" value="{{ csrf_token() }}">
			<input name="action" type="hidden" value="{action}">
			<input name="ids" type="hidden" value="{ids}">
	</form>
</script>
<!-- end delete action template -->

<!-- begin datatable action column template -->
<script id="datatableActionTemplate" type="text/html">


	@permission('customers-edit')
		<a class="btn btn-warning btn-icon btn-circle" href="customers/{id}/edit">
			<i class="fa fa-edit"></i>
		</a>&nbsp;
	@endpermission

	@permission('customers-show')
		<a class="btn btn-info btn-icon btn-circle" href="customers/{id}">
			<i class="fa fa-file-text-o"></i>
		</a>&nbsp;
	@endpermission

	@permission('customers-delete')
		<a class="btn btn-danger btn-icon btn-circle" href="#" onClick="confirmDelete('delete_single','customers/{id}','City')">
			<i class="fa fa-times"></i>
		</a>&nbsp;
	@endpermission

</script>
<!-- end datatable action column template -->

@stop


<!-- add new js file -->
@section('js')

<script src="/js/datatablescript.js"></script>

<script type="text/javascript">
// Array holding selected row IDs
var rows_selected = [];
$(document).ready(function() {

	var datatableActionTemplate = $("#datatableActionTemplate").html();

	//Begin Datatable
    var table = $('#data-table').DataTable( {
        "processing": true,
        "serverSide": true,
        // "responsive": true,
        "searching": false, 
        "ajax": {
            url: 'customers/get-data/datatable',
            data: function (d) {
                d.name = $('input[name=name]').val();
                d.roles = $('select[name=roles]').val();
            }
        },
		dom:            "Bfrtip",
        scrollY:        true,
        scrollX:        true,
        scrollCollapse: true,
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],                
        buttons: [
            { extend: 'pageLength', className: 'btn-sm'},
            { extend: 'colvis', className: 'btn-sm' }
        ],
        // fixedColumns:   {
        //     leftColumns: 2
        // },
        fixedHeader: {
            header: true,
            headerOffset: $('#header').height()
        },

        "columns": [
          
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            // { data: "roles", name:'roles.display_name',  render: "[, ].display_name" },
            { data:'display_name',name:'display_name'},
            { data: 'id', name: '' },
            
        ],
        "columnDefs": [
        	{
                "orderable": false,
                "targets": 2
            },
	        {
	            // The `data` parameter refers to the data for the cell (defined by the
	            // `data` option, which defaults to the column being worked with, in
	            // this case `data: 0`.
	            "render": function ( data, type, row ) {

	            	return datatableActionTemplate.formatUnicorn({"id":data});
	            },
	            "searchable": false,
                "orderable": false,
                "targets":3,
	        }
	    ],
	    

    } );
    //End Datatable

    $('#search-form').on('submit', function(e) {
        table.draw();
        e.preventDefault();
    });



} );
//End Document Ready

disableAjaxButton($("#btnSearch"));
</script>

@stop
<!-- add new js file  -->

