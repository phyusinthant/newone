@extends('backend.master.master_with_side_bar')

@section('title')
	@lang('app.Customer')
@stop
<!-- add new css file -->
@section('css')

@stop
<!-- add new css file  -->

@section('contents')
	<div class="content-wrapper">
        <section class="content-header">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
					<li><a href="javascript:;">Home</a></li>
					<li><a href="{{ route('customers.index') }}">@lang('app.Customer')</a></li>
					<li class="active">@lang('app.Edit')</li>
			</ol>
			<!-- end breadcrumb -->

			<h4 class="bold"> @lang('app.Edit') <small> </small></h4>

			<div id="footer" class="footer" style="margin-left: 5px"></div>
		</section>

		<section class="content">
			<!-- start content panel -->
			<div class="panel panel-inverse">
			
				<!-- start content heading panel -->
				<div class="panel-heading">
					<h4 class="panel-title">@lang('app.Edit')</h4>
				</div>
				<!-- end content heading panel -->

				<!-- start content body panel -->
				<div class="panel-body">
					
					@include('backend.shared.errors')

					<!-- begin of form -->
		            <form name="customersForm" id="customersForm" method="POST" action="{{ route('customers.update',$customer->id) }}" enctype="multipart/form-data" >
		                {!! csrf_field() !!}
						<input type="hidden" name="_method" value="PATCH">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6">
					            <div class="form-group">
					                <strong>@lang('app.Name'):</strong>
					                <input type="text" 
							                name="name" 
							                id="name"
							                value="{{ old('name',$user->name) }}" 
							                placeholder="Name" 
							                class="form-control" 
							                data-parsley-required="true"
							                data-parsley-maxlength="40"
											data-parsley-errors-container= '#nameErr'
							                />
							        <span id="nameErr" class='parsley-errors-list filled'></span>        
					            </div>
					        </div>

					        <div class="col-xs-12 col-sm-6 col-md-6">
					            <div class="form-group">
					                <strong>@lang('app.Email'):</strong>
					                <input type="text" 
							                name="email" 
							                id="name"
							                value="{{ old('email',$user->email) }}" 
							                placeholder="Email" 
							                class="form-control" 
							                data-parsley-required="true"
							                data-parsley-maxlength="255"
							                data-parsley-type="email"
							                data-parsley-errors-container= '#emailErr'/>
							        <span id="emailErr" class='parsley-errors-list filled'></span>         
					            </div>
					        </div>

					        <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.Password'):</strong>
                                    <input type="password" 
                                            id="password"
                                            name="password" 
                                            placeholder="Password" 
                                            class="form-control"
                                            data-parsley-required="true"
                                            data-parsley-maxlength="255"
                                            />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.Confirm Password'):</strong>
                                    <input type="password" 
                                            id="confirm-password"
                                            name="confirm-password" 
                                            placeholder="Confirm Password" 
                                            class="form-control"
                                            data-parsley-required="true"
                                            data-parsley-maxlength="255"
                                            data-parsley-equalto="#password"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6">
					            <div class="form-group">
					                <strong>@lang('app.Phone'):</strong>
					                <input type="text" 
							                name="phone" 
							                id="phone"
							                value="{{ old('phone',$customer->phone) }}" 
							                placeholder="Phone" 
							                class="form-control" 
							                data-parsley-maxlength="40"
							                data-parsley-errors-container= '#phoneErr'/>
							        <span id="phoneErr" class='parsley-errors-list filled'></span>         
					            </div>
					        </div>

					        <div class="col-xs-12 col-sm-6 col-md-6">
					            <div class="form-group">
					                <strong>@lang('app.Address'):</strong>
					                <input type="text" 
							                name="address" 
							                id="address"
							                value="{{ old('address',$customer->address) }}" 
							                placeholder="Address" 
							                class="form-control" 
							                data-parsley-maxlength="40"
							                data-parsley-errors-container= '#addressErr'/>
							        <span id="addressErr" class='parsley-errors-list filled'></span>         
					            </div>
					        </div>

					        <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>@lang('app.Role'):</strong>
                                    <select name="roles[]" 
                                            multiple='multiple' 
                                            class="form-control" 
                                            data-parsley-required="true">

                                        @foreach($roles as $roleId => $roleName)
                                            @if(in_array($roleId, old('roles',$userRole)))
                                                 <option value="{{ $roleId }}" selected >{{ $roleName }}</option>
                                            @else
                                                 <option value="{{ $roleId }}">{{ $roleName }}</option>
                                            @endif 
                                        @endforeach
                                        
                                    </select>
                                </div>
                            </div>
					        
					        <div class="col-xs-12 col-sm-6 col-md-6">
					            <div class="form-group">
					                <strong>@lang('app.Status'):</strong>
					                <select name="status" id="" class="form-control">
					                	@foreach($status as $key=>$value)
					                		@if($key == $customer->status)
					                			<option value="{{$key}}" selected="true">{{$value}}</option>
					                		@else
					                			<option value="{{$key}}">{{$value}}</option>
					                		@endif
					                	@endforeach
					                </select>
					            </div>
					        </div>
					        
					        <div class="row">
							    <div class="col-lg-12 margin-tb">
							        <div class="pull-left">
							            <h2></h2>
							        </div>
							        <div class="pull-right">
							            <a class="btn btn-warning" href="{{ route('customers.index') }}"> @lang('app.Back to Listing')</a>
							            <button type="submit" class="btn btn-success">@lang('app.Save')</button>
							        </div>
							    </div>
							</div>
						</div>

					</form>
		            <!-- end of form -->
				</div>
				<!-- end content body panel -->
			</div>
			<!-- end content panel -->
		</section> <!-- end section .content -->
    </div> <!-- end .content-wrapper -->

    <div id="loader"></div>
@stop

<!-- add new js file -->
@section('js')

<script type="text/javascript">
$(document).ready(function() {
	
	$('#customersForm').parsley();
	$('#customersForm').preventDoubleSubmission();
});
//End Document Ready
$('#name').on('change',function(){
	var names = {!!$names!!};
	var val =this.value; 
        var tmp =val;
        var org_name = '{!!$customer->name!!}';
        $.each(names, function(key, value) 
        {
            if((val != "") && ($.trim(val.toLowerCase()) ==$.trim(value.toLowerCase())))
            {
                if(org_name == val)
                    $("#nameErr").html('');
                else{
                    $("#nameErr").html('Duplicate name - '+tmp);
                    $("#name").val(" ");
                    $("#name").focus();
                    return false;
                }                
            }
            else
                $("#nameErr").html('');
        }); 
});
</script>

@stop
<!-- add new js file  -->
